#language: pt
#encoding:utf-8

@erroRegressao  @editarItensOs
Funcionalidade: Editar itens na ordem de serviço

  Contexto: Lançar o Item Glico
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "261" com o seu número de registro
    E criar uma nova ordem de serviço
    E lancar o item "GLICO" para a OS criada
    E excluir resposta do questionário
    E clicar em novo lançamento de itens

  @cancelarItemOS
  Esquema do Cenário: Cancelar item da ordem de serviço
    Quando selecionar a ação do item "<acaoItem>"
    E selecionar tipo de cancelamento "<tipoCancelamento>" e preencher motivo de cancelamento "<motivoCancelamento>"
    Então o sistema valida se o item foi cancelado

    @motivoCancelamento2
    Cenários: Cancelar Item e informar o motivo do Cancelamento
        |    acaoItem   | tipoCancelamento | motivoCancelamento |
        | Cancelar Item | ERRO DE CONVENIO | teste cancelamento |

    @PendenciaInverterOs
  Esquema do Cenário: Alterar e inverter resultado do item
    Quando selecionar os itens e concluir
    Quando selecionar a ação do item "<acaoItem>"
    Então o sistema valida a ação do item "<acaoItem>"

    Cenários: Interno, Pendência, Urgência, Resultado Parcial
        |           acaoItem           |
        |      Interno (Inverter)      |
        |     Pendência (Inverter)     |
        |      Urgente (Inverter)      |
        | Resultado Parcial (Inverter) |

    @dataResult1
  Esquema do Cenário: Mensagem de alerta ao incluir data anterior a data da ordem de serviço ou data da coleta
    Quando selecionar a ação do item "<acaoItem>"
    E incluir uma data anterior a data da ordem de serviço ou anterior a data da coleta
    Então o sistema apresenta uma mensagem de erro pela ação do item "<acaoItem>"

    Cenários: Alterar Data da Coleta e Data de Resultado
        |      acaoItem     |
        |  Data da Coleta   |
        | Data do Resultado |

      @dataResult2
  Esquema do Cenario: Cenário: Alterar data da coleta e data do resultado
    Quando selecionar a ação do item "<acaoItem>"
    E incluir a data da coleta ou data do resultado
    Então o sistema valida a ação do item "<acaoItem>"

    Exemplos: Incluir data
        |      acaoItem       |
        |  Data da Coleta 2   |
        | Data do Resultado 2 |

    @alterarSetirExecutante  @setorExecutante
  Esquema do Cenário: Alterar o setor executante
    E selecionar a ação do item "<acaoItem>"
    E buscar o setor executante "<setorExecutante>"
    Quando selecionar o setor
    E aceitar o alerta da alteração
    E abrir tela de detalhes do item
    Então o sistema valida se foi feito a alteração do setor executante "<setorExecutanteValidado>"

    Exemplos: Validar alteração de setor executante
    |          acaoItem          | setorExecutante    | setorExecutanteValidado |
    | Alterar o Setor Executante |BIOLOGIA MOLECULAR  |BIOLOGIA MOLECULAR       |
    | Alterar o Setor Executante |BIOQUIMICA          |BIO                      |

#  Ainda não é possível receber autorização do convênio para que o caso funcione
  @cancelarGuia
  Esquema do Cenário: Não há guias para ser cancelada
    Quando selecionar a ação do item "<acaoItem>"
    Entao o sistema valida a mensagem informativa que nao ha guia a ser cancelada

    Cenários: Cancelar Guia(s)
    |     acaoItem     |
    | Cancelar Guia(s) |