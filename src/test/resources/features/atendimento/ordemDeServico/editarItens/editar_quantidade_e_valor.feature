#language: pt
#encoding:utf-8

@editarQuantidadeItens @quarentena
Funcionalidade: Editar quantidade e valor de itens

  Como um atendente
  Quero lançar ordem de serviço com item
  Para que seja feito a alteração de quantidade e valor do item

  Contexto: criar ordem de serviço
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "13" com o seu número de registro
    E criar uma nova ordem de serviço


  @NaoLiberado #Item no CADGF com a opção - Não Liberado para o Campo Tipo de Preco
  Cenario: Validar Mensagem do tipo de cálculo "Não Liberado"
    E lancar o item "GLICO" para a OS criada
    E excluir resposta do questionário
    Então verifico que o preco nao pode ser alterado nesta etapa

  #  Então e apresentado a mensagem "Não é possível alterar a quantidade para valor igual ou inferior à 0" e confirma o popup

  #Item no CADGF com a opção - Preço Liberado para o Campo Tipo de Preco / Convenio Particular
  @precoLiberado
  Cenario:  Validar alteracao do preço com o tipo de cálculo "Preço Liberado"
    E lancar o item "U01" para a OS criada
    E excluir resposta do questionário
    Quando alterar a quantidade do item para "5"

    Então o valor do item é alterado de acordo com a quantidade

  #Item no CADGF com a opção - Liberado apena se zerado para o Campo Tipo de Preco / Convenio Particular
  @valorZerado @pass
  Cenario:  Validar o valor zerado com o tipo de cálculo "Liberado apenas se zerado"
    E lancar o item "HEV" para a OS criada
    E excluir resposta do questionário
    Então verifico que o preco nao pode ser alterado nesta etapa

  #Item no CADGF com a opção - Liberado apena se zerado para o Campo Tipo de Preco / Convenio Particular
  @valorZeradoAlteracao
  Cenario:  Validar Alteracao do preço com o tipo de cálculo "Liberado apenas se zerado"
    E lancar o item "HEV" para a OS criada
    E excluir resposta do questionário
    Quando alterar a quantidade do item para "5"
    Então o valor do item é alterado de acordo com a quantidade
