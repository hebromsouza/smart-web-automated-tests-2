#language: pt
#encoding: utf-8

@smartweb @bugInterno @EmDesenvolvimento
Funcionalidade: Impressão de questionários na ordem de serviço

  Como atendente
  Quero responder questionários no momento da geração de ordem de serviço
  Para imprimi-los

  Contexto: Criar ordem de serviço
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "261" com o seu número de registro
    E criar nova ordem de serviço

  @SMART-19054
  Cenario: Imprmir questionários da ordem de serviço
    E responder "2" questionários com tipo "Avaliação de Atendimento"
    Quando imprimir questionários da ordem de serviço
    Então é exibido "2" questionários na impressão