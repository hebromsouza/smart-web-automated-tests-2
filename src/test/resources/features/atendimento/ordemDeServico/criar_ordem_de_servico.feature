#language: pt
#encoding:utf-8

@brokenTeste @bugInterno @criarOS
Funcionalidade: Criar Ordem de Serviço

  @convenioOSParticular
  Esquema do Cenário: Convênio e particular
    Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
    E buscar o paciente pela forma de busca "<formaDeBusca>" e dados de busca "<busca>"
    E clicar no botao avancar
   # E criar nova ordem de serviço selecionando código do convênio "<codigoConvenio>"
    E criar uma nova ordem de serviço com o plano "<plano>"
    #E criar uma nova ordem de serviço
    Quando incluir o item "<item>" com instrução
    Então o sistema valida se foi criado a nova ordem de serviço com plano "<plano>" selecionado

    Cenários: Particular e Convênio
        |    menu     |       subMenu       | formaDeBusca |      busca                | codigoConvenio | item  |     plano    |
        | Atendimento | Lançamento de Guias |     nome     | JOSE APARECIDO PARTICULAR |        1       | GLICO |  MEDIO       |
        | Atendimento | Lançamento de Guias |     nome     | RAMON MEIRA OLIVEIRA      |        HAV     | GLICO |  SGE APARTAMENTO |
        | Atendimento | Lançamento de Guias |     nome     | MARIA VITORIA GOMES SILVA |        BRA     | GLICO |  BRADESCO TOP   |


  @os_cpf
  Esquema do Cenário: Validar cpf ou CNPJ Inválidos na inserção do prestador
    Quando que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    Quando buscar paciente pelo nome sem avancar "ÍVARR BEINLAUSI"
    Quando informar o cpf_cnpj "<cpf_cnpj>"
    Então é apresentado a mensagem "<mensagem>" de alerta

    Exemplos:
      |     cpf_cnpj   |                    mensagem                          |
      |   7969990I01A  | Não foi possível completar a operação. Cpf inválido  |
      |   12345678910  | Não foi possível completar a operação. Cpf inválido  |
      | 9S717907000I82 | Não foi possível completar a operação. Cnpj inválido |
      | 12345678910123 | Não foi possível completar a operação. Cnpj inválido |

    @osCnpj
  Esquema do Cenário: Inserindo prestador com cpf_cnpj válido
    Quando que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    #E buscar um paciente "261" com o seu número de registro
    Quando buscar paciente pelo nome "ÍVARR BEINLAUSI"
    Quando informar o cpf_cnpj "<cpf_cnpj>"
    E criar uma nova ordem de serviço com o plano "<plano>"
   # Então o sistema valida inclusão do cpf_cnpj "<cpf_cnpj>" do prestador na tela de ordem de serviço
      Então o sistema valida se foi criado a nova ordem de serviço com plano "<plano>" selecionado

    Exemplos:
      |    cpf_cnpj    | plano           |
      |   79699901004  | BRADESCO TOP    |
      | 95717907000182 | BRADESCO TOP    |