#language: pt
#encoding:utf-8

@pass @bugExterno  @inserirItensNaOS
Funcionalidade: Inserir Itens na Ordem de Serviço

  @inserirItensNaOS
  Esquema do Cenário: Inserir itens buscando pelo tipo do item "<tipoItem>" - "<filtroTipoBusca>"
    Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
    E buscar um paciente "<paciente>" com o seu número de matricula e avançar
    E criar uma nova ordem de serviço
    Quando faço a busca do item "<item>" pelo tipo do item "<tipoItem>" e filtro por tipo de busca "<filtroTipoBusca>"
    E concluir inserção de item "<item>" com pelo tipo do item "<tipoItem>" filtro por tipo de busca "<filtroTipoBusca>"
   # E responder o questionário de avaliação
    E excluir resposta do questionário
    Então o sistema valida se foi feito a inserção de item "<item>" pelo tipo do item "<tipoItem>" e com filtro por tipo de busca "<filtroTipoBusca>"

    Cenários: Inserir Itens Buscando por tipo
      |    menu     |       subMenu       |     paciente    |      item      | tipoItem | filtroTipoBusca |
      | Atendimento | Lançamento de Guias | 744539002496001 |      GLICO     |  Serviço |       Smart     |
      | Atendimento | Lançamento de Guias | 744539002496001 | SGE MATERIAL 1 | Material |       Smart     |
      | Atendimento | Lançamento de Guias | 744539002496001 |       K114     |  Serviço |       Smart     |
      | Atendimento | Lançamento de Guias | 744539002496001 |       L80      |  Serviço |       Smart     |
      | Atendimento | Lançamento de Guias | 744539002496001 |    28010974    |  Serviço |     Cód. AMB    |
      | Atendimento | Lançamento de Guias | 744539002496001 |   6574589652   |  Serviço |     Cód. SUS    |
      | Atendimento | Lançamento de Guias | 744539002496001 |       N73      |  Serviço |    Cód. Smart   |
      | Atendimento | Lançamento de Guias | 744539002496001 |  RADIOTERAPIA  |  Serviço |       Nome      |

        #  | Atendimento | Lançamento de Guias | 744539002496001 |    KITGLAU     |    Kit   |       Smart     |
      #| Atendimento | Lançamento de Guias | 744539002496001 |        1       |  Serviço |  Cód. Convênio  |


  @buscaItensAmostra
  Esquema do Cenário: Iserir Itens buscando pela amostra do item
    Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
    E buscar um paciente "<paciente>" com o seu número de matricula e avançar
    E criar uma nova ordem de serviço
    E faço a busca do item "<item>" selecionando a amostra "<amostra>"
    Quando incluir o item
    E responder o questionário de avaliação
    Então o sistema valida se foi feito a inclusão do item com amostra "<amostra>"

    Cenários: Inserir Itens Buscando Amostra
      |    menu     |       subMenu       |     paciente    |  item | amostra |
      | Atendimento | Lançamento de Guias | 744539002496001 | GLICO |  Urina  |


  Cenario: Inserir itens validando data de resultado para sábado como dia util
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    Quando buscar paciente pelo nome "LUANA OLIVEIRA DE SOUZA"
    E criar nova ordem de serviço sem gravar
    E preencher dados da ordem de serviço
      | setorSolicitante |
      |   CONSULTORIOS   |
    Quando inserir item com resultado para sábado
    Então o resultado do item será calculado para um dia de sábado

  @SMART_19191  @sabadoNUtil
  Cenario: Inserir itens validando data de resultado para sábado não sendo dia util
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    Quando buscar paciente pelo nome "ISAIAS SILVA"
    E criar nova ordem de serviço sem gravar
    E preencher dados da ordem de serviço
      | setorSolicitante |
      |  SGE - RECEPÇÃO  |
    Quando inserir item com resultado para sábado
    E responder o questionário de avaliação
    Então o resultado do item será calculado para o próximo dia util