#language: pt
#encoding:utf-8

@smartweb @top10
Funcionalidade: Inserir itens com cotação inexistente

  Como um atendente
  Quero lançar um item
  Para que o sistema valide cotação por convênio

  Contexto: Logar no sistema Smart Web
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    #E buscar um paciente "252" com o seu número de registro
    Quando buscar paciente pelo nome "TESTE ORÇAMENTO"

  Esquema do Cenário: Validar mensagem cotação inexistente para exame que não está na tabela de preço do convênio- BLOQ_COT=S - FILTER_COT=S
    Quando criar nova ordem de serviço selecionando código do convênio "<codigoConvenio>"
    E buscar item "<item>"
    Então é apresentado um painel com a mensagem "<mensagem>" da cotação

    Exemplos:
      | codigoConvenio | item |             mensagem            |
      |       AML      |  HDL | Cotação inexistente ou vencida! |

  Esquema do Cenário: Validar mensagem cotação vencida ou inexistente - BLOQ_COT=S - FILTER_COT=S
    Quando criar nova ordem de serviço selecionando código do convênio "<codigoConvenio>"
    E incluir item "<item>" na ordem de serviço
    Então o sistema valida mensagem "<mensagem>" da busca de item com bloqueio por convênio

    Exemplos:
      | codigoConvenio | item |                  mensagem                |
      |       HAV      |  F11 | F11\nCotação inexistente para moeda 11A. |