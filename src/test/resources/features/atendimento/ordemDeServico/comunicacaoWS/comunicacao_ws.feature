#language: pt
#encoding: utf-8

@smartweb @top10 @EmDesenvolvimento
Funcionalidade: Comunicação com WebService do laboratório

  Como um atendente
  Quero lançar ordem de serviço com amostra
  Para ter o retorno da etiqueta com o resultado do laboratório

  Contexto: Logarno sistema
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    #E buscar um paciente "422" com o seu número de registro
    Quando buscar paciente pelo nome "RAMON MEIRA OLIVEIRA"

  Esquema do Cenario: Validar comunicação WS Alvaro, WS DB Diagnóstico, WS Pardinni
    E criar uma nova ordem de serviço incluindo o item "<item>"
    E responder o questionário de avaliação
    Quando selecionar tipo de impressão "<tipoImpressao>"
    E imprimir a etiqueta selecionando servidor de impressao "<servImp>"
#    Entao o sistema valida conexão com o web service do laboratório "<wsLaboratorio>"
    Quando abrir tela de detalhes do item "<itemDesc>"
    E abrir o diligenciamento
    E acessar a aba "<selecionarAba>"
    E abrir o xml de envio
    Entao o sistema valida xml de envio
    Quando fechar painel
    E abrir o xml de retorno
    Entao o sistema valida xml de retorno
    E fechar painel

    Exemplos:
      |  item |       itemDesc       |       tipoImpressao       |            servImp          |  wsLaboratorio | selecionarAba  |
      |  17OH | 17 - OH CORTICOSTERÓ | Etiqueta de Amostra/Exame | [nao Especificado] (ti4300) |      Alvaro    | Comunicação WS |
#      | F1197 |     AC VALPROICO     | Etiqueta de Amostra/Exame | [nao Especificado] (ti4300) | DB Diagnóstico | Comunicação WS |
#      | F1132 | Etiqueta de Amostra/Exame | [nao Especificado] (ti4300) |    Pardinni    | Comunicação WS |