#language: pt
#encoding: utf-8

@quarentena  @depreciado_devido_intervencao_humana
Funcionalidade: Validar impressão de etiquetas

  @impressaoEtq
  Cenario: Validar valores da etiqueta gerada
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "261" com o seu número de registro
    E criar uma nova ordem de serviço
    E incluir o item "TSH" sem instrução
    E concluir lançamento do item aceitando erro de autorização
    E excluir resposta do questionário
    Quando selecionar tipo de impressão "Etiqueta de Amostra/Exame"
    Entao o sistema valida quantidade de etiqueta "0"
    Quando inserir a quantidade de impressão de etiqueta "1"
    E imprimir a etiqueta selecionando servidor de impressao "[nao Especificado] (ti4375)"
    Entao o sistema valida o conteudo da etiqueta "C:\Smart\etq"

    # Depreciar caso ou adapta-lo pois há uma necessidade de intervenção humana.

  Esquema do Cenario: Validar status do exame na etiqueta "<status>"
    Quando incluir o item "GLICOSE" com instrução
    E concluir lançamento do item
    E selecionar tipo de impressão "Etiqueta de Amostra/Exame"
    Quando inserir a quantidade de impressão de etiqueta "1"
    E imprimir a etiqueta selecionando servidor de impressao "[nao Especificado] (ti4375)"
    Entao o sistema valida o status da etiqueta "<status>" com caminho do arquivo "C:\Smart\etq"

    Exemplos: PENDENTE
      | (PENDENTE) |