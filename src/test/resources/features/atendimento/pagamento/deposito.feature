#language: pt
#encoding:utf-8

@smartweb @realizarPagamentos
Funcionalidade: Realizar pagamento com depósito

  Como um atendente
  Quero registrar um deposito
  Para que o paciente pague o serviço realizado

  Contexto: Logar no sistema e criar nova ordem de serviço
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "13" com o seu número de registro
    E criar uma nova ordem de serviço
    E lancar o item "GLICO" para a OS criada
    E excluir resposta do questionário

    Cenário: Pagamento com depósito sem desconto validando impressao do recibo
      Quando abrir tela de depósito
      E realizar depósito com valor "3,00" menor da ordem de serviço
      E pagar a ordem de serviço
      E gravar o pagamento
      E validar impressão de recibo

#  @pagmDeposito
#  Cenário: Pagamento com depósito sem desconto validando nota fiscal
#    Quando abrir tela de depósito
#    E realizar depósito com valor "3,00" menor da ordem de serviço
#    E pagar a ordem de serviço
#    E gravar o pagamento
#    E validar geração de nota fiscal com valor da ordem de serviço
#    E validar exclusão de nota fiscal