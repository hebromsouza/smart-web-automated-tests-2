#language: pt
#encoding:utf-8

  @realizarPag
Funcionalidade: Realizar pagamento de Ordem de Serviço

  @oSfATURADA
  Cenario: Validar Ordem de serviço faturada
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "230" com o seu número de registro
    E criar uma nova ordem de serviço
    E lancar o item "GLICO" para a OS criada
    #E excluir resposta do questionário
    E responder o questionário de avaliação
    E pagar a ordem de serviço gerada
    Então o sistema deve exibir status de ordem de serviço faturada

  @particularOSDesconto
  Esquema do Cenário: Realizar pagamento de Ordem de Serviço Particular e validar desconto aplicado
    Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
    E buscar um paciente "<paciente>" com o seu número de matricula e avançar
    E criar uma nova ordem de serviço
    Quando incluir o item "<item>" com instrução
    E concluir lançamento do item sem selecionar
    E responder o questionário de avaliação
    E concluir o pagamento da ordem de serviço com desconto de "<valor>" e tipo de desconto "<tpDesconto>"
    Então valido se o sistema aplicou o desconto
    #E clico no botao comum "Gravar"
    E fechar tela de pagamento

    Cenários: Cenário Não Particular, Não Pergunta, Não Convênio e Não Cobre Sim respectivamente.
      |    menu     |       subMenu       |  paciente  | item  | valor |   tpDesconto   |
      | Atendimento | Lançamento de Guias | 9877600001 | GLICO | 10,0  |  SGE DESCONTO  |
      | Atendimento | Lançamento de Guias | 9877600001 | GLICO | 50,0  |  SGE DESCONTO  |
      | Atendimento | Lançamento de Guias | 9877600001 | GLICO | 20,0  | SGE DESCONTO 2 |

    @parcialCartao
  Esquema do Cenário: Realizar pagamento de Ordem de Serviço Particular e validar pagamento parcial com cartão
    Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
    E buscar um paciente "<regPaciente>" com o seu número de registro
    E criar uma nova ordem de serviço
    Quando incluir o item "<item>" com instrução
    E concluir lançamento do item sem selecionar
    E responder o questionário de avaliação
    E concluir o pagamento da ordem de serviço parcial de valor "<valor>" e pagamento com forma de pagamento "<formaPagamento>" e cartão "<cartao>" com vencimento "<vencimento>" e validade "<validade>"
    Então valido se o sistema aplicou o valor parcial correto
    E fechar tela de pagamento

    Cenários: Paciente Particular com cartão.
      |    menu     |       subMenu       | regPaciente | item  | valor | formaPagamento | cartao | vencimento | validade |
      | Atendimento | Lançamento de Guias |      13     | GLICO | 4,25  |    Cartão      |  VISA  | 31/12/2020 |  12/2020 |

    @cartao2x
  Cenario: Parcelamento no cartão em 2x
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "13" com o seu número de registro
    E criar uma nova ordem de serviço
    E lancar o item "GLICO" para a OS criada
    E excluir resposta do questionário
    E pagar a ordem de serviço gerada sem selecionar a forma de pagamento
    E selecionar a forma de pagamento "Cartão"
    E selecionar bandeira do cartão "VISA"
    Quando parcelar em "2" vezes preenchendo dados do cartão com vencimento "31/12/2020" e validade "12/2020"
    E gravar o pagamento
    Entao o sistema valida o valor das parcelas com quantidade "2"
    E fechar tela de pagamento

      @pagamentoQuarentena
  Esquema do Cenário: Realizar pagamento de Ordem de Serviço Particular onde o paciente não é o responsável.
    Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
    E buscar um paciente "<paciente>" com o seu número de matricula e avançar
    E criar uma nova ordem de serviço
    E lancar o item "GLICO" para a OS criada
    E responder o questionário de avaliação
   # E pagar a ordem de serviço gerada
    E pagar a ordem de serviço gerada sem selecionar a forma de pagamento
    E informo que o paciente nao é o responsavel
    Então valido se o sistema gravou o pagamento
    E fechar tela de pagamento

    Cenários: Paciente Particular.
      |    menu     |       subMenu       | paciente   |
      | Atendimento | Lançamento de Guias | 9877600001 |
      | Atendimento | Lançamento de Guias | 987654321  |
      | Atendimento | Lançamento de Guias | 0987654333 |
