#language: pt
#encoding:utf-8

@smartweb
Funcionalidade: Pagamento a receber

  Como um atendente
  Quero registrar um pagamento a receber
  Para que o paciente faça o serviço antes de efetuar o pagamento

  Contexto: Logar no sistema e lançar ordem de serviço para o paciente
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "13" com o seu número de registro
    Quando criar uma nova ordem de serviço incluindo o item "HDL"
    E excluir resposta do questionário

    Cenário: Registrar pagamento a receber
      E iniciar pagamento de ordem de serviço
      E selecionar a opção de pagamento a receber
      E preencher observação de forma de pagamento "06676"
      Quando gravar o pagamento
      E fechar tela de pagamento
      E abrir tela de itens da ordem de serviço
      E excluir resposta do questionário
      E abrir tela de pagamento
      E fechar tela de pagamento
      Quando abrir tela de pagamento a receber
      E receber pagamento em cheque com numero do banco "3456" numero da agencia "3" numero da conta "456" numero do cheque "3445" e tipo praça "M"
      E informar responsavel "RESPONSAVEL TESTE"
      E imprimir recibo
      E fechar tela de pagamento
      Então o sistema valida se foi realizado o recebimento de pagamento