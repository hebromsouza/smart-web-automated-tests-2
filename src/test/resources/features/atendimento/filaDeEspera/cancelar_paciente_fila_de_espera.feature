#language: pt
#encoding: utf-8


Funcionalidade: Cancelar pacientes na fila de espera

  Como um médico
  Quero acessar fila de espera
  Para cancelar pacientes

    @quarentena @cancelarPacienteFila
    Cenario: Cancelar senha gerada de pacientes na fila do médico
      Quando retirar senha no totem com setor "CLINICA SEMEG" e médico "ADRIANO EXECUTANTE"
      E voltar do totem para tela de smartweb
      E que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
      E acessar a tela de fila de espera
      E buscar fila de espera "ADRIANO"
      Quando cancelar senha do paciente na fila de espera
      Então a senha do paciente sai da fila do médico

    @quarentena
    Cenario: Cancelar todas as senhas na fila de espera
      Quando retirar senha no totem com setor "CLINICA SEMEG" e médico "ADRIANO EXECUTANTE"
      E voltar do totem para tela de smartweb
      E que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
      Quando acessar fila de espera do médico "ADRIANO"
      Então todas as senhas serão canceladas