#language: pt
#encoding: utf-8

@smartweb @top10  @adicionarPacienteFila2
Funcionalidade: Cadastrar Paciente e Adiciona-lo a fila de espera

  Como um atendente
  Quero adicionar um paciente na fila de espera
  Para que o paciente espere ser chamado para atendimento

#  Contexto: Logar no sistema
#    Dado que acesso a "Fila de Espera" no menu
#    E cadastro um novo paciente com dados faker
#
#
#  # Pacientes Devem agendar exames - Gerar Agenda
#
#
#
#
#
#
#
#
#    # variável de bloqueio por catação impede funcionamento do caso bloq_cot filter_cot
#    @quarentena
#    Cenário: Adicionar paciente na fila de espera pela impressão da ordem de serviço com kit
#  #    E buscar um paciente "12" com o seu número de registro
#      Quando buscar paciente pelo nome "PACIENTE PRONTUARIO EXAMES"
#      Quando criar nova ordem de serviço
#      E selecionar item "KITGLAU" do tipo "Kit"
#      E buscar médico "ABAL OLIVEIRA MAGALHAES"
#      E buscar médico "ADRIANO EXECUTANTE"
#      E buscar médico "MEDICO COLETA DOMICILIAR"
#      E concluir lançamento do item
#      E responder o questionário de avaliação
#      Quando imprimir ordem de serviço
#      E fechar tela de impressão de ordem de serviço
#      E acessar a tela de fila de espera
#      Quando buscar fila de espera "ABAL OLIVEIRA"
#      Então o sistema valida inclusão de paciente "TESTE ADRIANO" na fila de espera
#      Quando buscar fila de espera "ADRIANO"
#      Então o sistema valida inclusão de paciente "TESTE ADRIANO" na fila de espera
#      Quando buscar fila de espera "MEDICO COLETA DOMICI"
#      Então o sistema valida inclusão de paciente "TESTE ADRIANO" na fila de espera
#
#    Cenario: Validar mensagem de quantidade de pacientes na fila do médico
#      E acessar a tela de fila de espera
#      Quando buscar fila de espera "ADRIANO"
#      Então é informado a quantidade de pacientes na fila de espera do médico
#
#    @quarentena
#    Cenário: Adicionar paciente na fila de espera pela tela de fila de espera, colocando o paciente no final da fila
#      E acessar a tela de fila de espera
#      Quando adicionar na fila
#      #E buscar um paciente "284" com o seu número de registro
#      Quando buscar paciente pelo nome "TESTE QA"
#      E gravar inclusão do paciente na fila de espera "ABAL OLIVEIRA" com recepção "CONSULTORIOS"
#      Então o sistema valida inclusão de paciente "TESTE QA" no final da fila de espera
#
#    @quarentena
#    Cenário: Adicionar paciente na fila de espera pela tela de fila de espera, colocando o paciente na frente de outro paciente
#      E acessar a tela de fila de espera
#      Quando adicionar na fila
#      #E buscar um paciente "1" com o seu número de registro
#      Quando buscar paciente pelo nome "JOSE APARECIDO PARTICULAR"
#      E selecionar a opção de adicionar na frente do paciente "TESTE ADRIANO"
#      E gravar inclusão do paciente na fila de espera "ABAL OLIVEIRA" com recepção "CONSULTORIOS"
#      Então o sistema valida inclusão de paciente "TESTE DE LANÇAMENTO MASCULINO" na fila de espera