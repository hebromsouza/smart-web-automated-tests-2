#language: pt
#encoding:utf-8

@smartweb @top10
Funcionalidade: Impressao de ordem de serviço por recebimento de amostra

  Como um atendente
  Quero impreimir uma ordem de serviço pela tela de recebimento de amostra
  Para protocolar entrega de resultado

  Contexto: Logar no sistema e abrir a tela de recebimento de amostra
    Dado que acesso o menu "Atendimento" e sub-menu "Recebimento de Amostra"

    Esquema do Cenário: Impressao de ordem de serviço pelo recebimento e amostra
      Quando buscar um paciente "<registro>" com o seu número de registro
      E selecionar a ordem de serviço na tela de recebimento de amostra
      E selecionar tipo de impressão "<tipoImpressao>"
      Entao o sistema valida impressao de ordem de servico pelo nome do paciente "<nomePaciente>"

      Exemplos:
        | registro |   nomePaciente  |   tipoImpressao  |
        |    261   | ÍVARR BEINLAUSI | Ordem de Serviço |