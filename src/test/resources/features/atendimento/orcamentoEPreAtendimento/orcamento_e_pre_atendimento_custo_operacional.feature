#language: pt
#encoding:utf-8

 @inserirCustosOperacionais
Funcionalidade: Inserir itens com custo operacional por convênio
  
  Como um atendente
  Quero inserir itens no orçamento com custo operacional
  Para separar valor do custo operacional com valor do exame
  
  Contexto: Logar no sistema e abrir tela de orçamento
    Dado que acesso o menu "Atendimento" e sub-menu "Orçamento"
   # E buscar um paciente "416" com o seu número de registro
    Quando buscar paciente pelo nome "JOSE CARLOS MEIRA"
   Quando clicar no botao avancar


  Esquema do Cenário: Inserir itens com custo operacional separado
    E incluir o convenio "<convenio>"
    Quando gravar o orcamento e pre atendimento
    E inserir item "<item>" com coper
    Entao o sistema valida inclusao de item de custo operacional "COPER"
    E valor do custo operacional "<custoOperacional>" com cotacao "<cotacao>" do convenio "<convenio>"

    Exemplos:
    |convenio          | item | custoOperacional | cotacao |    convenio  |
    |BRADESCO SAUDE    |  N73 |        1,0       |  11,50  | SGE HAP VIDA |
    |SULAMERICA SAUDE  |  AAS |        2,0       |  11,50  | SGE HAP VIDA |

  @passCustoOperacional   @pass @top10 @passOK
  Esquema do Cenário: Inserir itens com custo operacional agregado
    E incluir o convenio "<convenio>"
    Quando gravar o orcamento e pre atendimento
    E inserir item "<item>" com coper
    Entao o sistema valida valor de item "<valorItem>" com custo operacional "<custoOperacional>" e cotacao "<cotacao>" agregado
    E inexistencia do servico coper "COPER"

    Exemplos:
      |     convenio     | item | valorItem | custoOperacional | cotacao |
      | SULAMERICA SAUDE |  N73 |   100,0   |       1,0        |  11,50  |
