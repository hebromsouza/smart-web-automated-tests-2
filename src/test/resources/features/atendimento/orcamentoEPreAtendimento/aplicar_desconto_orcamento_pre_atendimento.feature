#language: pt
#encoding: utf-8

 @dbz

Funcionalidade: Aplicar desconto no orçamento e/ou pré-atendimento

  Como um atendente
  Quero criar novo orçamento com desconto
  Para gerar pré-atendimento e/ou ordem de serviço

  Contexto: Logar no sistema e abrir tela de orçamento
    Dado que acesso o menu "Atendimento" e sub-menu "Orçamento"
    Quando buscar paciente pelo nome "JOSE CARLOS MEIRA"
    E avançar cadastro do paciente
    E incluir o item "GLICO" no sub-menu "Orçamento"

    @passOK @pass
    Cenario: Aplicar desconto no orçamento
      Quando selecionar desconto "SGE DESCONTO" do orçamento
      Então o sistema valida desconto aplicado no valor do item

      @descontoFail @erroRegressao @quarentena
    Cenario: Inserir desconto inferior a faixa cadastrada
      Quando selecionar desconto "SGE DESCONTO" do orçamento
      E inserir percentagem de desconto "2"
      Então e apresentado a mensagem "Atenção. A faixa de desconto informada está fora do intervalo permitido ( 5% à 100%  )." e confirma o popup

   @pass
   Cenario: Aplicar desconto manual no orçamento
      Quando selecionar desconto "SGE DESCONTO" do orçamento
      E inserir percentagem de desconto "10"
      E gravar o orcamento e pre atendimento
      Então o sistema valida desconto aplicado no valor do item

      @dbz2 @passOK @pass
      Cenario: Aplicar desconto no orçamento migrando para pré-atendimento
      Quando selecionar desconto "SGE DESCONTO 2" do orçamento
      E gravar o orcamento e pre atendimento
      Então o sistema valida desconto aplicado no valor do item
      Quando gerar pre-atendimento pela tela de orçamento
      Então é migrado para a tela de pré-atendimento
      E o desconto é aplicado para o pré-atendimento

    @bugEncontrado @descontoMigra @erroRegressao @quarentena
    Cenario: Aplicar desconto no orçamento migrando para ordem de serviço
      Quando selecionar desconto "SGE DESCONTO" do orçamento
      E gravar o orcamento e pre atendimento
      Então o sistema valida desconto aplicado no valor do item
      Quando gerar ordem de serviço
      E avançar cadastro do paciente
      Quando acessar ordem de serviço gerada pelo orçamento
      E acessar tela de itens da ordem de serviço pelo link de editar itens no lançamento
      E excluir resposta do questionário
      Então o sistema apresenta valor total com desconto informado no orçamento