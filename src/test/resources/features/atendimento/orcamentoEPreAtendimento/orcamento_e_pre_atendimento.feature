 #language: pt
#encoding:utf-8

@pass @not @passOK
Funcionalidade: Realizar orçamento e pré-atendimento de Itens com 4 convênios diferentes


	Esquema do Cenário: Realizar ou não lançamento de Itens em Orçamento e Pré-Atendimento com validação de restrição de convênio.
		Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
		Quando buscar um paciente "<paciente>" com o seu número de registro
		Quando confirmo o lançamento do item com a "<opcaoDeConvenio>"
		E incluir o item "<item>" no sub-menu "<subMenu>"
		Então o sistema realiza o lançamento do item de acordo com a opção de convênio "<opcaoDeConvenio>"

		Cenários: Alterar convenio no Orçamento
		 |        menu     |       subMenu    |  paciente |    item  |  opcaoDeConvenio     |
 		 |    Atendimento  |      Orçamento   |     12    |  SGEATB  |     PARTICULAR       |
		 |    Atendimento  |      Orçamento   |     12    |  SGECOLT |     BRADESCO SAUDE   |
		 |    Atendimento  |      Orçamento   |     12    |  SGECOLT |     SULAMERICA SAUDE |
		 |    Atendimento  |      Orçamento   |     12    | SGEECOAR |     SUS              |
 		 |    Atendimento  |  Pré-Atendimento |     12    | SGECG120 |     PARTICULAR       |
		 |    Atendimento  |  Pré-Atendimento |     12    | SGECG120 |     BRADESCO SAUDE   |
		 |    Atendimento  |  Pré-Atendimento |     12    | SGECG120 |     SULAMERICA SAUDE |
		 |    Atendimento  |  Pré-Atendimento |     12    | SGEECOAR |     SUS              |

	@precoDiferente
	Esquema do Cenário: Realizar alteração de convênio em Orçamento e Pré-Atendimento com validação de itens na tabela de preço.
		Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
		Quando buscar um paciente "<paciente>" com o seu número de registro
		E incluir o item "<item>" no sub-menu "<subMenu>"
		Quando confirmo a alteração do orçamento ou pré-atendimento com outro convênio "<convenio>"
		Então o sistema realiza a alteração do convênio e validação do item de acordo com a tabela de preço que "<tabelaDePreco>"

		Cenários: Item contém na tabela de preço com valor diferente, sem valor, e não contém na tabela de preço respectivamente - SMART-T166, SMART-T167 e SMART-T168 (VARIAVEL INI_COD -> ITEM_INEX = S).
		|     menu    |    subMenu      | paciente | item  |     convenio     |      tabelaDePreco     |
		| Atendimento | Pré-Atendimento |     13   | GLICO |SUS               |       eh sem preco     |
		| Atendimento |     Orçamento   |     13   | GLICO | SULAMERICA SAUDE |   nao contem preco     |
		#| Atendimento |     Orçamento   |     13   | GLICO |     UNIMED BH    | contem preco diferente |