#language: pt
#encoding: utf-8

@pass @bugInterno
Funcionalidade: Buscar orçamento ou pré-atendimento

  Como um atendente
  Quero buscar um orçamento ou pré-atendimento
  Para continuar com atendimento ao paciente

    @
    Esquema do Cenário: Buscar <subMenu>
      E que acesso o menu "Atendimento" e sub-menu "<subMenu>"
      E buscar paciente pelo nome "JOSE CARLOS MEIRA"
      E avançar cadastro do paciente
      Quando incluir o item "GLICO" no sub-menu "<subMenu>"
      E pegar o número do orçamento ou pre-atendimento gerado
      E que acesso o menu "Atendimento" e sub-menu "<subMenu>"
      Quando buscar o orcamento ou pre-atendimento pelo número gerado
      E avançar cadastro do paciente
      Então é apresentado orçamento ou pré-atendimento buscado

      Exemplos:
        |     subMenu     |
        |    Orçamento    |
        | Pré-Atendimento |