#language: pt
#encoding: utf-8

@erroRegressao @top10 @entrega_laudos
Funcionalidade: Registrar entrega de laudo

  Como um médico
  Quero imprimir um laudo médico
  Para que o sistema efetue a entrega de laudo para o paciente

  @erroRegressao1  @registrarEntregaLaudo
  Cenario: Acessar entrega de laudo e efetuar entrega pela ordem de serviço
    E que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "7445390024960012" com o seu número de matricula e avançar
    E criar uma nova ordem de serviço
    E incluir o item "HDL"
    E excluir resposta do questionário
    E que acesso o menu "Laboratório" e sub-menu "Resultado"
    E buscar paciente pelo nome "LUANA OLIVEIRA DE SOUZA"
    E selecionar a ordem de serviço gerada
    E gravar resultado de exame com valor de referência "100"
    Quando liberar o exame
    E que acesso o menu "Atendimento" e sub-menu "Entrega de Laudos"
    E buscar paciente pelo nome "LUANA OLIVEIRA DE SOUZA"
    E selecionar a ordem de serviço gerada
    Quando imprimir entrega de laudos para pessoa "LUANA OLIVEIRA DE SOUZA"
    E registrar entrega de laudos para pessoa "LUANA OLIVEIRA DE SOUZA"
    Então o sistema valida entrega de laudos do paciente

  @SMART_20349 @bugEncontradoDataWindow  @Laudo
  Cenário: Registrar entrega de laudo pelo orçamento sem médico solicitante
    E que acesso o menu "Atendimento" e sub-menu "Orçamento"
    E buscar um paciente "13" com o seu número de registro
    Quando incluir o item "HDL" no sub-menu "Orçamento"
    E gerar ordem de serviço
    E avançar cadastro do paciente
    Quando pegar número da ordem de serviço gerada na tela de orçamento
    E que acesso o menu "Laboratório" e sub-menu "Resultado"
    E buscar paciente pelo nome "LUANA OLIVEIRA DE SOUZA"
    E selecionar a ordem de serviço gerada pelo orçamento
    E gravar resultado de exame com valor de referência "100"
    Quando liberar o exame
    E que acesso o menu "Atendimento" e sub-menu "Entrega de Laudos"
    E buscar paciente pelo nome "LUANA OLIVEIRA DE SOUZA"
    E selecionar a ordem de serviço gerada pelo orçamento
    Quando imprimir entrega de laudos para pessoa "LUANA OLIVEIRA DE SOUZA"
    E registrar entrega de laudos para pessoa "LUANA OLIVEIRA DE SOUZA"
    Então o sistema valida entrega de laudos do paciente