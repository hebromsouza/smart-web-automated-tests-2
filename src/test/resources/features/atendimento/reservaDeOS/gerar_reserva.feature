#language: pt
#encoding:utf-8

@smartweb @bugInterno
Funcionalidade: Gerar reserva de ordem de serviço

  Como um atendente
  Quero gerar reserva de ordem de serviço
  Para que seja possível digitar os procedimentos em outro momento

  #Para que apareça o menu 'Reserva de OS', é necessário adicionar a variável 'RESERVA_OS' na tabela ini ou arquivo .ini sessão [SMART].

  Contexto: Logar no sistema
    Dado que acesso o menu "Atendimento" e sub-menu "Reserva de OS"

    @SMART-9797
    Cenario: Gerar reserva de OS
      Quando informar os dados da reserva de os
        | serieOs | quantidade |     setorSolic     |           observacao           |
        |   120   |      1     | SGE - RECEPÇÃO (3) | TESTE OBSERVAÇÃO RESERVA DE OS |
      E gerar reserva
      E confirmar popup de confirmação de geração de reserva de ordem de serviço com quantidade "1" e setor "SGE - RECEPÇÃO (3)"
      Então apresenta a tela de sucesso na geração de reserva de ordem de serviço
      Quando imprimir fichas de atendimento
      Então e apresentado a mensagem "Necessário cadastrar a ficha nas variáveis DW_RES ou OS_DW_RES" e confirma o popup