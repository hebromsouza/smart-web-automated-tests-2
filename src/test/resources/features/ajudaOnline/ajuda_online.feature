#language: pt
#encoding:utf-8

@pass @top10 @ajudaOnline  @passOK @testeHoje
Funcionalidade: Acessar a tela de ajuda online

  Como um usuário
  Quero acessar a tela de ajuda online
  Para que o sistema consiga ajudar com informações

  Cenario: Acessar ajuda online do sistema
    Quando acessar a pagina de ajuda online
    Entao o sistema valida pagina de ajuda online