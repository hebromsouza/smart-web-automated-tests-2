#language: pt
#encoding: utf-8

@brokenTeste  @aplicarQuestionarios
Funcionalidade: Aplicar questionários

  Cenario: Validação de mensagem de obrigatoriedade do questionario
    Dado que acesso o menu "Pesquisa de Avaliação"
    E aplicar novo questionário
    E selecionar novo questionário "Avaliação de Atendimento"
    Quando gravar questionário
    E gravar sem responder nenhuma questao do questionário
    Entao o sistema valida mensagem de alerta do questionário "Pergunta obrigatória não respondida: Oque achou do atendimento?"
    E clico Ok ou Enter para sair
    E excluir resposta do questionário de avaliação
    #E continua exclusão do questionário

    @pess
  Cenario: Validar mensagem de alerta validando conceito obtido no questionário
    Dado que acesso o menu "Pesquisa de Avaliação"
    E aplicar novo questionário
    E selecionar novo questionário "Avaliação de Atendimento"
    Quando gravar questionário
    E responder o questionário de avaliação
    Entao o sistema valida mensagem de alerta do questionário "Conceito obtido no questionário de avaliação : Bom"
    E clico Ok ou Enter para sair


  @validarQuest
  Cenario: Validar mensagem de alerta ao excluir questionário
    Dado que acesso o menu "Pesquisa de Avaliação"
    E aplicar novo questionário
    E selecionar novo questionário "Avaliação de Atendimento"
    Quando gravar questionário
    E excluir resposta do questionário de avaliação
   # Entao o sistema valida mensagem de alerta do questionário "Confirma a exclusão do formulário?"
    E clico Ok ou Enter para sair

  @validarQuest
  Cenario: Imprimir questionário de avaliação
    Dado que acesso o menu "Pesquisa de Avaliação"
    E aplicar novo questionário
    E selecionar novo questionário "Avaliação de Atendimento"
    Quando gravar questionário
    E responder o questionário de avaliação
    E aceitar alerta de conceito obtido
    Quando abrir o questionário respondido
    E imprimir o questionário
    Entao o sistema valida tela de impressão de questionário
    E excluir resposta do questionário de avaliação
    #Entao o sistema valida mensagem de alerta do questionário "Confirma a exclusão do formulário?"
    E clico Ok ou Enter para sair



#  @questionarioEmpty
#  Esquema do Cenário:
#    Dado que acesso o menu "Pesquisa de Avaliação"
#    E aplicar novo questionário
#    E selecionar novo questionário "<questionario>" e respondido por "<respondido>" com data "<data>"
#    Quando gravar questionário
#    Entao o sistema valida mensagem "<mensagem>"
#
#    Cenarios:
#      |      questionario       | respondido |      data      |                           mensagem                          |
#      |                         |   teste    |     atual      |         O campo questionário deve ser preenchido            |
#      | Pesquisa de Atendimento |   teste2   |    anterior    | Data anterior a data de hoje. Deseja responder mesmo assim? |
#      | Pesquisa de Atendimento |   teste3   | 00/00/00 00:00 |                        Data Inválida!                       |
