#language: pt
#encoding: utf-8

@smartweb
Funcionalidade: Retirada de senha

  Como um paciente
  Quero retirar uma senha
  Para entrar na fila de espera de um médico

  Contexto: Logar no sistema
    Dado que estou logado no sistema smart web
      |   login   | senha |
      | medicware |  mwa  |
    Quando acessar totem
    E selecionar setor "CLINICA SEMEG"

    Cenario: Retirar senha para entrar na fila de espera do médico
      E selecionar médico "ADRIANO EXECUTANTE"
      Quando retirar senha
      E acessar smartweb
      E que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
      E acessar a tela de fila de espera
      Quando buscar fila de espera "ADRIANO"
      Então a senha retirada aparece na fila de espera do médico "ADRIANO EXECUTANTE"
      E é informado a quantidade de pacientes na fila de espera do médico
      E cancelar senha gerada

    Cenário: Retirar senha por ordem do médico
      E selecionar médico "ADRIANO EXECUTANTE" com tipo de atendimento "Prioridade" no totem
      E selecionar médico 2 "ADRIANO EXECUTANTE" com tipo de atendimento "Exame" no totem
      E selecionar médico 3 "ADRIANO EXECUTANTE" com tipo de atendimento "Laboratório" no totem
      E selecionar médico 4 "ADRIANO EXECUTANTE" com tipo de atendimento "Preferencial" no totem
      Quando abrir painel de retirada de senha
      E retirar "2" senhas para tipo de atendimento "Prioridade"
      E retirar "3" senhas para tipo de atendimento "Exame"
      E retirar "2" senhas para tipo de atendimento "Laboratório"
      E retirar "4" senhas para tipo de atendimento "Preferencial"
      Quando fechar painel e voltar do totem para smartweb
      E que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
      E acessar a tela de fila de espera
      E buscar fila de espera "ADRIANO"
      Então o sistema mostra a quantidade de pacientes na fila de espera do médico
      Mas todas as senhas serão canceladas