#language: pt
#encoding: utf-8
################################################ ATENÇÃO - Para o correto funcionamento deste caso de teste. ################################################
################################################ É necessário que o Servidor de Impressões SERVIMP           ################################################
################################################ Esteja Ligado!!!                                           ################################################
################################################ smart.ini = ETQ_LAYOUT_AMOSTRA=2 e Setor Existente SETOR_SOLIC=SQA ########################################

@smartweb @totem
Funcionalidade: Chamar senha por tipo de atendimento

  Como um médico
  Quero chamar paciente por tipo de atendimento
  Para priorizar atendimento pelo tipo de atendimento

  Contexto: Logar no sistema e acessar totem selecionando médicos para retirada de senha
   # Dado que faco logout na aplicacao ignorando assim o login realizado pelo hooks
    Dado que acesso a url do totem "http://localhost/smartweb/totem/start.asp"
    E selecionar setor "CLINICA SEMEG"
    E selecionar médico "ADRIANO EXECUTANTE" com tipo de atendimento "Prioridade" no totem
    E selecionar médico 2 "ADRIANO EXECUTANTE" com tipo de atendimento "Exame" no totem
    E selecionar médico 3 "ADRIANO EXECUTANTE" com tipo de atendimento "Laboratório" no totem
    E selecionar médico 4 "ADRIANO EXECUTANTE" com tipo de atendimento "Preferencial" no totem
    Quando abrir painel de retirada de senha

    @chamarProximo
    Cenario: Validar Retirada de senha por Prioridade, Exame, Laboratório e Preferencial
      E retirar "2" senhas para tipo de atendimento "Prioridade"
      E retirar "3" senhas para tipo de atendimento "Exame"
      E retirar "2" senhas para tipo de atendimento "Laboratório"
      E retirar "4" senhas para tipo de atendimento "Preferencial"
      E fechar painel de retirada de senha e voltar a tela do totem
      Entao verifico que as senhas foram retiradas com sucesso e sem erros
      E trocar de janela, mudando para a do smartweb

     @filaEspera
     Cenario: Validar chamada de pacientes na fila de espera
      #E fechar painel de retirada de senha e voltar a tela do totem
      #E voltar a pagina inicial do smartweb

    #  E voltar do totem para tela de smartweb
    # Quando abrir chamada de senha
    Dado que acesso o menu "Fila de Espera" e sub-menu ""
      E acessar fila de espera do médico "ADRIANO"
      Quando chamar próxima senha
      Então a próxima senha é chamada no painel de chamada de senha com prefixo "PRI" do tipo de atendimento
      Quando chamar próxima senha
      Então a próxima senha é chamada no painel de chamada de senha com prefixo "EXM" do tipo de atendimento
      Quando chamar próxima senha
      Então a próxima senha é chamada no painel de chamada de senha com prefixo "EXM" do tipo de atendimento
      Quando chamar próxima senha
      Então a próxima senha é chamada no painel de chamada de senha com prefixo "LAB" do tipo de atendimento
      Quando chamar próxima senha
      Então a próxima senha é chamada no painel de chamada de senha com prefixo "PER" do tipo de atendimento
      Quando chamar próxima senha
      Então a próxima senha é chamada no painel de chamada de senha com prefixo "PER" do tipo de atendimento
      Quando chamar próxima senha
      Então a próxima senha é chamada no painel de chamada de senha com prefixo "PER" do tipo de atendimento
      Quando chamar próxima senha
      Então a próxima senha é chamada no painel de chamada de senha com prefixo "PRI" do tipo de atendimento
      Mas todas as senhas serão canceladas

    Cenario: Pular para próximo prefixo quando não existir para o atual
      E retirar "2" senhas para tipo de atendimento "Laboratório"
      E retirar "2" senhas para tipo de atendimento "Preferencial"
      Quando abrir chamada de senha
      E voltar do totem para tela de smartweb
      E acessar fila de espera do médico "ADRIANO"
      Quando chamar próxima senha
      Então a próxima senha é chamada no painel de chamada de senha com prefixo "LAB" do tipo de atendimento
      Quando chamar próxima senha
      Então a próxima senha é chamada no painel de chamada de senha com prefixo "PER" do tipo de atendimento
      Mas todas as senhas serão canceladas