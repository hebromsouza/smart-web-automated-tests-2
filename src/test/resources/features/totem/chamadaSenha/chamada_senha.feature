#language: pt
#encoding: utf-8

@smartweb @totemSenha
Funcionalidade: Chamada de senha

  Como um médico
  Quero chamar pacientes da fila de espera
  Para ser atendido

  Contexto: Logar no sistema e retirar senha no totem
    Quando retirar senha no totem com setor "CLINICA SEMEG" e médico "ADRIANO EXECUTANTE"

    Cenario: Efetuar chamada de senha
      E abrir chamada de senha
      E voltar do totem para tela de smartweb
      E que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
      E acessar a tela de fila de espera
      Quando buscar fila de espera "ADRIANO"
      E enviar mensagem para senha retirada na fila de espera
      Então a senha é chamada no painel de chamada de senha
      E cancelar senha gerada