#language: pt
#encoding: utf-8

@smartweb @totemRazao
Funcionalidade: Chamada de senha com razão social

  Como um atendente
  Quero adicionar paciente na fila de espera de um médico
  Para ser chamado para atendimento


    Cenario: Inserir razão social para chamada de senha
      Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
      Quando acessar a tela de fila de espera
      E adicionar na fila
      E buscar paciente pelo nome "LUANA OLIVEIRA DE SOUZA"
      E seleciono o paciente "LUANA OLIVEIRA DE SOUZA" exibido na busca
      E gravar inclusão do paciente na fila de espera "PIXEON" com recepção "CONSULTORIOS"
      Quando buscar fila de espera "ADRIANO"
      E acessar totem
      E selecionar setor "CONSULTORIOS"
      E selecionar médico "ADRIANO EXEC"
      Quando abrir chamada de senha
      E voltar do totem para tela de smartweb
      E enviar mensagem para paciente "LUANA RAZÃO SOCIAL" na fila de espera
      Então o paciente "Luana Razão Social" é chamado no painel de chamada de senha para o médico "MÉDICO: Adriano Exec"
      Mas todas as senhas serão canceladas