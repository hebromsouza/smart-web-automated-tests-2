#language: pt
#encoding: utf-8

@smartweb @bugInterno
Funcionalidade: Buscar registro clínico

  Como um atendente
  Quero inserir dados de busca do paciente em prontuário
  Para buscar registros clínicos do paciente
  
  Contexto: Logar no sistema e criar ordem de serviço
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    Quando buscar paciente pelo nome "LUANA OLIVEIRA DE SOUZA"
    E criar uma nova ordem de serviço incluindo o item "HEMO,HEMAN"
 #   E criar uma nova ordem de serviço
 #   E incluir o item "HDL" com instrução
    E excluir resposta do questionário
    E voltar a pagina e pegar o valor do campo de busca pela forma de busca "ordemServico"

    @SMART-17672
    Cenario: Buscar registro clínico por ordem de serviço
      E que acesso o menu "Prontuário"
      Quando buscar o paciente pela forma de busca "ordemServico" e dados de busca ""
      E acessar a aba "Registros Clínicos"
      Então o sistema valida se foi feito a busca apenas da ordem de serviço na tela de registros clínicos