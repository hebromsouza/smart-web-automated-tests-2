#language: pt
# encoding: UTF-8

@SMARTPEP @pasta:03 ### NGS1.S007
Funcionalidade: Auditoria continua e proteção dos registros de auditoria

  Como um usuário
  Gostaria de acessar a aplicação
  Para que eu possa validar proteção dos registros de auditoria.

  @prioridade:alta    ####NGS1.S007
  Esquema do Cenário: Validar que na tabela de busca auditoria contem os dados
    Dado que estou no sistema Smart Web
    E que preencho login e senha
      | login       |  senha       |
      | automation  | a12345678    |
    Quando eu realizar o login
    Quando que acesso o menu "<menu>" e sub-menu "<subMenu>"
      |    menu   | subMenu |
      | Auditoria |         |
    E que preencho dados da busca na  tela de auditoria
      |tipoDeEventos   |descricao                              |periodoInicio|periodoFim|usuario    |registroAfetado |identificador|
      |Atendimento     |Aberto atendimento para o paciente     |03/05/2019   |03/05/2019|MARIAB     |243             |78453        |
    Quando buscar na  tela de auditoria
    Então validar o que na tabela contem  "<Data e hora do evento>","<Nivel de criticidade>" ,"<Tipo de evento>","<Identificacao do componente gerador do evento>", "<Identificacao do usuario>", "<Indicacao de atividade>" e as "<Identificador univoco do registro>"
    E deslogar

    Exemplos:
      |Data e hora do evento    |Nivel de criticidade|Tipo de evento |Identificacao do componente gerador do evento| Identificacao do usuario| Indicacao de atividade                  |Identificador univoco do registro|
      |2019-05-03T09:45:16-03:00|   Aviso            |Atendimento    |          243                                |   MARIAB                | Aberto atendimento para o paciente (243)|         78453               |

  @prioridade:alta
  Esquema do Cenário: Validar que apenas os usaurio com perfio de auditor tem acesso ao menu de auditoria.
    Dado que estou no sistema Smart Web
    E que preencho login e senha
      | login       |  senha       |
      | documenta2  | a12345678    |
    Quando eu realizar o login
    Quando  for selecionada o Grupo de usuário "<grupo>"
    Entao o menu "Auditoria" não devera esta presente na tela Home
    E deslogar

    Exemplos:
      |grupo       |
      |Admin       |
      |CCIH        |
      |Enfermeiro  |
      |Gest Acesso |
      |Médico      |
      |Operador Sis|
      |Técnico Rad |
      |Recepcionist|
      |T Enfermagem|