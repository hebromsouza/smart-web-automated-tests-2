  #language: pt
  #encoding: UTF-8

  @SMARTPEP @validaVersaoSoftware @pasta:01
  Funcionalidade: Versão do software

    Como um usaraio
    Gostaria de acesar a aplicação
    Para qua eu possa validar dados do software.

     @prioridade:baixa
    Cenario: Validar informações do Software na tela de Login
      Dado que estou no sistema Smart Web
      Entao é apresentado no software nome do software "SmartPEP", nome do fornecedor "Pixeon", identificação da versão "v02.11", build "Build 00.01"

    @prioridade:baixa
    Cenario: Validar informações do Software nas Telas com usuario documenta2
      Dado  que estou no sistema Smart Web
      E que preencho login e senha
        | login      | senha     |
        | documenta2 | a12345678 |
      Quando eu realizar o login
      Quando  for selecionada o Grupo de usuário "Médico"
      Então é apresentado no software nome do software e a identificação da versão "SmartPEP — v02.11", build  "(Build 00.01)" e o  nome do fornecedor "Pixeon" na parte princial Home
      E que acesso o menu "<menu>"
        |         menu         |
        | Cadastro do Paciente |
      Então é apresentado no software nome do software e a identificação da versão "SmartPEP — v02.11", build  "(Build 00.01)" e o  nome do fornecedor "Pixeon" na parte princial Home
      E que acesso o menu "<menu>"
        |    menu    |
        | Prontuário |
      Então é apresentado no software nome do software e a identificação da versão "SmartPEP — v02.11", build  "(Build 00.01)" e o  nome do fornecedor "Pixeon" na parte princial Home
      E que acesso o menu "<menu>"
        |    menu    |
        | Cadastros  |
      Então é apresentado no software nome do software e a identificação da versão "SmartPEP — v02.11", build  "(Build 00.01)" e o  nome do fornecedor "Pixeon" na parte princial Home
      E deslogar

    @prioridade:baixa
    Cenario: Validar informações do Software nas Telas com usuario documenta2 com perfil de Recepcionista
      Dado que estou no sistema Smart Web
      E que preencho login e senha
        | login      | senha     |
        | documenta2 | a12345678 |
      Quando eu realizar o login
      Quando  for selecionada o Grupo de usuário "Recepcionist"
      Então é apresentado no software nome do software e a identificação da versão "SmartPEP — v02.11", build  "(Build 00.01)" e o  nome do fornecedor "Pixeon" na parte princial Home
      E que acesso o menu "<menu>"
        |     menu    |
        | Atendimento |
      Então é apresentado no software nome do software e a identificação da versão "SmartPEP — v02.11", build  "(Build 00.01)" e o  nome do fornecedor "Pixeon" na parte princial Home
      E que acesso o menu "<menu>"
        |         menu         |
        | Cadastro do Paciente |
      Então é apresentado no software nome do software e a identificação da versão "SmartPEP — v02.11", build  "(Build 00.01)" e o  nome do fornecedor "Pixeon" na parte princial Home
      E que acesso o menu "<menu>"
        |    menu    |
        | Prontuário |
      Então é apresentado no software nome do software e a identificação da versão "SmartPEP — v02.11", build  "(Build 00.01)" e o  nome do fornecedor "Pixeon" na parte princial Home
      E que acesso o menu "<menu>"
        |    menu    |
        | Cadastros  |
      Então é apresentado no software nome do software e a identificação da versão "SmartPEP — v02.11", build  "(Build 00.01)" e o  nome do fornecedor "Pixeon" na parte princial Home
      E deslogar

    @prioridade:baixa
    Cenario: Validar informações do Software no PDF de impressão de uma ANAMNESE com usuario carlosj
      Dado que estou no sistema Smart Web
      E que preencho login e senha
        | login      | senha     |
        | carlosj    | a12345678  |
      Quando eu realizar o login
      E que acesso o menu "<menu>"
        |    menu    |
        | Prontuário |
      E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
      | registro |
      | 191      |
      Quando buscar paciente Prontuário na tela Busca do paciente
      E acessar a aba "<abaProntuario>" na tela de Prontuário
        |   abaProntuario   |
        | Registro clínicos |
      Então é apresentado no software nome do software e a identificação da versão "SmartPEP — v02.11", build  "(Build 00.01)" e o  nome do fornecedor "Pixeon" na parte princial Home
      Quando  selecionar no  Adicionar a opção "Outros Registros..." na Aba Registro Clinico
      E selecionar o Formulário Anaminese na tela de Novo registro
      E editar o campo Queixa Principal para "Dor De Cabeça" da tela Formulario de anaminese
      Quando finalizar na tela Formulario de anaminese
      E Fechar a tela Formulario de anaminese
      Quando impimir na tela de Protuario na Aba Registro Clinico
      E informar a data inicial "30/04/2019" e a data Final "30/04/2019" na tela de periodo e busca
      E informar a Justificativa "Testes automatizados" e confiramr  na tela de Justificativa
      Então validar conteudo no PDF, nome do software, fornecedor, versão e build   "SmartPEP - Pixeon Medical Systems - Versão 02.11 (Build 00.01)"

