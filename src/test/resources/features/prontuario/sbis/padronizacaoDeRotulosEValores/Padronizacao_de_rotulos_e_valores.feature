#language: pt
# encoding: UTF-8


@SMARTPEP ### ECFA.S026
Funcionalidade:Direito de acesso

  @prioridade:alta
  Cenario: Imprimir prontuário com rótulos e valores padrão
    Dado que acesso o menu "<menu>"
      |    menu    |
      | Prontuário |
    E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
      | registro |
      | 191      |
    Quando buscar paciente Prontuário na tela Busca do paciente
    E acessar a aba "<abaProntuario>" na tela de Prontuário
      |   abaProntuario   |
      | Registro clínicos |
    Quando  selecionar no  Adicionar a opção "FORMULÁRIO DE ANAMNESE" na Aba Registro Clinico
    Quando confirmar o popoup "Confirma o registro de um novo(a) FORMULÁRIO DE ANAMNESE"
    E editar o campo Queixa Principal para "Estou com Dor De Cabeça." da tela Formulario de anaminese
    Quando Gravar na tela Formulario de anaminese
    E Fechar a tela Formulario de anaminese
    Quando impimir na tela de Protuario na Aba Registro Clinico
    E fechar a tela de Periodo
    Quando confirmar o popoup "Deseja imprimir todos os registros clínicos deste paciente?"
    E informar a Justificativa "Testes automatizados" e confiramr  na tela de Justificativa
    Então o sistema valida na impressão em PDF, o campo "História da Doença: Nada digno de nota" na tela de impresão do prontuario


  @caso:incompleto
  Cenario: Impressão de registro de lembrete
    Dado que estou no sistema Smart Web
    E que preencho login e senha
      |   login    | senha |
      |   carlosj  |  MWA  |
    Quando eu realizar o login
    E que acesso o menu "<menu>"
      |       menu      |
      | "Notas Globais" |
    E adicionar nova notas global
    E informar descrição da nota
    E gravar nova nota global
    E que acesso o menu "<menu>"
      |    menu    |
      | Prontuário |
    E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
      | registro |
      | 191      |
    Quando buscar paciente Prontuário na tela Busca do paciente
    Então o sistema valida se no campo de "Notas Globais" aparece o valor registrado