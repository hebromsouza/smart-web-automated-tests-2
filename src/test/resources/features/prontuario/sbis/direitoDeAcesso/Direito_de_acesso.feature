#language: pt
# encoding: UTF-8

@SMARTPEP ### ECFA.S032
Funcionalidade:Direito de acesso


  @prioridade:alta
  Cenario: Impressão do prontuário completo com comando único
    Dado que estou no sistema Smart Web
    E que preencho login e senha
      | login    |  senha    |
      | carlosj | a12345678  |
    Quando eu realizar o login
    E que acesso o menu "<menu>"
      |    menu    |
      | Prontuário |
    E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
      | registro |
      |   151    |
    Quando buscar paciente Prontuário na tela Busca do paciente
    E acessar a aba "<abaProntuario>" na tela de Prontuário
      |   abaProntuario   |
      | Registro clínicos |
    Quando impimir na tela de Protuario na Aba Registro Clinico
    E fechar a tela de Periodo
    Quando confirmar o popoup "Deseja imprimir todos os registros clínicos deste paciente?"
    E informar a Justificativa "Testes automatizados" e confiramr  na tela de Justificativa
    Então o sistema valida na impressão em PDF, o campo "Página 8/8" na tela de impresão do prontuario

 #   Então o sistema valida informações da impressão de acordo com o documento
 #   E valida relação sequencial entre as páginas "<numeroPagina>" e "<totalPagina>"
 #   E validar índice do prontuário indicando a relação e a ordem dos respectivos documentos
 #   E valida se foi impresso recibo como parte integrante do prontuário


  @prioridade:alta
  Esquema do Cenario: Impressão do prontuário completo pelo período
    Dado que estou no sistema Smart Web
    E que preencho login e senha
      | login    |  senha    |
      | carlosj  | a12345678  |
    Quando eu realizar o login
    E que acesso o menu "<menu>"
      |    menu    |
      | Prontuário |
    E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
      | registro |
      | 151      |
    Quando buscar paciente Prontuário na tela Busca do paciente
    E acessar a aba "<abaProntuario>" na tela de Prontuário
      |   abaProntuario   |
      | Registro clínicos |
    Quando impimir na tela de Protuario na Aba Registro Clinico
    E informar a data inicial "<periodoInicial>" e a data Final "<periodoFinal>" na tela de periodo e busca
    E informar a Justificativa "Testes automatizados" e confiramr  na tela de Justificativa
    Então o sistema valida na impressão em PDF, o campo "<periodoInicial>" na tela de impresão do prontuario
    E o sistema valida na impressão em PDF, o campo "<periodoFinal>" na tela de impresão do prontuario

    Exemplos:
      | periodoInicial | periodoFinal |
      |   03/04/2019   |  03/04/2019  |


  @caso:incompleto
  Cenario: Anexar arquivos externos
    Dado que estou no sistema Smart Web
    E que preencho login e senha
      | login    |  senha    |
      | carlosj  | a12345678  |
    Quando eu realizar o login
    E que acesso o menu "<menu>"
      |    menu    |
      | Prontuário |
    E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
      | registro |
      | 191      |
    Quando buscar paciente Prontuário na tela Busca do paciente
    E acessar a aba "<abaProntuario>" na tela de Prontuário
      |   abaProntuario   |
      | Registro clínicos |
    Quando  selecionar no  Adicionar a opção "FORMULARIO DE EXAME FISICO" na Aba Registro Clinico
    Então e apresentado a mensagem "Confirma o registro de um novo(a) FORMULARIO DE EXAME FISICO" e confirma o popup
    Quando anexar arquivos na tela de Formularios de exame Fisico
    Então e apresentado a mensagem "Para anexar arquivos será necessário instalar o plugin flash no navegador" e confirma o popup
    Quando habilitar o Flash na tela de arquivo
    E que acesso o menu "<menu>"
      |    menu    |
      | Prontuário |
    E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
      | registro |
      | 191      |
    Quando buscar paciente Prontuário na tela Busca do paciente
    E acessar a aba "<abaProntuario>" na tela de Prontuário
      |   abaProntuario   |
      | Registro clínicos |
    Quando  selecionar no  Adicionar a opção "FORMULARIO DE EXAME FISICO" na Aba Registro Clinico
    Então e apresentado a mensagem "Confirma o registro de um novo(a) FORMULARIO DE EXAME FISICO" e confirma o popup
    Quando anexar arquivos na tela de Formularios de exame Fisico
    E adicionar arquivos na tela de arquivos
    Quando informo o caminho do arquivo "\\10.71.5.104\desenv\Arquivos_Desenv\Allan\LAUDO DE HEMOGRAMA.pdf" na janela do windows e abrir
    Então e apresentado a mensagem "O(s) arquivo(s) é(são) laudo(s) externo(s)" e confirma o popup
    Então o sistema valida se foi anexado o arquivo na tela de arquivos
    Quando fechar tela de arquivos
    E informar "teste automatizado" no campo exame fisico na tela de Formularios de exame Fisico
    E Finalizaro laudo na tela de Formularios de exame Fisico
    E fechar formulário na tela de Formularios de exame Fisico
    Então o sistema valida se foi informado que o registro clínico "EXAME FISICO" esta com o status "(Externo), " e com o icone de anexo na  prontuario aba registro clinico
    Quando impimir na tela de Protuario na Aba Registro Clinico
    E fechar a tela de Periodo
    Então e apresentado a mensagem "Deseja imprimir todos os registros clínicos deste paciente?" e confirma o popup
    E informar a Justificativa "Testes automatizados" e confiramr  na tela de Justificativa
    Então o sistema valida na impressão em PDF, o campo "Página 8/8" na tela de impresão do prontuario
