#language: pt
# encoding: UTF-8

@SMARTPEP @pasta:04 # ECFA.S032
Funcionalidade:Controle de tentativas de login e gerenciamento de usuarios

  @prioridade:alta #caso 1
  Cenario:  Configuração do sistema e validar Tempo de expiração
    Dado que estou no sistema Smart Web
    E que preencho login e senha
      | login     | senha      |
      | ADM_ROBO  | a12345678  |
    Quando eu realizar o login
    E que acesso o menu "<menu>"
      |    menu   |
      | Cadastros |
    E acessar a configuração  "Configurações Gerais"  da tela de Cadastro
   Quando modificar o parametro tempo de expiração  para "1" minuto na tela de configurações Gerais
   E gravar na tela de configurações Gerais
   E Fechar na tela de configurações Gerais
   E deslogar
   E que preencho login e senha
     | login  | senha |
     | CARLOSC | A12345678 |
    Quando eu realizar o login
    Então espera "1" minuto e validar se o sistema apresenta a mensagem "Sessão encerrada por inatividade" na tela de login
    E que preencho login e senha
      | login     | senha |
      | ADM_ROBO  | a12345678  |
    Quando eu realizar o login
    E que acesso o menu "<menu>"
      |    menu   |
      | Cadastros |
    E acessar a configuração  "Configurações Gerais"  da tela de Cadastro
    Quando modificar o parametro tempo de expiração  para "99" minuto na tela de configurações Gerais
    E gravar na tela de configurações Gerais
    E Fechar na tela de configurações Gerais
    E deslogar

  @prioridade:alta
  Cenario: Configuração do sistema e validar quantidade de tentativa de acesso
    Dado que estou no sistema Smart Web
    E que preencho login e senha
      | login     | senha      |
      | ADM_ROBO  | a12345678  |
    Quando eu realizar o login
    E que acesso o menu "<menu>"
      |    menu   |
      | Cadastros |
    E acessar a configuração  "Configurações Gerais"  da tela de Cadastro
    Quando modificar o parametro quantidade de tentativa invalida para "2"
    E gravar na tela de configurações Gerais
    E Fechar na tela de configurações Gerais
    E deslogar
    Quando eu realizar o login
    E que preencho login e senha
      | login  | senha |
      | ROBSONR | mwa   |
    Quando eu realizar o login
    E  que preencho login e senha
      | login  | senha |
      | ROBSONR | mwa   |
    Quando eu realizar o login
    E  que preencho login e senha
      | login  | senha |
      | ROBSONR | mwa   |
    Quando eu realizar o login
    Então o sistema apresenta a mensagem "Conta bloqueada! Entre em contato com o administrador do sistema." na tela de login
    E que preencho login e senha
      | login     |  senha     |
      | ADM_ROBO  |a12345678  |
    Quando eu realizar o login
    E que acesso o menu "<menu>"
      |    menu   |
      | Cadastros |
    E selecionar o cadastro de "Usuários" na tela de Cadastros Cadastro de Gerais
    Quando informar de busca da tela de Usuários
      | login     |
      | ROBSONR   |
    E filtrar na tela de Usuários
    E selecionar o nome "ROBSON ROBÔ" na tela de Usuários
    Quando eu ativar a opção Ativo? na tela dados do usuario
    E eu ativar a selção sem prazo na tela dados do usuário
    Quando gravar na tela dados do usuario
    E fechar tela dados do usuario
    E deslogar
    E que preencho login e senha
      | login     |  senha     |
      | ROBSONR   | a12345678  |
    Quando eu realizar o login
    Então os sistema apresenta a tela Home da aplicação

  @prioridade:alta
  Esquema do Cenário: Registro de auditorias controle acesso de login
    Dado que estou no sistema Smart Web
    E informar um   usuario "<Usuario>" e a Senha "<Senha>"
    Quando eu realizar o login
    Então  o sistema apresenta a mensagem "<mensagem>" na tela de login

    Exemplos:
      | Usuario            | Senha             | mensagem                     |
      | Auditor 1          | Senha invalida	   | Usuário ou Senha inválidos.  |
      | Teste inexistente  | mwa			   | Usuário ou Senha inválidos.  |
      | ADM_ROBO           | a12345678         | 				              |


  @prioridade:alta #caso 2
  Cenario: Validar se o usuário com perfil de administrador pode bloquear e desbloquear outro usuário
    Dado que estou no sistema Smart Web
    E que preencho login e senha
      | login     |    senha   |
      | ADM_ROBO  | a12345678  |
    Quando eu realizar o login
    E que acesso o menu "<menu>"
      |    menu   |
      | Cadastros |
    E selecionar o cadastro de "Usuários" na tela de Cadastros Cadastro de Gerais
    Quando informar de busca da tela de Usuários
      | login    |
      | carlosj  |
    E filtrar na tela de Usuários
    E selecionar o nome "CARLOS J" na tela de Usuários
    Quando eu disativo a selção do usuario Ativo? da tela dados do usuario
    E eu disativo  a selção sem prazo na tela dados do usuário
    Quando informa a data atual na tela dados do usuário
    Quando gravar na tela dados do usuario
    E fechar tela dados do usuario
    E deslogar
    E que preencho login e senha
      | login    |  senha    |
      | carlosj | a12345678  |
    Quando eu realizar o login
    Então o sistema apresenta a mensagem "Conta bloqueada! Entre em contato com o administrador do sistema." na tela de login
    E que preencho login e senha
      | login     |  senha     |
      | ADM_ROBO  |a12345678  |
    Quando eu realizar o login
    E que acesso o menu "<menu>"
      |    menu   |
      | Cadastros |
    E selecionar o cadastro de "Usuários" na tela de Cadastros Cadastro de Gerais
    Quando informar de busca da tela de Usuários
      | login    |
      | carlosj  |
    E filtrar na tela de Usuários
    E selecionar o nome "CARLOS J" na tela de Usuários
    Quando eu ativar a opção Ativo? na tela dados do usuario
    E eu ativar a selção sem prazo na tela dados do usuário
    Quando gravar na tela dados do usuario
    E fechar tela dados do usuario
    E deslogar
    E que preencho login e senha
      | login     |  senha   |
      | carlosj | a12345678  |
    Quando eu realizar o login
    Então os sistema apresenta a tela Home da aplicação

