#language: pt
# encoding: UTF-8

@SMARTPEP @pasta:02 ## ECFA.S032
Funcionalidade:Responsabilidade sobre contribuicao aos registros sob supervisao

  Como um usaraio
  Gostaria de acesar a aplicação
  Para qua eu possa validar que o software possibilite que um usario supervisor de um outro usuario realiza tarefa de aprovação, Aguardando aprovação e o seu supervisor consegue aprovar,

  @prioridade:alta ##FUNC.18.09 - Responsabilidade sobre contribuição aos registros
  Cenario: validar que o estudante  pós-graduando ao gravar um registro no formulario de anaminese o regiastro dever fica com staus Aguardando aprovação o seu preceptor podera Aprovar.
    Dado que estou no sistema Smart Web
    E que preencho login e senha
      | login  | senha     |
      | Adm    | a12345678 |
    Quando eu realizar o login no SmartPEP
    E que acesso o menu "<menu>"
      |    menu    |
      | Cadastros  |
    E selecionar o cadastro de "Profissionais" na tela de Cadastros Cadastro de Gerais
    E que preencho informo dados da busca paciente Profissionais na tela Busca do Profissionais
     | nome |
     | ESTUDANTE POS GRADUANDO     |
    Quando buscar paciente Prontuário na tela Busca do Profissionais
    E selecionar o profissional "ESTUDANTE POS GRADUANDO" na tela Busca do Profissionais
    Quando habilitar o campo sob supervisão da tela dados do profissional
    E selecioanr o supervisor "CARLOS J" na tela dados do profissional
    E gravar dados e fechar a tela dados do profissional
    E deslogar
    E que preencho login e senha
      | login      | senha     |
      | ESTUDANTE | a12345678  |
    Quando eu realizar o login no SmartPEP
    E que acesso o menu "<menu>"
      |    menu    |
      | Prontuário |
    E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
      | registro |
      | 191      |
    Quando buscar paciente Prontuário na tela Busca do paciente
    E acessar a aba "<abaProntuario>" na tela de Prontuário
      |   abaProntuario   |
      | Registro clínicos |
    Quando  selecionar no  Adicionar a opção "Registro Clínico..." na Aba Registro Clinico
    E informar o registro "FORMULÁRIO ANAMNESE" e grava na tela de novo registro
    E editar o campo Queixa Principal para "Dor De Cabeça." da tela Formulario de anaminese
    Quando finalizar na tela Formulario de anaminese
    E Fechar a tela Formulario de anaminese
    Então validar se o Status do "FORMULÁRIO ANAMNESE - Aguardando aprovação" para o atendimento feito na tela de prontuário
    E deslogar
    E que preencho login e senha
      | login   | senha     |
      | carlosj | a12345678 |
    Quando eu realizar o login no SmartPEP
    E que acesso o menu "<menu>"
      |    menu    |
      | Prontuário |
    E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
      | registro |
      | 191      |
    Quando buscar paciente Prontuário na tela Busca do paciente
    E acessar a aba "<abaProntuario>" na tela de Prontuário
      |   abaProntuario   |
      | Registro clínicos |
    E Seleciono o último FORMULÁRIO ANAMNESE - Aguardando aprovação inserido na  tela de novo registro
    Quando aprovar da tela Formulario de anaminese
    Então e apresentado a mensagem "Registro aprovado." e confirma o popup da tela Formulario de anaminese
    E Fechar a tela Formulario de anaminese
    Então validar se o Status do "FORMULÁRIO ANAMNESE - Aprovado por: CARLOSJ" para o atendimento feito na tela de prontuário
    E deslogar

  @prioridade:alta  #FUNC.18.09 - Responsabilidade sobre contribuição aos registros
  Cenario: validar que o estudante  pós-graduando ao gravar um registro no formulario de anaminese o medico não preceptor não pode confirme as informações registradas pelo residente
    Dado que estou no sistema Smart Web
    E que preencho login e senha
      | login      | senha     |
      | Adm        | a12345678 |
    Quando eu realizar o login no SmartPEP
    E que acesso o menu "<menu>"
      |    menu    |
      | Cadastros  |
    E selecionar o cadastro de "Profissionais" na tela de Cadastros Cadastro de Gerais
    E que preencho informo dados da busca paciente Profissionais na tela Busca do Profissionais
      | nome |
      | ESTUDANTE POS GRADUANDO     |
    Quando buscar paciente Prontuário na tela Busca do Profissionais
    E selecionar o profissional "ESTUDANTE POS GRADUANDO" na tela Busca do Profissionais
    Quando habilitar o campo sob supervisão da tela dados do profissional
    E selecioanr o supervisor "CARLOS J" na tela dados do profissional
    E gravar dados e fechar a tela dados do profissional
    E deslogar
    E que preencho login e senha
      | login      | senha     |
      | ESTUDANTE | a12345678  |
    Quando eu realizar o login no SmartPEP
    E que acesso o menu "<menu>"
      |    menu    |
      | Prontuário |
    E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
      | registro |
      | 191      |
    Quando buscar paciente Prontuário na tela Busca do paciente
    E acessar a aba "<abaProntuario>" na tela de Prontuário
      |   abaProntuario   |
      | Registro clínicos |
    Quando  selecionar no  Adicionar a opção "Registro Clínico..." na Aba Registro Clinico
    E informar o registro "FORMULÁRIO ANAMNESE" e grava na tela de novo registro
    E editar o campo Queixa Principal para "Dor De Cabeça." da tela Formulario de anaminese
    Quando finalizar na tela Formulario de anaminese
    E Fechar a tela Formulario de anaminese
    Então validar se o Status do "FORMULÁRIO ANAMNESE - Aguardando aprovação" para o atendimento feito na tela de prontuário
    E deslogar
    E que preencho login e senha
      | login   | senha     |
      | CARLOSC | A12345678 |
    Quando eu realizar o login no SmartPEP
    E que acesso o menu "<menu>"
      |    menu    |
      | Prontuário |
    E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
      | registro |
      | 191      |
    Quando buscar paciente Prontuário na tela Busca do paciente
    E acessar a aba "<abaProntuario>" na tela de Prontuário
      |   abaProntuario   |
      | Registro clínicos |
    E Seleciono o último FORMULÁRIO ANAMNESE - Aguardando aprovação inserido na  tela de novo registro
    Então é apresentado a mensagem "O registro está aguardando aprovação" da tela acesso negado


