  #language: pt
  #encoding: UTF-8

  @SMARTPEP ###ECFA.S030
  Funcionalidade: Responsabilidade sobre contribuicao aos registros sob supervisao

    Como um usuário
    Gostaria de acessar a aplicação
    Para que eu possa validar a criação e registro do formulário de consentimento de algum procedimento para o paciente.

    ####FUNC.16.01 - Consentimento livre e esclarecido
    @caso:incompelto
    Cenario: Alterar estrutura de formulário de consentimento
      Dado que estou no sistema Smart Web
      E que preencho login e senha
        |   login      | senha       |
        |   ADM_ROBO   |  a12345678  |
      Quando eu realizar o login
      E que acesso o menu "<menu>"
        |    menu    |
        | Cadastros  |
      E selecionar o cadastro de "Formulários de Consentimento" na tela de Cadastros Cadastro de Gerais
      E selecionar o serviço
      Quando montar a estrutura
      E gravar formulário
      Entao o sistema apresenta mensagem de sucesso

      E valida alteração do formulário

    @caso:incompelto   ##FUNC.16.02 - Gerenciamento de consentimento livre e esclarecido
    Cenario: Preencher consentimento livre
      Dado que estou no sistema Smart Web
      E que preencho login e senha
        |   login    | senha |
        |   carlosj   |   a12345678  |
      Quando eu realizar o login
      E que acesso o menu "<menu>"
        |    menu    |
        | Prontuário |
      E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
        | registro |
        | 73       |
      Quando buscar paciente Prontuário na tela Busca do paciente
      E selecionar o cadastro de "Formulários de Consentimento" na tela de Cadastros Cadastro de Gerais
      E selecionar o serviço
      Quando responder o o consentimento
      E imprimir formulário
      Entao o sistema mostra tela de impressão com as informações
      E valida alteração do formulário

    @caso:incompleto  ##FUNC.16.02 - Gerenciamento de consentimento livre e esclarecido
    Cenario: Consultar consentimento preenchido
      Dado que estou no sistema Smart Web
        |   login   | senha        |
        |   carlosj |   a12345678  |
      Quando eu realizar o login
      E que acesso o menu "<menu>"
        |    menu    |
        | Prontuário |
      E que preencho informo dados da busca paciente Prontuário na tela Busca do paciente
        | registro |
        |    73    |
      Quando buscar paciente Prontuário na tela Busca do paciente
      Quando selecionar "Consentimentos Preenchidos"
      E selecionar o serviço
      Entao o sistema valida informações preenchidas com seu respectivo registro de tempo