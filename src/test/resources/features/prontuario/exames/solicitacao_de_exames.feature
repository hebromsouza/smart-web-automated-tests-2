#language: pt
#encoding:utf-8

@brokenTeste @top10
Funcionalidade: Realizar solicitação de grupo de exames

  Como um médico
  Quero solicitar grupo de exames
  Para o paciente

  @solicitarExames2
  Cenario: Solicitar grupo de exames para o paciente
    Dado que acesso o menu "Prontuário"
    Quando buscar paciente pelo nome "PACIENTE PRONTUARIO EXAMES"
    E acessar a aba "Exames"
    Quando fizer a solicitação de exames
    E selecionar grupo de "Cardiologia"
    E selecionar exames de grupo cardiologia
    E selecionar grupo de Laboratorio e sub-grupo de Bacteriologia
    E selecionar exames de grupo laboratorio
    Quando avançar para preenchimento de dados
    E selecionar o profissional "ADRIANO EXEC"
    E selecionar tipo de atendimento "Exames"
    E selecionar tipo de saída "Internação"
    Quando concluir solicitação
    Então cancelo a impressão da solicitação
    E o sistema valida pedido de exames de cardiologia e laboratorio
    E cancelo a solicitação de exames com motivo "Teste cancelamento"