#language: pt
#encoding: utf-8

@quarentena
Funcionalidade: Retirar paciente da visualização de internação

  Como um recepcionista
  Quero retirar paciente da internação
  Para visualizar todos os registros clínicos do paciente

  Contexto: Logar no sistema, abrir prontuário do paciente
    Dado que acesso o menu "Prontuário"
    E buscar paciente pelo nome "PACIENTE INTERNACAO"

  Cenário: Retirar paciente da visualização de internação
    Quando retirar paciente da visualização de internação
    E acessar a aba "Registros Clínicos" na tela de Prontuário
    Então o sistema valida se existe o registro clínico de internação
    E valida o médico "ADRIANO EXEC" do registro clínico