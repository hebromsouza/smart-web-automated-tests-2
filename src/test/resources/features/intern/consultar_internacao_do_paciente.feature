#language: pt
#encoding: utf-8

@pass1
Funcionalidade: Consultar paciente internado

  Como um recepcionista
  Quero consultar internação do paciente
  Para confirmar informações da internação do paciente

  Contexto: Logar no sistema, abrir prontuário do paciente
    Dado que acesso o menu "Prontuário"
    E buscar paciente pelo nome "Paciente Internação"

    Cenário: Consultar paciente internado
      Quando abrir detalhes da internação/tratamento
      Então o sistema valida status aberto da internação