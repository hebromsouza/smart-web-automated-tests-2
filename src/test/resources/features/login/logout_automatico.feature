#language:pt
#encoding:utf-8

@quarentena @top10 @EmDesenvolvimento
Funcionalidade: Logout automatico

  Como um usuário
  Quero logar em duas instâncias do sistema com o mesmo usuário
  Para validar logout automático

  Cenario: Deslogar automaticamente
    Dado que estou no sistema Smart Web
    E que preencho login e senha
      |   login   | senha |
      | medicware |  mwa  |
    Quando eu realizar o login
    Entao o sistema solicita o preenchimento da recepção
    Quando a recepção for selecionada "CONSULTORIOS"
    Entao o sistema realiza o login com sucesso
    Quando que estou no sistema Smart Web
    E que preencho login e senha
      | login     | senha |
      | medicware | mwa   |
    Quando eu realizar o login
    Então o sistema solicita o preenchimento da recepção
    Quando a recepção for selecionada "CONSULTORIOS"
    Então o sistema realiza o login com sucesso
    E valida se o acesso anterior foi deslogado automaticamente

  Cenario: Deslogar automaticamente ao logar com webclientes
    Dado que estou no sistema Smart Web utilizando webclientes
    E que preencho login e senha
      |   login   | senha |
      | medicware |  mwa  |
    Quando eu realizar o login
    Entao o sistema solicita o preenchimento da recepção
    Quando a recepção for selecionada "CONSULTORIOS"
    Entao o sistema realiza o login com sucesso
    Quando que estou no sistema Smart Web
    E que preencho login e senha
      | login     | senha |
      | medicware | mwa   |
    Quando eu realizar o login
    Então o sistema solicita o preenchimento da recepção
    Quando a recepção for selecionada "CONSULTORIOS"
    Então o sistema realiza o login com sucesso
    E valida se o acesso anterior foi deslogado automaticamente