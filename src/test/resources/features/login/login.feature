#language:pt
#encoding:utf-8

@quarentenaLogin
Funcionalidade: Login

  Contexto:
    Dado que estou no sistema Smart Web

  Cenário: Login valido no Smart Web
    Quando efetuo login no smartweb com o usuario "MEDICWARE" e a senha "123"
    Quando eu realizar o login
    Então o sistema solicita o preenchimento da recepção
    Quando a recepção for selecionada "CONSULTORIOS"
    Então o sistema realiza o login com sucesso


  Cenário: Login invalido no Smart Web
    Quando efetuo login no smartweb com o usuario "MEDICWARE" e a senha "123456"
 #   E que preencho login e senha
 #     | login     | senha |
 #     | medicware | 123456   |
    Quando eu realizar o login
    Então o sistema exibe mensagem informando que usuario ou senha estão inválidos
