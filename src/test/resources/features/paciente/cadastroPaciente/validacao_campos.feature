#language: pt
#encoding:utf-8

@pass @validarCamposCPF
Funcionalidade: Validar campos cadastro de paciente
  
  Como um atendente
  Quero inserir dados do paciente na tela de cadastro
  Para alteração ou criação do cadastro do paciente

  Contexto: Buscar pelo paciente
    E que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar paciente pelo nome sem avancar "ÍVARR BEINLAUSI"

    @cpf_alfanumerico
    Cenario: Inserir CPF com algarismos alfanumérico
      Quando inserir cpf "038765jd434" no cadastro do paciente
      E gravar cadastro do paciente
      Então é apresentado popup com mensagem "ERRO: CPF inválido. (038765jd434)" e confirma

    @cpf_invalido
    Cenario: Inserir CPF inválido
      Quando inserir cpf "12345678910" no cadastro do paciente
      E gravar cadastro do paciente
      Então é apresentado popup com mensagem "ERRO: CPF inválido. (12345678910)" e confirma

    @cpf_invalidoComPontuacao
    Cenario: Inserir CPF inválido com pontuação
      Quando inserir cpf "787.279.277-70" no cadastro do paciente
      E gravar cadastro do paciente
      Então é apresentado popup com mensagem "ERRO: CPF inválido. (787.279.277)" e confirma

    @cpf_valido
    Cenario: Inserir CPF válido
      Quando inserir cpf "77894574031" no cadastro do paciente
      E avançar cadastro do paciente
      Então o sistema valida inclusão do cpf sem pontuacao

    @cpf_validoPontuacao
    Cenario: Inserir CPF válido com pontuação
      Quando inserir cpf "778.945.740-31" no cadastro do paciente
      E gravar cadastro do paciente
      #E avançar cadastro do paciente
      #Então o sistema valida inclusão do cpf
      Então é apresentado popup com mensagem "ERRO: CPF inválido" e confirma