#language: pt
#encoding: utf-8

@erroRegressao @bugInterno
Funcionalidade: Imprimir etiqueta do paciente

  Como um atendente
  Quero imprimir etiqueta do paciente
  Para identificação

  Contexto: Logar no sistema
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"

    @imprimirEtiquetaPaciente
    Cenario: Imprir etiqueta do paciente
      Quando criar novo paciente "ROBO TESTE IMPRESSAO ETIQUETA PACIENTE"
      E imprimir etiqueta do paciente
      Então e apresentado a mensagem "Primeiro é necessário cadastrar o paciente, para depois imprimir etiqueta!" e confirma o popup