#language: pt
#encoding:utf-8

@buscar_paciente2
Funcionalidade: Realizar busca de paciente

  Esquema do Cenário: Realizar busca de paciente dependendo da forma de busca escolhida
    Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
    Quando buscar o paciente pela forma de busca "<formaDeBusca>" e dados de busca "<busca>"
    Então o sistema valida se foi feito a busca do paciente com forma de busca "<formaDeBusca>" e dados de busca "<busca>"

    @pass3 @pass
    Cenários: Busca de paciente pelas opções de buscas disponíveis
        | menu        | subMenu             | formaDeBusca   | busca                      |
        | Atendimento | Lançamento de Guias |       nome     | ÍVARR BEINLAUSI            |
        | Atendimento | Lançamento de Guias |     registro   |        261                 |
        | Atendimento | Lançamento de Guias |    prontuario  |         3                  |

        | Atendimento | Lançamento de Guias |        rg      |    411802264               |
        | Atendimento | Lançamento de Guias |     matricula  | 21242526                   |
        | Atendimento | Lançamento de Guias |       cns      | 757299246550001            |
  #      | Atendimento  | Lançamento de Guias | os             | 119 40699                  |
    #    | Atendimento | Lançamento de Guias |       cpf      |   37872307006              |
  @smartweb
  Esquema do Cenário: Gerar Ordem de Serviço e realizar busca de paciente por ordem de serviço, guia e amostra
    Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
    E buscar um paciente "<matriculaDopaciente>" com o seu número de matricula e avançar
    E criar uma nova ordem de serviço
  #  Quando adicionar uma nova ordem de serviço
    E incluir o item "<item>" com instrução
    E responder o questionário de avaliação
    Quando voltar a pagina e pegar o valor do campo de busca pela forma de busca "<formaDeBusca>"
    E que acesso o menu "<menu>" e sub-menu "<subMenu>"
    E buscar o paciente pela forma de busca "<formaDeBusca>" e dados de busca "<busca>"
    Então o sistema valida se foi feito a busca do paciente com forma de busca "<formaDeBusca>" e dados de busca "<busca>"

    Cenários: Busca de paciente particular com Ordem de Serviço. Guia. Amostra.
      |    menu     |       subMenu       | matriculaDopaciente   |  item  | formaDeBusca | busca     |
      | Atendimento | Lançamento de Guias | 21242526              |  GLICO | ordemServico |               |
      | Atendimento | Lançamento de Guias | 21242526              |  GLICO | guia         | 11141516    |
      | Atendimento | Lançamento de Guias | 21242526              |  GLICO | amostra      | 119000679001  |

  @pass
  Esquema do Cenário: Gerar Orçamento e Pré Atendimento e realizar busca de paciente com Orçamento e Pré Atendimento gerados
    Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
    E buscar um paciente "<paciente>" com o seu número de matricula e avançar
    E incluir o item "<item>" no sub-menu "<subMenu>"
    E pegar o número do orçamento ou pre-atendimento gerado
    E que acesso o menu "<menu>" e sub-menu "<subMenu>"
    Quando buscar o orcamento ou pre-atendimento pelo número gerado
    Então o sistema valida se foi feito a busca do paciente "<paciente>" pelo orçamento ou pre-atendimento

    Cenários: Buscar paciente particular com Orçamento e Pré-Atendimento
      |    menu     |     subMenu     | paciente |  item  |
      | Atendimento |    Orçamento    | 21242526 |  GLICO |
      | Atendimento | Pré-Atendimento | 21242526 |  GLICO |