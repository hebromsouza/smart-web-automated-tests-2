#language: pt
#encoding:utf-8

@pass @realizarCadastro
Funcionalidade: Realizar cadastro de acompanhante do paciente

   Esquema do Cenário: Realizar cadastro de acompanhante
    Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
    E buscar um paciente "<paciente>" com o seu número de matricula e avançar
    E fechar tela de intrução do convênio
    E acessar a tela de acompanhante
    Quando excluir o acompanhante existente
    E registrar novo acompanhante "<acompanhante>" do paciente de parentesco "<parentesco>"
    Então o sistema valida se foi feito o cadastro do acompanhante "<acompanhante>"

    Cenários: Registrar acompanhante
        |    menu     |       subMenu       |     paciente    |  acompanhante   | parentesco |
        | Atendimento | Lançamento de Guias | 744539002496001 | RAGNAR          |     MÃE    |
        | Atendimento | Lançamento de Guias | 744539002496001 | RAGNAR LOTHBROK |     PAI    |
        | Atendimento | Lançamento de Guias | 87445390024960  | RAGNAR  Mae     |     MÃE    |
        | Atendimento | Lançamento de Guias | 87445390024960  | RAGNAR LOTHBROK |     PAI    |
        | Atendimento | Lançamento de Guias | 9123456789      | Mae do QA       |     MÃE    |
        | Atendimento | Lançamento de Guias | 9123456789      | Pai do QA       |     PAI    |
          #| Atendimento | Lançamento de Guias | 21232526        | ISAIAS QA       |     PAI    |

  Esquema do Cenário: Realizar cadastro de acompanhante com registro de paciente
    Dado que acesso o menu "<menu>" e sub-menu "<subMenu>"
    E buscar um paciente "<paciente>" com o seu número de matricula e avançar
    E fechar tela de intrução do convênio
    E acessar a tela de acompanhante
    E acessar a aba de acompanhante paciente por paciente
    Quando excluir o acompanhante existente
    E registrar novo acompanhante buscando por registro "<registro>" e parentesco "<parentesco>"
    Então o sistema valida se foi feito o cadastro do acompanhante pelo registro "<registro>"

    Cenários: Registrar acompanhante paciente x paciente
        |    menu     |       subMenu       |     paciente    | registro | parentesco |
        | Atendimento | Lançamento de Guias | 744539002496001 |    12    |  AMIGO(A)  |