#language: pt
#encoding: utf-8

 @filtrarNF
Funcionalidade: Filtrar notas fiscais

  Como um atendente
  Quero filtar notas fiscais
  Para validar as notas fiscais geradas

  Contexto: Gerar ordens de serviço
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "230" com o seu número de registro
    E criar uma nova ordem de serviço
    E lancar o item "GLICO" para a OS criada
    E excluir resposta do questionário
    E pagar a ordem de serviço gerada
   # E que acesso o menu "Faturamento" e sub-menu "Notas Fiscais"



   # Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
#    E buscar um paciente "230" com o seu número de registro
#    E criar uma nova ordem de serviço
#    E lancar o item "GLICO" para a OS criada
#    E excluir resposta do questionário
#    E pagar a ordem de serviço gerada
#    E que acesso o menu "Faturamento" e sub-menu "Notas Fiscais"


  @filtrarNotasFiscaisNoDia  @quarentena
    Cenario: Filtrar notas fiscais geradas do dia
    E que acesso o menu "Faturamento" e sub-menu "Notas Fiscais"
      Quando filtrar notas fiscais do dia
      Então valido se existe a nota fiscal gerada com status "Aberta"

    @filtrarNotasFiscais @quarentena
    Cenario: Buscar notas fiscais pelo numero da nota
      E que acesso o menu "Faturamento" e sub-menu "Notas Fiscais"
      Quando filtrar notas fiscais pelo numero
      Então valido se existe a nota fiscal gerada com status "Aberta"