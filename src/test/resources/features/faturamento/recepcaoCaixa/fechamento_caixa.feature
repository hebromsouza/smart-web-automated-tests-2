#language: pt
#encoding: utf-8

@erroRegressao @bugInterno @fechamentoCaixaAll
Funcionalidade: Efetuar fechamento de caixa

  Como um atendente
  Quero efetuar fechamento de caixa
  Para prestar contas ao caixa central

  @SMART_17941 @fechamentoCaixaEnviar
      #Pagar OS de testes de forma manual -> corrige alguns problemas
  Cenario: Fechamento de caixa com ordens de serviço a enviar
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "230" com o seu número de registro
    E criar uma nova ordem de serviço
    E lancar o item "GLICO" para a OS criada
    E excluir resposta do questionário
    E pagar a ordem de serviço gerada
    E que acesso o menu "Faturamento" e sub-menu "Recepção (Caixa)"
    E que selecionar o setor Não Especificado
    #E que escolho o setor desejado "FATURAMENTO"
    Quando buscar ordens de serviço com status "A Enviar"
    #Caso irá falhar caso tenha na busca serviços com status "Aberto" - busca por apenas serviços A Enviar não funciona como esperado (https://portalcliente.pixeon.com/browse/SMART-21061)
    E efetuar fechamento de caixa
    E e apresentado a mensagem "Confirmar gerar o fechamento do caixa da recepção das OSs listadas com status a enviar?" e confirma o popup
 #   E pagar as OS aberta caso a mensagem "Existem atendimentos não pagos, logo o fechamento não poderá ser realizado." seja exibida
    Quando receber lote de guias
    E buscar lotes do dia
    Então valido que existe lotes encontrados

    @fechamentoOSEnviadas
  Cenario: Validar fechamento de caixa com OS enviados
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "230" com o seu número de registro
    E criar uma nova ordem de serviço
    E lancar o item "GLICO" para a OS criada
    E excluir resposta do questionário
    E pagar a ordem de serviço gerada
    Quando que acesso o menu "Faturamento" e sub-menu "Recepção (Caixa)"
    E que selecionar o setor Não Especificado
    E buscar ordens de serviço com status "Enviadas"
    Então valida se existe a ordem de servico


  @fechamentoCaixa1
    Cenario: Fechamento de caixa com ordens de serviço aberta
      Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
      E buscar um paciente "230" com o seu número de registro
      E criar uma nova ordem de serviço
      E lancar o item "GLICO" para a OS criada
      E excluir resposta do questionário
      E que acesso o menu "Faturamento" e sub-menu "Recepção (Caixa)"
      E que selecionar o setor Não Especificado
      Quando buscar ordens de serviço com status "Aberta"
      E efetuar fechamento de caixa
      Quando e apresentado a mensagem "Confirmar gerar o fechamento do caixa da recepção das OSs listadas com status a enviar?" e confirma o popup
      Então e apresentado a mensagem "Existem atendimentos não pagos, logo o fechamento não poderá ser realizado." e confirma o popup


    @pagarOsAberta
    Cenario: Pagar Ordem de Serviço com status Aberta do Fachamento de Caixa
      Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
      E buscar um paciente "230" com o seu número de registro
      E criar uma nova ordem de serviço
      E lancar o item "GLICO" para a OS criada
      E excluir resposta do questionário
      Dado que acesso o menu "Faturamento" e sub-menu "Recepção (Caixa)"
      E que selecionar o setor Não Especificado
      Quando buscar ordens de serviço com status "Aberta"
      Então visualizo as OS aberta, clico na OS e realizo o pagamento da OS
