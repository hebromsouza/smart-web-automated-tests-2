#language: pt
#encoding: utf-8

@erroRegressao @protocolarOSFaturamento
Funcionalidade: Protocolar ordens de serviço para faturamento

  Como um atendente
  Quero filtrar ordem de serviço geradas
  Para protocolar para faturamento

  Contexto: Cadastrar Paciente e Gerar ordem de serviço
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E informo paciente "nomePacienteAleatorio"
    E clico no botao comum "Buscar"
    E confirmo em OK o desejo de incluir um novo paciente
    Quando preencho os dados basicos de cadastro do paciente
      | nomeSocial    | nomeMae         | rg            | cpf                  | dtNascimento | sexo       | estadoCivil |
      | QaSocial      | Joana DArc      | RG_Random     | CPF_Gerado_Aleatorio | 14092002     | masculino  | solteiro    |
    E preencho os dados da operadora de saude do paciente
      | operadora         | validade  | matricula   | acomodacao  |
      | BRADESCO SAUDE    | 12102022  | 456852103   | APARTAMENTO |

      #| operadora         | validade  | matricula   | plano         | acomodacao  |
      #| BRADESCO SAUDE    | 12102022  | 456852103   | BRADESCO TOP  | APARTAMENTO |
    E preencho os dados de Contatos do paciente
      | cep        | tipoLogradouro  | logradouro  | numero  | cidade    | bairro  | complemento  | email                 | telefone    | celular   | smsOuWhatsApp    | contato   | outrotelefone  |
      | 41830070   | rua             | Rua: Pará   | 1001    | Salvador  | Pituba  | QD 200 A     | qa.testes@pixeon.com  | 7131461300  | 991715858 | whastsApp        | IsaiasQA  | 7199887766     |
    E clico no botao comum "Avançar"
    E criar uma nova ordem de serviço
   ## E lancar o item "GLICO" para a OS criada LGLICO
    E lancar o item "LGLICO" para a OS criada
    E excluir resposta do questionário

   @osStatusAberto
    Cenario: Validar OS com status Aberto
      E que acesso o menu "Faturamento" e sub-menu "Protocolo de Convênio"
      E que selecionar o setor Não Especificado
      E filtrar pelo periodo de "<data_atual>" a data atual
      Quando buscar ordens de serviço com status "Abertas"
      Entao verifico que apenas ordens de servico com o status "Abertas" são exibidas

    @protocoloFaturamento
    Cenario: Gerar lote de ordens de serviço por dia
      E que acesso o menu "Faturamento" e sub-menu "Protocolo de Convênio"
      E filtrar pelo periodo de "<data_atual>" a data atual
      E que selecionar o setor Não Especificado
      Quando buscar ordens de serviço com status "Abertas"
      E gerar protocolo de convênio
      Quando receber lote de guias
      E buscar lotes do dia
      Então valida se existe o lote gerado

    @gerarLOte
    Cenario: Gerar lote sem ordem de servico
      E que acesso o menu "Faturamento" e sub-menu "Protocolo de Convênio"
      Quando buscar ordens de serviço com status "Abertas"
      E gerar protocolo
      Então e apresentado a mensagem "Nenhuma OS encontrada." e confirma o popup

