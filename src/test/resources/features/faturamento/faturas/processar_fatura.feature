#language: pt
#encoding: utf-8

@pass  @passProcessarFaturas @passOK  @pass11
Funcionalidade: Processar faturas

  Como um faturista
  Quero processar faturas
  Para registrar faturamento das ordens de serviço

  Contexto: Logar no sistema
    #Quando gerar "1" ordens de serviço com item "GLICO" para paciente de registro "12"
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "12" com o seu número de registro
    E criar uma nova ordem de serviço
    E lancar o item "GLICO" para a OS criada
    E excluir resposta do questionário


    Cenario: Processar faturas
      E que acesso o menu "Faturamento" e sub-menu "Faturas"
    #  Quando processar fatura do dia selecionando empresa do convênio "HAP VIDA PLANO DE SAUDE" convênio "SGE HAP VIDA" contratado "CLINICA SEMEG" com atendimentos do dia
    Quando processar fatura do dia selecionando empresa do convênio "HAP VIDA PLANO DE SAUDE" convênio "SGE HAP VIDA" contratado "CLINICA GERAL" com atendimentos do dia
    Então valido processamento de fatura