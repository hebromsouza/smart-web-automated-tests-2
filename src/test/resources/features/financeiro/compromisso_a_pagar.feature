#language: pt
#encoding: utf-8

@pass
Funcionalidade: Criar novo compromisso a pagar

  Como um atendente
  Quero criar novo compromisso a pagar
  Para que o paciente efetue o pagamento do compromisso

  Contexto: Logar no sistema e acessar compromisso a pagar
    Dado que acesso o menu "Financeiro" e sub-menu "Compromisso a pagar"

    Cenario: Criar compromisso a pagar
      Quando gravar compromisso a pagar preenchendo com dados
        |    empresa           | lancamentos |      credor      | documento |       cpfCnpj     |  telefone  |                 observacao                |
        | CLINICA DE CONSULTAS |      1      | SGE FORNECEDOR 1 |    Doc1   | 7.461.330/6000-15 | 7136665454 | Teste Observação Novo Compromisso a pagar |
     #   | CLINICA SEMEG |      1      | SGE FORNECEDOR 1 |    Doc1   | 7.461.330/6000-15 | 7136665454 | Teste Observação Novo Compromisso a pagar |
      E informar valor total "250,5" com classe "Adiantamento" do compromisso
      Então é criado lançamento com valor "250,50" e vencimento para o mesmo dia

