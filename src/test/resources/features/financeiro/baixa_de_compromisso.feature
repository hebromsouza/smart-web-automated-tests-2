#language: pt
#encoding: utf-8

@pass  @realizarBaixaCompromisso
Funcionalidade: Baixa de compromisso

  Contexto: Criar compromisso a pagar
    Dado que acesso o menu "Financeiro" e sub-menu "Compromisso a pagar"
    Quando gravar compromisso a pagar preenchendo com dados
      |    empresa           | lancamentos |      credor      | documento |       cpfCnpj     |  telefone  |                 observacao                |
      | CLINICA DE CONSULTAS |      1      | SGE FORNECEDOR 1 |    Doc1   | 7.461.330/6000-15 | 7136665454 | Teste Observação Novo Compromisso a pagar |
    #  | CLINICA SEMEG |      1      | SGE FORNECEDOR 1 |    Doc1   | 7.461.330/6000-15 | 7136665454 | Teste Observação Novo Compromisso a pagar |
    E informar valor total "250,5" com classe "Adiantamento" do compromisso
    E que acesso o menu "Financeiro" e sub-menu "Baixa de compromisso"

    Cenario: Realizar baixa de compromisso
      Quando buscar baixa de compromisso com os dados
        |    status   |    empresa    |
        | Registrados | CLINICA DE CONSULTAS |
        #| Registrados | CLINICA SEMEG |
      Então é apresentado o compromisso registrado que foi gerado
      Quando pagar compromisso gerado
      Então o compromisso gerado some da busca de compromisso registrado
      Quando buscar baixa de compromisso com os dados
        | status |
        | Pagos  |
      Então é apresentado o compromisso pago que foi gerado

