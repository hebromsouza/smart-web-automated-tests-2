#language: pt
#encoding: utf-8

 @cancelarPagamentos
Funcionalidade: Cancelar pagamentos efetuados

  Contexto: Logar no sistema
    Dado que acesso o menu "Financeiro" e sub-menu "Compromisso a pagar"
    Quando gravar compromisso a pagar preenchendo com dados
      |    empresa           | lancamentos |      credor      | documento |       cpfCnpj     |  telefone  |                 observacao                |
      | CLINICA DE CONSULTAS |      1      | SGE FORNECEDOR 1 |    Doc1   | 7.461.330/6000-15 | 7136665454 | Teste Observação Novo Compromisso a pagar |
   #   | CLINICA SEMEG |      1      | SGE FORNECEDOR 1 |    Doc1   | 7.461.330/6000-15 | 7136665454 | Teste Observação Novo Compromisso a pagar |
    E informar valor total "250,5" com classe "Adiantamento" do compromisso
    E que acesso o menu "Financeiro" e sub-menu "Baixa de compromisso"
    Quando buscar baixa de compromisso com os dados
      |    status   |    empresa    |
      | Registrados | CLINICA DE CONSULTAS |
      #| Registrados | CLINICA SEMEG |
    E pagar compromisso gerado
    E que acesso o menu "Financeiro" e sub-menu "Pagamentos Efetuados"

    @cancelarPagEfetuados
    Cenario: Cancelar pagamentos efetuados
      Quando buscar pagamentos efetuados com empresa "CLINICA DE CONSULTAS"
     # Quando buscar pagamentos efetuados com empresa "CLINICA SEMEG"
      E cancelar compromisso em pagamentos efetuados
      Então o compromisso gerado some da busca de pagamentos efetuados