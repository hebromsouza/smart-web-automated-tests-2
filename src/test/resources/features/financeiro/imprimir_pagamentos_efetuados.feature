#language: pt
#encoding: utf-8

@pass  @imprimirPagamentos
Funcionalidade: Imprimir pagamentos efetuados de um compromisso a pagar

  Como um atendente
  Quero acessar tela de pagamentos efetuados
  Para imprimir os pagamentos

  Contexto: Pagar novo compromisso
    Dado que acesso o menu "Financeiro" e sub-menu "Compromisso a pagar"
    Quando gravar compromisso a pagar preenchendo com dados
      |    empresa          | lancamentos |      credor      | documento |       cpfCnpj     |  telefone  |                 observacao                |
      | CLINICA DE CONSULTAS|      1      | SGE FORNECEDOR 1 |    Doc1   | 7.461.330/6000-15 | 7136665454 | Teste Observação Novo Compromisso a pagar |
     #| CLINICA SEMEG |      1      | SGE FORNECEDOR 1 |    Doc1   | 7.461.330/6000-15 | 7136665454 | Teste Observação Novo Compromisso a pagar |
    E informar valor total "250,5" com classe "Adiantamento" do compromisso
    E que acesso o menu "Financeiro" e sub-menu "Baixa de compromisso"
    Quando buscar baixa de compromisso com os dados
      |    status   |    empresa           |
      | Registrados | CLINICA DE CONSULTAS |
    #  | Registrados | CLINICA SEMEG |
    Então é apresentado o compromisso registrado que foi gerado
    Quando pagar compromisso gerado
    E que acesso o menu "Financeiro" e sub-menu "Pagamentos Efetuados"

    Cenario: Imprimir pagamento efetuado
      Quando buscar pagamentos efetuados com empresa "CLINICA DE CONSULTAS"
     # Quando buscar pagamentos efetuados com empresa "CLINICA SEMEG"
      Então o sistema apresenta pagamentos efetuados do compromisso gerado
      Quando imprimir recibo do compromisso gerado
      Então o sistema apresenta o recibo de pagamentos efetuados do compromisso gerado