#language: pt
#encoding: utf-8

 @bugExterno1
Funcionalidade: Atendimento da solicitação de materiais

  Como um atendente
  Quero solicitar transferência de materiais para um subalmoxarifado
  Para fazer atendimento da solicitação

  @test @passOK @pass
  Cenario: Solicitação de tranferência de materiais
    Dado que acesso o menu "Solicitação de..." e sub-menu "...Transferência De Materiais"
    Quando selecionar setor "CONSULTORIOS" da solicitação de material
    E selecionar subalmoxarifado "SGE SUBALMOXARIFADO GERAL" na solicitação de material
    E selecionar subalmoxarifado "SGE SUBALMOXARIFADO VACINA" para transferencia
    E informar observação "Transferencia de Material." da solicitação de transferência
    Quando gravar solicitação de transferência de material pegando série e número
    E buscar item "SGE" na tela de solicitação de transferência de material
    E adicionar item selecionando quantidade "2"

    @SMART_17716 @quarentena
    Cenario: Atendimento parcial da solicitação de transferência
      E que acesso o menu "Estoque" e sub-menu "Atendimento de Solicitação"
      E selecionar subalmoxariado "SGE SUBALMOXARIFADO GERAL" na tela de atendimento de solicitações
      E selecionar tipo de solicitação "Solicitação de Transferência" de materiais na tela de atendimento de soliciações
      Quando filtrar atendimento de solicitações
      E abrir atendimento de solicitação pela série e número gerados na tela de solicitação de transferência
      E incluir quantidade "1" para dar baixa
      Quando imprimir atendimento da solicitação
      Então o sistema valida impressão do atendimento de solicitações
      Quando gravar atendimento de solicitação
      Então o sistema valida atendimento de solicitação
      Quando fechar painel
      Então o sistema valida ausencia da solicitação na tela de atendimento de solicitações pendentes
      E o sistema valida presença da solicitação na tela de atendimento de solicitações sem pendência