#language: pt
#encoding: utf-8

@pass @passOK
Funcionalidade: Cancelamento de solicitação de transferência de materiais

  Como um atendente
  Quero fazer cancelamento de solicitações de transferência de materiais
  Para que seja cancelada a disponibilidade de materiais em subalmoxarifado

    Contexto: Cancelamento de solicitação de transferência de materiais
      Dado que acesso o menu "Solicitação de..." e sub-menu "...Transferência De Materiais"
      Quando selecionar setor "CONSULTORIOS" da solicitação de material
      E selecionar subalmoxarifado "SGE SUBALMOXARIFADO GERAL" na solicitação de material
      E selecionar subalmoxarifado "SGE SUBALMOXARIFADO VACINA" para transferencia
      E informar observação "Transferencia de Material." da solicitação de transferência
      Quando gravar solicitação de transferência de material pegando série e número
      E buscar item "SGE" na tela de solicitação de transferência de material
      E adicionar item selecionando quantidade "1"


    Cenário: Validar cancelamento de item da solicitação de transferência de materiais
      Quando selecionar o item na tela de solicitação de transferência de materiais
      E cancelar item na tela de solicitação de transferência de materiais
      E que acesso o menu "Solicitação de..." e sub-menu "...Transferência De Materiais"
      Entao o sistema valida ausencia do numero da solicitação em solicitações recentes
      E o sistema valida cancelamento do item na tela de solicitação de transferência de materiais
      E o sistema valida status "Aberto" da solicitação de transferência de materiais


    Cenário: Validar cancelamento de solicitação de transferência de materiais
      Quando cancelar solicitação de transferência de materiais
      E que acesso o menu "Solicitação de..." e sub-menu "...Transferência De Materiais"
      Então o sistema valida ausencia do numero da solicitação em solicitações recentes
      E o sistema valida cancelamento do item na tela de solicitação de transferência de materiais
      E o sistema valida status "Cancelado" da solicitação de transferência de materiais