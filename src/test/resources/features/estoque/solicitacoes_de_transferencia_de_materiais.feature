#language: pt
#encoding: utf-8

@pass @passOK
Funcionalidade: Solicitações de tranferência de materiais

  Como um atendente
  Quero fazer solicitações de transferência de materiais
  Para que seja disponibilizado materiais em subalmoxarifado

    Cenário: Validar solicitação de transferência de materiais na tela de busca de solicitações
      E que acesso o menu "Solicitação de..." e sub-menu "...Transferência De Materiais"
      Quando selecionar setor "CONSULTORIOS" da solicitação de material
      E selecionar subalmoxarifado "SGE SUBALMOXARIFADO GERAL" na solicitação de material
      E selecionar subalmoxarifado "SGE SUBALMOXARIFADO VACINA" para transferencia
      E informar observação "Transferencia de Material." da solicitação de transferência
      Quando gravar solicitação de transferência de material pegando série e número
      E buscar item "SGE" na tela de solicitação de transferência de material
      E adicionar item selecionando quantidade "1"
      Quando buscar solicitações de transferência de materiais
      Entao o sistema valida solicitação de transferência de materiais

    Cenário: Validar solicitação de transferência de materiais
      Quando que acesso o menu "Solicitação de..." e sub-menu "...Transferência De Materiais"
      Então o sistema valida na tela de solicitação de transferência de materiais o número da solicitação