#language: pt
#encoding: utf-8

@smartweb @bugExterno  @localizacaoAmostra
Funcionalidade: Localizar amostra

  Como um auxiliar-técnico
  Quero fazer localização de amostra
  Para informar o histórico da localização da amostra

    #Exame deve conter amostra como DIURESE/LINFÓCITOS no CADMED(Tela Exames) para mostrar o painel de volume urinário

    @SMART-19112  @volumeUrinario
    Cenario: Informar volume urinário na localização da amostra
      Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
      E buscar um paciente "13" com o seu número de registro
      E criar uma nova ordem de serviço
      E lancar o item "CLEUR" para a OS criada
      E excluir resposta do questionário
      E pegar numero da amostra do item
      E que acesso o menu "Laboratório" e sub-menu "Localização de Amostra"
      Quando selecionar a opção de informar volume urinário/linfócito
      E localizar amostra do item lançado
      E informar tempo "12" e volume "10,00" do volume urinário na tela de outras informações
      Então o sistema valida se foi adicionado volume urinário com tempo "12" e volume "10,00"

    @SMART-19353 @bugInterno
    Cenario: Informar linfócitos na localização de amostra
      E criar uma nova ordem de serviço incluindo o item "HIVQT"
      E excluir resposta do questionário
      E pegar numero da amostra do item
      E que acesso o menu "Laboratório" e sub-menu "Localização de Amostra"
      Quando selecionar a opção de informar volume urinário/linfócito
      E localizar amostra do item lançado
      E informar linfócitos "30" na tela de outras informações
      Então o sistema valida se foi adicionado linfócitos "30"

    @SMART-19365 @bugInterno
    Cenario: Fechar painel antes de gravar valor de volume urinário
      E criar uma nova ordem de serviço incluindo o item "CLEUR"
      E excluir resposta do questionário
      E pegar numero da amostra do item
      E que acesso o menu "Laboratório" e sub-menu "Localização de Amostra"
      Quando selecionar a opção de informar volume urinário/linfócito
      E localizar amostra do item lançado
      E fechar painel
      Então o sistema valida localização de amostra do item lançado na ordem de serviço