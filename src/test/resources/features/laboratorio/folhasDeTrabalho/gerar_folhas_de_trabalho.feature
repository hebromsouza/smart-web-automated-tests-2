#language: pt
#encoding:utf-8

@pass @top10  @folhaSalario
Funcionalidade: Gerar folhas de trabalho


  Esquema do Cenario: Gerar folhas de trabalho
    Dado que acesso o menu "Laboratório" e sub-menu "Folha de Trabalho"
    Quando selecionar a opção gerar folhas
    E que selecionar o setor "<setor>"
    E que selecionar a bancada "<bancada>"
    Quando buscar as folhas
    E selecionar as folhas
    Quando gerar folhas
    E concluir geração de folhas
    E buscar folhas emitidas pela bancada "<bancada>"
    Então o sistema valida geração de folhas para bancada "<bancada>"

    Exemplos:
      |    setor   |   bancada  |
      | BIOQUIMICA | BIOQUÍMICA |

  @cancelarFolha
  Esquema do Cenario: Cancelar folhas de trabalho
    Dado que acesso o menu "Laboratório" e sub-menu "Folha de Trabalho"
    Quando buscar folhas emitidas pela bancada "<bancada>"
    E pegar o número da primeira folha emitida
    Quando checar a folha emitida
    E cancelar as folha selecionada
    Entao o sistema valida cancelamento da folha selecionada

    Exemplos:
      |   bancada  |
      | BIOQUÍMICA |