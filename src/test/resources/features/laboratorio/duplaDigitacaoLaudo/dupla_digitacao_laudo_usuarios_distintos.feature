#language: pt
#encoding:utf-8

@quarentena @top10
Funcionalidade: Realizar dupla digitação de laudo utilizando usuários distintos

  Como um médico
  Quero dar resultado a um exame que o sistema obrigue a dupla digitacao com outro médico
  Para dar resultado ao mesmo exame e liberar o resultado para o paciente

  Contexto: Logar no sistema e gerar ordem de serviço
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "421" com o seu número de registro
    E que faço a inclusão de item "HIV" em uma nova ordem de serviço com setor solicitenate "IMUNOLOGIA"
    E responder o questionário de avaliação

    Cenario: Dupla digitação com usuários distintos
      E que acesso o menu "Laboratório" e sub-menu "Resultado"
      Quando buscar um paciente "421" com o seu número de registro
      E selecionar a ordem de serviço
      E inserir o resultado do exame "Reagente" e "1,6"
      Quando gravar o resultado do exame
      Entao o sistema valida se o status do exame foi alterado para "Executado "
      Quando liberar o exame
      Então é apresentado popup com mensagem "Digitação de conferência não realizada." e confirma
      Quando confirmar resultados
      E inserir o resultado do exame "Reagente" e "1,6"
      E gravar o resultado do exame
      E liberar o exame
      Então é apresentado popup com mensagem "A digitação de conferência deve ser realizada por outro usuário." e confirma
      Quando deslogar
      E que estou logado no sistema smart web sem selecionar recapcao
        |   login   | senha |
        |  adriano  |   1   |
      E que acesso o menu "Laboratório" e sub-menu "Resultado"
      E buscar um paciente "421" com o seu número de registro
      Quando selecionar a ordem de serviço
      E confirmar resultados
      E inserir o resultado do exame "Reagente" e "1,6"
      E gravar o resultado do exame
      Entao o sistema valida se o status do exame foi alterado para "Liberado "