#language: pt
#encoding:utf-8

@quarentena
Funcionalidade: Realizar dupla digitação de laudo utilizando qualquer usuário

  Como um médico
  Quero dar resultado a um exame que o sistema obrigue a dupla digitacao com qualquer médico
  Para dar resultado ao mesmo exame e liberar o resultado para o paciente

  Contexto: Logar no sistema e gerar ordem de serviço
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
      E buscar um paciente "421" com o seu número de registro
    #  E que faço a inclusão de item "HIVGE" em uma nova ordem de serviço com setor solicitenate "IMUNOLOGIA"
      E que faço a inclusão de item "HIVGE"
      E excluir resposta do questionário

  Cenario: Dupla digitação com qualquer usuário
    E que acesso o menu "Laboratório" e sub-menu "Resultado"
    Quando buscar um paciente "421" com o seu número de registro
    E selecionar a ordem de serviço
    E inserir o resultado do exame "Detectável" e ""
    Quando gravar o resultado do exame
    Entao o sistema valida se o status do exame foi alterado para "Executado "
    Quando liberar o exame
    Então é apresentado popup com mensagem "Digitação de conferência não realizada." e confirma
    Quando confirmar resultados
    E inserir o resultado do exame "Detectável" e ""
    E gravar o resultado do exame
    Quando liberar o exame
    Entao o sistema valida se o status do exame foi alterado para "Liberado "
    E clico para visualizar o laudo

    #Corrigir erro no ultimo passo (Entoa) mudar frame, pois aparece um popup de exame HIV Genotipagem