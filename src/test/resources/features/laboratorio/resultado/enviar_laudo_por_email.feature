#language: pt
#encoding: utf-8

@smartweb  @enviarLaudo
Funcionalidade: Enviar laudo por email

  Como um bioquímico
  Quero enviar laudo por email
  Para que seja feito a análise das informações do laudo

  Contexto: Logar no sistema e criar ordem de serviço
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "12" com o seu número de registro
    Quando criar uma nova ordem de serviço incluindo o item "GLICO"
   # E criar uma nova ordem de serviço
  #  E incluir o item "HDL" com instrução
    E excluir resposta do questionário

    Esquema do Cenário: Cenario: Enviar laudo por email
      E que acesso o menu "Laboratório" e sub-menu "Resultado"
      E buscar um paciente "12" com o seu número de registro
      Quando selecionar a ordem de serviço gerada
      Então gravo e valido se o sistema gravou a ordem de serviço com o valor "<resultado>" e status "<status>"
      Quando enviar laudo por email
      E enviar laudo para outro email "qa.testes@pixeon.com"
      E realizar login no email "qa.testes@pixeon.com" e senha "Voz68665"
      Então valida se foi enviado o email para o paciente "Teste Adriano"

      Exemplos:
        | resultado |     status   |
        |   70,0    |  'Liberado ' |