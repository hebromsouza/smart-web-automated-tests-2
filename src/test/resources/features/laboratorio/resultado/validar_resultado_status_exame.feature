#language: pt
#encoding:utf-8

@erroRegressao @resultadoExameLAB
Funcionalidade: Gravar resultado de exame laboratorial

  Esquema do Cenário: Realizar a gravação de resultado de exame e validar resultado "<resultado>"
    Dado que acesso o menu "<menu_1>" e sub-menu "<subMenu_1>"
    E buscar um paciente "<paciente>" com o seu número de matricula e avançar
    E criar uma nova ordem de serviço
    Quando incluir o item "<item>" com instrução
    E que acesso o menu "<menu_2>" e sub-menu "<subMenu_2>"
    E buscar um paciente "<paciente>" com o seu número de matricula
    Quando selecionar a ordem de serviço gerada
    E gravar o resultado "<resultado>"
    Então valido se o sistema gravou o resultado "<resultado>"

    Cenários: Inserindo valores minimos, medianos e máximos de resultado respectivamente.
      |    menu_1     |       subMenu_1   |     paciente    | item  |    menu_2   | subMenu_2 | resultado |
      | Atendimento | Lançamento de Guias | 744539002496001 | GLICO | Laboratório | Resultado |   70,0    |
      | Atendimento | Lançamento de Guias | 744539002496001 | GLICO | Laboratório | Resultado |   90,0    |
      | Atendimento | Lançamento de Guias | 744539002496001 | GLICO | Laboratório | Resultado |   110,0   |

    @resultadoExameLAB1
  Esquema do Cenário: Validar status <status> de resultado de exame
    Dado que acesso o menu "<menu_1>" e sub-menu "<subMenu_1>"
    E buscar um paciente "<paciente>" com o seu número de matricula e avançar
    E criar uma nova ordem de serviço
    Quando incluir o item "<item>" com instrução
    E que acesso o menu "<menu_2>" e sub-menu "<subMenu_2>"
    E buscar um paciente "<paciente>" com o seu número de matricula
    Quando selecionar a ordem de serviço gerada
    Então gravo e valido se o sistema gravou a ordem de serviço com o valor "<resultado>" e status "<status>"

    Cenários:  Avaliando gravação do status executado, exec av e liberado
      |    menu_1   |       subMenu_1     |     paciente    | item  |    menu_2   | subMenu_2 | resultado |     status   |
      | Atendimento | Lançamento de Guias | 744539002496001 | GLICO | Laboratório | Resultado |   70,0    | 'Executado ' |
      | Atendimento | Lançamento de Guias | 744539002496001 | GLICO | Laboratório | Resultado |   90,0    | 'Exec (Av) ' |
      | Atendimento | Lançamento de Guias | 744539002496001 | GLICO | Laboratório | Resultado |   110,0   |  'Liberado ' |
      | Atendimento | Lançamento de Guias | 744539002496001 | GLICO | Laboratório | Resultado |   110,0   |  'Impresso ' |