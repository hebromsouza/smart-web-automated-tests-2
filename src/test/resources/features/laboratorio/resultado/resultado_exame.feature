#language: pt
#encoding:utf-8

@erroRegressao @bugExterno  @resultadoExame
Funcionalidade: Inserir resultado de exame

  Como um bioquímico
  Quero inserir resultados do exame
  Para disponibilizar entrega de resultado

    @SMART-18798  @fatalErrorResultado
    Cenario: Verificar existência de Fatal Error na tela de resultado de exames
      E que acesso o menu "Laboratório" e sub-menu "Resultado"
      Quando buscar um paciente "13" com o seu número de registro
      E selecionar a ordem de serviço gerada
      Quando abrir painel de resultados do exame
      Então o sistema verifica os valores de referencia

    @leucograma
    Cenario: Informar mensagem de Leucograma obrigatorio
      E que acesso o menu "Laboratório" e sub-menu "Resultado"
      Quando buscar um paciente "13" com o seu número de registro
      E selecionar a ordem de serviço gerada
    #  E abrir painel de resultados do exame
    #  Quando preencher resultado para eritrograma
     #   | hemoglobina | hematocrito | hemacias |  rdwcv |
     #   |     14,0    |     41      |   4,65   |   13   |
     # E gravar resultado de homograma
     # Então e apresentado a mensagem "Contagem diferencial diferente do Leucograma diferente de 100%" e confirma o popup

#    Cenario: Inserir resultado de exame pela lupa de resultado de exames
#      E que acesso o menu "Laboratório" e sub-menu "Resultado"
#      Quando buscar um paciente "13" com o seu número de registro
#      E selecionar a ordem de serviço gerada
#      E abrir painel de resultados do exame
#      Quando preencher resultado para eritrograma
#        | hemoglobina | hematocrito | hemacias |  rdwcv |
#        |     14,0    |     41      |   4,65   |   13   |
#      E acessar a aba "Leucograma"
#      E preencher resultado para leucograma
#        | metamielocitos | bastoes | segmentados | eosinofilos | linfocitos | linfAtipicos | monocitos | basofilos | blastos | promielocitos | mielocitos |
#        |       25       |   0,5   |      7      |     0,5     |      4     |     25       |     1     |    0,2    |   15    |      15       |    6,8     |
#      Quando gravar resultado de homograma
#      Então o sistema valida se o status do exame foi alterado para "Executado "
#      E validar resultado "Normal" não editável