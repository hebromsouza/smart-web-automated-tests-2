#language: pt
#encoding: utf-8

@quarentena @EmDesenvolvimento @anexarArquivo
Funcionalidade: Anexar arquivos no atendimento

  Como um atendente
  Quero anexar arquivos na tela de atendimento
  Para adicionar evidências do atendimento

  Contexto: Logar no sistema
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"

    Esquema do Cenário: Anexar e visualizar arquivos no cadastro do paciente
      E buscar o paciente pela forma de busca "nome" e dados de busca "LUANA OLIVEIRA DE SOUZA"
      E abrir a tela de arquivos
      Quando adicionar arquivos na tela de anexos paciente
      E abrir arquivo informando caminho "<caminho>" na janela do windows
      Então o sistema valida anexo do arquivo na tela de arquivos anexados

      Exemplos:
      |                              caminho                              |
      | \\10.71.5.104\desenv\Arquivos_Desenv\Allan\LAUDO DE HEMOGRAMA.pdf |