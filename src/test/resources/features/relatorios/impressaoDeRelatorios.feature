#language: pt
#encoding: utf-8

@pass @Relatorios
Funcionalidade: Impressão de relatórios

  Como atendente
  Quero fazer impressão de relatórios
  Para verificação de informações da empresa no sistema

  @passEmissorRel
  Cenario: Imprimir movimentação de caixa
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "Movimentação de Caixa"
    E inserir data período do dia na tela "Movimentação de Caixa"
    E selecionar recepção "CONSULTORIOS"
    E selecionar usuário "MEDICWARE SISTEMAS (MEDICWARE)"
    Quando buscar relatório
    Então o sistema valida impressao de relatório

    @recepcionista
  Cenario: Imprimir atendimento por recepcionista
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "ATENDIMENTOS POR RECEPCIONISTAS"
    E inserir data período do dia na tela "ATENDIMENTOS POR RECEPCIONISTAS"
    E inserir setor solicitante "CONS"
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Imprimir compromissos em aberto
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "COMPROMISOS EM ABERTO"
    E emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão de atendimento por posto de coleta - ordenado por OS
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "RELATÓRIO DE ATENDIMENTO POR POSTO DE COLETA - ORDENADO POR OS"
    E inserir data período do dia na tela "RELATÓRIO DE ATENDIMENTO POR POSTO DE COLETA - ORDENADO POR OS"
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão de exames por data de entrada
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "EXAMES POR DATA DE ENTRADA"
    E inserir data período do dia na tela "EXAMES POR DATA DE ENTRADA"
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão tabelas de preço
    Dado que clico no icone de emissor de relatórios
    E acessar a tela do relatório "VALIDAÇÃO TABELAS DE PREÇOS"
    E inserir setor solicitante "0001,1503,1602"
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão internação tempo de permanência
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "Internação - Tempo Permanência"
    E inserir data período do dia na tela "Internação - Tempo Permanência"
    E inserir setor solicitante "CON"
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão internação tempo de permanência sem especificar unidade
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "Internação - Tempo Permanência"
    E inserir data período do dia na tela "Internação - Tempo Permanência"
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão internação tempo de permanência sem especificar tempo de permanência
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "Internação - Tempo Permanência"
    E inserir data período do dia na tela "Internação - Tempo Permanência"
    E limpar campo tempo de permanência
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão internação tempo de permanência sem especificar data inicial
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "Internação - Tempo Permanência"
    E inserir data período do dia na tela "Internação - Tempo Permanência"
    E limpar campo data inicial
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão internação tempo de permanência sem especificar data final
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "Internação - Tempo Permanência"
    E inserir data período do dia na tela "Internação - Tempo Permanência"
    E limpar campo data final
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)"
    E inserir setor solicitante "CON" na tela de área técnica - exames realizados na coleta
    E inserir data período do dia na tela "AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)"
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE) sem especificar setor solicitante
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)"
    E inserir data período do dia na tela "AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)"
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE) sem especificar status de execução
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)"
    E limpar campo status de execução
    E inserir data período do dia na tela "AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)"
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE) sem especificar data inicial
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)"
    E inserir data período do dia na tela "AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)"
    E limpar campo data inicial tela área técnica - exames realizados na coleta
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE) sem especificar data final
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)"
    E inserir data período do dia na tela "AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)"
    E limpar campo data final tela área técnica - exames realizados na coleta
    Quando emitir relatorio
    Então o sistema valida impressao de relatório

  Cenario: Impressão EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE) sem especificar item
    Dado que clico no icone de emissor de relatórios
    Quando acessar a tela do relatório "AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)"
    E limpar código do item da tela  área técnica - exames realizados na coleta
    E inserir data período do dia na tela "AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)"
    Quando emitir relatorio
    Então o sistema valida impressao de relatório