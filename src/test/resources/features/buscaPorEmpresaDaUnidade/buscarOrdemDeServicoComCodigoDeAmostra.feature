#language: pt
#encoding:utf-8

@smartweb @top10 @quarentena
Funcionalidade: Buscar ordem de servico pela empresa da unidade logada com código de amostra único

  Como um atendente
  Quero realizar busca de ordem de servico com código de amostra único
  Para que seja identificado o automaticamente pela empresa logada

  Cenario: Buscar ordem de servico
    E que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "261" com o seu número de registro
    E criar uma nova ordem de serviço
    Quando incluir o item "GLICO" com instrução
    E concluir lançamento do item aceitando erro de autorização
    E responder o questionário de avaliação
    E que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar ordem de serviço pelo código de amostra "119002358001"
    Então o sistema valida a busca de ordem de serviço
    Quando deslogar
    E que preencho login e senha
      | login     |  senha |
      | medicware |  mwa   |
    Quando eu realizar o login
    E a recepção for selecionada "LAB DE PATOLOGIA CLÍNICA"
    E o sistema realiza o login com sucesso
    E que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar ordem de serviço pelo código de amostra "119002358001"
    Então o sistema valida ausência de ordem de serviço