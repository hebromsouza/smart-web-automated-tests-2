#language: pt
#encoding:utf-8

@erroRegressao @top10 @testAleatorio @Realizardiligenciamento
Funcionalidade: Realizar diligenciamento de exames

  Como um atendente
  Quero incluir diligenciamento de exames
  Para rastrear

  @criar_e_excluir_diligenciamento
  Esquema do Cenário: Criar novo diligenciamento
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "13" com o seu número de registro
    E criar uma nova ordem de serviço
    E lancar o item "<item>" para a OS criada
    E responder o questionário de avaliação
    E abrir tela de detalhes do item "<itemDesc>"
    E abrir o diligenciamento
    Quando acessar a aba "Diligenciamento"
    E criar novo diligenciamento
    E selecionar evento "<evento>" do diligenciamento
    E incluir observação do diligenciamento
    Quando gravar novo diligenciamento
    Entao o sistema valida inclusao do diligenciamento com evento "<evento>"

    Exemplos:
      |      evento     | item  |       itemDesc     |
      | DILIGENCIAMENTO | GLICO | GLICOSE (GLICEMIA) |

    #Este caso depende do caso acima para funcionar. No caso acima o diligenciamento é criado com o usuário medicware e no caso abaixo, tenta-se excluir com outro usuário.
  @criar_e_excluir_diligenciamento
  Esquema do Cenário: Validar mensagem ao excluir diligenciamento logando com outro usuário na aplicacao
    Dado que faco logout na aplicacao ignorando assim o login realizado pelo hooks
    Dado abrir tela de login
    Quando efetuo login no smartweb com o usuario "isaias" e a senha "123"
    Quando eu realizar o login
    Então o sistema realiza o login com sucesso

    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "13" com o seu número de registro
    E abrir tela de detalhes do item "<itemDesc>"
    E abrir o diligenciamento
    Quando acessar a aba "Diligenciamento"
    E exluir o diligenciamento
    Entao o sistema valida mensagem de erro de exclusao "<mensagemExclusao>"

    Exemplos:
      |                         mensagemExclusao                        |          itemDesc  |
      | Somente o usuário que registro o diligenciamento pode excluí-lo | GLICOSE (GLICEMIA) |

    @toEnd
  Cenário: Excluir diligenciamento criado com mesmo usuário
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E buscar um paciente "13" com o seu número de registro
    E criar uma nova ordem de serviço
    E lancar o item "GLICO" para a OS criada
    E responder o questionário de avaliação
    E abrir tela de detalhes do item "GLICOSE (GLICEMIA)"
    E abrir o diligenciamento
    Quando acessar a aba "Diligenciamento"
    E exluir o diligenciamento
    Entao o deligenciamento é excluido com sucesso
