#language: pt
#encoding: utf-8

@smartweb @bugInterno
Funcionalidade: Rastreabilidade de exames

  Como um atendente
  Quero acessar a tela de diligenciamento
  Para verificar rastreabilidade

  Contexto: Logar no sistema e criar ordem de serviço
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    Quando buscar paciente pelo nome "PACIENTE DILIGENCIAMENTO"
    E criar uma nova ordem de serviço incluindo o item "GLICO"
    E excluir resposta do questionário
    E pegar numero da amostra do item

    @SMART-16671
    Cenario: Verificar rastreabilidade de exame
      E que acesso o menu "Laboratório" e sub-menu "Localização de Amostra"
      Quando localizar amostra do item lançado
      E que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
      E buscar paciente pelo nome "PACIENTE DILIGENCIAMENTO"
      E avançar cadastro do paciente
      Quando abrir tela de detalhes do item
      E abrir o diligenciamento
      Quando acessar a aba "Rastreabilidade"
      Então o sistema valida rastreabilidade do exame com número da amostra do item lançado