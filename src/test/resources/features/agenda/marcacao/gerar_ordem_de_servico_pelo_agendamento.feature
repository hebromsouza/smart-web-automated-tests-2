#language: pt
#encoding:utf-8

@erroRegressao @agenda @top10 @Marcacao
Funcionalidade: Realizar marcação de paciente

  #Habilitar a parametrização  REG_CH_LOC=S na seção ATENDE do smart.ini
  Esquema do Cenário: Validar recepção logada ao registrar chegada
    Quando que acesso o menu "Agenda" e sub-menu "Marcação"
    E filtrar agenda pela especialidade "<especialidade>" medico "<medico>" e unidade "<unidade>"
    Quando selecionar o dia e horário da marcação
    E buscar o paciente pela forma de busca "<formaBusca>" e dados de busca "<paciente>" e avançar
    E selecionar procedimento "<procedimento>"
    E seleciona o paciente "<paciente>" na agenda do médico selecionando "Registrar chegada"
   # E avançar cadastro do paciente
   # Quando seleciona o paciente "<paciente>" na agenda do médico selecionando "Ordem de serviço"
   # Entao o sistema valida herança de setor solicitante na ordem de serviço de acordo com setor logado "<setor>"

    Exemplos:
      |especialidade  |          medico         |    unidade    | formaBusca |     paciente    | procedimento | setor |
      |CLINICA MEDICA | ADRIANO EXEC, CRM 18257 | CLINICA SEMEG |    nome    | ÍVARR BEINLAUSI | MARCAÇÃO WEB |  CON  |