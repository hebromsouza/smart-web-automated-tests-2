#language: pt
#encoding: utf-8

@erroRegressao @agenda @passCancelarAgendamento
Funcionalidade: Cancelar agendamento do paciente

  # Funciona apenas se rodar 1x no dia... Rodando +1 vez, dá erro... Para reverter o erro e funcionar, cancele manualmente e deixe sem nenhuma marcacao
  @cancelarAgendamento
  Cenario: Cancelar marcação da consulta do paciente agendado anteriormente
    Dado que acesso o menu "Agenda" e sub-menu "Marcação"
    Quando filtrar agenda pela especialidade "CLINICA MEDICA" medico "ADRIANO EXEC, CRM 18257" e unidade "CLINICA SEMEG"
    E selecionar o dia e horário da marcação
    E buscar paciente pelo nome "ÍVARR BEINLAUSI"
    E selecionar procedimento "MARCAÇÃO WEB"
    Então o paciente "ÍVARR BEINLAUSI" é agendado
    Então o médico cancela marcação para o paciente "ÍVARR BEINLAUSI"
    Entao o medico verifica se o cancelamento foi realizado com sucesso
