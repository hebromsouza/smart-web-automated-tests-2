#language: pt
#encoding: utf-8

@brokenTeste @agenda  @bugInterno @agendarPaciente
Funcionalidade: Agendar paciente

  Como um atendente
  Quero agendar um paciente
  Para entrar na fila de espera de um médico

    @agendarPaciente
    Cenario: Marcar consulta do paciente na agenda médica
      Dado que acesso o menu "Agenda" e sub-menu "Marcação"
      Quando filtrar agenda pela especialidade "CLINICA MEDICA" medico "MÉDICO AUTOMACAO, CRM 18260" e unidade "CLINICA SEMEG"
      E selecionar o dia e horário da marcação
      E buscar um paciente "261" com o seu número de registro
      E selecionar procedimento "MARCAÇÃO WEB"
      Quando confirmar marcação web para o paciente
      Então verifico que o paciente "ÍVARR BEINLAUSI" foi agendado com sucesso
