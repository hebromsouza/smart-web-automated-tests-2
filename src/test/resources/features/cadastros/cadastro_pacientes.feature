#language: pt
#encoding: utf-8

@erroRegressao
Funcionalidade: Cadastro Básico de Pacientes

  #Contexto:
  #  Dado que o cadastro do paciente é realizado com sucesso


  @CadPacienteRobo
  Cenário: Realizar o Cadastro do Paciente
    Dado que acesso o menu "Atendimento" e sub-menu "Lançamento de Guias"
    E informo paciente "nomePacienteAleatorio"
    E clico no botao comum "Buscar"
    E confirmo em OK o desejo de incluir um novo paciente
    Quando preencho os dados basicos de cadastro do paciente
      | nomeSocial    | nomeMae         | rg            | cpf                  | dtNascimento | sexo       | estadoCivil |
      | QaSocial      | Joana DArc      | RG_Random     | CPF_Gerado_Aleatorio | 14092002     | masculino  | solteiro    |
    E preencho os dados da operadora de saude do paciente
      | operadora         | validade  | matricula   | plano         | acomodacao  |
      | BRADESCO SAUDE    | 12102022  | 456852103   | BRADESCO TOP  | APARTAMENTO |
    E preencho os dados de Contatos do paciente
      | cep        | tipoLogradouro  | logradouro  | numero  | cidade    | bairro  | complemento  | email                 | telefone    | celular   | smsOuWhatsApp    | contato   | outrotelefone  |
      | 41830070   | rua             | Rua: Pará   | 1001    | Salvador  | Pituba  | QD 200 A     | qa.testes@pixeon.com  | 7131461300  | 991715858 | whastsApp        | IsaiasQA  | 7199887766     |
    E clico no botao comum "Avançar"
