package com.pixeon.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Random;

public class UtilSmartWeb {


    /**
     *     * @author: Isaias Silva
     * Gerar nome aleatorio com um tamanho determinado
     * @return
     */
    private static Random rand = new Random();
    private static char[] letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    public  String nomeAleatorio (int tamanho) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i <tamanho+1; i++) {
            int ch = rand.nextInt (letras.length);
            sb.append (letras [ch]);
        }
        return sb.toString();
    }



    /**
     * @author: Gilson
     * @param caminhoUrl
     */
    public static void deletarArquivosDeUmDiretorio(String caminhoUrl) {
        //File file = new File("C:\\Smart\\servimp");
        File file = new File(caminhoUrl);
        File afile[] = file.listFiles();
        int i = 0;
        for (int j = afile.length; i < j; i++) {
            File arquivos = afile[i];
            System.out.println(arquivos.getName());
            arquivos.delete();
        }
    }


    public static String lerArquivo(String caminhoAquivo) {
        String tudo = "";
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(caminhoAquivo));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            tudo = sb.toString();
            br.close();
        }
        catch (Exception e)   {
            e.printStackTrace();
        }
        return tudo;
    }

    //Ler array de string e clicar
    public static void clicarArray(String string){
        String arrayString[] = string.split(";");

        for(int i=0; i<=arrayString.length; i++){
           //getDriver().findElement(By.linkText(arrayString[i])).click();
        }
    }

}
