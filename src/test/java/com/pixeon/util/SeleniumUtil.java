package com.pixeon.util;

import com.google.common.collect.Iterables;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;
import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessBufferedFileInputStream;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class SeleniumUtil {

	/**
	 * LOG
	 */
	final static Logger logger = Logger.getLogger(SeleniumUtil.class);

	private static SeleniumWaitHelper waitHelper = new SeleniumWaitHelper(
			ThucydidesWebDriverSupport.getDriver());

	/**
	 * Select item in div (Grid).
	 * 
	 * @author caio.ribeiro
	 * @since 11/09/2018
	 * @param listMenuElement
	 * @param listMenu
	 */

	public static void menuSmart(List<WebElement> listMenuElement, List<String> listMenu) {

		logger.info("menuSmart");

		try {
			for (String menu : listMenu) {
				for (WebElement menuElement : listMenuElement) {
					if (menu != null && !menu.isEmpty() && menuElement.getText().equals(menu)) {
						if (Iterables.getLast(listMenu, null) != menu ) {
							waitHelper.moveActions(menuElement);
							waitHelper.whenReady(menuElement.findElement(By.tagName("li")));
							listMenuElement = menuElement.findElements(By.tagName("li"));
							break;
						} else {
							waitHelper.clickWhenReady(menuElement);
						}
					}
				}
			}

		} catch (StaleElementReferenceException e) {
			logger.error("StaleElementReferenceException - menuSmart: " + e.getMessage());
		} catch (Exception e) {
			logger.error("Erro - menuSmart: " + e.getMessage());
		}
	}

	public static void selectItemDiv(List<WebElement> elements,
			String valueGrid) {

		// TODO verificar possibilidade de tratamento caso não encontre o
		// registro na lista.

		for (WebElement element : elements) {

			if (element.getText().equals(valueGrid)) {
				waitHelper.clickWhenReady(element);
			}

		}

	}

	/**
	 * Select item in div (Grid) with.
	 * 
	 * @author caio.ribeiro
	 * @since 26/09/2018
	 * @param elements
	 * @param valueGrid
	 */
	public static void selectItemDivDoubleClick(List<WebElement> elements,
			String valueGrid) {
		for (WebElement element : elements) {
			if (element.getText().equals(valueGrid)) {
				Actions action = new Actions(
						ThucydidesWebDriverSupport.getDriver());
				action.doubleClick(element).perform();
			}
		}
	}

	/**
	 * Select item in div (Grid) with.
	 *
	 * @author allan.amorim
	 * @since 10/09/2019
	 */
	public static String javaScriptExecutorKeyboard(String atributo, String valor, String argumento, String acao, String querySelector){
		JavascriptExecutor js = (JavascriptExecutor) ThucydidesWebDriverSupport.getDriver();
		WebElement element = null;
		switch (atributo){
			case "xpath":
				try {
					Thread.sleep(1000);
					element = ThucydidesWebDriverSupport.getDriver().findElement(By.xpath(valor));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				break;
			default:
				try {
					Thread.sleep(1000);
					element = ThucydidesWebDriverSupport.getDriver().findElement(By.id(valor));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}

		js.executeScript("arguments[0].setAttribute("+argumento+")", element);
		JavascriptExecutor executor = (JavascriptExecutor) ThucydidesWebDriverSupport.getDriver();

		return toDefineActionExecuteScript(acao, element, executor, querySelector);
	}

	/**
	 * Select item in div (Grid) with.
	 *
	 * @author allan.amorim
	 * @since 10/09/2019
	 */
	public static String toDefineActionExecuteScript(String acao, WebElement element, JavascriptExecutor executor, String querySelector){
		JavascriptExecutor js = (JavascriptExecutor) ThucydidesWebDriverSupport.getDriver();
		switch (acao){
			case "click":
				try {
					Thread.sleep(1000);
					executor.executeScript("arguments[0].click();", element);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				break;
			case "view":
				try {
					Thread.sleep(1000);
					String elementQuerySelector = js.executeScript("return "+querySelector).toString();
					return elementQuerySelector;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
		return "";
	}

	/**
	 * Return existence alert.
	 *
	 * @author allan.amorim
	 * @since 17/12/2019
	 */
	public static boolean isAlertPresent()
	{
		try
		{
			Thread.sleep(1000);
			ThucydidesWebDriverSupport.getDriver().switchTo().alert();
			return true;
		}
		catch (NoAlertPresentException | InterruptedException Ex)
		{
			return false;
		}
	}

	/**
	 * Select item in div (Grid) with.
	 * 
	 * @author weslley.oliveira
	 * @since 26/09/2018
	 * @param elements
	 * @param valueGrid
	 */
	public static Boolean existsItemDiv(List<WebElement> elements,
			String valueGrid) {
		for (WebElement element : elements) {
			if (element.getText().trim().equals(valueGrid)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Set checkbox true or false.
	 *
	 * @author weslley.oliveira
	 * @return
	 * @param checkBox
	 * @param value "selecionado" ou "naoSelecionado"
	 */
	public static void setCheckBox(WebElementFacade checkBox, String value) {
		waitHelper.setCheck(checkBox, value);
	}

	public static void informarTextoEmJanelaDoWindowsEconfiramr(String texto) {
		try {
			Thread.sleep(3000);
			int length = texto.length();
			for (int i = 0; i < length; i++) {
				typeCaracterTeclado(texto.charAt(i));
			}
			Thread.sleep(3000);
			Robot robot1=new Robot();
			robot1.keyPress(KeyEvent.VK_ENTER);
			robot1.keyRelease(KeyEvent.VK_ENTER);

		} catch (Exception e) {
			Logger.getLogger("StaleElementReferenceException -: " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

//	public static void navegarOutraAba(String url){
//		ThucydidesWebDriverSupport.
//		//WebDriver driver = new FirefoxDriver();
//		driver.navigate().to(url);
//	}

	/**
	 * Get date now formated with hours.
	 *
	 * @author allan.amorim
	 * @return data atual com formato
	 */
	public static String dataAtualFormated(){
		Date data = new Date();
		SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		return formatador.format(data);
	}

	/**
	 * Get date now.
	 *
	 * @author allan.amorim
	 * @return data atual com formato
	 */
	public static String getDataAtual() {
		Date data = new Date();
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		return (formatador.format(data));
	}


	/**
	 * Get date now formated.
	 *
	 * @author allan.amorim
	 * @return
	 * @param formato "formato de data"
	 */
	public static String getDataAtualFormato(String formato){
		Date data = new Date();
		SimpleDateFormat formatador = new SimpleDateFormat(formato);
		return (formatador.format(data));
	}


	/**
	 * Set week day.
	 *
	 * @author allan.amorim
	 * @return dia da semana
	 */
	public static int weekDay() throws ParseException {
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		Date dataAtual = formatador.parse(getDataAtual());
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(dataAtual);
		int weekday = gc.get(GregorianCalendar.DAY_OF_WEEK);

		return weekday;
	}


	/**
	 * Set days.
	 *
	 * @author allan.amorim
	 * @return
	 * @param formato "formato de data"
	 * @param dias dias para adição a data
	 */
	public static String adicionarDiasDataAtual(String formato, int dias){
		Date a = new Date();
		a.setDate(a.getDate() + dias);

		SimpleDateFormat dataFormatada = new SimpleDateFormat(formato);
		return dataFormatada.format(a);
	}

	public static void typeCaracterTeclado(char character) {
		Robot robot;
		try {
			robot = new Robot();

			switch (character) {

				case 'a': robot.keyPress(KeyEvent.VK_A); break;

				case 'b': robot.keyPress(KeyEvent.VK_B); break;

				case 'c': robot.keyPress(KeyEvent.VK_C); break;

				case 'd': robot.keyPress(KeyEvent.VK_D); break;

				case 'e': robot.keyPress(KeyEvent.VK_E); break;

				case 'f': robot.keyPress(KeyEvent.VK_F); break;

				case 'g': robot.keyPress(KeyEvent.VK_G); break;

				case 'h': robot.keyPress(KeyEvent.VK_H); break;

				case 'i': robot.keyPress(KeyEvent.VK_I); break;

				case 'j': robot.keyPress(KeyEvent.VK_J); break;

				case 'k': robot.keyPress(KeyEvent.VK_K); break;

				case 'l': robot.keyPress(KeyEvent.VK_L); break;

				case 'm': robot.keyPress(KeyEvent.VK_M); break;

				case 'n': robot.keyPress(KeyEvent.VK_N); break;

				case 'o': robot.keyPress(KeyEvent.VK_O); break;

				case 'p': robot.keyPress(KeyEvent.VK_P); break;

				case 'q': robot.keyPress(KeyEvent.VK_Q); break;

				case 'r': robot.keyPress(KeyEvent.VK_R); break;

				case 's': robot.keyPress(KeyEvent.VK_S);break;

				case 't': robot.keyPress(KeyEvent.VK_T);break;

				case 'u': robot.keyPress(KeyEvent.VK_U); break;

				case 'v': robot.keyPress(KeyEvent.VK_V); break;

				case 'w': robot.keyPress(KeyEvent.VK_W); break;

				case 'x': robot.keyPress(KeyEvent.VK_X); break;

				case 'y': robot.keyPress(KeyEvent.VK_Y); break;

				case 'z': robot.keyPress(KeyEvent.VK_Z); break;

				case 'A':  robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_A);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'B': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_B);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'C':

					robot.keyPress( KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_C);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'D': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_D);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'E': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_E);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'F': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_F);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'G': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_G);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'H': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_H);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'I': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_I);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'J': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_J);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'K': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_K);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'L': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_L);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'M': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_M);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'N': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_N);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'O': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_O);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'P': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_P);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'Q': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_Q);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'R': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_R);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'S': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_S);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'T': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_T);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'U': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_U);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'V': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_V);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'W': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_W);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'X': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_X);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'Y': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_Y);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case 'Z': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_Z);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case '`': robot.keyPress(KeyEvent.VK_BACK_QUOTE);

					robot.keyRelease(KeyEvent.VK_SHIFT);

					break;

				case '0': robot.keyPress(KeyEvent.VK_0); break;

				case '1': robot.keyPress(KeyEvent.VK_1); break;

				case '2': robot.keyPress(KeyEvent.VK_2); break;

				case '3': robot.keyPress(KeyEvent.VK_3); break;

				case '4': robot.keyPress(KeyEvent.VK_4); break;

				case '5': robot.keyPress(KeyEvent.VK_5); break;

				case '6': robot.keyPress(KeyEvent.VK_6); break;

				case '7': robot.keyPress(KeyEvent.VK_7); break;

				case '8': robot.keyPress(KeyEvent.VK_8); break;

				case '9': robot.keyPress(KeyEvent.VK_9); break;

				case '-': robot.keyPress(KeyEvent.VK_MINUS); break;

				case '=': robot.keyPress(KeyEvent.VK_EQUALS); break;

				case '~': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_BACK_QUOTE);

					break;

				case '!': robot.keyPress(KeyEvent.VK_EXCLAMATION_MARK); break;

				case '@': robot.keyPress(KeyEvent.VK_AT); break;

				case '#': robot.keyPress(KeyEvent.VK_NUMBER_SIGN); break;

				case '$': robot.keyPress(KeyEvent.VK_DOLLAR); break;

				case '%': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_5);

					break;

				case '^': robot.keyPress(KeyEvent.VK_CIRCUMFLEX); break;

				case '&': robot.keyPress(KeyEvent.VK_AMPERSAND); break;

				case '*': robot.keyPress(KeyEvent.VK_ASTERISK); break;

				case '(': robot.keyPress(KeyEvent.VK_LEFT_PARENTHESIS); break;

				case ')': robot.keyPress(KeyEvent.VK_RIGHT_PARENTHESIS); break;

				case '_': robot.keyPress(KeyEvent.VK_UNDERSCORE); break;

				case '+': robot.keyPress(KeyEvent.VK_PLUS); break;

				case '\t': robot.keyPress(KeyEvent.VK_TAB); break;

				case '\n': robot.keyPress(KeyEvent.VK_ENTER); break;

				case '[': robot.keyPress(KeyEvent.VK_OPEN_BRACKET); break;

				case ']': robot.keyPress(KeyEvent.VK_CLOSE_BRACKET); break;

				case '\\': robot.keyPress(KeyEvent.VK_BACK_SLASH); break;

				case '{': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_OPEN_BRACKET);

					break;

				case '}': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_CLOSE_BRACKET); break;

				case '|': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_BACK_SLASH);

					break;

				case ';': robot.keyPress(KeyEvent.VK_SEMICOLON); break;

				case ':': robot.keyPress(KeyEvent.VK_COLON); break;

				case '\'': robot.keyPress(KeyEvent.VK_QUOTE); break;

				case '"': robot.keyPress(KeyEvent.VK_QUOTEDBL); break;

				case ',': robot.keyPress(KeyEvent.VK_COMMA); break;

				case '<': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_COMMA); break;

				case '.': robot.keyPress(KeyEvent.VK_PERIOD); break;

				case '>': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_PERIOD); break;

				case '/': robot.keyPress(KeyEvent.VK_SLASH); break;

				case '?': robot.keyPress(KeyEvent.VK_SHIFT);

					robot.keyPress(KeyEvent.VK_SLASH); break;

				case ' ': robot.keyPress(KeyEvent.VK_SPACE); break;

				default:

					throw new IllegalArgumentException("Não é possível digitar o caractere " + character);
			}

		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author Gilson Lima
	 * @param txtParavalidar
	 */
	public static void validarPDF(String txtParavalidar) {
		WebElementFacade URLpdf = (WebElementFacade) ThucydidesWebDriverSupport.getDriver().findElement(By.xpath("//*//div[@class='bd']//iframe"));

		try {

//			carregarTelaInicial();

			try {
				URLpdf.waitUntilPresent();
			} catch (Exception e) {
				System.out.println("elemento não contrado " + e);
			}

			Assert.assertTrue(validaPDFContem(txtParavalidar));

		} catch (Exception e) {
			Logger.getLogger("StaleElementReferenceException -: " + e.getMessage());
			Assert.assertTrue(false);

		}
	}

	/**
	 * validaPDFContem
	 * Comentário: Esse método tem alguns débitos técnico, foi a maneira mais rápida.
	 *
	 * @param textoMassa
	 * @return boolean - resultado se contém texto no PDF
	 * @author Gilson Lima
	 */
	public static boolean validaPDFContem(String textoMassa) {
		//String s = System.getProperty("user.dir")+"src/test/resources/files/getTempFile.pdf";
		String s = "C:\\Users\\"+System.getProperty("user.name")+"\\Downloads\\getTempFile.pdf";

		boolean status;
		Robot bot;

		try {
			File file = new File(s);
			if (file.exists()) {
				file.deleteOnExit();
				file.delete();
			}
			Thread.sleep(1200);
			bot = new Robot();
			bot.mouseMove(1133, 146);
			bot.mouseMove(1188, 146);
			bot.mouseMove(1198, 146);
			bot.mouseMove(1210, 146);
			bot.mousePress(InputEvent.BUTTON1_MASK);
			bot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
			Thread.sleep(1300);
			bot.keyPress(KeyEvent.VK_ENTER);
			bot.keyPress(KeyEvent.VK_ESCAPE);

			PDFTextStripper pdfStripper = null;
			PDDocument pdDoc = null;
			COSDocument cosDoc = null;
			PDFParser parser;
			int qtPaginas = 0;
			Thread.sleep(1300);
			parser = new PDFParser(new RandomAccessBufferedFileInputStream(s));
			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdDoc = new PDDocument(cosDoc);
			qtPaginas = pdDoc.getNumberOfPages();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(qtPaginas);
			String parsedText = pdfStripper.getText(pdDoc);
			//status=(parsedText.contains("Página"+(qtPaginas-1)+"/"+(qtPaginas-1)));
			if (parsedText.contains("Página " + (qtPaginas - 1) + "/" + (qtPaginas - 1)) && (parsedText.contains(textoMassa))) {
				status = true;
			} else {
				status = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
			status = false;
		}

		return status;

	}





	//
	public static void deletarArquivosDeUmDiretorio(String caminhoUrl)  {
		//File file = new File("C:\\Smart\\servimp");
		File file = new File(caminhoUrl);
		File afile[] = file.listFiles();
		int i = 0;

		try {
			for (int j = afile.length; i < j; i++) {
				File arquivos = afile[i];
				//System.out.println(arquivos.getName());
				Thread.sleep(50);
				arquivos.delete();
			}

		} catch (Exception e) {
			Assert.assertTrue("erro ao deletar arquivo do diretorio",false);
		}


	}

//
	public static void copiaArquivo(String OrigenArquivo, String destinoArquivo)  {
		try {
			FileInputStream in = new FileInputStream(OrigenArquivo);
			FileOutputStream out = new FileOutputStream(destinoArquivo);

			byte[] buf = new byte[1024];
			int len;

			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			Assert.assertTrue("----------------  Erro ao tenta copia Arquivo ----------",true);
		}
	}


	public static String capituranomeUtimaDiretorioCriado(String caminhoUrl)  {
		Date dataInicial = null;
		Date dataFinal = null;
		String dataInicio = "14/01/2001 00:45:00";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String nomeCompleto=null;
		String CaminhoFixo="\\\\10.71.5.104\\desenv\\SmartWeb_Versoes\\";
		DateFormat df = DateFormat.getDateTimeInstance();
		try {

			dataInicial = sdf.parse(dataInicio);
			Thread.sleep(400);
			File file = new File(CaminhoFixo+caminhoUrl);
			File afile[] = file.listFiles();
			int i = 0;


			for (int j = afile.length; i < j; i++) {
				File arquivos = afile[i];
				dataFinal = sdf.parse(df.format(arquivos.lastModified()));

				if(dataInicial.compareTo(dataFinal) < 0){
					dataInicial=sdf.parse(df.format(arquivos.lastModified()));
					nomeCompleto=(String.valueOf(arquivos)).replace("'\'","'/'");
					// System.out.println(nomeCompleto);
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			//  System.out.println("nok");
			Assert.assertTrue(false);
		}
		System.out.println("Diretorio que o arquivo exe será copiado "+nomeCompleto);
		return nomeCompleto;
	}





}
