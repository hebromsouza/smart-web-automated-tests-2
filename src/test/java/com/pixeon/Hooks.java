package com.pixeon;

import com.pixeon.datamodel.login.DataLogin;
import com.pixeon.pages.BasePage;
import com.pixeon.pages.home.PageHome;
import com.pixeon.pages.login.PageLogin;
import com.pixeon.steps.serenity.login.login.LoginSteps;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class Hooks extends BasePage {



	List<DataLogin> dataLogin;

	PageLogin pageLogin;
	PageHome pageHome;

	@Steps
	LoginSteps loginSteps;


	@Before
	public void beforeScenario() throws InterruptedException {
		// Implement me
		System.out.println("================= ANTES DE INICIAR CADA CENARIO, EFETUE LOGIN NA APLICAÇÃO ==================");
		loginSteps.logout();
		//loginSteps.efetuarLoginSmartWeb("MEDICWARE", "mwa");
		loginSteps.efetuarLoginSmartWeb("MEDICWARE", "1");
		loginSteps.carregarTelaInicial();
	}


//
//	@After
//	public void afterScenario() {
//		System.out.println("================= APÓS EXECUTAR CADA CENÁRIO, FAÇA LOGOUT NA APLICAÇÃO =================");
//
//		loginSteps.deslogarSmartweb();


}
