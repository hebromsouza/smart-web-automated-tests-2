package com.pixeon.steps.definitionSteps.estoque;

import com.pixeon.steps.serenity.estoque.AtendimentoDeSolicitacoesSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAtendimentoDeSolicitacoes {

    @Steps
    AtendimentoDeSolicitacoesSteps atendimentoDeSolicitacoesSteps;

    @Quando("^selecionar subalmoxariado \"([^\"]*)\" na tela de atendimento de solicitações$")
    public void selecionar_subalmoxariado_na_tela_de_atendimento_de_solicitações(String subalmoxarifado) {
        atendimentoDeSolicitacoesSteps.selecionarSubAlmoxarifadoTelaAtendimentoSolicitacao(subalmoxarifado);
    }

    @Quando("^selecionar tipo de solicitação \"([^\"]*)\" de materiais na tela de atendimento de soliciações$")
    public void selecionar_tipo_de_solicitação_de_materiais_na_tela_de_atendimento_de_soliciações(String tipoSolicitacao) {
        atendimentoDeSolicitacoesSteps.selecionarTipoSolicitacaoMateriaisTelaAtendimentoSolicitacoes(tipoSolicitacao);
    }

    @Quando("^filtrar atendimento de solicitações$")
    public void filtrar_atendimento_de_solicitações() {
        atendimentoDeSolicitacoesSteps.filtrarAtendimentoSolicitacoes();
    }

    @Quando("^abrir atendimento de solicitação pela série e número gerados na tela de solicitação de transferência$")
    public void abrir_atendimento_de_solicitação_pela_série_e_número_gerados_na_tela_de_solicitação_de_transferência() {
        atendimentoDeSolicitacoesSteps.abrirAtendimentoSolicitacaoPelaSerieNumeroGeradosTelaSolicitacaoTransferencia();
    }

    @Quando("^incluir quantidade \"([^\"]*)\" para dar baixa$")
    public void incluir_quantidade_para_dar_baixa(String quantidade) {
        atendimentoDeSolicitacoesSteps.incluirQuantidadeParaBaixa(quantidade);
    }

    @Quando("^imprimir atendimento da solicitação$")
    public void imprimir_atendimento_da_solicitação() {
        atendimentoDeSolicitacoesSteps.imprimirAtendimentoSolicitacao();
    }

    @Então("^o sistema valida impressão do atendimento de solicitações$")
    public void o_sistema_valida_impressão_do_atendimento_de_solicitações() {
        atendimentoDeSolicitacoesSteps.validarImpressaoAtendimentoSolicitacoes();
    }

    @Quando("^gravar atendimento de solicitação$")
    public void gravar_atendimento_de_solicitação() {
        atendimentoDeSolicitacoesSteps.gravarAtendimentoSolicitacoes();
    }

    @Então("^o sistema valida atendimento de solicitação$")
    public void o_sistema_valida_atendimento_de_solicitação() {
        atendimentoDeSolicitacoesSteps.validarAtendimentoSolicitacao();
    }

    @Então("^o sistema valida ausencia da solicitação na tela de atendimento de solicitações pendentes$")
    public void o_sistema_valida_ausencia_da_solicitação_na_tela_de_atendimento_de_solicitações_pendentes() {
        atendimentoDeSolicitacoesSteps.validarAusenciaSolicitacaoTelaAtendimentoSolicitacoesPendentes();
    }

    @Então("^o sistema valida presença da solicitação na tela de atendimento de solicitações sem pendência$")
    public void o_sistema_valida_presença_da_solicitação_na_tela_de_atendimento_de_solicitações_sem_pendência() {
        atendimentoDeSolicitacoesSteps.validarPresencaSolicitacao();
    }

}
