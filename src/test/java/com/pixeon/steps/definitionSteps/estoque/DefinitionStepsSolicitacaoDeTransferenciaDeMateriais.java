package com.pixeon.steps.definitionSteps.estoque;

import com.pixeon.steps.serenity.estoque.SolicitacaoDeTransferenciaDeMateriaisSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsSolicitacaoDeTransferenciaDeMateriais {

    @Steps
    SolicitacaoDeTransferenciaDeMateriaisSteps solicitacaoDeTransferenciaDeMateriaisSteps;

    @Quando("^selecionar setor \"([^\"]*)\" da solicitação de material$")
    public void selecionar_setor_da_solicitação_de_material(String setorSolicitante) {
        solicitacaoDeTransferenciaDeMateriaisSteps.selecionarSetorSolicitacaoMaterial(setorSolicitante);
    }


    @Quando("^selecionar subalmoxarifado \"([^\"]*)\" na solicitação de material$")
    public void selecionar_subalmoxarifado_na_solicitação_de_material(String subalmoxarifado) {
        solicitacaoDeTransferenciaDeMateriaisSteps.selecionarSubAlmoxarifadoSolicitacaoMaterial(subalmoxarifado);
    }

    @Quando("^selecionar subalmoxarifado \"([^\"]*)\" para transferencia$")
    public void selecionar_subalmoxarifado_para_transferencia(String subalmoxarifadoTransf) {
        solicitacaoDeTransferenciaDeMateriaisSteps.selecionarSubAlmoxarifadoParaTransferencia(subalmoxarifadoTransf);
    }

    @Quando("^informar observação \"([^\"]*)\" da solicitação de transferência$")
    public void informar_observação_da_solicitação_de_transferência(String observacao) {
        solicitacaoDeTransferenciaDeMateriaisSteps.informarObservacaoSolicitacaoTransferencia(observacao);
    }

    @Quando("^gravar solicitação de transferência de material pegando série e número$")
    public void gravar_solicitação_de_transferência_de_material_pegando_série_e_número() {
        solicitacaoDeTransferenciaDeMateriaisSteps.gravarSolicitacaoTransferenciaMaterialPegandoSerieNumero();
    }

    @Quando("^buscar item \"([^\"]*)\" na tela de solicitação de transferência de material$")
    public void buscar_item_na_tela_de_solicitação_de_transferência_de_material(String item) {
        solicitacaoDeTransferenciaDeMateriaisSteps.buscarItemTelaSolicitacaoTransferenciaMaterial(item);
    }

    @Quando("^adicionar item selecionando quantidade \"([^\"]*)\"$")
    public void adicionar_item_selecionando_quantidade(String quantidade) {
        solicitacaoDeTransferenciaDeMateriaisSteps.adicionarItemSelecionandoQuantidade(quantidade);
    }

    @Quando("^buscar solicitações de transferência de materiais$")
    public void buscar_solicitações_de_transferência_de_materiais() {
        solicitacaoDeTransferenciaDeMateriaisSteps.buscarSolicitacoesDeTransferenciaDeMateriais();
    }

    @Quando("^selecionar o item na tela de solicitação de transferência de materiais$")
    public void selecionar_o_item_na_tela_de_solicitação_de_transferência_de_materiais() {
        solicitacaoDeTransferenciaDeMateriaisSteps.selecionarItem();
    }

    @Quando("^cancelar item na tela de solicitação de transferência de materiais$")
    public void cancelar_item_na_tela_de_solicitação_de_transferência_de_materiais() {
        solicitacaoDeTransferenciaDeMateriaisSteps.cancelarItem();
    }

    @Entao("^o sistema valida solicitação de transferência de materiais$")
    public void o_sistema_valida_solicitação_de_transferência_de_materiais() {
        solicitacaoDeTransferenciaDeMateriaisSteps.validarSolicitacaoDeTransferenciaDeMateriais();
    }

    @Então("^o sistema valida na tela de solicitação de transferência de materiais o número da solicitação$")
    public void o_sistema_valida_na_tela_de_solicitação_de_transferência_de_materiais_o_número_da_solicitação() {
        solicitacaoDeTransferenciaDeMateriaisSteps.validarNaTelaDeSolicitacaoDeTransferenciaDeMateriaisNumeroDaSolicitacao();
    }

    @Entao("^o sistema valida ausencia do numero da solicitação em solicitações recentes$")
    public void o_sistema_valida_ausencia_do_numero_da_solicitação_em_solicitações_recentes() {
        solicitacaoDeTransferenciaDeMateriaisSteps.validarAusenciaNumeroSolicitacoesRecentes();
    }

    @Então("^o sistema valida cancelamento do item na tela de solicitação de transferência de materiais$")
    public void o_sistema_valida_cancelamento_do_item_na_tela_de_solicitação_de_transferência_de_materiais() {
        solicitacaoDeTransferenciaDeMateriaisSteps.validarCancelamentoDoItem();
    }

    @Quando("^cancelar solicitação de transferência de materiais$")
    public void cancelar_solicitação_de_transferência_de_materiais() {
        solicitacaoDeTransferenciaDeMateriaisSteps.cancelarSolicitacao();
    }

    @Então("^o sistema valida status \"([^\"]*)\" da solicitação de transferência de materiais$")
    public void o_sistema_valida_status_da_solicitação_de_transferência_de_materiais(String status) {
        solicitacaoDeTransferenciaDeMateriaisSteps.validarStatusSolicitacao(status);
    }

}