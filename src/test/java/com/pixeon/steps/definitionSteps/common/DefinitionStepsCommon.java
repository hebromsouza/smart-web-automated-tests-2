package com.pixeon.steps.definitionSteps.common;
import com.pixeon.steps.serenity.common.CommonSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsCommon {

    @Steps
    CommonSteps commonSteps;

    @Então("^e apresentado a mensagem \"([^\"]*)\" e confirma o popup$")
    public void e_apresentado_a_mensagem_e_confirma_o_popup(String mensagem) {
        commonSteps.e_apresentado_a_mensagem_e_confirma_o_popup(mensagem);
    }

    @Quando("^informo o caminho do arquivo \"([^\"]*)\" na janela do windows e abrir$")
    public void informo_o_caminho_do_arquivo_na_janela_do_windows_e_abrir(String texto) {
        commonSteps.informo_o_caminho_do_arquivo_na_janela_do_windows_e_abrir(texto);
    }

    @Quando("^abrir arquivo informando caminho \"([^\"]*)\" na janela do windows$")
    public void abrir_arquivo_informando_caminho_na_janela_do_windows(String caminho) {
        commonSteps.abrirArquivoInformandoCaminhoNaJanelaDoWindows(caminho);
    }

    @Quando("^acessar a aba \"([^\"]*)\"$")
    public void acessar_a_aba(String aba) {
        commonSteps.selecionarAba(aba);
    }

    @Quando("^fechar painel$")
    public void fechar_painel() {
        commonSteps.closePainel();
    }

    @Então("^é apresentado popup com mensagem \"([^\"]*)\" e confirma$")
    public void é_apresentado_popup_com_mensagem_e_confirma(String mensagem) {
        commonSteps.validarPopUpConfirmar(mensagem);
    }

}
