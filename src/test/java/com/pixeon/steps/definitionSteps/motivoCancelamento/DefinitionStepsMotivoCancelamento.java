package com.pixeon.steps.definitionSteps.motivoCancelamento;

import com.pixeon.steps.serenity.motivoCancelamento.MotivoCancelamentoSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsMotivoCancelamento {

    @Steps
    MotivoCancelamentoSteps motivoCancelamentoSteps;

    @Quando("^informar motivo de cancelamento \"([^\"]*)\" e observação \"([^\"]*)\"$")
    public void informar_motivo_de_cancelamento_e_observação(String motivo, String obs) {
        motivoCancelamentoSteps.informarMotivoCancelamentoObservacao(motivo, obs);
    }

}
