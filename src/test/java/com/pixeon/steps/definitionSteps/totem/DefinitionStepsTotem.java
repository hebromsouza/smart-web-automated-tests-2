package com.pixeon.steps.definitionSteps.totem;

import com.pixeon.steps.serenity.totem.TotemSteps;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsTotem {

    @Steps
    TotemSteps totemSteps;

    @Dado("^que acesso a url do totem \"([^\"]*)\"$")
    public void que_acesso_a_url_do_tokem(String url) {
        totemSteps.acesar_url_tokem(url);
    }

    @Quando("^acessar smartweb$")
    public void acessar_smartweb() {
        totemSteps.acessarSmartweb();
    }

    @Quando("^selecionar setor \"([^\"]*)\"$")
    public void selecionar_setor(String setor) {
        totemSteps.selecionarSetor(setor);
    }

    @Quando("^selecionar médico \"([^\"]*)\"$")
    public void selecionar_médico(String medico) {
        totemSteps.selecionarMedico(medico);
    }

    @Quando("^retirar senha$")
    public void retirar_senha() {
        totemSteps.retirarSenha();
    }

    @Quando("^abrir chamada de senha$")
    public void abrir_chamada_de_senha() {
        totemSteps.abrirChamadaSenha();
    }

    @Quando("^voltar do totem para tela de smartweb$")
    public void voltar_do_totem_para_tela_de_smartweb() {
        totemSteps.voltarPaginaTotemSmartweb();
    }

    @Quando("^fechar painel de retirada de senha e voltar a tela do totem$")
    public void fechar_painelde_retiradasenha_voltar_teladoTotem() {
        totemSteps.fecharPainelVoltarAtelaDoTotem();
    }

    @Quando("^trocar de janela, mudando para a do smartweb$")
    public void trocar_janela_sairdototem_para_smartweb() {
        totemSteps.trocarJanelaSairdoTotemParaSmartweb();
    }

    @Entao("^verifico que as senhas foram retiradas com sucesso e sem erros$")
    public void verifico_que_as_senhas_foram_retiradas_com_sucesso_e_sem_erros() {
        totemSteps.verifico_que_as_senhas_foram_retiradas_com_sucesso_e_sem_erros();
    }



    @Então("^a senha é chamada no painel de chamada de senha$")
    public void a_senha_é_chamada_no_painel_de_chamada_de_senha() {
        totemSteps.validarChamadaSenhaPainel();
    }

    @Então("^cancelar senha gerada$")
    public void cancelar_senha_gerada() {
        totemSteps.cancelarSenhaGerada();
    }

    @Quando("^retirar senha no totem com setor \"([^\"]*)\" e médico \"([^\"]*)\"$")
    public void retirar_senha_no_totem_com_setor_e_médico(String url, String setor, String medico) {
        totemSteps.retirarSenhaNoTotemSetorMedico(url, setor, medico);
    }

    @Quando("^selecionar médico \"([^\"]*)\" com tipo de atendimento \"([^\"]*)\" no totem$")
    public void selecionar_médico_com_tipo_de_atendimento_no_totem(String medico, String tipoAtendimento) {
        totemSteps.selecionarMedicoComTipoDeAtendimento(medico, tipoAtendimento);
    }

    @Quando("^selecionar médico (\\d+) \"([^\"]*)\" com tipo de atendimento \"([^\"]*)\" no totem$")
    public void selecionar_médico_com_tipo_de_atendimento_no_totem(int idMedico, String medico, String tipoAtendimento) {
        totemSteps.selecionarListaMedicosComTipoDeAtendimento(idMedico, medico, tipoAtendimento);
    }

    @Quando("^abrir painel de retirada de senha$")
    public void abrir_painel_de_retirada_de_senha() {
        totemSteps.abrirPainelRetiradaSenha();
    }

    @Quando("^retirar \"([^\"]*)\" senhas para tipo de atendimento \"([^\"]*)\"$")
    public void retirar_senhas_para_tipo_de_atendimento(int quantidadeSenhas, String tipoAtendimento) {
        totemSteps.retirarSenhasTipoAtendimento(quantidadeSenhas, tipoAtendimento);
    }

    @Então("^a próxima senha é chamada no painel de chamada de senha com prefixo \"([^\"]*)\" do tipo de atendimento$")
    public void a_próxima_senha_é_chamada_no_painel_de_chamada_de_senha_com_prefixo_do_tipo_de_atendimento(String prefixo) {
        totemSteps.validarChamadaProximaSenhaPainelComPrefixoTipoAtendimento(prefixo);
    }

    @Então("^o paciente \"([^\"]*)\" é chamado no painel de chamada de senha para o médico \"([^\"]*)\"$")
    public void o_paciente_é_chamado_no_painel_de_chamada_de_senha_para_o_médico(String paciente, String medico) {
        totemSteps.validarChamadaPacientePainelDeChamadaParaMedico(paciente, medico);
    }

}