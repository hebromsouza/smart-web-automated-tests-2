package com.pixeon.steps.definitionSteps.anexarArquivos;

import com.pixeon.steps.serenity.anexarArquivos.AnexarArquivosSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAnexarArquivos {

    @Steps
    AnexarArquivosSteps anexarArquivosSteps;

    @Quando("^abrir a tela de arquivos$")
    public void abrir_a_tela_de_arquivos() {
        anexarArquivosSteps.abrirTelaArquivos();
    }

    @Quando("^adicionar arquivos na tela de anexos paciente$")
    public void adicionar_arquivos_na_tela_de_anexos_paciente() {
        anexarArquivosSteps.adicionarArquivos();
    }

    @Então("^o sistema valida anexo do arquivo na tela de arquivos anexados$")
    public void o_sistema_valida_anexo_do_arquivo_na_tela_de_arquivos_anexados() {
        anexarArquivosSteps.validarArquivoAnexado();
    }

}
