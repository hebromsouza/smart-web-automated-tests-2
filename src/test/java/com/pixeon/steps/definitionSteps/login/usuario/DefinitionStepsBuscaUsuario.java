package com.pixeon.steps.definitionSteps.login.usuario;
import com.pixeon.datamodel.login.DataBuscaUsusario;
import com.pixeon.steps.serenity.login.usuario.BuscaUsuarioSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsBuscaUsuario {

    @Steps
    BuscaUsuarioSteps buscaUsuarioSteps;

    @Quando("^informar de busca da tela de Usuários$")
    public void informar_de_busca_da_tela_de_Usuários(List<DataBuscaUsusario> dataBuscaUsusario) {
        buscaUsuarioSteps.informar_de_busca_da_tela_de_Usuários(dataBuscaUsusario);
    }

    @Quando("^filtrar na tela de Usuários$")
    public void filtrar_na_tela_de_Usuários() {
        buscaUsuarioSteps.filtrar_na_tela_de_Usuários();
    }

    @Quando("^selecionar o nome \"([^\"]*)\" na tela de Usuários$")
    public void selecionar_o_nome_na_tela_de_Usuários(String nome) {
        buscaUsuarioSteps.selecionar_o_nome_na_tela_de_Usuários(nome);
    }
}
