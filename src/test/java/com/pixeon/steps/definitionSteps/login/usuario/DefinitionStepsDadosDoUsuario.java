package com.pixeon.steps.definitionSteps.login.usuario;
import com.pixeon.steps.serenity.login.usuario.DadosDoUsuarioSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsDadosDoUsuario {

    @Steps
    DadosDoUsuarioSteps dadosDoUsuarioSteps;

    @Quando("^eu disativo a selção do usuario Ativo\\? da tela dados do usuario$")
    public void eu_disativo_a_selção_do_usuario_Ativo_da_tela_dados_do_usuario() {
        dadosDoUsuarioSteps.eu_disativo_a_selcao_do_usuario_Ativo_da_tela_dados_do_usuario();
    }

    @Quando("^gravar na tela dados do usuario$")
    public void gravar_na_tela_dados_do_usuario() {
        dadosDoUsuarioSteps.gravar_na_tela_dados_do_usuario();
    }


    @Quando("^eu ativar a opção Ativo\\? na tela dados do usuario$")
    public void eu_ativar_a_opção_Ativo_na_tela_dados_do_usuario() {
        dadosDoUsuarioSteps.eu_ativar_a_opcao_Ativo_na_tela_dados_do_usuario();
    }

    @Quando("^eu disativo  a selção sem prazo na tela dados do usuário$")
    public void eu_disativo_a_selção_sem_prazo_na_tela_dados_do_usuário() {
        dadosDoUsuarioSteps.eu_disativo_a_selecao_sem_prazo_na_tela_dados_do_ususrio();
    }


    @Quando("^eu ativar a selção sem prazo na tela dados do usuário$")
    public void eu_ativar_a_selcao_sem_prazo_na_tela_dados_do_usuario() {
        dadosDoUsuarioSteps.eu_ativar_a_selcao_sem_prazo_na_tela_dados_do_usuario();
    }

    @Quando("^informa a data atual na tela dados do usuário$")
    public void informa_a_data_atual_na_tela_dados_do_usuário() {
        dadosDoUsuarioSteps.informa_a_data_atual_na_tela_dados_do_usuario();
    }

    @Quando("^fechar tela dados do usuario$")
    public void fechar_tela_dados_do_usuario() {
        dadosDoUsuarioSteps.fechar_tela_dados_do_usuario();
    }


}
