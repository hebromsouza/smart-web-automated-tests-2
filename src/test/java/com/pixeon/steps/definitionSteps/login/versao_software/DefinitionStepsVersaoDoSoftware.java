package com.pixeon.steps.definitionSteps.login.versao_software;

import com.pixeon.steps.serenity.login.login.LoginSteps;
import com.pixeon.steps.serenity.login.versao_software.VersaoDoSoftwareSteps;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsVersaoDoSoftware {

    @Steps
    VersaoDoSoftwareSteps versaoDoSoftwareSteps;

    @Steps
    LoginSteps loginSteps;

    @Dado("^que estou no sistema SmartPEP$")
    public void que_estou_no_sistema_SmartPEP() {
        loginSteps.abreSmartWeb();
    }

    @Entao("^é apresentado no software nome do software \"([^\"]*)\", nome do fornecedor \"([^\"]*)\", identificação da versão \"([^\"]*)\", build \"([^\"]*)\"$")
    public void é_apresentado_no_software_nome_do_software_nome_do_fornecedor_identificação_da_versão_build(String nomeSoftware, String nomeFornecedor, String versaoSoftware, String buildSoftware) {
        versaoDoSoftwareSteps.validaVersaoDoSoftware(nomeSoftware, nomeFornecedor, versaoSoftware, buildSoftware);
    }
}
