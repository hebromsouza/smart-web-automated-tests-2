package com.pixeon.steps.definitionSteps.login.login;

import com.pixeon.datamodel.login.DataLogin;
import com.pixeon.steps.serenity.login.login.LoginSteps;
import com.pixeon.steps.serenity.home.HomeSteps;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsLogin {

    //Funcionalidade apenas para SmartPEP - Código deve ser retirado quando ajusrar login SBIS
    public static List<DataLogin> dataLogihome;

    @Steps
    LoginSteps loginSteps;

    @Steps
    HomeSteps homeSteps;

    @Dado("^que estou no sistema Smart Web$")
    public void que_estou_no_sitema_Smart_Web() {
        loginSteps.abreSmartWeb();
    }

    @Dado("^que preencho login e senha$")
    public void que_preencho_login_e_senha(List<DataLogin> dataLogin) {
        loginSteps.preencherLoginESenha(dataLogin);

        //Funcionalidade apenas para SmartPEP - Código deve ser retirado quando ajusrar login SBIS
       // dataLogihome = dataLogin;
    }
    @Dado("^efetuo login no smartweb com o usuario \"([^\"]*)\" e a senha \"([^\"]*)\"$")
    public void informar_um_usuario_e_a_Senha(String usuario, String senha) {
        loginSteps.informar_usuario_senha(usuario, senha);
    }

    @Dado("^que faco logout na aplicacao ignorando assim o login realizado pelo hooks$")
    public void quefacologout_na_app_ignorandologindo_hooks() {
        loginSteps.quefacologout_na_app_ignorandologindo_hooks();
    }

    @Dado("^abrir tela de login$")
    public void abrir_tela_login() {
        loginSteps.abrir_tela_login();
    }


 //   @Dado("^que faco logout na aplicacao ignorando assim o login do hooks$")
//    @Dado("^informar um  usuario \"([^\"]*)\" e a Senha \"([^\"]*)\"$")
//    public void informar_um2_usuario_e_a_Senha(String usuario, String senha) {
//        loginSteps.informar_um_usuario_e_a_Senha(usuario, senha);
//    }

    @Quando("^eu realizar o login$")
    public void eu_realizar_o_login() {
        loginSteps.realizaLogin();
    }

    @Quando("^eu realizar o login no SmartPEP$")
    public void eu_realizar_o_login_no_SmartPEP() {
        loginSteps.realizaLoginSmartPEP();
    }

    @Quando("^for selecionada o Grupo de usuário \"([^\"]*)\"$")
    public void for_selecionada_o_Grupo_de_usuário(String grupoDeUsuario) {
        loginSteps.for_selecionada_o_Grupo_de_usuário(grupoDeUsuario);
    }

    @Então("^o sistema solicita o preenchimento da recepção$")
    public void o_sistema_solicita_o_preenchimento_da_recepção() {
        loginSteps.solicitaPreenchimentoRecepcao();
    }

    @Então("^espera \"([^\"]*)\" minuto e validar se o sistema apresenta a mensagem \"([^\"]*)\" na tela de login$")
    public void espera_minuto_e_validar_se_o_sistema_apresenta_a_mensagem_na_tela_de_login(String tempo, String mensagem) {
        loginSteps.espera_minuto_e_validar_se_o_sistema_apresenta_a_mensagem_na_tela_de_login(tempo, mensagem);
    }

    @Quando("^a recepção for selecionada \"([^\"]*)\"$")
    public void a_recepção_for_selecionada(String recepcao) {
       loginSteps.selecionaRecepcao(recepcao);
    }

    @Então("^o sistema realiza o login com sucesso$")
    public void o_sistema_realiza_o_login_com_sucesso() {
        homeSteps.realizaLOginSucesso();
    }

    @Então("^o sistema exibe mensagem informando que usuario ou senha estão inválidos$")
    public void o_sistema_exibe_mensagem_informando_que_usuario_ou_senha_estão_inválidos() {
        loginSteps.exibeMensagemLoginInvalido();
    }

    @Então("^o sistema apresenta a mensagem \"([^\"]*)\" na tela de login$")
    public void o_sistema_apresenta_a_mensagem_na_tela_de_login(String mensagem) {
        loginSteps.o_sistema_apresenta_a_mensagem_na_tela_de_login(mensagem);
    }
}
