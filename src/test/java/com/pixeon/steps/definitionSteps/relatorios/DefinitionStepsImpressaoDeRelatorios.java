package com.pixeon.steps.definitionSteps.relatorios;

import com.pixeon.steps.serenity.relatorios.ImpressaoDeRelatoriosSteps;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsImpressaoDeRelatorios {

    @Steps
    ImpressaoDeRelatoriosSteps impressaoDeRelatoriosSteps;

    @Dado("^que clico no icone de emissor de relatórios$")
    public void acessar_a_tela_de_emissor_de_relatórios() {
        impressaoDeRelatoriosSteps.acessarTelaEmissorDeRelatorios();
    }

    @Quando("^acessar a tela do relatório \"([^\"]*)\"$")
    public void acessar_a_tela_do_relatório(String relatorio) {
        impressaoDeRelatoriosSteps.acessarTelaRelatorio(relatorio);
    }

    @Quando("^inserir data período do dia na tela \"([^\"]*)\"$")
    public void inserir_data_periodo_do_dia_na_tela(String relatorio) {
        impressaoDeRelatoriosSteps.inserirDataPeriodolHoje(relatorio);
    }

    @Quando("^selecionar recepção \"([^\"]*)\"$")
    public void selecionar_recepção(String recepcao) {
        impressaoDeRelatoriosSteps.selecionarRecepcao(recepcao);
    }

    @Quando("^selecionar usuário \"([^\"]*)\"$")
    public void selecionar_usuário(String usuario) {
        impressaoDeRelatoriosSteps.selecionarUsuario(usuario);
    }

    @Quando("^buscar relatório$")
    public void buscar_relatório() {
        impressaoDeRelatoriosSteps.buscarRelatorio();
    }

    @Quando("^inserir setor solicitante \"([^\"]*)\"$")
    public void inserir_setor_solicitante(String setorSolicitante) {
        impressaoDeRelatoriosSteps.inserirSetorSolicitante(setorSolicitante);
    }

    @Quando("^emitir relatorio$")
    public void emitir_relatorio() {
        impressaoDeRelatoriosSteps.emitirRelatorio();
    }

    @Quando("^limpar campo tempo de permanência$")
    public void limpar_campo_tempo_de_permanência() {
        impressaoDeRelatoriosSteps.limparCampoTempoPermanencia();
    }

    @Então("^o sistema valida impressao de relatório$")
    public void o_sistema_valida_impressao_de_relatório() {
        impressaoDeRelatoriosSteps.validarImpressaoRelatorio();
    }

    @Quando("^limpar campo data inicial$")
    public void limpar_campo_data_inicial() {
        impressaoDeRelatoriosSteps.limparCampoDataInicial();
    }

    @Quando("^limpar campo data final$")
    public void limpar_campo_data_final() {
        impressaoDeRelatoriosSteps.limparCampoDataFinal();
    }

    @Quando("^inserir setor solicitante \"([^\"]*)\" na tela de área técnica - exames realizados na coleta$")
    public void inserir_setor_solicitante_na_tela_de_área_técnica_exames_realizados_na_coleta(String setorSolicitante) {
        impressaoDeRelatoriosSteps.inserirSetorSolicitanteTelaAreaTecnica(setorSolicitante);
    }

    @Quando("^limpar campo status de execução$")
    public void limpar_campo_status_de_execução() {
        impressaoDeRelatoriosSteps.limparCampoStatusExecucao();
    }

    @Quando("^limpar campo data inicial tela área técnica - exames realizados na coleta$")
    public void limpar_campo_data_inicial_tela_área_técnica_exames_realizados_na_coleta() {
        impressaoDeRelatoriosSteps.limparCampoDataInicialTelaAreaTecnica();
    }

    @Quando("^limpar campo data final tela área técnica - exames realizados na coleta$")
    public void limpar_campo_data_final_tela_área_técnica_exames_realizados_na_coleta() {
        impressaoDeRelatoriosSteps.limparCampoDataFinalTelaAreaTecnica();
    }

    @Quando("^limpar código do item da tela  área técnica - exames realizados na coleta$")
    public void limpar_código_do_item_da_tela_área_técnica_exames_realizados_na_coleta() {
        impressaoDeRelatoriosSteps.limparCampoCodigoItemTelaAreaTecnica();
    }

}
