package com.pixeon.steps.definitionSteps.diligenciamento.diligenciamento;

import com.pixeon.steps.serenity.diligenciamento.diligenciamento.DiligenciamentoOrdemDeServicoSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsDiligenciamentoOrdemDeServico {

    @Steps
    DiligenciamentoOrdemDeServicoSteps diligenciamentoOrdemDeServicoSteps;

    @Quando("^abrir o diligenciamento$")
    public void abrir_o_diligenciamento() {
        diligenciamentoOrdemDeServicoSteps.abrirDiligenciamento();
    }

    @Quando("^criar novo diligenciamento$")
    public void criar_novo_diligenciamento() {
        diligenciamentoOrdemDeServicoSteps.novoDiligenciamento();
    }


    @Quando("^selecionar evento \"([^\"]*)\" do diligenciamento$")
    public void selecionar_evento_do_diligenciamento(String evento) {
        diligenciamentoOrdemDeServicoSteps.selecionarEvento(evento);
    }

    @Quando("^incluir observação do diligenciamento$")
    public void incluir_observação_do_diligenciamento() {
        diligenciamentoOrdemDeServicoSteps.incluirObservacao();
    }

    @Quando("^gravar novo diligenciamento$")
    public void gravar_novo_diligenciamento() {
        diligenciamentoOrdemDeServicoSteps.gravarDiligenciamento();
    }

    @Quando("^exluir o diligenciamento$")
    public void exluir_o_diligenciamento() {
        diligenciamentoOrdemDeServicoSteps.excluirDiligenciamento();
    }

    @Entao("^o sistema valida mensagem de erro de exclusao \"([^\"]*)\"$")
    public void o_sistema_valida_mensagem_de_erro_de_exclusao(String mensagemExclusao) {
        diligenciamentoOrdemDeServicoSteps.validarMensagemErroExclusao(mensagemExclusao);
    }

    @Entao("^o deligenciamento é excluido com sucesso")
    public void o_deligenciamentoè_excluido_comSucesso() {
        diligenciamentoOrdemDeServicoSteps.o_deligenciamentoè_excluido_comSucesso();
    }

    @Entao("^o sistema valida exclusao do diligenciamento$")
    public void o_sistema_valida_exclusao_do_diligenciamento() {
        diligenciamentoOrdemDeServicoSteps.validarExclusaoDiligenciamento();
    }

    @Entao("^o sistema valida inclusao do diligenciamento com evento \"([^\"]*)\"$")
    public void o_sistema_valida_inclusao_do_diligenciamento_com_evento(String evento) {
        diligenciamentoOrdemDeServicoSteps.validarInclusaoDiligenciamento(evento);
    }

}
