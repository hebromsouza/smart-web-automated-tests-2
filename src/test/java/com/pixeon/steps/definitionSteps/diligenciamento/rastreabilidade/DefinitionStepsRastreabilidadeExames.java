package com.pixeon.steps.definitionSteps.diligenciamento.rastreabilidade;

import com.pixeon.steps.serenity.diligenciamento.rastreabilidade.RastreabilidadeExamesSteps;
import cucumber.api.java.pt.Então;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsRastreabilidadeExames {

    @Steps
    RastreabilidadeExamesSteps rastreabilidadeExamesSteps;

    @Então("^o sistema valida rastreabilidade do exame com número da amostra do item lançado$")
    public void o_sistema_valida_rastreabilidade_do_exame_com_numero_da_amostra_do_item_lancado() {
        rastreabilidadeExamesSteps.validarNumeroAmostraRastreabilidade();
    }

}
