package com.pixeon.steps.definitionSteps.intern;

import com.pixeon.steps.serenity.intern.RetirarPacienteInternacaoSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsRetirarPacienteInternacao {

    @Steps
    RetirarPacienteInternacaoSteps retirarPacienteInternacaoSteps;

    @Quando("^retirar paciente da visualização de internação$")
    public void retirar_paciente_da_visualização_de_internação() {
        retirarPacienteInternacaoSteps.retirarPacienteVisualizacaoInternacao();
    }

    @Então("^o sistema valida se existe o registro clínico de internação$")
    public void o_sistema_valida_se_existe_o_registro_clínico_de_internação() {
        retirarPacienteInternacaoSteps.validarExistenciaDeRegistroClinicoInternacao();
    }

    @Então("^valida o médico \"([^\"]*)\" do registro clínico$")
    public void valida_o_médico_do_registro_clínico(String medico) {
        retirarPacienteInternacaoSteps.validarExistenciaMedicoRegistroClinico(medico);
    }

}
