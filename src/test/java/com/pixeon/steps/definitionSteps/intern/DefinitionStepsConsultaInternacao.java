package com.pixeon.steps.definitionSteps.intern;

import com.pixeon.steps.serenity.intern.ConsultaInternacaoSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsConsultaInternacao {

    @Steps
    ConsultaInternacaoSteps consultaInternacaoSteps;

    @Quando("^abrir detalhes da internação/tratamento$")
    public void abrir_detalhes_da_internação_tratamento() {
        consultaInternacaoSteps.abrirDetalhesInternacaoTratamento();
    }

    @Então("^o sistema valida status aberto da internação$")
    public void o_sistema_valida_status_aberto_da_internação() {
        consultaInternacaoSteps.validarStatusInternacao();
    }

}
