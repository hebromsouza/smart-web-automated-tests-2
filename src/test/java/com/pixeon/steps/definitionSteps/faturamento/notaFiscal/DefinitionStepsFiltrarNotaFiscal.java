package com.pixeon.steps.definitionSteps.faturamento.notaFiscal;

import com.pixeon.steps.serenity.faturamento.notaFiscal.FiltrarNotaFiscalSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsFiltrarNotaFiscal {

    @Steps
    FiltrarNotaFiscalSteps filtrarNotaFiscalSteps;

    @Quando("^filtrar notas fiscais do dia$")
    public void filtrar_notas_fiscais_do_dia() {
        filtrarNotaFiscalSteps.filtrarNotas();
    }

    @Quando("^filtrar notas fiscais pelo numero$")
    public void filtrar_notas_fiscais_pelo_numero() {
        filtrarNotaFiscalSteps.filtrarNotasPeloNumero();
    }

    @Então("^valido se existe a nota fiscal gerada com status \"([^\"]*)\"$")
    public void valido_se_existe_a_nota_fiscal_gerada_com_status(String status) {
        filtrarNotaFiscalSteps.validarExistenciaNotaFiscal(status);
    }

}
