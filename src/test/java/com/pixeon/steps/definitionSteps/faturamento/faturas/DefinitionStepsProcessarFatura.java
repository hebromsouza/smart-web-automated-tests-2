package com.pixeon.steps.definitionSteps.faturamento.faturas;

import com.pixeon.steps.serenity.faturamento.faturas.ProcessarFaturaSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsProcessarFatura {

    @Steps
    ProcessarFaturaSteps processarFaturaSteps;

    @Quando("^processar fatura do dia selecionando empresa do convênio \"([^\"]*)\" convênio \"([^\"]*)\" contratado \"([^\"]*)\" com atendimentos do dia$")
    public void processar_fatura_do_dia_selecionando_empresa_do_convênio_convênio_contratado_com_atendimentos_do_dia(String empresaConvenio, String convenio, String contratado) {
        processarFaturaSteps.processarFaturaEmpresaConvenioContratato(empresaConvenio, convenio, contratado);
    }

    @Então("^valido processamento de fatura$")
    public void valido_processamento_de_fatura() {
        processarFaturaSteps.validarProcessamentoFatura();
    }

}
