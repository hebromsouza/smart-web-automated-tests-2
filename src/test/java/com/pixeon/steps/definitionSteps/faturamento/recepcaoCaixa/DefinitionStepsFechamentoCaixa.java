package com.pixeon.steps.definitionSteps.faturamento.recepcaoCaixa;

import com.pixeon.steps.serenity.faturamento.recepcaoCaixa.FechamentoCaixaSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsFechamentoCaixa {

    @Steps
    FechamentoCaixaSteps fechamentoCaixaSteps;

    @Quando("^efetuar fechamento de caixa$")
    public void efetuar_fechamento_de_caixa() {
        fechamentoCaixaSteps.fechamentoCaixa();
    }

    @Então("^valida se existe a ordem de servico$")
    public void valida_se_existe_a_ordem_de_servido() {
        fechamentoCaixaSteps.validarExistenciaOrdemDeServico();
    }

}
