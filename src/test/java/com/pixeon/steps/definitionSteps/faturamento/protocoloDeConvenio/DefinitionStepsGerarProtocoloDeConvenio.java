package com.pixeon.steps.definitionSteps.faturamento.protocoloDeConvenio;

import com.pixeon.steps.serenity.faturamento.protocoloDeConvenio.GerarProtocoloDeConvenioSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsGerarProtocoloDeConvenio {

    @Steps
    GerarProtocoloDeConvenioSteps gerarProtocoloDeConvenioSteps;

    @Quando("^buscar ordens de serviço com status \"([^\"]*)\"$")
    public void buscar_ordens_de_serviço_com_status(String status) {
        gerarProtocoloDeConvenioSteps.buscarOrdensDeServicoComStatus(status);
    }

    @Então("^verifico que apenas ordens de servico com o status \"([^\"]*)\" são exibidas$")
    public void verificarque_apenas_os_comstatus_saoexibidas(String status) {
        gerarProtocoloDeConvenioSteps.verificarque_apenas_os_comstatus_saoexibidas(status);
    }

    @Quando("^gerar protocolo de convênio$")
    public void gerar_protocolo_de_convênio() {
        gerarProtocoloDeConvenioSteps.gerarProtocoloDeConvenio();
    }

    @Quando("^receber lote de guias$")
    public void receber_lote_de_guias() {
        gerarProtocoloDeConvenioSteps.receberLoteDeGuias();
    }

    @Então("^pagar as OS aberta caso a mensagem \"([^\"]*)\" seja exibida$")
    public void pagar_os_status_aberta(String mensagem) {
        gerarProtocoloDeConvenioSteps.pagarOSComStatusAberta(mensagem);
    }

    @Quando("^buscar lotes do dia$")
    public void buscar_lotes_do_dia() {
        gerarProtocoloDeConvenioSteps.buscarLotesDoDia();
    }

    @Então("^valido que existe lotes encontrados$")
    public void valido_que_existe_lotes_encontrados() {
        gerarProtocoloDeConvenioSteps.validarExistenciaLoteGerado();
    }

    @Quando("^gerar protocolo$")
    public void gerar_protocolo() {
        gerarProtocoloDeConvenioSteps.gerarProtocolo();
    }

    @Então("^valida se existe o lote gerado$")
    public void valida_se_existe_o_lote_gerado() {
        gerarProtocoloDeConvenioSteps.validarExistenciaLoteGerado();
    }


    @Então("^visualizo as OS aberta, clico na OS e realizo o pagamento da OS$")
    public void visualizo_as_OS_aberta_clico_na_OS_e_realizo_o_pagamento_da_OS() {
        gerarProtocoloDeConvenioSteps.visualizo_as_OS_aberta_clico_na_OS_e_realizo_o_pagamento_da_OS();
    }

}
