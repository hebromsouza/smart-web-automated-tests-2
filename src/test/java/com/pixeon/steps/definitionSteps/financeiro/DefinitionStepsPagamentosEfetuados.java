package com.pixeon.steps.definitionSteps.financeiro;

import com.pixeon.steps.serenity.financeiro.PagamentosEfetuadosSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsPagamentosEfetuados {

    @Steps
    PagamentosEfetuadosSteps pagamentosEfetuadosSteps;

    @Quando("^buscar pagamentos efetuados com empresa \"([^\"]*)\"$")
    public void buscar_pagamentos_efetuados_com_empresa(String empresa) {
        pagamentosEfetuadosSteps.buscarPagamentosEfetuadosComEmpresa(empresa);
    }

    @Então("^o sistema apresenta pagamentos efetuados do compromisso gerado$")
    public void o_sistema_apresenta_pagamentos_efetuados_do_compromisso_gerado() {
        pagamentosEfetuadosSteps.validarPagamentosEfetuadosCompromissoGerado();
    }

    @Quando("^imprimir recibo do compromisso gerado$")
    public void imprimir_recibo_do_compromisso_gerado() {
        pagamentosEfetuadosSteps.imprimirReciboCompromissoGerado();
    }

    @Então("^o sistema apresenta o recibo de pagamentos efetuados do compromisso gerado$")
    public void o_sistema_apresenta_o_recibo_de_pagamentos_efetuados_do_compromisso_gerado() {
        pagamentosEfetuadosSteps.validarReciboDePagamentosEfetuadosCompromissoGerado();
    }

    @Quando("^cancelar compromisso em pagamentos efetuados$")
    public void cancelar_compromisso_em_pagamentos_efetuados() {
        pagamentosEfetuadosSteps.cancelarCompromisso();
    }

    @Então("^o compromisso gerado some da busca de pagamentos efetuados$")
    public void o_compromisso_gerado_some_da_busca_de_pagamentos_efetuados() {
        pagamentosEfetuadosSteps.validarInexistenciaCompromissoGerado();
    }

}