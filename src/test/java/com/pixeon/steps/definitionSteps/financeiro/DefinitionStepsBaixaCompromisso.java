package com.pixeon.steps.definitionSteps.financeiro;

import com.pixeon.datamodel.financeiro.DataBaixaCompromisso;
import com.pixeon.steps.serenity.financeiro.BaixaCompromissoSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsBaixaCompromisso {

    @Steps
    BaixaCompromissoSteps baixaCompromissoSteps;

    @Quando("^buscar baixa de compromisso com os dados$")
    public void buscar_baixa_de_compromisso_com_os_dados(List<DataBaixaCompromisso> dataBaixaCompromisso) {
        baixaCompromissoSteps.buscarBaixaCompromissoComDados(dataBaixaCompromisso);
    }

    @Então("^é apresentado o compromisso registrado que foi gerado$")
    public void é_apresentado_o_compromisso_registrado_que_foi_registrado_gerado() {
        baixaCompromissoSteps.validarExistenciaCompromissoRegistradoGerado();
    }

    @Então("^é apresentado o compromisso pago que foi gerado$")
    public void é_apresentado_o_compromisso_pago_que_foi_registrado_gerado() {
        baixaCompromissoSteps.validarExistenciaCompromissoPagoGerado();
    }

    @Quando("^pagar compromisso gerado$")
    public void pagar_compromisso_gerado() {
        baixaCompromissoSteps.pagarCompromissoGerado();
    }

    @Então("^o compromisso gerado some da busca de compromisso registrado$")
    public void o_compromisso_gerado_some_da_busca_de_compromisso_registrado() {
        baixaCompromissoSteps.validarInexistenciaCompromissoRegistradoGerado();
    }

}
