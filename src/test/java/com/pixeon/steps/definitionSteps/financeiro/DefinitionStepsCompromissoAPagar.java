package com.pixeon.steps.definitionSteps.financeiro;

import com.pixeon.datamodel.financeiro.DataCompromissoAPagar;
import com.pixeon.steps.serenity.financeiro.CompromissoAPagarSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsCompromissoAPagar {

    @Steps
    CompromissoAPagarSteps compromissoAPagarSteps;

    @Quando("^gravar compromisso a pagar preenchendo com dados$")
    public void gravar_compromisso_a_pagar_preenchendo_com_dados(List<DataCompromissoAPagar> dataCompromissoAPagar) {
        compromissoAPagarSteps.gravarCompromissoPreenchendoComDados(dataCompromissoAPagar);
    }

    @Quando("^informar valor total \"([^\"]*)\" com classe \"([^\"]*)\" do compromisso$")
    public void informar_valor_total_com_classe_do_compromisso(String valorTotal, String classe) {
        compromissoAPagarSteps.informarValorTotalComClasse(valorTotal, classe);
    }

    @Então("^é criado lançamento com valor \"([^\"]*)\" e vencimento para o mesmo dia$")
    public void é_criado_lançamento_com_valor_e_vencimento_para_o_mesmo_dia(String valor) {
        compromissoAPagarSteps.validarLancamentoComValorVencimentoMesmoDia(valor);
    }

}
