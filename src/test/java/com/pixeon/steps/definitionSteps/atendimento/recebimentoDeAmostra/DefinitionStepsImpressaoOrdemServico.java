package com.pixeon.steps.definitionSteps.atendimento.recebimentoDeAmostra;

import com.pixeon.steps.serenity.atendimento.recebimentoDeAmostra.ImpressaoOrdemServicoSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsImpressaoOrdemServico {

    @Steps
    ImpressaoOrdemServicoSteps impressaoOrdemServicoSteps;

    @Quando("^selecionar a ordem de serviço na tela de recebimento de amostra$")
    public void selecionar_a_ordem_de_serviço_na_tela_de_recebimento_de_amostra() {
        impressaoOrdemServicoSteps.selecionarOrdemServico();
    }


    @Entao("^o sistema valida impressao de ordem de servico pelo nome do paciente \"([^\"]*)\"$")
    public void o_sistema_valida_impressao_de_ordem_de_servico_pelo_nome_do_paciente(String nomePaciente) {
        impressaoOrdemServicoSteps.validarImpressaoOrdemServicoPeloNome(nomePaciente);
    }

}
