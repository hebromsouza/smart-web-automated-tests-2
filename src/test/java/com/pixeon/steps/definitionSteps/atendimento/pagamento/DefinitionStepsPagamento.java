package com.pixeon.steps.definitionSteps.atendimento.pagamento;

import com.pixeon.steps.serenity.atendimento.pagamento.PagamentoSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsPagamento {

    @Steps
    PagamentoSteps pagamentoSteps;

    @Quando("^concluir o pagamento da ordem de serviço$")
    public void concluir_o_pagamento_da_ordem_de_serviço() {
        pagamentoSteps.concluirPagamento();
    }

    @Quando("^concluir o pagamento da ordem de serviço com desconto de \"([^\"]*)\" e tipo de desconto \"([^\"]*)\"$")
    public void concluir_o_pagamento_da_ordem_de_serviço_com_desconto_de_e_tipo_de_desconto(String valor, String tpDesconto) {
        pagamentoSteps.concluirPagamentoAplicandoDesconto(valor, tpDesconto);
    }

    @Quando("^concluir o pagamento da ordem de serviço parcial de valor \"([^\"]*)\" e pagamento com forma de pagamento \"([^\"]*)\" e cartão \"([^\"]*)\" com vencimento \"([^\"]*)\" e validade \"([^\"]*)\"$")
    public void concluir_o_pagamento_da_ordem_de_serviço_parcial_de_valor_e_pagamento_com_forma_de_pagamento_e_cartão_com_vencimento_e_validade(String valor, String formaPagamento, String cartao, String vencimento, String validade) {
        pagamentoSteps.concluirPagamentoComValorParcial(valor, formaPagamento, cartao, vencimento, validade);
    }

    @Quando("^concluir o pagamento da ordem de serviço para pessoa \"([^\"]*)\" com CPF/CNPJ \"([^\"]*)\"$")
    public void concluir_o_pagamento_da_ordem_de_serviço_para_pessoa_com_registro(String tipoPessoa, String cpfCnpj) {
        pagamentoSteps.concluirPagamentoComPacienteNaoResponsavel(tipoPessoa, cpfCnpj);
    }

    @Quando("^selecionar a opção de pagamento$")
    public void selecionar_a_opção_de_pagamento() {
        pagamentoSteps.selecionarOpcaoPagamento();
    }

    @Quando("^pagar a ordem de serviço$")
    public void pagar_a_ordem_de_serviço() {
        pagamentoSteps.pagarOrdemDeServico();
    }

    @Quando("^selecionar a forma de pagamento \"([^\"]*)\"$")
    public void selecionar_a_forma_de_pagamento(String formaPagamento) {
        pagamentoSteps.selecionarFormaPagamento(formaPagamento);
    }

    @Quando("^selecionar bandeira do cartão \"([^\"]*)\"$")
    public void selecionar_bandeira_do_cartão(String bandeiraCartao) {
        pagamentoSteps.selecionarBandeiraCartao(bandeiraCartao);
    }

    @Quando("^preencher dados do cartão com vencimento \"([^\"]*)\" e validade \"([^\"]*)\"$")
    public void preencher_dados_do_cartão_com_vencimento_e_validade(String vencimento, String validade) {
        pagamentoSteps.preencherDadosCartao(vencimento, validade);
    }

    @Quando("^infomrar quantidade de parcelas \"([^\"]*)\" do pagamento$")
    public void infomrar_quantidade_de_parcelas_do_pagamento(String parcelas) {
        pagamentoSteps.informarQuantidadeParcelas(parcelas);
    }

    @Quando("^parcelar em \"([^\"]*)\" vezes preenchendo dados do cartão com vencimento \"([^\"]*)\" e validade \"([^\"]*)\"$")
    public void parcelar_em_vezes_preenchendo_dados_do_cartão_com_vencimento_e_validade(String quantidadeParcelas, String vencimento, String validade) {
        pagamentoSteps.parcelarPagamentoPreencherDadosCartao(quantidadeParcelas, vencimento, validade);
    }

    @Quando("^gravar o pagamento$")
    public void gravar_o_pagamento() {
        pagamentoSteps.gravarPagamento();
    }

    @Quando("^iniciar pagamento de ordem de serviço$")
    public void iniciar_pagamento_de_ordem_de_serviço() {
        pagamentoSteps.iniciarPagamentoOrdemDeServico();
    }


    @Quando("^selecionar a opção de pagamento a receber$")
    public void selecionar_a_opção_de_pagamento_a_receber() {
        pagamentoSteps.selecionarFormaPagamento("A Receber");
    }

    @Quando("^preencher observação de forma de pagamento \"([^\"]*)\"$")
    public void preencher_observação_de_forma_de_pagamento(String observacaoFormaPagamento) {
        pagamentoSteps.preencherObservacaoFormaPagamento(observacaoFormaPagamento);
    }

    @Quando("^fechar tela de pagamento$")
    public void fechar_tela_de_pagamento() {
        pagamentoSteps.fecharTelaPagamento();
    }

    @Quando("^abrir tela de pagamento$")
    public void abrir_tela_de_pagamento() {
        pagamentoSteps.abrirTelaPagamento();
    }

    @Quando("^abrir tela de pagamento a receber$")
    public void abrir_tela_de_pagamento_a_receber() {
        pagamentoSteps.abrirTelaPagamentoAReceber();
    }

    @Quando("^receber pagamento em cheque com numero do banco \"([^\"]*)\" numero da agencia \"([^\"]*)\" numero da conta \"([^\"]*)\" numero do cheque \"([^\"]*)\" e tipo praça \"([^\"]*)\"$")
    public void receber_pagamento_em_cheque_com_numero_do_banco_numero_da_agencia_numero_da_conta_numero_do_cheque_e_tipo_praça(String numBanco, String numAgencia, String numConta, String numCheque, String praca) {
        pagamentoSteps.receberPagamentoEmCheque(numBanco, numAgencia, numConta, numCheque, praca);
    }

    @Quando("^informar responsavel \"([^\"]*)\"$")
    public void informar_responsavel(String responsavel) {
        pagamentoSteps.informarResponsavel(responsavel);
    }

    @Quando("^imprimir recibo$")
    public void imprimir_recibo() {
        pagamentoSteps.imprimirRecibo();
    }

    @Entao("^o sistema valida o valor das parcelas com quantidade \"([^\"]*)\"$")
    public void o_sistema_valida_o_valor_das_parcelas_com_quantidade(int quantidadeParcelas) {
        pagamentoSteps.validarValorParcelas(quantidadeParcelas);
    }

    @Então("^o sistema deve exibir status de ordem de serviço faturada$")
    public void o_sistema_deve_exibir_status_de_ordem_de_serviço_faturada() {
        pagamentoSteps.validarSeItemDeOrdemDeServicoFoiFaturada();
    }

    @Então("^valido se o sistema aplicou o desconto$")
    public void valido_se_o_sistema_aplicou_o_desconto() {
        pagamentoSteps.validarAplicacaoDeDesconto();
    }

    @Então("^valido se o sistema aplicou o valor parcial correto$")
    public void valido_se_o_sistema_aplicou_o_valor_parcial_correto() {
        pagamentoSteps.validarValorParcial();
    }

    @Então("^valido se o sistema gravou o pagamento$")
    public void valido_se_o_sistema_gravou_o_pagamento() {
        pagamentoSteps.validarPagamento();
    }

    @Então("^o sistema valida se foi realizado o recebimento de pagamento$")
    public void o_sistema_valida_se_foi_realizado_o_recebimento_de_pagamento() {
        pagamentoSteps.validarRecebimentoDePagamento();
    }

    @Quando("^abrir tela de depósito$")
    public void abrir_tela_de_depósito() {
        pagamentoSteps.abrirTelaDeposito();
    }


    @Quando("^realizar depósito com valor \"([^\"]*)\" menor da ordem de serviço$")
    public void realizar_depósito_com_valor_menor_da_ordem_de_serviço(String valor) {
        pagamentoSteps.depositoComValorMenorDaOrdemDeServico(valor);
    }

    @Então("^o sistema valida o valor do depósito \"([^\"]*)\"$")
    public void o_sistema_valida_o_valor_do_depósito(String valorDeposito) {
        pagamentoSteps.validarValorDeposito(valorDeposito);
    }

    @Quando("^adicionar nova parcela$")
    public void adicionar_nova_parcela() {
        pagamentoSteps.adicionarNovaParcela();
    }

    @Quando("^pagar a ordem de serviço gerada$")
    public void  pagar_a_ordem_de_serviço_gerada() {
        pagamentoSteps.concluirPagamento();
    }

    @Quando("^pagar a ordem de serviço gerada sem selecionar a forma de pagamento$")
    public void pagar_a_ordem_de_serviço_gerada_semforma_pagamento() {
        pagamentoSteps.concluirPagamentoSemFormaPagamento();
    }


    @Quando("^pagar a ordem de serviço gerando nota fiscal$")
    public void pagar_a_ordem_de_serviço_gerando_nota_fiscal() {
        pagamentoSteps.pagamentoOrdemDeServicoGerandoNotaFiscal();
    }

    @Então("^o sistema valida valor da nova parcela$")
    public void o_sistema_valida_valor_da_nova_parcela() {
        pagamentoSteps.validarValorNovaParcela();
    }

    @Então("^validar impressão de recibo$")
    public void validar_impressão_de_recibo() {
        pagamentoSteps.validarImpressaoRecibo();
    }

    @Então("^validar geração de nota fiscal com valor da ordem de serviço$")
    public void validar_geração_de_nota_fiscal_com_valor_da_ordem_de_serviço() {
        pagamentoSteps.validarGeracaoNotaFiscalComValorDaOrdemDeServico();
    }

    @Então("^validar exclusão de nota fiscal$")
    public void validar_exclusão_de_nota_fiscal() {
        pagamentoSteps.validarExclusaoNotaFiscal();
    }

}