package com.pixeon.steps.definitionSteps.atendimento.ordemDeServico;

import com.pixeon.datamodel.login.DataLogin;
import com.pixeon.steps.serenity.atendimento.ordemDeServico.LancamentoDeOrdemDeServicoSteps;
import com.pixeon.steps.serenity.home.HomeSteps;
import com.pixeon.steps.serenity.login.login.LoginSteps;
import com.pixeon.steps.serenity.menu.MenuSteps;
import com.pixeon.steps.serenity.paciente.PacienteSteps;
import cucumber.api.java.pt.Dado;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsLoginELancamentoDeOrdemDeServicoPadrao {

    @Steps
    LoginSteps loginSteps;
    @Steps
    HomeSteps homeSteps;
    @Steps
    MenuSteps menuSteps;
    @Steps
    PacienteSteps pacienteSteps;
    @Steps
    LancamentoDeOrdemDeServicoSteps lancamentoDeOrdemDeServicoSteps;

    @Dado("^que estou logado no sistema smart web$")
    public void que_estou_logado_no_sistema_smart_web(List<DataLogin> dataLogin) {
        loginSteps.abreSmartWeb();
        loginSteps.preencherLoginESenha(dataLogin);
        loginSteps.realizaLogin();
        loginSteps.solicitaPreenchimentoRecepcao();
        loginSteps.selecionaRecepcao("ADM CENTRO MEDICO");
        homeSteps.realizaLOginSucesso();
    }

    @Dado("^que estou logado no sistema smart web sem selecionar recapcao$")
    public void que_estou_logado_no_sistema_smart_web_sem_selecionar_recapcao(List<DataLogin> dataLogin) {
        loginSteps.abreSmartWeb();
        loginSteps.preencherLoginESenha(dataLogin);
        loginSteps.realizaLogin();
        homeSteps.realizaLoginSucessoCarregandoTelaInicial();
    }

    @Dado("^que faço a inclusão de item em uma nova ordem de serviço$")
    public void que_faço_a_inclusão_de_item_em_uma_nova_ordem_de_serviço() {
        menuSteps.selecionarMenu("Atendimento","Lançamento de Guias");
        pacienteSteps.buscarPacienteComNumeroDeRegisto("261");
        lancamentoDeOrdemDeServicoSteps.incluirNovaOrdemDeServico();
        lancamentoDeOrdemDeServicoSteps.lancarItemServicoComInstrucao("GLICO");
    }

}
