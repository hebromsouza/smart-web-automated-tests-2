package com.pixeon.steps.definitionSteps.atendimento.etiqueta;

import com.pixeon.steps.serenity.atendimento.etiqueta.ImpressaoEtiquetaSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsImpressaoEtiqueta {

    @Steps
    ImpressaoEtiquetaSteps impressaoEtiquetaSteps;

    @Quando("^selecionar tipo de impressão \"([^\"]*)\"$")
    public void selecionar_tipo_de_impressão(String tipoImpressao) {
        impressaoEtiquetaSteps.selecionarTipoImpressao(tipoImpressao);
    }

    @Entao("^o sistema valida quantidade de etiqueta \"([^\"]*)\"$")
    public void o_sistema_valida_quantidade_de_etiqueta(String quantidade) {
        impressaoEtiquetaSteps.validaQuantidadeEtiqueta(quantidade);
    }

    @Quando("^inserir a quantidade de impressão de etiqueta \"([^\"]*)\"$")
    public void inserir_a_quantidade_de_impressão_de_etiqueta(String quantidadeEtiqueta) {
        impressaoEtiquetaSteps.inserirQuantidadeImpressaoEtiqueta(quantidadeEtiqueta);
    }

    @Quando("^imprimir a etiqueta selecionando servidor de impressao \"([^\"]*)\"$")
    public void imprimir_a_etiqueta_selecionando_servidor_de_impressao(String servImp) {
        impressaoEtiquetaSteps.imprimirEtiqueta(servImp);
    }

    @Entao("^o sistema valida o conteudo da etiqueta \"([^\"]*)\"$")
    public void o_sistema_valida_o_conteudo_da_etiqueta(String caminhoEtiqueta) {
        impressaoEtiquetaSteps.validaConteudoEtiqueta(caminhoEtiqueta);
    }

    @Entao("^o sistema valida o status da etiqueta \"([^\"]*)\" com caminho do arquivo \"([^\"]*)\"$")
    public void o_sistema_valida_o_status_da_etiqueta_com_caminho_do_arquivo(String status, String caminhoEtiqueta) {
        impressaoEtiquetaSteps.validaStatusEtiquetaComCaminhoArquivo(status, caminhoEtiqueta);
    }

}