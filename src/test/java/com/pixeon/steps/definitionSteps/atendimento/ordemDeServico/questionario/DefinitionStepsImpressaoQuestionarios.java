package com.pixeon.steps.definitionSteps.atendimento.ordemDeServico.questionario;

import com.pixeon.steps.serenity.atendimento.ordemDeServico.questionario.ImpressaoQuestionariosSteps;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsImpressaoQuestionarios {

    @Steps
    ImpressaoQuestionariosSteps impressaoQuestionariosSteps;

    @Dado("^responder \"([^\"]*)\" questionários com tipo \"([^\"]*)\"$")
    public void responder_questionários_com_tipo(String quantidade, String tipoQuestionario) {
        impressaoQuestionariosSteps.responderQuestionariosComTipo(quantidade, tipoQuestionario);
    }

    @Quando("^imprimir questionários da ordem de serviço$")
    public void imprimir_questionários_da_ordem_de_serviço() {
        impressaoQuestionariosSteps.imprimirQuestionarioOrdemDeServico();
    }

    @Então("^é exibido \"([^\"]*)\" questionários na impressão$")
    public void é_exibido_questionários_na_impressão(String quantidade) {
        impressaoQuestionariosSteps.validarImpressaoDeQuestionarios();
    }

}
