package com.pixeon.steps.definitionSteps.atendimento.ordemDeServico;

import com.pixeon.steps.serenity.atendimento.ordemDeServico.EditarItensOrdemDeServicoSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsEditarItensOrdemDeServico {

    @Steps
    EditarItensOrdemDeServicoSteps editarItensOrdemDeServicoSteps;

    @Quando("^o item for selecionado$")
    public void o_item_for_selecionado() {
        editarItensOrdemDeServicoSteps.selecionarItem();
    }


    @Quando("^selecionar a ação do item \"([^\"]*)\"$")
    public void selecionar_a_ação_do_item(String acaoItem) {
        editarItensOrdemDeServicoSteps.acaoItem(acaoItem);
    }

    @Quando("^selecionar os itens e concluir$")
    public void selecionarItensEConcluir() {
        editarItensOrdemDeServicoSteps.selecionarItem();
    }

    @Quando("^selecionar tipo de cancelamento \"([^\"]*)\" e preencher motivo de cancelamento \"([^\"]*)\"$")
    public void selecionar_tipo_de_cancelamento_preencher_motivo_cancelamento(String tipoCancelamento, String motivoCancelamento) {
        editarItensOrdemDeServicoSteps.motivoCancelamento(tipoCancelamento, motivoCancelamento);
    }

    @Quando("^incluir uma data anterior a data da ordem de serviço ou anterior a data da coleta$")
    public void incluir_uma_data_anterior_a_data_da_ordem_de_serviço_ou_anterior_a_data_da_coleta() {
        editarItensOrdemDeServicoSteps.incluirDataAnterior();
    }

    @Quando("^incluir a data da coleta ou data do resultado$")
    public void incluir_a_data_da_coleta_ou_data_do_resultado() {
        editarItensOrdemDeServicoSteps.incluirDataColetaResultado();
    }

    @Quando("^buscar o setor executante \"([^\"]*)\"$")
    public void buscar_o_setor_executante(String setorExecutante) {
        editarItensOrdemDeServicoSteps.buscarSetor(setorExecutante);
    }

    @Quando("^selecionar o setor$")
    public void selecionar_o_setor() {
        editarItensOrdemDeServicoSteps.selecionarSetor();
    }

    @Quando("^aceitar o alerta da alteração$")
    public void aceitar_o_alerta_da_alteração() {
        editarItensOrdemDeServicoSteps.aceitarAlerta();
    }

    @Quando("^abrir tela de detalhes do item$")
    public void abrir_tela_de_detalhes_do_item() {
        editarItensOrdemDeServicoSteps.abrirTelaDetalhes();
    }

    @Quando("^abrir tela de detalhes do item \"([^\"]*)\"$")
    public void abrir_tela_de_detalhes_do_item(String itemDesc) {
        editarItensOrdemDeServicoSteps.abrirTelaDetalhes(itemDesc);
    }

    @Quando("^concluir lançamento do item aceitando erro de autorização$")
    public void concluir_lançamento_do_item_aceitando_erro_de_autorização() throws InterruptedException {
        editarItensOrdemDeServicoSteps.concluirLancamentoDoItem();
    }

    @Quando("^concluir lançamento do item$")
    public void concluir_lançamento_do_item() throws InterruptedException {
        editarItensOrdemDeServicoSteps.concluirInclusaoItem();
    }

    @Quando("^concluir lançamento do item sem selecionar$")
    public void concluir_lançamento_do_item_sem_selecionar() throws InterruptedException {
        editarItensOrdemDeServicoSteps.concluirInclusaoItemSemSelecionarItens();
    }

    @Quando("^clicar em novo lançamento de itens$")
    public void clicar_em_novo_lançamento_de_itens() {
        editarItensOrdemDeServicoSteps.novoLancamentoItens();
    }

    @Quando("^confirmar cancelamento no webservice$")
    public void confirmar_cancelamento_no_webservice() {
        editarItensOrdemDeServicoSteps.confirmarCancelamentoWebService();
    }

    @Então("^o sistema valida se o item foi cancelado$")
    public void o_sistema_valida_se_o_item_foi_cancelado() {
        editarItensOrdemDeServicoSteps.validarItemCancelado();
    }

    @Então("^o sistema valida a ação do item \"([^\"]*)\"$")
    public void o_sistema_valida_a_ação_do_item(String acaoItem) {
        editarItensOrdemDeServicoSteps.validarAcaoItem(acaoItem);
    }

    @Então("^o sistema apresenta uma mensagem de erro pela ação do item \"([^\"]*)\"$")
    public void o_sistema_apresenta_uma_mensagem_de_erro_pela_ação_do_item(String acaoItem) {
        editarItensOrdemDeServicoSteps.validarAlertaDataAnterior(acaoItem);
    }

    @Então("^o sistema valida se foi feito a alteração do setor executante \"([^\"]*)\"$")
    public void o_sistema_valida_se_foi_feito_a_alteração_do_setor_executante(String setorExecutante) {
        editarItensOrdemDeServicoSteps.validarAlteracaoSetorExecutante(setorExecutante);
    }

    @Então("^o sistema valida cancelamento da\\(s\\) guia\\(s\\) pela ação do item \"([^\"]*)\"$")
    public void o_sistema_valida_cancelamento_da_s_guia_s_pela_ação_do_item(String acaoItem) {
        editarItensOrdemDeServicoSteps.validarCancelamentoGuia(acaoItem);
    }

    @Então("^o sistema valida a mensagem informativa que nao ha guia a ser cancelada$")
    public void o_sistema_valida_mensagem_informativa_naoaguia_a_ser_cancelada() {
        editarItensOrdemDeServicoSteps.validarMensagemNaoTemGuia();
    }

    @Quando("^alterar a quantidade do item para \"([^\"]*)\"$")
    public void alterar_a_quantidade_do_item_para(String quantidadeItem) {
        editarItensOrdemDeServicoSteps.alterarQuantidadeItem(quantidadeItem);
    }

    @Então("^verifico que o preco nao pode ser alterado nesta etapa$")
    public void verifico_preco_nao_pode_ser_alterado() {
        editarItensOrdemDeServicoSteps.validarLinkNaoPodeAlterarPreco();
    }

    @Então("^o valor do item é alterado de acordo com a quantidade$")
    public void o_valor_do_item_é_alterado_de_acordo_com_a_quantidade() {
        editarItensOrdemDeServicoSteps.validarValor();
    }



    @Então("^o link de alteração de quantidade do item estará desabilitado$")
    public void o_link_de_alteração_de_quantidade_do_item_estará_desabilitado() {
        editarItensOrdemDeServicoSteps.alteracaoQuantidadeDesabilitado();
    }

    @Então("^o link de alteração do valor do item estará desabilitado$")
    public void o_link_de_alteração_do_valor_do_item_estará_desabilitado() {
        editarItensOrdemDeServicoSteps.alterarValorItemDesabilitado();
    }

    @Então("^o sistema apresenta valor total com desconto informado no orçamento$")
    public void o_sistema_apresenta_valor_total_com_desconto_informado_no_orçamento() {
        editarItensOrdemDeServicoSteps.validarValorItemComDescontoAplicadoNoOrcamento();
    }

}
