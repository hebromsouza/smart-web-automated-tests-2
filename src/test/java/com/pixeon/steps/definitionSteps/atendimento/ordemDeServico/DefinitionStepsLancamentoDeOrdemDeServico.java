package com.pixeon.steps.definitionSteps.atendimento.ordemDeServico;

import com.pixeon.datamodel.ordemDeServico.DataOrdemDeServico;
import com.pixeon.steps.serenity.atendimento.ordemDeServico.LancamentoDeOrdemDeServicoSteps;
import com.pixeon.steps.serenity.menu.MenuSteps;
import com.pixeon.steps.serenity.paciente.PacienteSteps;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsLancamentoDeOrdemDeServico {

    @Steps
    LancamentoDeOrdemDeServicoSteps lancamentoDeOrdemDeServicoSteps;

    @Steps
    MenuSteps menuSteps;

    @Steps
    PacienteSteps pacienteSteps;

    @Dado("^criar nova ordem de serviço selecionando código do convênio \"([^\"]*)\"$")
    public void criar_nova_ordem_de_serviço_selecionando_código_do_convênio(String codigoConvenio) {
        lancamentoDeOrdemDeServicoSteps.criarNovaOrdemDeServicoSelecionandoConvenio(codigoConvenio);
    }

    @Dado("^acessar a tela de ordem de serviço$")
    public void acessar_a_tela_de_ordem_de_serviço() {}

    @Dado("^criar uma nova ordem de serviço incluindo o item \"([^\"]*)\"$")
    public void criar_uma_nova_ordem_de_serviço_incluindo_o_item(String item) throws InterruptedException {
        lancamentoDeOrdemDeServicoSteps.novaOrdemServicoIcluindoItem(item);
    }

    @Dado("^criar nova ordem de serviço sem gravar$")
    public void criar_nova_ordem_de_serviço_sem_gravar() {
        pacienteSteps.criar_uma_nova_ordem_de_serviço_sem_gravar();
    }

    @Quando("^criar nova ordem de serviço$")
    public void criar_nova_ordem_de_serviço() {
        lancamentoDeOrdemDeServicoSteps.criarNovaOrdemDeServico();
    }

    @Quando("^preencher dados da ordem de serviço$")
    public void preencher_dados_da_ordem_de_serviço(List<DataOrdemDeServico> dataOrdemDeServico) {
        pacienteSteps.preencher_dados_da_ordem_de_serviço(dataOrdemDeServico);
    }

    @Então("^o sistema valida se foi criado a nova ordem de serviço com plano \"([^\"]*)\" selecionado$")
    public void o_sistema_valida_se_foi_criado_a_nova_ordem_de_serviço_com_plano_selecionado(String plano) {
        lancamentoDeOrdemDeServicoSteps.validarNovaOrdemDeServico(plano);
    }

    @Entao("^o sistema valida herança de setor solicitante na ordem de serviço de acordo com setor logado \"([^\"]*)\"$")
    public void o_sistema_valida_herança_de_setor_solicitante_na_ordem_de_serviço_de_acordo_com_setor_logado(String setor) {
        lancamentoDeOrdemDeServicoSteps.validarHerancaSetorSolicitante(setor);
    }

    @Quando("^gerar \"([^\"]*)\" ordens de serviço com item \"([^\"]*)\" para paciente de registro \"([^\"]*)\"$")
    public void gerarOrdensDeServiçoComItemParaPacienteDeRegistro(int quantidade, String item, String registroPaciente) throws InterruptedException {
        menuSteps.selecionarMenu("Atendimento", "Lançamento de Guias");
        pacienteSteps.buscarPacienteComNumeroDeRegisto(registroPaciente);

     //lancamentoDeOrdemDeServicoSteps.criarNovaOrdemDeServico(quantidade, item);
    }

    @Quando("^clicar no botão nova ordem de serviço$")
    public void clicar_no_botão_nova_ordem_de_serviço() {
        lancamentoDeOrdemDeServicoSteps.clicarBotaoNovaOrdemDeServico();
    }

    @Quando("^informar o cpf_cnpj \"([^\"]*)\" do prestador de serviço$")
    public void informar_o_cpf_cnpj_do_prestador_de_serviço(String cpfCnpj) {
        lancamentoDeOrdemDeServicoSteps.informarCpfCnpjPrestadorServico(cpfCnpj);
    }
    @Quando("^informar o cpf_cnpj \"([^\"]*)\"$")
    public void informar_o_cpf_cnpj(String cpfCnpj) {
        pacienteSteps.informar_o_cpf_cnpj(cpfCnpj);
    }

    @Quando("^informo o motivo de alteracao$")
    public void informo_motivo_alteracao() {
        pacienteSteps.informo_motivo_alteracao();
    }


    @Quando("^gravar ordem de serviço$")
    public void gravar_ordem_de_serviço() {
        lancamentoDeOrdemDeServicoSteps.gravarOrdemDeServico();
    }

    @Então("^é apresentado a mensagem \"([^\"]*)\" de alerta como popup na tela de ordem de serviço$")
    public void é_apresentado_a_mensagem_de_alerta_como_popup_na_tela_de_ordem_de_serviço(String mensagem) {
        lancamentoDeOrdemDeServicoSteps.validarMensagemPopUp(mensagem);
    }

    @Então("^é apresentado a mensagem \"([^\"]*)\" de alerta$")
    public void é_apresentado_a_mensagem_de_alerta(String mensagem) {
        pacienteSteps.é_apresentado_a_mensagem_de_alerta(mensagem);
    }

    @Então("^o sistema valida inclusão do cpf_cnpj \"([^\"]*)\" do prestador na tela de ordem de serviço$")
    public void o_sistema_valida_inclusão_do_cpf_cnpj_do_prestador_na_tela_de_ordem_de_serviço(String cpfCnpj) {
        lancamentoDeOrdemDeServicoSteps.validarInclusaoCpfCnpjPrestador(cpfCnpj);
    }

    @Quando("^acessar tela de itens da ordem de serviço pelo link de editar itens no lançamento$")
    public void acessar_tela_de_itens_da_ordem_de_serviço_pelo_link_de_editar_itens_no_lançamento() {
        lancamentoDeOrdemDeServicoSteps.acessarItensDaOrdemDeServico();
    }

}
