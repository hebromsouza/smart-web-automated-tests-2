package com.pixeon.steps.definitionSteps.atendimento.filaDeEspera;

import com.pixeon.steps.serenity.atendimento.filaDeEspera.FilaDeEsperaSteps;
import com.pixeon.steps.serenity.menu.MenuSteps;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsFilaDeEspera {

    @Steps
    FilaDeEsperaSteps filaDeEsperaSteps;
    @Steps
    MenuSteps menuSteps;


    @Dado("^que acesso a \"([^\"]*)\" no menu$")
    public void acessar_menu_fila_espera(String filaEspera) {
        filaDeEsperaSteps.menuFilaEspera(filaEspera);
    }


    @Quando("^acessar a tela de fila de espera$")
    public void acessar_a_tela_de_fila_de_espera() {
        filaDeEsperaSteps.abrirFilaDeEspera();
    }

    @Quando("^adicionar na fila$")
    public void adicionar_na_fila() {
        filaDeEsperaSteps.adicionarNaFila();
    }

    @Quando("^buscar fila de espera \"([^\"]*)\"$")
    public void buscar_fila_de_espera(String filaMedico) {
        filaDeEsperaSteps.buscarFilaDeEspera(filaMedico);
    }

    @Quando("^selecionar a opção de adicionar na frente do paciente \"([^\"]*)\"$")
    public void selecionar_a_opção_de_adicionar_na_frente_do_paciente(String paciente) {
        filaDeEsperaSteps.selecionarOpcaoAdicionarNaFrenteDoPaciente(paciente);
    }

    @Quando("^gravar inclusão do paciente na fila de espera \"([^\"]*)\" com recepção \"([^\"]*)\"$")
    public void gravar_inclusão_do_paciente_na_fila_de_espera_com_recepção(String filaMedico, String recepcao) {
        filaDeEsperaSteps.gravarInclusaoPacienteFilaEsperaComRecepcao(filaMedico, recepcao);
    }

    @Então("^o sistema valida inclusão de paciente \"([^\"]*)\" na fila de espera$")
    public void o_sistema_valida_inclusão_de_paciente_na_fila_de_espera(String paciente) {
        filaDeEsperaSteps.validarInclusaoDePacienteNaFilaDeEspera(paciente);
    }

    @Então("^o sistema valida inclusão de paciente \"([^\"]*)\" no final da fila de espera$")
    public void o_sistema_valida_inclusão_de_paciente_no_final_da_fila_de_espera(String paciente) {
        filaDeEsperaSteps.validarInclusaoPacienteFinalFilaEspera(paciente);
    }

    @Então("^a senha retirada aparece na fila de espera do médico \"([^\"]*)\"$")
    public void a_senha_retirada_aparece_na_fila_de_espera_do_médico(String medico) {
        filaDeEsperaSteps.validarInclusaoDeSenhaNaFilaDeEspera(medico);
    }

    @Então("^é informado a quantidade de pacientes na fila de espera do médico$")
    public void é_informado_a_quantidade_de_pacientes_na_fila_de_espera_do_médico() {
        filaDeEsperaSteps.validarQuantidadePacientesFilaDeEspera();
    }

    @Quando("^enviar mensagem para senha retirada na fila de espera$")
    public void enviar_mensagem_para_senha_retirada_na_fila_de_espera() {
        filaDeEsperaSteps.enviarMensagemSenhaRetiradaFilaDeEspera();
    }

    @Quando("^cancelar senha do paciente na fila de espera$")
    public void cancelar_senha_do_paciente_na_fila_de_espera() {
        filaDeEsperaSteps.cancelarSenhaDoPacienteFilaDeEspera();
    }

    @Então("^a senha do paciente sai da fila do médico$")
    public void a_senha_do_paciente_sai_da_fila_do_médico() {
        filaDeEsperaSteps.validarExclusaoSenhaDoPacienteFilaDeEspera();
    }

    @Então("^o sistema mostra a quantidade de pacientes na fila de espera do médico$")
    public void o_sistema_mostra_a_quantidade_de_pacientes_na_fila_de_espera_do_médico() {
        filaDeEsperaSteps.validarQuantidadePacientesFilaDeEsperaDoMedico();
    }

    @Então("^todas as senhas serão canceladas$")
    public void todas_as_senhas_serão_canceladas() {
        filaDeEsperaSteps.cancelarSenhasDaFilaDeEsperaDoMedico();
    }

    @Quando("^chamar próxima senha$")
    public void chamar_próxima_senha() {
        filaDeEsperaSteps.chamarProximaSenha();
    }

    //Verificar no smart.ini a parametrização, na seção WEB -> FLE_OPCOES=S está ativada
    @Quando("^acessar fila de espera do médico \"([^\"]*)\"$")
    public void acessar_fila_de_espera_do_médico(String medico) {
        menuSteps.selecionarMenu("Atendimento", "Lançamento de Guias");
        filaDeEsperaSteps.acessarFilaEsperaMedico(medico);
    }

    @Quando("^enviar mensagem para paciente \"([^\"]*)\" na fila de espera$")
    public void enviar_mensagem_para_paciente_na_fila_de_espera(String paciente) {
        filaDeEsperaSteps.enviarMensagemPacienteFilaDeEspera(paciente);
    }

}
