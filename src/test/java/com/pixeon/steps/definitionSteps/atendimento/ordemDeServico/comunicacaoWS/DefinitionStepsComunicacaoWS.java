package com.pixeon.steps.definitionSteps.atendimento.ordemDeServico.comunicacaoWS;

import com.pixeon.steps.serenity.atendimento.ordemDeServico.comunicacaoWS.ComunicacaoWSSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsComunicacaoWS {

    @Steps
    ComunicacaoWSSteps comunicacaoWSSteps;

    @Quando("^abrir o xml de envio$")
    public void abrir_o_xml_de_envio() {
        comunicacaoWSSteps.abrirXmlEnvio();
    }

    @Quando("^abrir o xml de retorno$")
    public void abrir_o_xml_de_retorno() {
        comunicacaoWSSteps.abrirXmlRetorno();
    }

    @Entao("^o sistema valida conexão com o web service do laboratório \"([^\"]*)\"$")
    public void o_sistema_valida_conexão_com_o_web_service_do_laboratório(String wsLaboratorio) {
        comunicacaoWSSteps.validarConexaoWebServiceLaboratorio(wsLaboratorio);
    }

    @Entao("^o sistema valida xml de envio$")
    public void o_sistema_valida_xml_de_envio() {
        comunicacaoWSSteps.validarXmlEnvio();
    }

    @Entao("^o sistema valida xml de retorno$")
    public void o_sistema_valida_xml_de_retorno() {
        comunicacaoWSSteps.validarXmlRetorno();
    }

}
