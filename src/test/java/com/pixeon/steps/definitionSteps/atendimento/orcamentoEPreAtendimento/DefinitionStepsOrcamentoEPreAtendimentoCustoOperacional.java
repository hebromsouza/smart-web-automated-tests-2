package com.pixeon.steps.definitionSteps.atendimento.orcamentoEPreAtendimento;

import com.pixeon.steps.serenity.atendimento.orcamentoEPreAtendimento.OrcamentoEPreAtendimentoCustoOperacionalSteps;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsOrcamentoEPreAtendimentoCustoOperacional {

    @Steps
    OrcamentoEPreAtendimentoCustoOperacionalSteps orcamentoEPreAtendimentoCustoOperacionalSteps;

    @Dado("^incluir o convenio \"([^\"]*)\"$")
    public void incluir_o_convenio(String convenio) {
        orcamentoEPreAtendimentoCustoOperacionalSteps.incluirConvenio(convenio);
    }

    @Quando("^gravar o orcamento e pre atendimento")
    public void gravar_o_orcamento_e_pre_atendimento() {
        orcamentoEPreAtendimentoCustoOperacionalSteps.gravarOrcamentoEPreAtendimento();
    }

    @Quando("^inserir item \"([^\"]*)\" com coper$")
    public void inserir_item_com_coper(String item) {
        orcamentoEPreAtendimentoCustoOperacionalSteps.inserirItemComCoper(item);
    }

    @Entao("^o sistema valida inclusao de item de custo operacional \"([^\"]*)\"$")
    public void o_sistema_valida_inclusao_de_item_de_custo_operacional(String coper) {
        orcamentoEPreAtendimentoCustoOperacionalSteps.validarInclusaoItemCustoOperacional(coper);
    }

    @Entao("^valor do custo operacional \"([^\"]*)\" com cotacao \"([^\"]*)\" do convenio \"([^\"]*)\"$")
    public void valor_do_custo_operacional_com_cotacao_do_convenio(Float custoOperacional, Float cotacao, String convenio) {
        orcamentoEPreAtendimentoCustoOperacionalSteps.validarCustoOperacionalComCotacaoConvenio(custoOperacional, cotacao, convenio);
    }
    @Entao("^o sistema valida valor de item \"([^\"]*)\" com custo operacional \"([^\"]*)\" e cotacao \"([^\"]*)\" agregado$")
    public void o_sistema_valida_valor_de_item_com_custo_operacional_e_cotacao_agregado(Float valorItem, Float custoOperacional, Float cotacao) {
        orcamentoEPreAtendimentoCustoOperacionalSteps.validarValorItemComCustoOperacionalAgregado(valorItem, custoOperacional, cotacao);
    }

    @Entao("^inexistencia do servico coper \"([^\"]*)\"$")
    public void inexistencia_do_servico_coper(String coper) {
        orcamentoEPreAtendimentoCustoOperacionalSteps.validarInexistenciaServicoCoper(coper);
    }


}
