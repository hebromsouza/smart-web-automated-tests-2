package com.pixeon.steps.definitionSteps.atendimento.ordemDeServico;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;
import com.pixeon.steps.serenity.atendimento.ordemDeServico.InserirItensOrdemDeServicoSteps;

import java.text.ParseException;

public class DefinitionStepsInserirItensOrdemDeServico {

    @Steps
    InserirItensOrdemDeServicoSteps inserirItensOrdemDeServicoSteps;

    @Dado("^que faço a inclusão de item \"([^\"]*)\" em uma nova ordem de serviço com setor solicitenate \"([^\"]*)\"$")
    public void que_faço_a_inclusão_de_item_em_uma_nova_ordem_de_serviço_com_setor_solicitenate(String item, String setorSolicitante) throws InterruptedException {
      inserirItensOrdemDeServicoSteps.incluirItemNovaOrdemDeServico(item, setorSolicitante);
    }

    @Dado("^que faço a inclusão de item \"([^\"]*)\"$")
    public void que_faço_a_inclusão_de_item_(String item) throws InterruptedException {
        inserirItensOrdemDeServicoSteps.buscarServico(item);
    }

    @Quando("^faço a busca do item \"([^\"]*)\" pelo tipo do item \"([^\"]*)\" e filtro por tipo de busca \"([^\"]*)\"$")
    public void faço_a_busca_do_item_pelo_tipo_do_item_e_filtro_por_tipo_de_busca(String item, String tipoItem, String filtroTipoBusca) {
        inserirItensOrdemDeServicoSteps.buscarItensComParametrosDeBusca(item, tipoItem, filtroTipoBusca);
    }

    @Quando("^concluir inserção de item \"([^\"]*)\" com pelo tipo do item \"([^\"]*)\" filtro por tipo de busca \"([^\"]*)\"$")
    public void concluir_inserção_de_item_com_pelo_tipo_do_item_filtro_por_tipo_de_busca(String item, String tipoItem, String filtroTipoBusca) {
        inserirItensOrdemDeServicoSteps.concluirInsercaoDeItem(item, tipoItem, filtroTipoBusca);
    }

    @Quando("^buscar item \"([^\"]*)\"$")
    public void buscar_item(String item) {
        inserirItensOrdemDeServicoSteps.buscarItem(item);
    }

    @Quando("^faço a busca do item \"([^\"]*)\" selecionando a amostra \"([^\"]*)\"$")
    public void faço_a_busca_do_item_selecionando_a_amostra(String item, String amostra) {
        inserirItensOrdemDeServicoSteps.buscarItemSelecionandoAmostra(item, amostra);
    }

    @Quando("^incluir o item$")
    public void incluir_o_item() {
        inserirItensOrdemDeServicoSteps.incluirItem();
    }

    @Quando("^incluir item \"([^\"]*)\" na ordem de serviço$")
    public void incluir_item_na_ordem_de_serviço(String item) {
        inserirItensOrdemDeServicoSteps.inserirItemOrdemDeServico(item);
    }

    @Quando("^inserir item \"([^\"]*)\" na ordem de servico$")
    public void inserir_item_na_ordem_de_servico(String item) {
        inserirItensOrdemDeServicoSteps.inserirItemOrdemDeServico(item);
    }

    @Quando("^cadastro um novo paciente com dados faker$")
    public void cadastrar_novo_paciente_faker() {
        inserirItensOrdemDeServicoSteps.cadastrar_novo_paciente_faker();
    }

    @Quando("^buscar médico \"([^\"]*)\"$")
    public void buscar_médico(String profissional) {
        inserirItensOrdemDeServicoSteps.buscarProfissional(profissional);
    }

    @Quando("^abrir tela de itens da ordem de serviço$")
    public void abrir_tela_de_itens_da_ordem_de_serviço() {
        inserirItensOrdemDeServicoSteps.abrirTelaItensOrdemDeServico();
    }

    @Então("^o sistema valida se foi feito a inserção de item \"([^\"]*)\" pelo tipo do item \"([^\"]*)\" e com filtro por tipo de busca \"([^\"]*)\"$")
    public void o_sistema_valida_se_foi_feito_a_inserção_de_item_pelo_tipo_do_item_e_com_filtro_por_tipo_de_busca(String item, String tipoItem, String filtroTipoBusca) {
        inserirItensOrdemDeServicoSteps.validaInsercaoDeItem(item, tipoItem, filtroTipoBusca);
    }

    @Então("^o sistema valida se foi feito a inclusão do item com amostra \"([^\"]*)\"$")
    public void o_sistema_valida_se_foi_feito_a_inclusão_do_item_com_amostra(String amostra) {
        inserirItensOrdemDeServicoSteps.validaInclusaoItemComAmostra(amostra);
    }

    @Então("^o sistema valida mensagem \"([^\"]*)\" da busca de item com bloqueio por convênio$")
    public void o_sistema_valida_mensagem_da_busca_de_item_com_bloqueio_por_convênio(String mensagem) {
        inserirItensOrdemDeServicoSteps.validarMensagemBuscaItemBloqueioCotacao(mensagem);
    }

    @Então("^é apresentado um painel com a mensagem \"([^\"]*)\" da cotação$")
    public void é_apresentado_um_painel_com_a_mensagem_da_cotação(String mensagem) {
        inserirItensOrdemDeServicoSteps.validarMensagemPainelAlertaCotacao(mensagem);
    }

    @Quando("^pegar numero da amostra do item$")
    public void pegar_numero_da_amostra_do_item() {
        inserirItensOrdemDeServicoSteps.pegarNumeroAmostraItem();
    }

    @Quando("^inserir item com resultado para sábado$")
    public void inserir_item_com_resultado_para_sábado() throws ParseException {
        inserirItensOrdemDeServicoSteps.inserirItemResultadoSabado();
    }

    @Então("^o resultado do item será calculado para um dia de sábado$")
    public void o_resultado_do_item_será_calculado_para_um_dia_de_sábado() {
        inserirItensOrdemDeServicoSteps.validarResultadoItemSabadoUtil();
    }

    @Então("^o resultado do item será calculado para o próximo dia util$")
    public void o_resultado_do_item_será_calculado_para_o_próximo_dia_util() {
        inserirItensOrdemDeServicoSteps.validarResultadoItemProximoDiaUtil();
    }

}
