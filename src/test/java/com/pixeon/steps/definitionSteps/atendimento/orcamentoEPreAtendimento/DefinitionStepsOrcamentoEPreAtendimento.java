package com.pixeon.steps.definitionSteps.atendimento.orcamentoEPreAtendimento;

import com.pixeon.steps.serenity.atendimento.orcamentoEPreAtendimento.OrcamentoEPreAtendimentoSteps;
import com.pixeon.steps.serenity.menu.MenuSteps;
import com.pixeon.steps.serenity.paciente.PacienteSteps;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsOrcamentoEPreAtendimento {

    @Steps
    OrcamentoEPreAtendimentoSteps orcamentoEPreAtendimentoSteps;

    @Steps
    MenuSteps menuSteps;

    @Steps
    PacienteSteps pacienteSteps;

    @Dado("^que acesso o menu \"([^\"]*)\" e sub-menu \"([^\"]*)\"$")
    public void que_acesso_o_menu_e_sub_menu(String menu, String subMenu) {
        menuSteps.selecionarMenu(menu,subMenu);
    }

    @Dado("^que acesso o menu \"([^\"]*)\"$")
    public void que_acesso_o_menu(String menu) {
        menuSteps.selecionarMenuOnly(menu);
    }


//    @Dado("^que acesso o menu \"([^\"]*)\"")
//    public void que_acesso_o_menu_e_sub_menu_only(String menu) {
//        menuSteps.selecionarMenuOnly(menu);
//    }

    @Dado("^filtrar pelo periodo de \"([^\"]*)\" a data atual$")
    public void filtrar_pelo_periodo_informado(String dtInicial) {
        menuSteps.filtrar_pelo_periodo_informado(dtInicial);
    }

    @Dado("^que selecionar o setor Não Especificado$")
    public void seleciono_o_setorNaoEspecificado() {
        menuSteps.seleciono_o_setorNaoEspecificado();
    }

    @Dado("^que escolho o setor desejado \"([^\"]*)\"$")
    public void que_escolho_o_setor_desejado(String setor) {
        menuSteps.que_escolho_o_setor_desejado(setor);
    }

    @Quando("^incluir o item \"([^\"]*)\" no sub-menu \"([^\"]*)\"$")
    public void incluir_o_item_no_sub_menu(String item, String subMenu) {
        orcamentoEPreAtendimentoSteps.incluirItemNoSubMenu(item,subMenu);
    }

    @Quando("^gerar uma nova OS para o paciente de registro \"([^\"]*)\"$")
    public void gerar_uma_nova_OS_para_o_paciente1(String item, String registro) {
        pacienteSteps.buscarPacienteComNumeroDeRegisto(registro);
        orcamentoEPreAtendimentoSteps.gerar_uma_nova_OS_para_o_paciente(item);
    }

    @Quando("^gerar uma nova OS com o item \"([^\"]*)\" para o paciente de registro \"([^\"]*)\"$")
    public void gerar_uma_nova_OS_para_o_paciente(String item, String registro) {
        pacienteSteps.buscarPacienteComNumeroDeRegisto(registro);
        orcamentoEPreAtendimentoSteps.gerar_uma_nova_OS_para_o_paciente(item);
    }

    @Quando("^efetuo o pagamento da OS$")
    public void efetuo_o_pagamento_da_OS() {
        pacienteSteps.efetuo_o_pagamento_da_OS();
    }

    @Quando("^confirmo o lançamento do item com a \"([^\"]*)\"$")
    public void confirmo_o_lançamento_do_item_com_outro_convênio(String convenio) {
        orcamentoEPreAtendimentoSteps.confirmarAlteracaoDeConvenio(convenio);
    }

    @Quando("^gerar ordem de serviço$")
    public void gerar_ordem_de_serviço() {
        orcamentoEPreAtendimentoSteps.gerarOrdemDeServico();
    }

    @Então("^o sistema realiza o lançamento do item de acordo com a opção de convênio \"([^\"]*)\"$")
    public void o_sistema_realiza_ou_não_o_lançamento_do_item_de_acordo_com_a_opção_de_convênio(String opcaoConvenio) {
        orcamentoEPreAtendimentoSteps.validarOpcaoDeConvenio(opcaoConvenio);
    }

    @Quando("^confirmo a alteração do orçamento ou pré-atendimento com outro convênio \"([^\"]*)\"$")
    public void confirmo_a_alteração_do_orçamento_ou_pré_atendimento_com_outro_convênio(String convenio) {
        orcamentoEPreAtendimentoSteps.confirmarAlteracaoDeOrcamentoOuPreAtendimento(convenio);

    }

    @Então("^o sistema realiza a alteração do convênio e validação do item de acordo com a tabela de preço que \"([^\"]*)\"$")
    public void o_sistema_realiza_a_alteração_do_convênio_e_validação_do_item_de_acordo_com_a_tabela_de_preço_que(String tabPreco) {
        orcamentoEPreAtendimentoSteps.validarTabelaDePreco(tabPreco);
    }

    @Quando("^gerar pre-atendimento pela tela de orçamento$")
    public void gerar_pre_atendimento_pela_tela_de_orçamento() {
        orcamentoEPreAtendimentoSteps.gerarPreAtendimento();
    }

    @Então("^é migrado para a tela de pré-atendimento$")
    public void é_migrado_para_a_tela_de_pré_atendimento() {
        orcamentoEPreAtendimentoSteps.validarAlteracaoParaPreAtendimento();
    }

    @Então("^é apresentado orçamento ou pré-atendimento buscado$")
    public void é_apresentado_orçamento_ou_pré_atendimento_buscado() {
        orcamentoEPreAtendimentoSteps.validarOrcamentoPreAtendimento();
    }

    @Quando("^pegar número da ordem de serviço gerada na tela de orçamento$")
    public void pegar_número_da_ordem_de_serviço_gerada_na_tela_de_orçamento() {
        orcamentoEPreAtendimentoSteps.pegarNumeroOrdemServicoGeradaOrcamento();
    }

    @Quando("^acessar ordem de serviço gerada pelo orçamento$")
    public void acessar_ordem_de_serviço_gerada_pelo_orçamento() {
        orcamentoEPreAtendimentoSteps.acessarOrdemServicoPeloOrcamento();
    }

}
