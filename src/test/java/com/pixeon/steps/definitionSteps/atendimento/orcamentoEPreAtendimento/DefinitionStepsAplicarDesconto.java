package com.pixeon.steps.definitionSteps.atendimento.orcamentoEPreAtendimento;

import com.pixeon.steps.serenity.atendimento.orcamentoEPreAtendimento.AplicarDescontoSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAplicarDesconto {

    @Steps
    AplicarDescontoSteps aplicarDescontoSteps;

    @Quando("^selecionar desconto \"([^\"]*)\" do orçamento$")
    public void selecionar_desconto_do_orçamento(String desconto) {
        aplicarDescontoSteps.selecionarDesconto(desconto);
    }

    @Então("^o sistema valida desconto aplicado no valor do item$")
    public void o_sistema_valida_desconto_aplicado_no_valor_do_item() {
        aplicarDescontoSteps.validarDescontoAplicado();
    }

    @Então("^o desconto é aplicado para o pré-atendimento$")
    public void o_desconto_é_aplicado_para_o_pré_atendimento() {
        aplicarDescontoSteps.validarDescontoAplicado();
    }

    @Quando("^inserir percentagem de desconto \"([^\"]*)\"$")
    public void inserir_percentagem_de_desconto(String percentual) {
        aplicarDescontoSteps.inserirPercentualDesconto(percentual);
    }

}
