package com.pixeon.steps.definitionSteps.atendimento.ordemDeServico;

import com.pixeon.steps.serenity.atendimento.ordemDeServico.ImprimirOrdemDeServicoSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsImprimirOrdemDeServico {

    @Steps
    ImprimirOrdemDeServicoSteps imprimirOrdemDeServicoSteps;

    @Quando("^imprimir ordem de serviço$")
    public void imprimir_ordem_de_serviço() {
        imprimirOrdemDeServicoSteps.imprimirOrdemDeServico();
    }

    @Quando("^fechar tela de impressão de ordem de serviço$")
    public void fechar_tela_de_impressão_de_ordem_de_serviço() {
        imprimirOrdemDeServicoSteps.fecharTelaImpressao();
    }

}
