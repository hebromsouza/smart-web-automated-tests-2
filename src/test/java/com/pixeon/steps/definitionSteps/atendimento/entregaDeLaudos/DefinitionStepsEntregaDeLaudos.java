package com.pixeon.steps.definitionSteps.atendimento.entregaDeLaudos;

import com.pixeon.steps.serenity.atendimento.entregaDeLaudos.EntregaDeLaudosSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsEntregaDeLaudos {

    @Steps
    EntregaDeLaudosSteps entregaDeLaudosSteps;

    @Quando("^imprimir entrega de laudos para pessoa \"([^\"]*)\"$")
    public void imprimir_entrega_de_laudos_para_pessoa(String nomePessoaEntrega) {
        entregaDeLaudosSteps.imprimirEntregaLaudos(nomePessoaEntrega);
    }


    @Quando("^registrar entrega de laudos para pessoa \"([^\"]*)\"$")
    public void registrar_entrega_de_laudos_para_pessoa(String nomePessoaEntrega) {
        entregaDeLaudosSteps.registrarEntregaLaudos(nomePessoaEntrega);
    }

    @Então("^o sistema valida entrega de laudos do paciente$")
    public void o_sistema_valida_entrega_de_laudos_do_paciente() {
        entregaDeLaudosSteps.validarEntregaLaudosPaciente();
    }

}
