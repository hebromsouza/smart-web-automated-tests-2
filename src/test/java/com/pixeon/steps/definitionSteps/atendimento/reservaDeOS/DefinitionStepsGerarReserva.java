package com.pixeon.steps.definitionSteps.atendimento.reservaDeOS;

import com.pixeon.datamodel.reservaDeOS.DataReservaOS;
import com.pixeon.steps.serenity.atendimento.reservaDeOS.GerarReservaSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsGerarReserva {

    @Steps
    GerarReservaSteps gerarReservaSteps;

    @Quando("^informar os dados da reserva de os$")
    public void informar_os_dados_da_reserva_de_os(List<DataReservaOS> dataReservaOS) {
        gerarReservaSteps.informarDadosReservaOS(dataReservaOS);
    }

    @Quando("^gerar reserva$")
    public void gerar_reserva() {
        gerarReservaSteps.gerarReserva();
    }

    @Então("^apresenta a tela de sucesso na geração de reserva de ordem de serviço$")
    public void apresenta_a_tela_de_sucesso_na_geração_de_reserva_de_ordem_de_serviço() {
        gerarReservaSteps.validarPainelReservaGerada();
    }

    @Quando("^confirmar popup de confirmação de geração de reserva de ordem de serviço com quantidade \"([^\"]*)\" e setor \"([^\"]*)\"$")
    public void confirmar_popup_de_confirmação_de_geração_de_reserva_de_ordem_de_serviço_com_quantidade_e_setor(String quantidade, String setorSolicitante) {
        gerarReservaSteps.confirmarPopUpGeracaoReserva(quantidade, setorSolicitante);
    }

    @Quando("^imprimir fichas de atendimento$")
    public void imprimir_fichas_de_atendimento() {
        gerarReservaSteps.imprimirFichasAtendimento();
    }

}
