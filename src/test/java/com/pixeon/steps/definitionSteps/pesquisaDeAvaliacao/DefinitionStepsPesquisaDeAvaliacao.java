package com.pixeon.steps.definitionSteps.pesquisaDeAvaliacao;

import com.pixeon.steps.serenity.presquisaDeAvaliacao.PesquisaDeAvaliacaoSteps;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsPesquisaDeAvaliacao {

    @Steps
    PesquisaDeAvaliacaoSteps pesquisaDeAvaliacaoSteps;


    @Quando("^responder o questionário de avaliação$")
    public void responder_o_questionário_de_avaliação() {
        pesquisaDeAvaliacaoSteps.responderQuestionario();
    }

    @Dado("^aplicar novo questionário$")
    public void aplicar_novo_questionário() {
        pesquisaDeAvaliacaoSteps.aplicarNovoQuestionario();
    }


    @Dado("^selecionar novo questionário \"([^\"]*)\"$")
    public void selecionar_novo_questionário(String questionario) {
        pesquisaDeAvaliacaoSteps.selecionarQuestionario(questionario);
    }

    @Quando("^gravar questionário$")
    public void gravar_questionário() {
        pesquisaDeAvaliacaoSteps.gravarQuestionario();
    }

    @Quando("^gravar sem responder nenhuma questao do questionário$")
    public void gravar_resposta_do_questionário() {
        // Não necessario responder questionario...verificando...mensagem de obrigatoriedade.
      //  pesquisaDeAvaliacaoSteps.responderQuestionario();

        pesquisaDeAvaliacaoSteps.gravarQuestionario();
    }

    @Quando("^excluir resposta do questionário$")
    public void excluir_resposta_do_questionário() {
        pesquisaDeAvaliacaoSteps.excluirRespostaQuestionario();
    }

    @Quando("^excluir resposta do questionário de avaliação$")
    public void excluir_resposta_do_questionário_de_avaliação() {
        pesquisaDeAvaliacaoSteps.excluirRespostaQuestionarioAvaliacao();
    }

    @Quando("^aceitar alerta de conceito obtido$")
    public void aceitar_alerta_de_conceito_obtido() {
        pesquisaDeAvaliacaoSteps.aceitarAlertaConceitoObtido();
    }


    @Quando("^abrir o questionário respondido$")
    public void abrir_o_questionário_respondido() {
        pesquisaDeAvaliacaoSteps.abrirQuestionarioRespondido();
    }

    @Quando("^imprimir o questionário$")
    public void imprimir_o_questionário() {
        pesquisaDeAvaliacaoSteps.imprimirQuestionario();
    }

    @Entao("^o sistema valida mensagem de alerta do questionário \"([^\"]*)\"$")
    public void o_sistema_valida_mensagem_de_alerta_do_questionário(String mensagemAlerta) {
        pesquisaDeAvaliacaoSteps.validarMensagemAlertaQuestionario(mensagemAlerta);
    }

    @Entao("^clico Ok ou Enter para sair$")
    public void clicoOk_or_Enter_sair() {
        pesquisaDeAvaliacaoSteps.clicoOk_or_Enter_sair();
    }



    @Dado("^selecionar novo questionário \"([^\"]*)\" e respondido por \"([^\"]*)\" com data \"([^\"]*)\"$")
    public void selecionar_novo_questionário_e_respondido_por_com_data(String questionario, String respondido, String data) {

    }

    @Entao("^continua exclusão do questionário$")
    public void continua_exclusão_do_questionário() {
        pesquisaDeAvaliacaoSteps.continuaExclusaoQuestionario();
    }

    @Entao("^o sistema valida mensagem \"([^\"]*)\"$")
    public void o_sistema_valida_mensagem(String mensagem) {

    }

    @Entao("^o sistema valida tela de impressão de questionário$")
    public void o_sistema_valida_tela_de_impressão_de_questionário() {
        pesquisaDeAvaliacaoSteps.validaImpressaoQuestionario();
    }

}
