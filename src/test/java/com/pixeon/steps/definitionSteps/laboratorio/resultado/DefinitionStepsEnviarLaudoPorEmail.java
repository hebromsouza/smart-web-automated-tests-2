package com.pixeon.steps.definitionSteps.laboratorio.resultado;

import com.pixeon.steps.serenity.laboratorio.resultado.EnviarLaudoPorEmailSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsEnviarLaudoPorEmail {

    @Steps
    EnviarLaudoPorEmailSteps enviarLaudoPorEmailSteps;

    @Quando("^enviar laudo por email$")
    public void enviar_laudo_por_email() {
        enviarLaudoPorEmailSteps.enviarLaudoPorEmail();
    }

    @Quando("^enviar laudo para outro email \"([^\"]*)\"$")
    public void enviar_laudo_para_outro_email(String email) {
        enviarLaudoPorEmailSteps.enviarLaudoParaOutroEmail(email);
    }

}
