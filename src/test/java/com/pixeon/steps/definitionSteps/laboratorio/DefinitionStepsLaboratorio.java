package com.pixeon.steps.definitionSteps.laboratorio;

import com.pixeon.datamodel.cadastros.DataModelCadastroPacientes;
import com.pixeon.steps.serenity.atendimento.ordemDeServico.LancamentoDeOrdemDeServicoSteps;
import com.pixeon.steps.serenity.laboratorio.OrdemDeServicoSteps;
import com.pixeon.steps.serenity.laboratorio.resultado.ResultadosSteps;
import com.pixeon.steps.serenity.paciente.PacienteSteps;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsLaboratorio {

    @Steps
    PacienteSteps pacienteSteps;

    @Steps
    LancamentoDeOrdemDeServicoSteps lancamentoDeOrdemDeServicoSteps;

    @Steps
    OrdemDeServicoSteps ordemDeServicoSteps;

    @Steps
    ResultadosSteps resultadosSteps;

    @Quando("^buscar um paciente \"([^\"]*)\" com o seu número de matricula e avançar$")
    public void buscar_um_paciente_com_o_seu_número_de_matricula_e_avancar(String matPaciente) {
        pacienteSteps.buscarPacienteComNumeroDeMatriculaEAvancar(matPaciente);
    }

    @E("^informo paciente \"([^\"]*)\"$")
    public void informo_paciente_aleatorio(String nome) {
        pacienteSteps.informo_paciente_aleatorio(nome);
    }


    @E("^clico no botao comum \"([^\"]*)\"$")
    public void clico_no_botao_comum(String btComum) {
        pacienteSteps.clico_no_botao_comum(btComum);
    }

    @E("^confirmo em OK o desejo de incluir um novo paciente$")
    public void confirmo_em_OK_o_desejo_de_incluir_um_novo_paciente() {
        pacienteSteps.confirmo_em_OK_o_desejo_de_incluir_um_novo_paciente();
    }

/*
    @Quando("^preencho os dados básicos do cadastro do paciente$")
    public void preencho_os_dados_básicos_do_cadastro_do_paciente(Map<String, String> dadosCadostroPaciente) {
        pacienteSteps.preencho_os_dados_basicos_do_cadastro_do_paciente(dadosCadostroPaciente);
    }
*/

    @Quando("^preencho os dados basicos de cadastro do paciente$")
    public void preencho_os_dados_basicos_de_cadastro_do_paciente(List<DataModelCadastroPacientes> dadosBasicosPaciente) {
        pacienteSteps.preencho_os_dados_basicos_do_cadastro_do_paciente(dadosBasicosPaciente);
    }

    @Quando("^preencho os dados da operadora de saude do paciente$")
    public void preencho_os_dados_da_operadora_de_saude_do_paciente(List<DataModelCadastroPacientes> dadosOperadoraSaude) {
        pacienteSteps.preencho_os_dados_da_operadora_de_saude_do_paciente(dadosOperadoraSaude);
    }

    @Quando("^preencho os dados de Contatos do paciente$")
    public void preencho_os_dados_de_contato_do_paciente(List<DataModelCadastroPacientes> dadosContatoPaciente) {
        pacienteSteps.preencho_os_dados_de_contato_do_paciente(dadosContatoPaciente);
    }


    @E("^criar uma nova ordem de serviço$")
    public void criar_uma_nova_ordem_de_serviço() {
        pacienteSteps.criar_uma_nova_ordem_de_serviço();
    }

    @E("^lancar o item \"([^\"]*)\" para a OS criada$")
    public void lancar_item_para_os_criada(String item) {
        pacienteSteps.lancar_item_para_os_criada(item);
    }

    @E("^criar uma nova ordem de serviço código do convênio \"([^\"]*)\"$")
    public void criar_uma_nova_ordem_de_serviço_com_codigo_convenio(String convenio) {
        pacienteSteps.criar_uma_nova_ordem_de_serviço_com_codigo_convenio(convenio);
    }

    @E("^criar uma nova ordem de serviço com o plano \"([^\"]*)\"$")
    public void criar_uma_nova_ordem_de_serviço_com_o_plano(String plano) {
        pacienteSteps.criar_uma_nova_ordem_de_serviço_com_o_plano(plano);
    }

    @Quando("^adicionar uma nova ordem de serviço$")
    public void adicionar_uma_nova_ordem_de_serviço() {
        lancamentoDeOrdemDeServicoSteps.adicionar_uma_nova_ordem_de_serviço();
    }

    @Quando("^incluir o item \"([^\"]*)\" com instrução$")
    public void incluir_o_item_com_instrução(String item) {
        lancamentoDeOrdemDeServicoSteps.lancarItemServicoComInstrucao(item);
    }

    @Quando("^incluir o item \"([^\"]*)\"$")
    public void incluir_o_item_na_os(String item) {
        lancamentoDeOrdemDeServicoSteps.incluir_o_item_na_os(item);
    }

    @Quando("^incluir o item \"([^\"]*)\" sem instrução$")
    public void incluir_o_item_sem_instrução(String item) {
        lancamentoDeOrdemDeServicoSteps.lancamentoItemServicoSemInstrucao(item);
    }

    @Quando("^buscar um paciente \"([^\"]*)\" com o seu número de matricula$")
    public void buscar_um_paciente_com_o_seu_número_de_matricula(String matPaciente) {
        pacienteSteps.buscarPacientePorMatricula(matPaciente);
    }

    @Quando("^selecionar a ordem de serviço$")
    public void selecionar_a_ordem_de_serviço() {
        ordemDeServicoSteps.clicarNaOrdemDeServico();
    }

    @Quando("^verifico que o resultado ja esta disponivel$")
    public void verifico_que_o_resultado_jaesta_disponivel() {
        ordemDeServicoSteps.verificarseResultadoEstaDisponivel();
    }

    @Quando("^selecionar a ordem de serviço gerada$")
    public void selecionar_a_ordem_de_serviço_gerada() {
        ordemDeServicoSteps.clicarNaOrdemDeServicoGerada();
    }

    @Quando("^selecionar a ordem de serviço gerada pelo orçamento$")
    public void selecionar_a_ordem_de_serviço_gerada_pelo_orçamento() {
        ordemDeServicoSteps.clicarOrdemDeServicoGeradaNoOrcamento();
    }

    @Quando("^selecionar a ordem de serviço da lista \"([^\"]*)\"$")
    public void selecionar_a_ordem_de_serviço_da_lista(String lista) {
        ordemDeServicoSteps.clicarLinkOrdemDeServicoNaLista(lista);
    }

    @Quando("^gravar o resultado \"([^\"]*)\"$")
    public void gravar_o_resultado_do_exame(String resultado) {
        resultadosSteps.gravarResultado(resultado);
    }

    @Quando("^gravar resultado de exame com valor de referência \"([^\"]*)\"$")
    public void gravar_resultado_de_exame_com_valor_de_referência(String valorReferencia) {
        resultadosSteps.gravarResultadoExameComValorDeReferencia(valorReferencia);
    }

    @Então("^valido se o sistema gravou o resultado \"([^\"]*)\"$")
    public void valido_se_o_sistema_gravou_o_resultado(String resultado) {
        resultadosSteps.validarValorGravadoDeResultado(resultado);
    }

    @Então("^gravo e valido se o sistema gravou a ordem de serviço com o valor \"([^\"]*)\" e status \"([^\"]*)\"$")
    public void gravo_e_valido_se_o_sistema_gravou_a_ordem_de_serviço_com_o_valor_e_status(String resultado, String status) {
        String status1 = status.replace("'","");
        resultadosSteps.gravarResultadoComStatus(resultado,status1);
    }

    @Quando("^abrir painel de resultados do exame$")
    public void abrir_painel_de_resultados_do_exame() {
        resultadosSteps.abrirPainelResultadosExame();
    }

    @Então("^o sistema verifica os valores de referencia$")
    public void o_sistema_valida_erro_de_painel() {
        resultadosSteps.validarErroPainel();
    }

}
