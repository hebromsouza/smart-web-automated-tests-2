package com.pixeon.steps.definitionSteps.laboratorio.resultado;

import com.pixeon.datamodel.laboratorio.DataResultadoEritrograma;
import com.pixeon.datamodel.laboratorio.DataResultadoLeucograma;
import com.pixeon.steps.serenity.laboratorio.resultado.ResultadosSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsResultadoDeExame {

    @Steps
    ResultadosSteps resultadosSteps;

    @Quando("^inserir o resultado do exame \"([^\"]*)\" e \"([^\"]*)\"$")
    public void inserir_o_resultado_do_exame_e(String resultado, String resultado2) {
        resultadosSteps.inserirResultadoDoExame(resultado, resultado2);
    }

    @Quando("^gravar o resultado do exame$")
    public void gravar_o_resultado_do_exame() {
        resultadosSteps.gravarResultadoDoExame();
    }

    @Entao("^o sistema valida se o status do exame foi alterado para \"([^\"]*)\"$")
    public void o_sistema_valida_se_o_status_do_exame_foi_alterado_para(String statusExame) {
        resultadosSteps.validarStatusExecutado(statusExame);
    }

    @Entao("^clico para visualizar o laudo$")
    public void clico_para_visualizar_lauro() {
        resultadosSteps.clico_para_visualizar_lauro();
    }

    @Quando("^confirmar resultados$")
    public void confirmar_resultados() {
        resultadosSteps.confirmarResultados();
    }

    @Quando("^liberar o exame$")
    public void liberar_o_exame() {
        resultadosSteps.liberarExame();
    }

    @Quando("^preencher resultado para eritrograma$")
    public void preencher_resultado_para_eritrograma(List<DataResultadoEritrograma> dataResultadoHemogramas) {
        resultadosSteps.preencherResultadoEritrograma(dataResultadoHemogramas);
    }

    @Quando("^preencher resultado para leucograma$")
    public void preencher_resultado_para_leucograma(List<DataResultadoLeucograma> dataResultadoLeucograma) {
        resultadosSteps.preencherResultadoLeucograma(dataResultadoLeucograma);
    }

    @Quando("^gravar resultado de homograma$")
    public void gravar_resultado_de_homograma() {
        resultadosSteps.gravarResultadoHemograma();
    }

    @Então("^validar resultado \"([^\"]*)\" não editável$")
    public void validar_resultado_não_editável(String resultado) {
        resultadosSteps.validarResultadoNaoEditavel(resultado);
    }

}
