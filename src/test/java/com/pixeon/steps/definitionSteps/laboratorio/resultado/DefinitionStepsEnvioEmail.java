package com.pixeon.steps.definitionSteps.laboratorio.resultado;

import com.pixeon.steps.serenity.laboratorio.resultado.EnvioEmailSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsEnvioEmail {

    @Steps
    EnvioEmailSteps envioEmailSteps;

    @Quando("^realizar login no email \"([^\"]*)\" e senha \"([^\"]*)\"$")
    public void realizar_login_no_email_e_senha(String email, String senha) {
        envioEmailSteps.abrirEmailOutlook365();
        envioEmailSteps.realizarLoginEmail(email, senha);
    }

    @Então("^valida se foi enviado o email para o paciente \"([^\"]*)\"$")
    public void valida_se_foi_enviado_o_email_para_o_paciente(String nomePaciente) {
        envioEmailSteps.validarEnvioEmail(nomePaciente);
    }

}
