package com.pixeon.steps.definitionSteps.laboratorio.folhasDeTrabalho;

import com.pixeon.steps.serenity.laboratorio.folhasDeTrabalho.BuscarFolhasDeTrabalhoSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsBuscarFolhasDeTrabalho {

    @Steps
    BuscarFolhasDeTrabalhoSteps buscarFolhasDeTrabalhoSteps;

    @Quando("^buscar folhas emitidas pela bancada \"([^\"]*)\"$")
    public void buscar_folhas_emitidas_pela_bancada(String bancada) {
        buscarFolhasDeTrabalhoSteps.buscarFolhasEmitidas(bancada);
    }

    @Quando("^pegar o número da primeira folha emitida$")
    public void pegar_o_número_da_primeira_folha_emitida() {
        buscarFolhasDeTrabalhoSteps.numeroDaPrimeiraFolhaEmitida();
    }

    @Quando("^checar a folha emitida$")
    public void checar_a_folha_emitida() {
        buscarFolhasDeTrabalhoSteps.checarFolhaEmitida();
    }

    @Quando("^cancelar as folha selecionada$")
    public void cancelar_as_folha_selecionada() {
        buscarFolhasDeTrabalhoSteps.cancelarFolhaSelecionada();
    }

    @Entao("^o sistema valida cancelamento da folha selecionada$")
    public void o_sistema_valida_cancelamento_da_folha_selecionada() {
        buscarFolhasDeTrabalhoSteps.validarCancelamentoDaFolhaSelecionada();
    }

}
