package com.pixeon.steps.definitionSteps.laboratorio.folhasDeTrabalho;

import com.pixeon.steps.serenity.laboratorio.folhasDeTrabalho.GerarFolhasDeTrabalhoSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsGerarFolhasDeTrabalho {

    @Steps
    GerarFolhasDeTrabalhoSteps folhasDeTrabalhoSteps;

    @Quando("^selecionar a opção gerar folhas$")
    public void selecionar_a_opção_gerar_folhas() {
        folhasDeTrabalhoSteps.selecionarOpcaoGerarFolhas();
    }

    @Quando("^que selecionar o setor \"([^\"]*)\"$")
    public void que_selecionar_o_setor(String setor) {
        folhasDeTrabalhoSteps.selecionarSetor(setor);
    }

    @Quando("^que selecionar a bancada \"([^\"]*)\"$")
    public void que_selecionar_a_bancada(String bancada) {
        folhasDeTrabalhoSteps.selecionarBancada(bancada);
    }

    @Quando("^buscar as folhas$")
    public void buscar_as_folhas() {
        folhasDeTrabalhoSteps.buscarFolhas();
    }

    @Quando("^selecionar as folhas$")
    public void selecionar_as_folhas() {
        folhasDeTrabalhoSteps.selecionarFolhas();
    }

    @Quando("^gerar folhas$")
    public void gerar_folhas() {
        folhasDeTrabalhoSteps.gerarFolhas();
    }

    @Quando("^concluir geração de folhas$")
    public void concluir_geração_de_folhas() {
        folhasDeTrabalhoSteps.concluirGeracaoFolhas();
    }

    @Então("^o sistema valida geração de folhas para bancada \"([^\"]*)\"$")
    public void o_sistema_valida_geração_de_folhas_para_bancada(String bancada) {
        folhasDeTrabalhoSteps.validarGeracaoDeFolhas(bancada);
    }

}