package com.pixeon.steps.definitionSteps.laboratorio.localizacaoDeAmostra;

import com.pixeon.steps.serenity.laboratorio.localizacaoDeAmostra.LocalizacaoAmostraSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsLocalizacaoAmostra {

    @Steps
    LocalizacaoAmostraSteps localizacaoAmostraSteps;

    @Quando("^localizar amostra do item lançado$")
    public void localizar_amostra_do_item_lançado() {
        localizacaoAmostraSteps.localizarAmostraDoItemLancado();
    }

    @Quando("^selecionar a opção de informar volume urinário/linfócito$")
    public void selecionar_a_opção_de_informar_volume_urinário_linfócito() {
        localizacaoAmostraSteps.selecionarOpcaoInformarVolumeUrinarioLinfocito();
    }

    @Quando("^informar tempo \"([^\"]*)\" e volume \"([^\"]*)\" do volume urinário na tela de outras informações$")
    public void informar_tempo_e_volume_do_volume_urinário_na_tela_de_outras_informações(String tempo, String volume) {
        localizacaoAmostraSteps.informarTempoVolume(tempo, volume);
    }

    @Então("^o sistema valida se foi adicionado volume urinário com tempo \"([^\"]*)\" e volume \"([^\"]*)\"$")
    public void o_sistema_valida_se_foi_adicionado_volume_urinário_com_tempo_e_volume(String tempo, String volume) {
        localizacaoAmostraSteps.validarAdicaoVolumeUrinarioTempoVolume(tempo, volume);
    }

    @Quando("^informar linfócitos \"([^\"]*)\" na tela de outras informações$")
    public void informar_linfócitos_na_tela_de_outras_informações(String linfocitos) {
        localizacaoAmostraSteps.informarLinfocitos(linfocitos);
    }

    @Então("^o sistema valida se foi adicionado linfócitos \"([^\"]*)\"$")
    public void o_sistema_valida_se_foi_adicionado_linfócitos(String linfocitos) {
        localizacaoAmostraSteps.validarAdicaoLinfocitos(linfocitos);
    }

    @Então("^o sistema valida localização de amostra do item lançado na ordem de serviço$")
    public void o_sistema_valida_localização_de_amostra_do_item_lançado_na_ordem_de_serviço() {
        localizacaoAmostraSteps.validarLocalizacaoAmostra();
    }

}
