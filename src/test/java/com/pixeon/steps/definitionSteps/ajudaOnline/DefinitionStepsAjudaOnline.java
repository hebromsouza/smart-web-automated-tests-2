package com.pixeon.steps.definitionSteps.ajudaOnline;

import com.pixeon.steps.serenity.ajudaOnline.AjudaOnlineSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAjudaOnline {

    @Steps
    AjudaOnlineSteps ajudaOnlineSteps;

    @Quando("^acessar a pagina de ajuda online$")
    public void acessar_a_pagina_de_ajuda_online() {
        ajudaOnlineSteps.acessarPaginaAjudaOnline();
    }


    @Entao("^o sistema valida pagina de ajuda online$")
    public void o_sistema_valida_pagina_de_ajuda_online() {
        ajudaOnlineSteps.validarPaginaAjudaOnline();
    }

}
