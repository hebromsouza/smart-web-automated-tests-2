package com.pixeon.steps.definitionSteps.baixarVersao;

import com.pixeon.steps.serenity.atendimento.etiqueta.ImpressaoEtiquetaSteps;
import com.pixeon.steps.serenity.baixarVersao.BaixarVersaoSteps;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsBaixarVersao {

    @Steps
    BaixarVersaoSteps baixarVersaoSteps;

    @Dado("^que baixei a ultima versão disponivel do smartweb$")
    public void que_baixei_a_ultima_versão_disponivel_do_smartweb() {
        baixarVersaoSteps.que_baixei_a_ultima_versão_disponivel_do_smartweb();
    }


    @Quando("^faço a instalação da versão$")
    public void faço_a_instalação_da_versão() {
        baixarVersaoSteps.faço_a_instalação_da_versão();
    }


}