package com.pixeon.steps.definitionSteps.cadastros.profissionais;

import com.pixeon.datamodel.cadastros.DataModelBuscaDeProfinionais;
import com.pixeon.steps.serenity.cadastros.profissionais.BuscaDeProfissionaisSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsBuscaDeProfissionais {

    @Steps
    BuscaDeProfissionaisSteps buscaDeProfissionaisSteps;

    @Quando("^que preencho informo dados da busca paciente Profissionais na tela Busca do Profissionais$")
    public void que_preencho_informo_dados_da_busca_paciente_Profissionais_na_tela_Busca_do_Profissionais(List<DataModelBuscaDeProfinionais> dataModelBuscaDeProfinionais) {
        buscaDeProfissionaisSteps.que_preencho_informo_dados_da_busca_paciente_Profissionais_na_tela_Busca_do_Profissionais(dataModelBuscaDeProfinionais);
    }

    @Quando("^buscar paciente Prontuário na tela Busca do Profissionais$")
    public void buscar_paciente_Prontuário_na_tela_Busca_do_Profissionais() {
        buscaDeProfissionaisSteps.buscar_paciente_Prontuário_na_tela_Busca_do_Profissionais();
    }

    @Quando("^selecionar o profissional \"([^\"]*)\" na tela Busca do Profissionais$")
    public void selecionar_o_profissional_na_tela_Busca_do_Profissionais(String profissional) {
        buscaDeProfissionaisSteps.selecionar_o_profissional_na_tela_Busca_do_Profissionais(profissional);
    }


}
