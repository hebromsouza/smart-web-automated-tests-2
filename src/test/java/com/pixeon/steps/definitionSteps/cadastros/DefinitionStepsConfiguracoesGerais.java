package com.pixeon.steps.definitionSteps.cadastros;

import com.pixeon.steps.serenity.cadastros.ConfiguracoesGeraisSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsConfiguracoesGerais {

    @Steps
    ConfiguracoesGeraisSteps configuracoesGeraisSteps;

    @Quando("^modificar o parametro tempo de expiração  para \"([^\"]*)\" minuto na tela de configurações Gerais$")
    public void modificar_o_parametro_tempo_de_expiração_para_minuto_na_tela_de_configurações_Gerais(String tempo) {
        configuracoesGeraisSteps.modificar_o_parametro_tempo_de_expiração_para_minuto_na_tela_de_configurações_Gerais(tempo);
    }

    @Quando("^gravar na tela de configurações Gerais$")
    public void gravar_na_tela_de_configurações_Gerais() {
        configuracoesGeraisSteps.gravar_na_tela_de_configurações_Gerais();
    }

    @Quando("^Fechar na tela de configurações Gerais$")
    public void fechar_na_tela_de_configurações_Gerais() {
        configuracoesGeraisSteps.fechar_na_tela_de_configurações_Gerais();
    }

    @Quando("^modificar o parametro quantidade de tentativa invalida para \"([^\"]*)\"$")
    public void modificar_o_parametro_quantidade_de_tentativa_invalida_para(String tentativa) {
        configuracoesGeraisSteps.modificar_o_parametro_quantidade_de_tentativa_invalida_para(tentativa);
    }
    
}
