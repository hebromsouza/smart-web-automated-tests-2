package com.pixeon.steps.definitionSteps.cadastros;
import com.pixeon.steps.serenity.cadastros.CadastrosSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsCadastros{

    @Steps
    CadastrosSteps cadastrosSteps;

    @Quando("^selecionar o cadastro de \"([^\"]*)\" na tela de Cadastros Cadastro de Gerais$")
    public void selecionar_o_cadastro_de_na_tela_de_Cadastros_Cadastro_de_Gerais(String cadastro) {
        cadastrosSteps.selecionar_o_cadastro_de_na_tela_de_Cadastros_Cadastro_de_Gerais(cadastro);
    }

    @Quando("^acessar a configuração  \"([^\"]*)\"  da tela de Cadastro$")
    public void acessar_a_configuração_da_tela_de_Cadastro(String configuracao) {
        cadastrosSteps.acessar_a_configuração_da_tela_de_Cadastro(configuracao);
    }

}
