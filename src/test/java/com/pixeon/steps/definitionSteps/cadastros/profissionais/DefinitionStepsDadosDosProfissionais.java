package com.pixeon.steps.definitionSteps.cadastros.profissionais;
import com.pixeon.steps.serenity.cadastros.profissionais.DadosDosProfissionaisSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsDadosDosProfissionais {

    @Steps
    DadosDosProfissionaisSteps dadosDosProfissionaisSteps;


    @Quando("^habilitar o campo sob supervisão da tela dados do profissional$")
    public void habilitar_o_campo_sob_supervisão_da_tela_dados_do_profissional() {
        dadosDosProfissionaisSteps.habilitar_o_campo_sob_supervisão_da_tela_dados_do_profissional();
    }

    @Quando("^selecioanr o supervisor \"([^\"]*)\" na tela dados do profissional$")
    public void selecioanr_o_supervisor_na_tela_dados_do_profissional(String supervisorImediato) {
        dadosDosProfissionaisSteps.selecioanr_o_supervisor_na_tela_dados_do_profissional(supervisorImediato);
    }

    @Quando("^gravar dados e fechar a tela dados do profissional$")
    public void gravar_dados_e_fechar_a_tela_dados_do_profissional() {
        dadosDosProfissionaisSteps.gravar_dados_e_fechar_a_tela_dados_do_profissional();
    }


}
