package com.pixeon.steps.definitionSteps.paciente.cadastroPaciente;

import com.pixeon.steps.serenity.paciente.cadastroPaciente.CadastroPacienteSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsCadastroPaciente {

    @Steps
    CadastroPacienteSteps cadastroPacienteSteps;

    @Quando("^inserir cpf \"([^\"]*)\" no cadastro do paciente$")
    public void inserir_cpf_no_cadastro_do_paciente(String cpf) {
        cadastroPacienteSteps.inserirCpfPaciente(cpf);
    }

    @Quando("^gravar cadastro do paciente$")
    public void gravar_cadastro_do_paciente() {
        cadastroPacienteSteps.gravarCadastroDoPaciente();
    }

    @Então("^o sistema valida inclusão do cpf$")
    public void o_sistema_valida_inclusão_do_cpf() {
        cadastroPacienteSteps.validarInclusaoCpf();
    }

    @Então("^o sistema valida inclusão do cpf sem pontuacao$")
    public void o_sistema_valida_inclusão_do_cpf_sem_pontuacao() {
        cadastroPacienteSteps.validarInclusaoCpfSemPontuacao();
    }


    @Quando("^imprimir etiqueta do paciente$")
    public void imprimir_etiqueta_do_paciente() {
        cadastroPacienteSteps.imprimirEtiquetaPaciente();
    }

}
