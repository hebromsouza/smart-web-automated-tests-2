package com.pixeon.steps.definitionSteps.paciente;

import com.pixeon.steps.serenity.paciente.CriarPacienteSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsCriarPaciente {

    @Steps
    CriarPacienteSteps criarPacienteSteps;

    @Quando("^criar novo paciente \"([^\"]*)\"$")
    public void criar_novo_paciente(String paciente) {
        criarPacienteSteps.criarPacienteComNome(paciente);
    }

}
