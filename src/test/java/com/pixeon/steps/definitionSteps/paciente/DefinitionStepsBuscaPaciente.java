package com.pixeon.steps.definitionSteps.paciente;

import com.pixeon.steps.serenity.atendimento.ordemDeServico.LancamentoDeOrdemDeServicoSteps;
import com.pixeon.steps.serenity.paciente.BuscarPacienteSteps;

import com.pixeon.steps.serenity.paciente.PacienteSteps;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsBuscaPaciente {

    @Steps
    BuscarPacienteSteps buscarPacienteSteps;
    @Steps
    PacienteSteps pacienteSteps;
    @Steps
    LancamentoDeOrdemDeServicoSteps lancamentoDeOrdemDeServicoSteps;

    @Dado("^buscar paciente pelo nome \"([^\"]*)\"$")
    public void buscar_paciente_pelo_nome(String nome) {
        buscarPacienteSteps.buscarPacientePeloNome(nome);
    }

    @Dado("^buscar paciente pelo nome sem avancar \"([^\"]*)\"$")
    public void buscar_paciente_pelo_nome_semavancar(String nome) {
        buscarPacienteSteps.buscarPacientePeloNomeSemAvancar(nome);
    }

    @Dado("^seleciono o paciente \"([^\"]*)\" exibido na busca$")
    public void buscar_paciente_pelo_nome_exibido_na_busca(String nome) {
        buscarPacienteSteps.SelecionarPacientePeloNome(nome);
    }

    @Quando("^buscar um paciente \"([^\"]*)\" com o seu número de registro$")
    public void buscar_um_paciente_com_o_seu_número_de_registro(String paciente) {
        pacienteSteps.buscarPacienteComNumeroDeRegisto(paciente);
    }

    @Quando("^clicar no botao avancar$")
    public void clicar_no_botao_avancar() {
        pacienteSteps.clicar_no_botao_avancar();
    }

    @Quando("^buscar o paciente pela forma de busca \"([^\"]*)\" e dados de busca \"([^\"]*)\"$")
    public void buscar_o_paciente_pela_forma_de_busca_e_dados_de_busca(String formaDeBusca, String busca) {
        buscarPacienteSteps.buscarPacienteComDadosDeBusca(formaDeBusca, busca);
    }

    @Quando("^buscar o paciente pela forma de busca \"([^\"]*)\" e dados de busca \"([^\"]*)\" e avançar$")
    public void buscar_o_paciente_pela_forma_de_busca_e_dados_de_busca_e_avançar(String formaDeBusca, String busca) {
        buscarPacienteSteps.buscarPacienteComDadosDeBuscaEAvancar(formaDeBusca, busca);
    }

    @Quando("^buscar ordem de serviço pelo código de amostra \"([^\"]*)\"$")
    public void buscar_ordem_de_serviço_pelo_código_de_amostra(String codigoAmostra) {
        buscarPacienteSteps.buscarOrdemServicoComCodigoAmostra(codigoAmostra);
    }

    @Quando("^voltar a pagina e pegar o valor do campo de busca pela forma de busca \"([^\"]*)\"$")
    public void voltar_a_pagina_e_pegar_o_valor_do_campo_de_busca_pela_forma_de_busca(String formaDeBusca) {
        buscarPacienteSteps.pegarValorCamposOrdemServico(formaDeBusca);
    }

    @Dado("^pegar o número do orçamento ou pre-atendimento gerado$")
    public void pegar_o_número_do_orçamento_ou_pre_atendimento_gerado() {
        buscarPacienteSteps.pegarNumeroOrcamentoPreAtendimento();
    }

    @Quando("^buscar o orcamento ou pre-atendimento pelo número gerado$")
    public void buscar_o_orcamento_ou_pre_atendimento_pelo_número_gerado() {
        buscarPacienteSteps.buscarOrcamentoPreAtendimento();
    }

    @Então("^o sistema valida se foi feito a busca do paciente \"([^\"]*)\" pelo orçamento ou pre-atendimento$")
    public void o_sistema_valida_se_foi_feito_a_busca_do_paciente_pelo_orçamento_ou_pre_atendimento(String matricula) {
        buscarPacienteSteps.validarBuscaPacienteComOrcamentoPreAtendimento(matricula);
    }

    @Então("^o sistema valida se foi feito a busca do paciente com forma de busca \"([^\"]*)\" e dados de busca \"([^\"]*)\"$")
    public void o_sistema_valida_se_foi_feito_a_busca_do_paciente_com_forma_de_busca_e_dados_de_busca(String formaDeBusca, String busca) {
        buscarPacienteSteps.validarBuscaPaciente(formaDeBusca, busca);
    }

    @Então("^o sistema valida a busca de ordem de serviço$")
    public void o_sistema_valida_a_busca_de_ordem_de_serviço() {
        lancamentoDeOrdemDeServicoSteps.validarBuscaOrdemServico();
    }

    @Então("^o sistema valida ausência de ordem de serviço$")
    public void o_sistema_valida_ausência_de_ordem_de_serviço() {
        lancamentoDeOrdemDeServicoSteps.validarAusenciaOrdemServico();
    }

    @Quando("^buscar o paciente com \"([^\"]*)\" \"([^\"]*)\" e nome \"([^\"]*)\"$")
    public void buscar_o_paciente_com_e_nome(String tipoBusca, String busca, String nomePaciente) {
        buscarPacienteSteps.buscarPacienteComDadosDeBusca(tipoBusca, busca, nomePaciente);
    }

    @Então("^o sistema valida se foi feito a busca do paciente com \"([^\"]*)\" \"([^\"]*)\" e nome \"([^\"]*)\"$")
    public void o_sistema_valida_se_foi_feito_a_busca_do_paciente_com_e_nome(String tipoBusca, String busca, String nomePaciente) {
        //
    }

}
