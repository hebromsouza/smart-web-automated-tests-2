package com.pixeon.steps.definitionSteps.paciente;


import com.pixeon.datamodel.paciente.DataModelBuscaDePacienteProntuario;
import com.pixeon.steps.serenity.paciente.BuscaDePacienteProntuarioSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsBuscaDePacienteProntuario {

    @Steps
    BuscaDePacienteProntuarioSteps buscaDePacienteProntuarioSteps;

  //  @Quando("^que preencho informo dados da busca paciente Prontuário$")
  //  public void que_preencho_informo_dados_da_busca_paciente_Prontuário(List<DataModelBuscaDeProntuario> dataBuscaDeProntuario) {
 //       buscaDePacienteProntuarioSteps.preencherbuscaDePacienteProntuario(dataBuscaDeProntuario);
 //   }

    @Quando("^que preencho informo dados da busca paciente Prontuário na tela Busca do paciente$")
    public void que_preencho_informo_dados_da_busca_paciente_Prontuário_na_tela_Busca_do_paciente(List<DataModelBuscaDePacienteProntuario> dataBuscaDePacienteProntuario) {
        buscaDePacienteProntuarioSteps.preencherbuscaDePacienteProntuario(dataBuscaDePacienteProntuario);
    }

    @Quando("^buscar paciente Prontuário na tela Busca do paciente$")
    public void buscar_paciente_Prontuario() {
        buscaDePacienteProntuarioSteps.buscarPacienteProntuario();
    }

 }
