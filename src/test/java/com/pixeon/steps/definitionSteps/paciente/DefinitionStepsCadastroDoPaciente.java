package com.pixeon.steps.definitionSteps.paciente;

import com.pixeon.steps.serenity.paciente.CadastroDoPacienteSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsCadastroDoPaciente {

    @Steps
    CadastroDoPacienteSteps cadastroDoPacienteSteps;

    @Quando("^avançar cadastro do paciente$")
    public void avançar_cadastro_do_paciente() {
        cadastroDoPacienteSteps.avancarCadastroPaciente();
    }

}
