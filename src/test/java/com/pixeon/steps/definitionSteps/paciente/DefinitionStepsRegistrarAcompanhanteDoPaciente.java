package com.pixeon.steps.definitionSteps.paciente;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;
import com.pixeon.steps.serenity.paciente.RegistarAcompanhanteDoPacienteSteps;

public class DefinitionStepsRegistrarAcompanhanteDoPaciente {

    @Steps
    RegistarAcompanhanteDoPacienteSteps registarAcompanhanteDoPaciente;

    @Dado("^fechar tela de intrução do convênio$")
    public void fechar_tela_de_intrução_do_convênio() {
        registarAcompanhanteDoPaciente.fecharPainel();
    }

    @Dado("^acessar a tela de acompanhante$")
    public void acessar_a_tela_de_acompanhante() {
        registarAcompanhanteDoPaciente.acessarTelaAcompanhante();
    }

    @Dado("^acessar a aba de acompanhante paciente por paciente$")
    public void acessar_a_aba_de_acompanhante_paciente_por_paciente() {
        registarAcompanhanteDoPaciente.abaAcompanhantePacientePorPaciente();
    }

    @Quando("^excluir o acompanhante existente$")
    public void excluir_o_acompanhante_existente() {
        registarAcompanhanteDoPaciente.excluirAcompanhanteExistente();
    }

    @Quando("^registrar novo acompanhante \"([^\"]*)\" do paciente de parentesco \"([^\"]*)\"$")
    public void registrar_novo_acompanhante_do_paciente_de_parentesco(String acompanhante, String parentesco) {
        registarAcompanhanteDoPaciente.novoAcompanhanteComParentesco(acompanhante, parentesco);
    }

    @Quando("^registrar novo acompanhante buscando por registro \"([^\"]*)\" e parentesco \"([^\"]*)\"$")
    public void registrar_novo_acompanhante_buscando_por_registro_e_parentesco(String registro, String parentesco) {
        registarAcompanhanteDoPaciente.registrarNovoAcompanhanteBuscandoPorRegistroParentesco(registro, parentesco);
    }

    @Então("^o sistema valida se foi feito o cadastro do acompanhante \"([^\"]*)\"$")
    public void o_sistema_valida_se_foi_feito_o_cadastro_do_acompanhante(String acompanhante) {
        registarAcompanhanteDoPaciente.validaRegistroAcompanhante(acompanhante);
    }

    @Então("^o sistema valida se foi feito o cadastro do acompanhante pelo registro \"([^\"]*)\"$")
    public void o_sistema_valida_se_foi_feito_o_cadastro_do_acompanhante_pelo_registro(String registro) {
        registarAcompanhanteDoPaciente.validaRegistroAcompanhantePacientePorPaciente(registro);
    }

}
