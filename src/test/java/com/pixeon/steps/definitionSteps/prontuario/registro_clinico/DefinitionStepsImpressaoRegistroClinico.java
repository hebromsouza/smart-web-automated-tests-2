package com.pixeon.steps.definitionSteps.prontuario.registro_clinico;

import com.pixeon.steps.serenity.prontuario.registro_clinico.ImpressaoRegistroClinicoSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsImpressaoRegistroClinico {

    @Steps
    ImpressaoRegistroClinicoSteps impressaoRegistroClinicoSteps;

    @Quando("^selecionar no  Adicionar a opção \"([^\"]*)\" na Aba Registro Clinico$")
    public void selecionar_no_Adicionar_a_opção_na_Aba_Registro_Clinico(String item) {
        impressaoRegistroClinicoSteps.selecionarNoAdicionarAOpcao(item);
    }

//    @Quando("^impimir na tela de Protuario$")
////    public void impimir_na_tela_de_Protuario() {
////        prontuarioDoPacienteAbaRegistroClinicoSteps.impimir_na_tela_de_Protuario();
////    }

    @Quando("^impimir na tela de Protuario na Aba Registro Clinico$")
    public void impimir_na_tela_de_Protuario_na_Aba_Registro_Clinico() {
        impressaoRegistroClinicoSteps.impimir_na_tela_de_Protuario_na_Aba_Registro_Clinico();
    }

    @Então("^validar conteudo no PDF, nome do software, fornecedor, versão e build   \"([^\"]*)\"$")
    public void validar_conteudo_no_PDF_nome_do_software_fornecedor_versão_e_build(String txtParavalidar) {
        impressaoRegistroClinicoSteps.validar_conteudo_no_PDF_nome_do_software_fornecedor_versão_e_build(txtParavalidar);
    }
}
