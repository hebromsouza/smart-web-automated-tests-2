package com.pixeon.steps.definitionSteps.prontuario.registro_clinico;

import com.pixeon.steps.serenity.prontuario.registro_clinico.RegistrosClinicosSteps;
import cucumber.api.java.pt.Então;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsRegistrosClinicos {

    @Steps
    RegistrosClinicosSteps registrosClinicosSteps;

    @Então("^o sistema valida se foi feito a busca apenas da ordem de serviço na tela de registros clínicos$")
    public void o_sistema_valida_se_foi_feito_a_busca_apenas_da_ordem_de_serviço_na_tela_de_registros_clínicos() {
        registrosClinicosSteps.validarRegistroClinicoPorOrdemServico();
    }

}
