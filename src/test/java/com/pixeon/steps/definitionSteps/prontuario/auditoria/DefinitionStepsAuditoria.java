package com.pixeon.steps.definitionSteps.prontuario.auditoria;


import com.pixeon.datamodel.busca_auditoria.DataBuscaAuditoria;
import com.pixeon.steps.serenity.prontuario.auditoria.AuditoriaSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsAuditoria {

    @Steps
    AuditoriaSteps auditoriaSteps;

    @Quando("^que preencho dados da busca na  tela de auditoria$")
    public void que_preencho_dados_da_busca_na_tela_de_auditoria(List<DataBuscaAuditoria> dataBuscaAuditoria) {
        auditoriaSteps.que_preencho_dados_da_busca_na_tela_de_auditoria(dataBuscaAuditoria);
    }



    @Quando("^buscar na  tela de auditoria$")
    public void buscar_na_tela_de_auditoria() {
        auditoriaSteps.buscar_na_tela_de_auditoria();
    }



    @Então("^validar o que na tabela contem  \"([^\"]*)\",\"([^\"]*)\" ,\"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" e as \"([^\"]*)\"$")
    public void validar_o_que_na_tabela_contem_e_as(String data, String criticidade, String evento, String identificaco_do_componente, String dentificacao_do_usuario, String  indicacacao_de_atividade, String identificador_do_registro) {
        auditoriaSteps.validar_o_que_na_tabela_contem_e_as(data,criticidade,evento,identificaco_do_componente,dentificacao_do_usuario, indicacacao_de_atividade, identificador_do_registro);

    }



}
