package com.pixeon.steps.definitionSteps.prontuario.registro_clinico;
import com.pixeon.steps.serenity.prontuario.registro_clinico.ArquivosSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsArquivos {

    @Steps
    ArquivosSteps arquivosSteps;

    @Quando("^adicionar arquivos na tela de arquivos$")
    public void adicionar_arquivos_na_tela_de_arquivos() {
        arquivosSteps.adicionar_arquivos_na_tela_de_arquivos();
    }

    @Então("^o sistema valida se foi anexado o arquivo na tela de arquivos$")
    public void o_sistema_valida_se_foi_anexado_o_arquivo_na_tela_de_arquivos() {
        arquivosSteps.o_sistema_valida_se_foi_anexado_o_arquivo_na_tela_de_arquivos();
    }

    @Quando("^fechar tela de arquivos$")
    public void fechar_tela_de_arquivos() {
        arquivosSteps.fechar_tela_de_arquivos();
    }

    @Quando("^habilitar o Flash na tela de arquivo$")
    public void habilitar_o_Flash_na_tela_de_arquivo() {
        arquivosSteps.habilitar_o_Flash_na_tela_de_arquivo();
    }

}
