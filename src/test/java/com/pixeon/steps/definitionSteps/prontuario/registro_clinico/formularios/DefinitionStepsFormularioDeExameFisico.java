package com.pixeon.steps.definitionSteps.prontuario.registro_clinico.formularios;
import com.pixeon.steps.serenity.prontuario.registro_clinico.formularios.FormularioDeExameFisicoSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsFormularioDeExameFisico {

    @Steps
    FormularioDeExameFisicoSteps formularioDeExameFisicoSteps;

    @Quando("^anexar arquivos na tela de Formularios de exame Fisico$")
    public void anexar_arquivos_na_tela_de_Formularios_de_exame_Fisico() {
        formularioDeExameFisicoSteps.anexar_arquivos_na_tela_de_Formularios_de_exame_Fisico();
    }


    @Quando("^informar \"([^\"]*)\" no campo exame fisico na tela de Formularios de exame Fisico$")
    public void informar_no_campo_exame_fisico_na_tela_de_Formularios_de_exame_Fisico(String exame_fisico) {
        formularioDeExameFisicoSteps.informar_no_campo_exame_fisico_na_tela_de_Formularios_de_exame_Fisico(exame_fisico);
    }

    @Quando("^Finalizaro laudo na tela de Formularios de exame Fisico$")
    public void finalizaro_laudo_na_tela_de_Formularios_de_exame_Fisico() {
        formularioDeExameFisicoSteps.finalizaro_laudo_na_tela_de_Formularios_de_exame_Fisico();
    }

    @Quando("^fechar formulário na tela de Formularios de exame Fisico$")
    public void fechar_formulário_na_tela_de_Formularios_de_exame_Fisico() {
    formularioDeExameFisicoSteps.fechar_formulário_na_tela_de_Formularios_de_exame_Fisico();
    }
}
