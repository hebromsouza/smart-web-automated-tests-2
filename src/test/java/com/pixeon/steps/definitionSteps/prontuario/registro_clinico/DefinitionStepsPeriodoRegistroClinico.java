package com.pixeon.steps.definitionSteps.prontuario.registro_clinico;


import com.pixeon.steps.serenity.prontuario.registro_clinico.PeriodoRegistroClinicoSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsPeriodoRegistroClinico {

    @Steps
    PeriodoRegistroClinicoSteps periodoRegistroClinicoSteps;

    @Quando("^informar a data inicial \"([^\"]*)\" e a data Final \"([^\"]*)\" na tela de periodo e busca$")
    public void informar_a_data_inicial_e_a_data_Final_na_tela_de_periodo_e_busca(String dataInicio, String dataFim) {
        periodoRegistroClinicoSteps.informar_a_data_inicial_e_a_data_Final_na_tela_de_periodo_e_busca(dataInicio,dataFim);
    }
    @Quando("^fechar a tela de Periodo$")
    public void fechar_a_tela_de_Periodo() {
        periodoRegistroClinicoSteps.fechar_a_tela_de_Periodo();
    }

    @Quando("^confirmar o popoup \"([^\"]*)\"$")
    public void confirmar_o_popoup(String mensagem) {
        periodoRegistroClinicoSteps.confirmar_o_popoup(mensagem);
    }

    @Então("^o sistema valida na impressão em PDF, o campo \"([^\"]*)\" na tela de impresão do prontuario$")
    public void o_sistema_valida_na_impressão_em_PDF_o_campo_na_tela_de_impresão_do_prontuario(String mesagem) {
        periodoRegistroClinicoSteps.o_sistema_valida_na_impressao_em_PDF_o_campo_na_tela_de_impresao_do_prontuario(mesagem);
    }

}
