package com.pixeon.steps.definitionSteps.prontuario.exames;

import com.pixeon.steps.serenity.prontuario.exames.ExamesSteps;
import cucumber.api.java.pt.Então;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsExames {

    @Steps
    ExamesSteps examesSteps;

    @Então("^cancelo a solicitação de exames com motivo \"([^\"]*)\"$")
    public void cancelo_a_solicitação_de_exames_com_motivo(String motivo) {
        examesSteps.cancelarSolicitacaoExamesComMotivo(motivo);
    }

}
