package com.pixeon.steps.definitionSteps.prontuario.acessoNegado;
import com.pixeon.steps.serenity.prontuario.acessoNegado.AcessoNegadoSteps;
import cucumber.api.java.pt.Então;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepAcessoNegado {

    @Steps
    AcessoNegadoSteps acessoNegadoSteps;

    @Então("^é apresentado a mensagem \"([^\"]*)\" da tela acesso negado$")
    public void é_apresentado_a_mensagem_da_tela_acesso_negado(String acessonegado) {
        acessoNegadoSteps.é_apresentado_a_mensagem_da_tela_acesso_negado(acessonegado);
    }
}
