package com.pixeon.steps.definitionSteps.prontuario.registro_clinico.formularios;

import com.pixeon.steps.serenity.prontuario.registro_clinico.formularios.FormularioDeAnamineseSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsFormularioDeAnaminese {

    @Steps
    FormularioDeAnamineseSteps formularioDeAnamineseSteps;

    @Quando("^editar o campo Queixa Principal para \"([^\"]*)\" da tela Formulario de anaminese$")
    public void editar_o_campo_Queixa_Principal_para_da_tela_Formulario_de_anaminese(String queixa_Principal) {
        formularioDeAnamineseSteps.inserirDadosTxtQueixaPrincipal(queixa_Principal);
    }

    @Quando("^Gravar na tela Formulario de anaminese$")
    public void gravar_na_tela_Formulario_de_anaminese() {
        formularioDeAnamineseSteps.selecionaGravar();
    }

    @Quando("^assinar  na tela Formulario de anaminese$")
    public void assinar_na_tela_Formulario_de_anaminese() {
        formularioDeAnamineseSteps.selecionaAssinar();
    }

    @Quando("^Fechar a tela Formulario de anaminese$")
    public void fechar_a_tela_Formulario_de_anaminese() {
        formularioDeAnamineseSteps.fechar_a_tela_Formulario_de_anaminese();
    }

    @Quando("^aprovar da tela Formulario de anaminese$")
    public void aprovar_da_tela_Formulario_de_anaminese() {
        formularioDeAnamineseSteps.aprovar_da_tela_Formulario_de_anaminese();
    }

    @Então("^e apresentado a mensagem \"([^\"]*)\" e confirma o popup da tela Formulario de anaminese$")
    public void e_apresentado_a_mensagem_e_confirma_o_popup_da_tela_Formulario_de_anaminese(String mensagem) {
        formularioDeAnamineseSteps.e_apresentado_a_mensagem_e_confirma_o_popup_da_tela_Formulario_de_anaminese(mensagem);
    }

    @Quando("^finalizar na tela Formulario de anaminese$")
    public void finalizar_na_tela_Formulario_de_anaminese() { formularioDeAnamineseSteps.finalizar_na_tela_Formulario_de_anaminese(); }

}
