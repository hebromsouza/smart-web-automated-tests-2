package com.pixeon.steps.definitionSteps.prontuario.registro_clinico;
import com.pixeon.steps.serenity.prontuario.registro_clinico.JustificativaSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;


public class DefinitionStepsJustificativa {

    @Steps
    JustificativaSteps justificativaSteps;

    @Quando("^informar a Justificativa \"([^\"]*)\" e confiramr  na tela de Justificativa$")
    public void informar_a_Justificativa_e_confiramr_na_tela_de_Justificativa(String justificativa) {
        justificativaSteps.informar_a_Justificativa_e_confiramr_na_tela_de_Justificativa(justificativa);
    }
}
