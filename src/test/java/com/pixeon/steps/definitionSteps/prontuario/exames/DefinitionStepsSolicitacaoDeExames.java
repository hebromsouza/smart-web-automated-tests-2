package com.pixeon.steps.definitionSteps.prontuario.exames;

import com.pixeon.steps.serenity.prontuario.exames.SolicitacaoDeExamesSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsSolicitacaoDeExames {

    @Steps
    SolicitacaoDeExamesSteps solicitacaoDeExamesSteps;

    @Quando("^fizer a solicitação de exames$")
    public void fizer_a_solicitação_de_exames() {
        solicitacaoDeExamesSteps.solicitacaoExame();
    }

    @Quando("^selecionar grupo de \"([^\"]*)\"$")
    public void selecionar_grupo_de(String grupo) {
        solicitacaoDeExamesSteps.selecionarGrupo(grupo);
    }


    @Quando("^selecionar exames de grupo cardiologia$")
    public void selecionar_exames_de_grupo_cardiologia() {
        solicitacaoDeExamesSteps.selecionarExamesCardiologia();
    }

    @Quando("^selecionar grupo de Laboratorio e sub-grupo de Bacteriologia$")
    public void selecionar_grupo_de_Laboratorio_e_sub_grupo_de_Bacteriologia() {
        solicitacaoDeExamesSteps.selecionarGrupoLaboratorioESubGrupoBacteriologia();
    }

    @Quando("^selecionar exames de grupo laboratorio$")
    public void selecionar_exames_de_grupo_laboratorio() {
        solicitacaoDeExamesSteps.selecionarExamesGrupoLaboratorio();
    }

    @Quando("^avançar para preenchimento de dados$")
    public void avançar_para_preenchimento_de_dados() {
        solicitacaoDeExamesSteps.avancarPreenchimentoDados();
    }

    @Quando("^selecionar o profissional \"([^\"]*)\"$")
    public void selecionar_o_profissional(String profissional) {
        solicitacaoDeExamesSteps.selecionarProfissional(profissional);
    }

    @Quando("^selecionar tipo de atendimento \"([^\"]*)\"$")
    public void selecionar_tipo_de_atendimento(String tipoAtendimento) {
        solicitacaoDeExamesSteps.selecionarTipoAtendimento(tipoAtendimento);
    }

    @Quando("^selecionar tipo de saída \"([^\"]*)\"$")
    public void selecionar_tipo_de_saída(String tipoSaida) {
        solicitacaoDeExamesSteps.selecionarTipoSaida(tipoSaida);
    }

    @Quando("^concluir solicitação$")
    public void concluir_solicitação() {
        solicitacaoDeExamesSteps.concluirSolicitacao();
    }

    @Então("^cancelo a impressão da solicitação$")
    public void cancelo_a_impressão_da_solicitação() {
        solicitacaoDeExamesSteps.cancelarImpressaoSolicitacao();
    }

    @Então("^o sistema valida pedido de exames de cardiologia e laboratorio$")
    public void o_sistema_valida_pedido_de_exames_de_cardiologia_e_laboratorio() {
        solicitacaoDeExamesSteps.validarPedidoExamesCardiologiaLaboratorio();
    }

}
