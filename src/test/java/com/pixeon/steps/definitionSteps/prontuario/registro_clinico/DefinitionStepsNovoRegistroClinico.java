package com.pixeon.steps.definitionSteps.prontuario.registro_clinico;

import com.pixeon.steps.serenity.prontuario.registro_clinico.NovoRegistroClinicoSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsNovoRegistroClinico {

    @Steps
    NovoRegistroClinicoSteps novoRegistroClinicoSteps;

    @Quando("^selecionar o Formulário Anaminese na tela de Novo registro$")
    public void selecionar_o_Formulário_Anaminese_na_tela_de_Novo_registro() {
        novoRegistroClinicoSteps.selecionaFormularioDeAnaminese();
    }
    @Quando("^informar o registro \"([^\"]*)\" e grava na tela de novo registro$")
    public void informar_o_registro_e_grava_na_tela_de_novo_registro(String registro) {
        novoRegistroClinicoSteps.infromar_o_registro_e_grava_na_tela_de_novo_registro(registro);
    }

    @Então("^validar se o Status do \"([^\"]*)\" para o atendimento feito na tela de prontuário$")
    public void validar_se_o_Status_do_para_o_atendimento_feito_na_tela_de_prontuário(String formulario) {
        novoRegistroClinicoSteps.validar_se_o_Status_do_para_o_atendimento_feito_na_tela_de_prontuário(formulario);
    }

    @Quando("^Seleciono o último FORMULÁRIO ANAMNESE - Aguardando aprovação inserido na  tela de novo registro$")
    public void seleciono_o_último_FORMULÁRIO_ANAMNESE_Aguardando_aprovação_inserido_na_tela_de_novo_registro() {
        novoRegistroClinicoSteps.seleciono_o_último_FORMULÁRIO_ANAMNESE_Aguardando_aprovação_inserido_na_tela_de_novo_registro();
    }
    @Então("^o sistema valida se foi informado que o registro clínico \"([^\"]*)\" esta com o status \"([^\"]*)\" e com o icone de anexo na  prontuario aba registro clinico$")
    public void o_sistema_valida_se_foi_informado_que_o_registro_clínico_esta_com_o_status_e_com_o_icone_de_anexo_na_prontuario_aba_registro_clinico(String formulario, String status) {
        novoRegistroClinicoSteps.o_sistema_valida_se_foi_informado_que_o_registro_clínico_esta_com_o_status_e_com_o_icone_de_anexo_na_prontuario_aba_registro_clinico(formulario,status);
    }


}
