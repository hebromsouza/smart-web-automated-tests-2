package com.pixeon.steps.definitionSteps.prontuario.selecao_aba_prontuario;

import com.pixeon.steps.serenity.prontuario.selecao_aba_prontuario.SelecaoAbaProntuarioSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsSelecaoAbaProntuario {

    @Steps
    SelecaoAbaProntuarioSteps selecaoAbaProntuarioSteps;

    @Quando("^acessar a aba \"([^\"]*)\" na tela de Prontuário$")
    public void acessar_a_aba_na_tela_de_Prontuário(String abaProntuario) {
        selecaoAbaProntuarioSteps.solecionarAbaRegistro(abaProntuario);
    }

}
