package com.pixeon.steps.definitionSteps.home;

import com.pixeon.steps.serenity.home.HomeSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsHome {

    @Steps
    HomeSteps homeSteps;

    @Então("^é apresentado no software nome do software e a identificação da versão \"([^\"]*)\", build  \"([^\"]*)\" e o  nome do fornecedor \"([^\"]*)\" na parte princial Home$")
    public void é_apresentado_no_software_nome_do_software_e_a_identificação_da_versão_build_e_o_nome_do_fornecedor_na_parte_princial_Home(String StringnomeSoftware_versaoSoftware, String buildSoftware, String nomeFornecedor) {
        homeSteps.validaVersaoDoSoftware(StringnomeSoftware_versaoSoftware, buildSoftware, nomeFornecedor);
    }

    @Então("^deslogar$")
    public void deslogar() {
        homeSteps.deslogar();
    }

    @Então("^o menu \"([^\"]*)\" não devera esta presente na tela Home$")
    public void o_menu_não_devera_esta_presente_na_tela_Home(String menuNaoPresente) {
        homeSteps.o_menu_nao_devera_esta_presente_na_tela_Home(menuNaoPresente);
    }

    @Então("^os sistema apresenta a tela Home da aplicação$")
    public void os_sistema_apresenta_a_tela_Home_da_aplicação() {
        homeSteps.os_sistema_apresenta_a_tela_Home_da_aplicacao();
    }

}
