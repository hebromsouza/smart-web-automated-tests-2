package com.pixeon.steps.definitionSteps.agenda;

import com.pixeon.steps.serenity.agenda.AgendamentoSteps;
import com.pixeon.steps.serenity.motivoCancelamento.MotivoCancelamentoSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAgendamento {

    @Steps
    AgendamentoSteps agendamentoSteps;
    @Steps
    MotivoCancelamentoSteps motivoCancelamentoSteps;

    @Quando("^filtrar agenda pela especialidade \"([^\"]*)\" medico \"([^\"]*)\" e unidade \"([^\"]*)\"$")
    public void filtrar_agenda_pela_especialidade_medico_e_unidade(String especialidade, String medico, String unidade) {
        agendamentoSteps.filtrarAgendaComEspecialidadeMedicoUnidade(especialidade, medico, unidade);
    }

    @Quando("^selecionar o dia e horário da marcação$")
    public void selecionar_o_dia_e_horário_da_marcação() {
        agendamentoSteps.selecionarDiaHorarioMarcacao();
    }

    @Quando("^selecionar procedimento \"([^\"]*)\"$")
    public void selecionar_procedimento(String procedimento) {
        agendamentoSteps.selecionarProcedimento(procedimento);
    }

    @Quando("^confirmar marcação web para o paciente$")
    public void confirmar_marcação_web_para_o_paciente() {
        agendamentoSteps.confirmarMarcacaoWebPaciente();
    }

    @Quando("^seleciona o paciente \"([^\"]*)\" na agenda do médico selecionando \"([^\"]*)\"$")
    public void seleciona_o_paciente_na_agenda_do_médico_selecionando(String paciente, String acaoAgenda) {
        agendamentoSteps.selecionaPacienteAgendaMedicoSelecionandoAcaoAgenda(paciente, acaoAgenda);
    }

    @Quando("^voltar para a página de agendamento$")
    public void voltar_para_a_página_de_agendamento() {
        agendamentoSteps.voltarTelaAgendamento();
    }

    @Então("^verifico que o paciente \"([^\"]*)\" foi agendado com sucesso$")
    public void verifico_que_o_paciente_foi_agendado_com_sucesso(String paciente) {
        agendamentoSteps.VerificarPacienteAgendado(paciente);
    }

    @Então("^o sistema valida se aparece os horários do médico \"([^\"]*)\"$")
    public void o_sistema_valida_se_aparece_os_horários_do_médico(String medico) {
        agendamentoSteps.validarAgendaMedico(medico);
    }

    @Quando("^cancelar agendamento$")
    public void cancelar_agendamento() {
        agendamentoSteps.cancelarAgenda();
    }

//    @Então("^o sistema valida cancelamento da agenda para o paciente \"([^\"]*)\"$")
//    public void o_sistema_valida_cancelamento_da_agenda_para_o_paciente(String paciente) {
//        agendamentoSteps.validarCancelamentoAgenda(paciente);
//    }

    @Então("^o paciente \"([^\"]*)\" é agendado$")
    public void o_paciente_é_agendado(String paciente) throws InterruptedException {
        agendamentoSteps.validarPacienteAgendado(paciente);
    }

    @Então("^o médico cancela marcação para o paciente \"([^\"]*)\"$")
    public void o_médico_cancela_marcação_para_o_paciente(String paciente) {
        agendamentoSteps.cancelarMarcacaoPaciente(paciente);
        motivoCancelamentoSteps.informarMotivoCancelamentoObservacao("ALTERAÇÃO NO NOME DO MÉDICO", "Teste observação cancelamento");
    }

    @Então("^o medico verifica se o cancelamento foi realizado com sucesso$")
    public void medico_verifica_o_cancelamento_sucesso() {
        agendamentoSteps.validarCancelamentoAgenda();
    }

}
