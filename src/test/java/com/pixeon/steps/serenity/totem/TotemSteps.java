package com.pixeon.steps.serenity.totem;

import com.pixeon.pages.atendimento.filaDeEspera.PageFilaDeEspera;
import com.pixeon.pages.atendimento.ordemDeServico.PageBuscaDeProfissional;
import com.pixeon.pages.login.PageLogin;
import com.pixeon.pages.totem.PageDetalhesPacienteFilaEspera;
import com.pixeon.pages.totem.PageRetiradaSenha;
import com.pixeon.pages.totem.PageTotem;
import com.pixeon.steps.serenity.login.login.LoginSteps;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;


public class TotemSteps {

    PageTotem pageTotem;
    PageLogin pageLogin;
    PageBuscaDeProfissional pageBuscaDeProfissional;
    PageRetiradaSenha pageRetiradaSenha;
    PageFilaDeEspera pageFilaDeEspera;
    PageDetalhesPacienteFilaEspera pageDetalhesPacienteFilaDeEspera;


    @Steps
    LoginSteps loginSteps;

    static String winHandleBefore;
    static String winHandleBeforeTotem;

    @Step
    public void acesar_url_tokem(String url) {
     //   pageTotem.open();
        pageTotem.getDriver().get(url);
        pageTotem.validarTelaTotemAberta();

    }

    @Step
    public void selecionarSetor(String setor) {
        pageTotem.clicarLupaBuscarSetor();
        pageTotem.escreverSetor(setor);
        pageTotem.clicarBotaoBuscarSetores();
        pageTotem.clicarLinkSetor(setor);
    }

    @Step
    public void selecionarMedico(String medico) {
        clicarLupaMedico(1);
        pageBuscaDeProfissional.digitarNomeMedico(medico);
        pageBuscaDeProfissional.clicarBotaoBuscar();
        pageBuscaDeProfissional.clicarLinkNomeMedico();
    }

    @Step
    public void clicarLupaMedico(int idMedico){
        switch (idMedico){
            case 2:
                pageTotem.clicarLupaMedico2();
                break;
            case 3:
                pageTotem.clicarLupaMedico3();
                break;
            case 4:
                pageTotem.clicarLupaMedico4();
                break;
            default:
                pageTotem.clicarLupaSelecionarMedico();
        }
    }

    @Step
    public void clicarLupaTipoAtendimento(int idMedico){
        switch (idMedico){
            case 2:
                pageTotem.clicarLupaTipoAtendimento2();
                break;
            case 3:
                pageTotem.clicarLupaTipoAtendimento3();
                break;
            case 4:
                pageTotem.clicarLupaTipoAtendimento4();
                break;
            default:
                pageTotem.clicarLupaTipoAtendimento();
        }
    }

    @Step
    public void selecionarMedico(int idMedico, String medico){
        clicarLupaMedico(idMedico);
        pageBuscaDeProfissional.digitarNomeMedico(medico);
        pageBuscaDeProfissional.clicarBotaoBuscar();
        pageBuscaDeProfissional.clicarLinkNomeMedico();
    }

    @Step
    public void selecionarTipoAtendimento(String tipoAtendimento){
        clicarLupaTipoAtendimento(1);
        pageTotem.clicarTipoAtendimento(tipoAtendimento);
    }

    @Step
    public void selecionarTipoAtendimento(int idMedico, String tipoAtendimento){
        clicarLupaTipoAtendimento(idMedico);
        pageTotem.clicarTipoAtendimento(tipoAtendimento);
    }

    @Step
    public void acessarSmartweb(){
        pageTotem.acessarSmartWeb();
    }

    @Step
    public void retirarSenha() {
        pageTotem.clicarBotaoRetiradaSenha();
        pageRetiradaSenha.clicarBotaoToqueAqui();
        pageTotem.getDriver().switchTo().window(winHandleBeforeTotem);
    }

    @Step
    public void voltarPaginaTotemSmartweb(){
        pageTotem.switchWindowClose("fs");
    //    pageTotem.switchWindow(winHandleBeforeTotem);
        pageTotem.getDriver().navigate().back();
    //    pageTotem.switchWindow(winHandleBefore);
        pageTotem.carregarFrameDefault();
    }

    @Step
    public void fecharPainelVoltarAtelaDoTotem() {
        pageTotem.switchWindowClose("fs");
    }

    @Step
    public void trocarJanelaSairdoTotemParaSmartweb() {
        pageTotem.getDriver().switchTo().window(winHandleBeforeTotem);
        pageTotem.getDriver().navigate().back();


        //pageTotem.switchWindow(winHandleBefore);
        //pageTotem.carregarFrameDefault();
    }



    @Step
    public void verifico_que_as_senhas_foram_retiradas_com_sucesso_e_sem_erros() {
        pageTotem.switchWindow("1");
        pageTotem.validarTelaTotemAberta();
        pageTotem.ValidacaoSenhasRetiradasSemErros();
    }

    @Step
    public void abrirChamadaSenha() {
    //    pageTotem.switchWindowClose("fs");
        pageTotem.clicarBotaoChamadaSenha();
    }

    @Step
    public void validarChamadaSenhaPainel() {
        pageTotem.switchWindow("fs");
        String retiradaSenha = pageFilaDeEspera.retornaSenhaGeradaNaRetiradaDeSenhaDoTotem(PageRetiradaSenha.retiradaSenha);
        pageTotem.validarChamadaSenhaPainel(retiradaSenha);
    }

    @Step
    public void cancelarSenhaGerada() {
        pageTotem.switchWindowClose("fs");
        pageTotem.switchWindow(winHandleBefore);
        pageTotem.carregarFrameDefault();
        pageFilaDeEspera.clicarSenhaGeradaNoTotem(pageFilaDeEspera.retornaSenhaGeradaNaRetiradaDeSenhaDoTotem(PageRetiradaSenha.retiradaSenha));
        pageFilaDeEspera.clicarLinkDetalhes();
        pageDetalhesPacienteFilaDeEspera.selecionarStatusPacienteFilaDeEspera("Cancelado");
        pageDetalhesPacienteFilaDeEspera.clicarBotaoGravar();
        Assert.assertFalse(pageFilaDeEspera.retornaVisibilidadeSenhaGeradaFilaEspera(PageRetiradaSenha.retiradaSenha));
    }

    @Step
    public void retirarSenhaNoTotemSetorMedico(String url, String setor, String medico) {
        acesar_url_tokem(url);
        selecionarSetor(setor);
        selecionarMedico(medico);
        retirarSenha();
    }

    @Step
    public void selecionarMedicoComTipoDeAtendimento(String medico, String tipoAtendimento) {
        selecionarMedico(medico);
        selecionarTipoAtendimento(tipoAtendimento);
    }

    @Step
    public void abrirPainelRetiradaSenha() {
        pageTotem.clicarBotaoRetiradaSenha();
    }

    @Step
    public void retirarSenhasTipoAtendimento(int quantidadeSenhas, String tipoAtendimento) {
        for (int i=0; i<quantidadeSenhas; i++){
            pageRetiradaSenha.clicarBotaoToqueAquiTipoAtendimento(tipoAtendimento);
        }
    }

    @Step
    public void selecionarListaMedicosComTipoDeAtendimento(int idMedico, String medico, String tipoAtendimento) {
        selecionarMedico(idMedico, medico);
        selecionarTipoAtendimento(idMedico, tipoAtendimento);
    }

    @Step
    public void validarChamadaProximaSenhaPainelComPrefixoTipoAtendimento(String prefixo) {
        pageTotem.switchWindow("fs");
        pageTotem.validarChamadaSenhaPainel(prefixo);
        pageTotem.switchWindow(winHandleBefore);
        pageTotem.carregarFrameDefault();
    }

    @Step
    public void validarChamadaPacientePainelDeChamadaParaMedico(String paciente, String medico) {
        pageTotem.switchWindow("fs");
        pageTotem.validarChamadaSenhaPainel(paciente);
        //pageTotem.validarChamadaSenhaPainelMedico(medico);
        pageTotem.switchWindow(winHandleBefore);
        pageTotem.carregarFrameDefault();
    }

}