package com.pixeon.steps.serenity.agenda;

import com.pixeon.pages.agenda.PageAgendamento;
import com.pixeon.pages.agenda.PageConfirmacaoDeAgendamento;
import com.pixeon.pages.common.PageActionCommon;
import com.pixeon.util.SeleniumUtil;
import net.thucydides.core.annotations.Step;

public class AgendamentoSteps {

    PageAgendamento pageAgendamento;
    PageConfirmacaoDeAgendamento pageConfirmacaoDeAgendamento;
    PageActionCommon pageActionCommon;

    @Step
    public void filtrarAgendaComEspecialidadeMedicoUnidade(String especialidade, String medico, String unidade){
        pageActionCommon.divAguarde();
        pageAgendamento.selecionarEspecialidade(especialidade);
        pageAgendamento.selecionarMedico(medico);
        pageAgendamento.selecionarunidade(unidade);
    }

    @Step
    public void selecionarDiaHorarioMarcacao(){
        pageAgendamento.clicarDiaLivre();
        pageAgendamento.clicarHorarioDisponivel();
    }

    @Step
    public void selecionarProcedimento(String procedimento){
        pageConfirmacaoDeAgendamento.selecionarProcedimento(procedimento);
        pageConfirmacaoDeAgendamento.digitarObservacao();
        //pageConfirmacaoDeAgendamento.clicarBotaoConfirmar();
    }

    @Step
    public void confirmarMarcacaoWebPaciente(){
        pageConfirmacaoDeAgendamento.clicarBotaoConfirmar();
        pageConfirmacaoDeAgendamento.aceitarPopUpConfirmacaoAgendamento();
        pageConfirmacaoDeAgendamento.aceitarPopUpPrazoConsulta();

        if(SeleniumUtil.isAlertPresent()) {
            pageConfirmacaoDeAgendamento.aceitarPopUpMarcacaoMesmoMedico();
        }
        pageActionCommon.divAguarde();
    }

    @Step
    public void selecionaPacienteAgendaMedicoSelecionandoAcaoAgenda(String paciente, String acaoAgenda){
        pageAgendamento.clicarPacienteAgendado(paciente);
        pageAgendamento.clicarAcaoAgenda(acaoAgenda);
    }

    @Step
    public void voltarTelaAgendamento() {
        pageAgendamento.clicarBotaoVoltarParaAgenda();
        pageAgendamento.confirmarPopUpRetornarParaTeste();
    }

    @Step
    public void validarAgendaMedico(String medico) {
        pageAgendamento.validarAgendaMedico(medico);
    }

    @Step
    public void VerificarPacienteAgendado(String paciente) {
        pageAgendamento.VerificarEValidarPacienteAgendado(paciente);
    }


    @Step
    public void cancelarAgenda() {
        pageAgendamento.clicarBotaoCancelarMarcacao();
        pageAgendamento.confirmarPopUpCancelamentoMarcacao();
    }

    @Step
    public void validarCancelamentoAgenda() {
        pageAgendamento.validarCancelamentoAgenda();
    }

    @Step
    public void validarPacienteAgendado(String paciente) {
        pageAgendamento.VerificarSePacienteFoiAgendado(paciente);
    }

    @Step
    public void cancelarMarcacaoPaciente(String paciente) {
        pageAgendamento.clicarPacienteAgendado(paciente);
     //   pageAgendamento.clicarAcaoAgenda("Cancelar marcação");
    //    pageAgendamento.confirmarPopUpCancelamentoMarcacao();
    }

}
