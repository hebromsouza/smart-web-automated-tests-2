package com.pixeon.steps.serenity.intern;

import com.pixeon.pages.prontuario.PageProntuarioHome;
import com.pixeon.pages.prontuario.registro_clinico.PageRegistrosClinicos;
import net.thucydides.core.annotations.Step;

public class RetirarPacienteInternacaoSteps {

    PageProntuarioHome pageProntuarioHome;

    PageRegistrosClinicos pageRegistrosClinicos;

    @Step
    public void retirarPacienteVisualizacaoInternacao() {
        pageProntuarioHome.clicarSairDaInternacaoTratamento();
    }

    @Step
    public void validarExistenciaDeRegistroClinicoInternacao() {
        pageRegistrosClinicos.validarExistenciaRegistroClinicoInternacao();
    }

    @Step
    public void validarExistenciaMedicoRegistroClinico(String medico){
        pageRegistrosClinicos.validarExistenciaMedicoRegistroClinico(medico);
    }

}
