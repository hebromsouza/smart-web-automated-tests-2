package com.pixeon.steps.serenity.intern;

import com.pixeon.pages.intern.PageDetalhesInternacao;
import net.thucydides.core.annotations.Step;

public class ConsultaInternacaoSteps {

    PageDetalhesInternacao pageDetalhesInternacao;

    @Step
    public void abrirDetalhesInternacaoTratamento() {
        pageDetalhesInternacao.clicarIconeInternacaoTratamento();
    }

    @Step
    public void validarStatusInternacao() {
        pageDetalhesInternacao.verificarExistenciaStatusAberto();
    }
}
