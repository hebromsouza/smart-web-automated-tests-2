package com.pixeon.steps.serenity.baixarVersao;

import com.pixeon.pages.atendimento.entregaDeLaudos.PageEntregaLaudos;
import com.pixeon.util.SeleniumUtil;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.io.IOException;

public class BaixarVersaoSteps {

    SeleniumUtil seleniumUtil;
   @Step
    public void que_baixei_a_ultima_versão_disponivel_do_smartweb() {
     // deletar arquivo
       seleniumUtil.deletarArquivosDeUmDiretorio("C:\\repositorio\\smart-web-automated-tests\\exe");
     //copiar ultimo arquivo gerado para pasta
       seleniumUtil.copiaArquivo(seleniumUtil.capituranomeUtimaDiretorioCriado(""),"C:\\repositorio\\smart-web-automated-tests\\exe\\smartweb.exe");
    }


    @Step
    public void faço_a_instalação_da_versão() {
       try {
            Process p = Runtime.getRuntime().exec(new String[] {"C:\\repositorio\\smart-web-automated-tests\\lib\\Exec Smartweb.bat"});
            Thread.sleep(200000);
            Assert.assertTrue("Atualização efetuada",true);
            System.out.println("-----------------------------Atualização efetuada ---------------------------");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.assertTrue(e.getMessage(),false);
        } catch (InterruptedException e) {
           e.printStackTrace();
           Assert.assertTrue(e.getMessage(),false);
       }
    }



}
