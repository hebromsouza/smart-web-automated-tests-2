package com.pixeon.steps.serenity.cadastros.profissionais;

import com.pixeon.datamodel.cadastros.DataModelBuscaDeProfinionais;
import com.pixeon.pages.cadastros.profissionais.PageBuscaDeProfissionais;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class BuscaDeProfissionaisSteps {
    PageBuscaDeProfissionais pageBuscaDeProfissionais;

    @Step
    public void que_preencho_informo_dados_da_busca_paciente_Profissionais_na_tela_Busca_do_Profissionais(List<DataModelBuscaDeProfinionais> dataModelBuscaDeProfinionais) {
        pageBuscaDeProfissionais.preencheCamposDeBuscaDePafissionais(dataModelBuscaDeProfinionais);
    }

    @Step
    public void buscar_paciente_Prontuário_na_tela_Busca_do_Profissionais() {
        pageBuscaDeProfissionais.clicaFiltrar();
    }

    @Step
    public void selecionar_o_profissional_na_tela_Busca_do_Profissionais(String profissional) {
        pageBuscaDeProfissionais.clicaNoNomeDoProficonal(profissional);
    }
}
