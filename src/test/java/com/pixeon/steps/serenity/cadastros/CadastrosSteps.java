package com.pixeon.steps.serenity.cadastros;

import com.pixeon.pages.cadastros.PageCadastros;
import net.thucydides.core.annotations.Step;

public class CadastrosSteps {

    PageCadastros pageCadastros;

    @Step
    public void selecionar_o_cadastro_de_na_tela_de_Cadastros_Cadastro_de_Gerais(String cadastro) {
        pageCadastros.clicarNoTipoCadastro(cadastro);
    }

    @Step
    public void acessar_a_configuração_da_tela_de_Cadastro(String configuracao) {
        pageCadastros.clicarLinkoConfiguracao(configuracao);
    }

}
