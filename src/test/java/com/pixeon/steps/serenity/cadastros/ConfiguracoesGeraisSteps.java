package com.pixeon.steps.serenity.cadastros;

import com.pixeon.pages.cadastros.PageConfiguracoesGerais;
import net.thucydides.core.annotations.Step;

public class ConfiguracoesGeraisSteps {

    PageConfiguracoesGerais pageConfiguracoesGerais;

    @Step
    public void modificar_o_parametro_tempo_de_expiração_para_minuto_na_tela_de_configurações_Gerais(String tempo) {
        pageConfiguracoesGerais.informarTempoDeExpiracaoDaSessao(tempo);
    }

    @Step
    public void gravar_na_tela_de_configurações_Gerais() {
        pageConfiguracoesGerais.clicarbtnGravar();
    }

    @Step
    public void fechar_na_tela_de_configurações_Gerais() {
        pageConfiguracoesGerais.clicarbtnFechar();
    }

    @Step
    public void modificar_o_parametro_quantidade_de_tentativa_invalida_para(String tentativa) {
        pageConfiguracoesGerais.informatetativalogin(tentativa);
    }

}
