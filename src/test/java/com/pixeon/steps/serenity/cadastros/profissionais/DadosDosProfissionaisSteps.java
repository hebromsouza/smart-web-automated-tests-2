package com.pixeon.steps.serenity.cadastros.profissionais;

import com.pixeon.pages.cadastros.profissionais.PageDadosDosProfissionais;
import net.thucydides.core.annotations.Step;

public class DadosDosProfissionaisSteps {

    PageDadosDosProfissionais pageDadosDosProfissionais;

    @Step
    public void habilitar_o_campo_sob_supervisão_da_tela_dados_do_profissional() {
        pageDadosDosProfissionais.selecianarchkSupervisao();
    }
    @Step
    public void selecioanr_o_supervisor_na_tela_dados_do_profissional(String supervisorImediato) {
        pageDadosDosProfissionais.selecionarSupervisoImediato(supervisorImediato);
    }
    @Step
    public void gravar_dados_e_fechar_a_tela_dados_do_profissional() {
        pageDadosDosProfissionais.clicarGravar();
        pageDadosDosProfissionais.clicarFechar();
    }

}
