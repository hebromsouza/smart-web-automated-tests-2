package com.pixeon.steps.serenity.atendimento.orcamentoEPreAtendimento;

import com.pixeon.pages.atendimento.orcamentoEPreAtendimento.PageOrcamentoEPreAtendimento;
import net.thucydides.core.annotations.Step;

public class OrcamentoEPreAtendimentoCustoOperacionalSteps {

    PageOrcamentoEPreAtendimento pageOrcamentoEPreAtendimento;

    @Step
    public void incluirConvenio(String convenio){
        pageOrcamentoEPreAtendimento.selecionarConvenio(convenio);
    }

    @Step
    public void gravarOrcamentoEPreAtendimento(){
        pageOrcamentoEPreAtendimento.clicarBtnGravar();
    }

    @Step
    public void inserirItemComCoper(String item){
        pageOrcamentoEPreAtendimento.escreverItem(item);
        pageOrcamentoEPreAtendimento.clicarBtnBuscar();
        pageOrcamentoEPreAtendimento.clicarBtnIncluir();
    }

    @Step
    public void validarInclusaoItemCustoOperacional(String coper){
        pageOrcamentoEPreAtendimento.validarInclusaoItemCustoOperacional(coper);
    }

    @Step
    public void validarCustoOperacionalComCotacaoConvenio(Float custoOperacional, Float cotacao, String convenio){
        pageOrcamentoEPreAtendimento.validarCustoOperacionalComCotacaoConvenio(custoOperacional, cotacao, convenio);
    }

    @Step
    public void validarValorItemComCustoOperacionalAgregado(Float valorItem, Float custoOperacional, Float cotacao){
        pageOrcamentoEPreAtendimento.validarValorItemComCustoOperacionalAgregado(valorItem, custoOperacional, cotacao);
    }

    @Step
    public void validarInexistenciaServicoCoper(String coper){
        pageOrcamentoEPreAtendimento.validarInexistenciaServicoCoper(coper);
    }

}
