package com.pixeon.steps.serenity.atendimento.filaDeEspera;

import com.pixeon.pages.atendimento.filaDeEspera.PageFilaDeEspera;
import com.pixeon.pages.totem.PageDetalhesPacienteFilaEspera;
import com.pixeon.pages.totem.PageRetiradaSenha;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class FilaDeEsperaSteps {

    PageFilaDeEspera pageFilaDeEspera;
    PageDetalhesPacienteFilaEspera pageDetalhesPacienteFilaDeEspera;

    @Step
    public void abrirFilaDeEspera(){
        pageFilaDeEspera.clicarLinkFilaDeEspera();
        pageFilaDeEspera.aceitarAlertaInicialDaFila();
    }

    @Step
    public void menuFilaEspera(String filaEspera){
        pageFilaDeEspera.SelecionarMenuFilaEspera(filaEspera);
    }

    @Step
    public void buscarFilaDeEspera(String filaMedico){
        pageFilaDeEspera.selecionarFila(filaMedico);
    }

    @Step
    public void adicionarNaFila(){
        pageFilaDeEspera.clicarBotaoAdicionarNaFila();
    }

    @Step
    public void selecionarOpcaoAdicionarNaFrenteDoPaciente(String paciente){
        pageFilaDeEspera.clicarRadioNaFrenteDe();
        pageFilaDeEspera.selecionarOrdemDoPacienteDaFila(paciente);
    }

    @Step
    public void gravarInclusaoPacienteFilaEsperaComRecepcao(String filaMedico, String recepcao){
        pageFilaDeEspera.selecionarFilaMedico(filaMedico);
        pageFilaDeEspera.selecionarRecepcao(recepcao);
        pageFilaDeEspera.clicarBotaoGravar();
        pageFilaDeEspera.aceitarAlertaInicialDaFila();
    }

    @Step
    public void validarInclusaoDePacienteNaFilaDeEspera(String paciente){
        pageFilaDeEspera.validaPacienteFilaEspera(paciente);
    }

    @Step
    public void validarInclusaoPacienteFinalFilaEspera(String paciente){
        pageFilaDeEspera.validarPacienteFinalFilaEspera(paciente);
    }

    @Step
    public void validarInclusaoDeSenhaNaFilaDeEspera(String medico) {
        pageFilaDeEspera.validarInclusaoSenhaFilaEspera(PageRetiradaSenha.retiradaSenha, medico);
    }

    @Step
    public void validarQuantidadePacientesFilaDeEspera() {
        pageFilaDeEspera.validarQuantidadePacientesFilaDeEspera();
    }

    @Step
    public void enviarMensagemSenhaRetiradaFilaDeEspera() {
        pageFilaDeEspera.carregarFrameDefault();
        pageFilaDeEspera.clicarSenhaGeradaNoTotem(PageRetiradaSenha.retiradaSenha);
        pageFilaDeEspera.clicarEnviarMensagem();
    }

    @Step
    public void cancelarSenhaDoPacienteFilaDeEspera() {
        pageFilaDeEspera.clicarSenhaGeradaNoTotem(pageFilaDeEspera.retornaSenhaGeradaNaRetiradaDeSenhaDoTotem(PageRetiradaSenha.retiradaSenha));
        pageFilaDeEspera.clicarLinkDetalhes();
        pageDetalhesPacienteFilaDeEspera.selecionarStatusPacienteFilaDeEspera("Cancelado");
        pageDetalhesPacienteFilaDeEspera.clicarBotaoGravar();
    }

    @Step
    public void validarExclusaoSenhaDoPacienteFilaDeEspera() {
        Assert.assertFalse(pageFilaDeEspera.retornaVisibilidadeSenhaGeradaFilaEspera(PageRetiradaSenha.retiradaSenha));
    }

    @Step
    public void validarQuantidadePacientesFilaDeEsperaDoMedico() {
        pageFilaDeEspera.validarQuantidadeSenhasFilasDeEspera();
    }

    @Step
    public void cancelarSenhasDaFilaDeEsperaDoMedico() {
        while (pageFilaDeEspera.retornarVisibilidadeDePacienteNaFilaDoMedico()){
            pageFilaDeEspera.clicarLinkNomePaciente();
            pageFilaDeEspera.clicarLinkDetalhes();
            pageDetalhesPacienteFilaDeEspera.selecionarStatusPacienteFilaDeEspera("Cancelado");
            pageDetalhesPacienteFilaDeEspera.clicarBotaoGravar();
        }
        Assert.assertFalse(pageFilaDeEspera.retornarVisibilidadeDePacienteNaFilaDoMedico());
    }

    @Step
    public void acessarFilaEsperaMedico(String medico) {
        pageFilaDeEspera.clicarLinkFilaDeEspera();
        pageFilaDeEspera.aceitarAlertaInicialDaFila();
        pageFilaDeEspera.selecionarFila(medico);
    }

    @Step
    public void chamarProximaSenha() {
        pageFilaDeEspera.clicarBotaoChamarProximo();
    }

    @Step
    public void enviarMensagemPacienteFilaDeEspera(String paciente) {
        pageFilaDeEspera.clicarPacienteFilaDeEspera(paciente);
        pageFilaDeEspera.clicarEnviarMensagem();
    }

}
