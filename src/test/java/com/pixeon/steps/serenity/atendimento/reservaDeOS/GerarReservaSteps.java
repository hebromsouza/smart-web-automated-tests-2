package com.pixeon.steps.serenity.atendimento.reservaDeOS;

import com.pixeon.datamodel.reservaDeOS.DataReservaOS;
import com.pixeon.pages.atendimento.reservaDeOS.PageReservaDeOS;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class GerarReservaSteps {

    PageReservaDeOS pageReservaDeOS;

    @Step
    public void informarDadosReservaOS(List<DataReservaOS> dataReservaOS) {
        pageReservaDeOS.preencherCamposReservaOS(dataReservaOS);
    }

    @Step
    public void gerarReserva() {
        pageReservaDeOS.clicarBotaoGerarReserva();
    }

    @Step
    public void validarPainelReservaGerada() {
        pageReservaDeOS.validarPainelReservaGerada();
    }

    @Step
    public void confirmarPopUpGeracaoReserva(String quantidade, String setorSolicitante) {
        pageReservaDeOS.confirmarPopUpGeracaoReserva(quantidade, setorSolicitante);
    }

    @Step
    public void imprimirFichasAtendimento() {
        pageReservaDeOS.clicarBotaoImprimirFichasAtendimento();
    }

}
