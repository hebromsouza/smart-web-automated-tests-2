package com.pixeon.steps.serenity.atendimento.ordemDeServico;

import com.pixeon.pages.atendimento.ordemDeServico.PageEditarItensOrdemDeServico;
import com.pixeon.pages.atendimento.ordemDeServico.PageInserirItensOrdemDeServico;
import com.pixeon.pages.atendimento.ordemDeServico.PageLancamentoDeItens;
import com.pixeon.pages.atendimento.ordemDeServico.PageLancamentoDeOrdemDeServico;
import com.pixeon.util.SeleniumUtil;
import net.thucydides.core.annotations.Step;

import java.text.ParseException;

import static org.junit.Assert.assertTrue;

public class InserirItensOrdemDeServicoSteps {

    PageInserirItensOrdemDeServico pageInserirItensOrdemDeServico;
    PageLancamentoDeOrdemDeServico pageLancamentoDeOrdemDeServico;
    PageLancamentoDeItens pageLancamentoDeItens;
    PageEditarItensOrdemDeServico pageEditarItensOrdemDeServico;

    @Step
    public void buscarItensComParametrosDeBusca(String item, String tipoItem, String filtroTipoBusca){
        pageInserirItensOrdemDeServico.buscarItens(item);
        pageInserirItensOrdemDeServico.escolherTipoDeFiltro(tipoItem);
        pageInserirItensOrdemDeServico.escolherTipoDeBusca(filtroTipoBusca);
        pageInserirItensOrdemDeServico.clicarBotaoBuscar();
    }

    @Step
    public void concluirInsercaoDeItem(String item, String tipoItem, String filtroTipoBusca){
        if(filtroTipoBusca.equals("Nome")){
            pageInserirItensOrdemDeServico.selecionarItem();
            pageInserirItensOrdemDeServico.clicarBotaoIncluir();
        } else if(tipoItem.equals("Kit")){
            pageInserirItensOrdemDeServico.clicarBotaoIncluir();
            pageInserirItensOrdemDeServico.inserirProfissional();
        }
        else {
            pageInserirItensOrdemDeServico.incluirItensSelecionados();
        }

        pageLancamentoDeItens.confirmaInclusaoItemNovamente();
        pageInserirItensOrdemDeServico.concluirLancamentoDeItens();
    }

    @Step
    public void buscarItemSelecionandoAmostra(String item, String amostra){
        pageInserirItensOrdemDeServico.buscarItens(item);
        pageInserirItensOrdemDeServico.buscarAmostra(amostra);
        pageInserirItensOrdemDeServico.clicarBotaoBuscar();
    }

    @Step
    public void buscarItem(String item){
        pageInserirItensOrdemDeServico.buscarItens(item);
        pageInserirItensOrdemDeServico.clicarBotaoBuscar();
    }

    @Step
    public void incluirItem(){
        pageInserirItensOrdemDeServico.incluirItensSelecionados();
        pageInserirItensOrdemDeServico.concluirLancamentoDeItens();
    }

    @Step
    public void incluirItemOrdemDeServico(String item) {
        pageInserirItensOrdemDeServico.buscarItens(item);
        pageInserirItensOrdemDeServico.clicarBotaoBuscar();
        pageInserirItensOrdemDeServico.incluirItensSelecionados();
    }

    @Step
    public void inserirItemOrdemDeServico(String item){
        pageInserirItensOrdemDeServico.buscarItens(item);
        pageInserirItensOrdemDeServico.clicarBotaoBuscar();
        pageInserirItensOrdemDeServico.incluirItens();
    }

    @Step
    public void cadastrar_novo_paciente_faker(){
        pageInserirItensOrdemDeServico.CadastrarPacienteFaker();
    }

    @Step
    public void incluirItemNovaOrdemDeServico(String item, String setorSolicitante) throws InterruptedException {
        pageLancamentoDeOrdemDeServico.fecharJanelaInstrucoes();
        pageLancamentoDeOrdemDeServico.clicarNoBotaoNovaOs();
    //    pageLancamentoDeOrdemDeServico.fecharJanelaInstrucoes();
        pageLancamentoDeOrdemDeServico.selecionarSetorSolicitante(setorSolicitante);
        pageLancamentoDeOrdemDeServico.clicarNoBotaoGravar();
        pageLancamentoDeItens.lancarItemServicoSemInstrucao(item);
        pageEditarItensOrdemDeServico.concluirLancamentoDeItem();
    }

    @Step
    public void buscarServico(String item) throws InterruptedException {
        pageLancamentoDeOrdemDeServico.fecharJanelaInstrucoes();
        pageLancamentoDeOrdemDeServico.clicarNoBotaoBuscar(item);

    }

    @Step
    public void inserirItemDoTipo(String item, String tipoItem){
        pageLancamentoDeItens.escreverItemServico(item);
        pageLancamentoDeItens.selecionarTipoFiltroItem(tipoItem);
        pageLancamentoDeItens.buscarItemServico();
        pageLancamentoDeItens.incluirItemServico();
    }

    @Step
    public void buscarProfissional(String profissional){
        pageLancamentoDeItens.clicarLinkProfissional(profissional);
    }

    @Step
    public void abrirTelaItensOrdemDeServico(){
        pageLancamentoDeItens.clicarLinkItensOrdemDeServico();
    }

    @Step
    public void validaInsercaoDeItem(String item, String tipoItem, String filtroTipoBusca){
         pageInserirItensOrdemDeServico.validaInsercaoItem2(item);
    }

    @Step
    public void validaInclusaoItemComAmostra(String amostra){
        assertTrue(pageInserirItensOrdemDeServico.validaInclusaoItemComAmostra(amostra));
    }

    @Step
    public void validarMensagemBuscaItemBloqueioCotacao(String mensagem){
        pageInserirItensOrdemDeServico.validarBloqueioCotacao(mensagem);
    }

    @Step
    public void pegarNumeroAmostraItem() {
        pageInserirItensOrdemDeServico.abrirTelaDeDescricaoDeItens();
        pageInserirItensOrdemDeServico.pegarNumeroAmostraItem();
        pageInserirItensOrdemDeServico.closePanel();
    }

    @Step
    public String escolherItemParaResultadoSabado() throws ParseException {
        String item = null;
        switch (SeleniumUtil.weekDay()){
            case 7:
                item = "17OH";
                break;
            case 6:
                item = "32050011";
                break;
            case 5:
                item = "AMGIP";
                break;
            case 4:
                item = "DACC";
                break;
            case 3:
                item = "AAM";
                break;
            case 2:
                item = "17NEO";
                break;
            case 1:
                item = "11DES";
                break;
        }

        return item;
    }

    @Step
    public void inserirItemResultadoSabado() throws ParseException {
        pageInserirItensOrdemDeServico.buscarItens(escolherItemParaResultadoSabado());
        pageInserirItensOrdemDeServico.clicarBotaoBuscar();
        pageInserirItensOrdemDeServico.incluirItensSelecionados();
        pageInserirItensOrdemDeServico.concluirLancamentoDeItens();

    }

    @Step
    public void validarResultadoItemSabadoUtil() {
        pageInserirItensOrdemDeServico.validarResultadoItemSabado(7);
    }

    @Step
    public void validarResultadoItemProximoDiaUtil() {
        pageInserirItensOrdemDeServico.validarResultadoItemSabado(9);
    }

    @Step
    public void validarMensagemPainelAlertaCotacao(String mensagem) {
        pageInserirItensOrdemDeServico.validarAlertaCotacaoPainel(mensagem);
    }

}