package com.pixeon.steps.serenity.atendimento.etiqueta;

import com.pixeon.pages.atendimento.ordemDeServico.PageItensDaOrdemDeServico;
import com.pixeon.pages.etiqueta.PageImpressaoEtiquetas;
import static org.junit.Assert.assertTrue;
import net.thucydides.core.annotations.Step;

public class ImpressaoEtiquetaSteps {

    PageItensDaOrdemDeServico pageItensDaOrdemDeServico;
    PageImpressaoEtiquetas pageImpressaoEtiquetas;

    @Step
    public void selecionarTipoImpressao(String tipoImpressao){
        pageItensDaOrdemDeServico.selecionarImpressao(tipoImpressao);
    }

    @Step
    public void validaQuantidadeEtiqueta(String quantidade){
//        pageImpressaoEtiquetas.validarQuantidade(quantidade);
        pageImpressaoEtiquetas.inserirQuantidadeEtiqueta(quantidade);
    }

    @Step
    public void inserirQuantidadeImpressaoEtiqueta(String quantidadeEtqueta){
        pageImpressaoEtiquetas.inserirQuantidadeEtiqueta(quantidadeEtqueta);
    }

    @Step
    public void imprimirEtiqueta(String servImp){
        pageImpressaoEtiquetas.selecionarServidorImpressao(servImp);
        pageImpressaoEtiquetas.clicarImprimir();
    }

    @Step
    public void validaConteudoEtiqueta(String caminhoEtiqueta){
        assertTrue(pageImpressaoEtiquetas.validarConteudoEtiqueta(caminhoEtiqueta));
    }

    @Step
    public void validaStatusEtiquetaComCaminhoArquivo(String status, String caminhoEtiqueta){
        pageImpressaoEtiquetas.validarStatusEtiqueta(status, caminhoEtiqueta);
    }

}
