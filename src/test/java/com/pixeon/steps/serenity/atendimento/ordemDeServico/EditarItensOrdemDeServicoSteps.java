package com.pixeon.steps.serenity.atendimento.ordemDeServico;

import com.pixeon.pages.atendimento.ordemDeServico.PageEditarItensOrdemDeServico;
import com.pixeon.pages.atendimento.ordemDeServico.PageLancamentoDeItens;
import com.pixeon.util.SeleniumUtil;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static org.junit.Assert.assertTrue;

public class EditarItensOrdemDeServicoSteps {

    PageEditarItensOrdemDeServico pageEditarItensOrdemDeServico;
    PageLancamentoDeItens pageLancamentoDeItens;

    @Step
    public void selecionarItem(){
        pageEditarItensOrdemDeServico.selecionarItem();
    }

    @Step
    public void validarValorTotal(){
        pageEditarItensOrdemDeServico.validarValorTotal();
    }



    @Step
    public void acaoItem(String acaoItem){
        pageEditarItensOrdemDeServico.escolherAcaoItem(acaoItem);
    }

    @Step
    public void motivoCancelamento(String tipoCancelamento, String motivoCancelamento){
        pageEditarItensOrdemDeServico.motivoCancelamento(tipoCancelamento);
        pageEditarItensOrdemDeServico.observacaoCancelamento(motivoCancelamento);
        pageEditarItensOrdemDeServico.clicarBotaoOk();
    }

    @Step
    public void incluirDataAnterior(){
        pageEditarItensOrdemDeServico.digitarDataDeColetaResultadoAnterior();
    }

    @Step
    public void incluirDataColetaResultado(){
        pageEditarItensOrdemDeServico.digitarDataColetaResultado();
        pageEditarItensOrdemDeServico.clicarBotaoOkBuscar();
        if (SeleniumUtil.isAlertPresent()){
            pageEditarItensOrdemDeServico.confirmarResultadoSabado();
        }
    }

    @Step
    public void buscarSetor(String setorExecutante){
        pageEditarItensOrdemDeServico.digitarNomeSetor(setorExecutante);
        pageEditarItensOrdemDeServico.clicarBotaoOkBuscar();
    }

    @Step
    public void selecionarSetor(){
        pageEditarItensOrdemDeServico.selecionarSetor();
    }

    @Step
    public void aceitarAlerta(){
        pageEditarItensOrdemDeServico.aceitarAlerta();
    }

    @Step
    public void abrirTelaDetalhes(){
        pageEditarItensOrdemDeServico.clicarDetalhesItem();
    }

    @Step
    public void abrirTelaDetalhes(String itemDesc){
        pageEditarItensOrdemDeServico.clicarDetalhesItem(itemDesc);
    }

    @Step
    public void concluirLancamentoDoItem() throws InterruptedException {
        pageLancamentoDeItens.selectAllItens();
        pageEditarItensOrdemDeServico.concluirLancamentoDeItem();
        pageEditarItensOrdemDeServico.clicarBotaoOkAutorizacao();
    }

    @Step
    public void concluirInclusaoItem() throws InterruptedException {
        pageLancamentoDeItens.selectAllItens();
        pageLancamentoDeItens.clicarBotaoConfirmar();
        pageEditarItensOrdemDeServico.concluirLancamentoDeItem();
        pageEditarItensOrdemDeServico.clicarBotaoOkAutorizacao();
    }

    @Step
    public void concluirInclusaoItemSemSelecionarItens() throws InterruptedException {
        pageEditarItensOrdemDeServico.concluirLancamentoDeItem();
    }

    @Step
    public void novoLancamentoItens(){
        pageEditarItensOrdemDeServico.novoLancamentoDeItem();
    }

    @Step
    public void confirmarCancelamentoWebService(){
        pageEditarItensOrdemDeServico.confirmarCancelamentoNoWebService();
    }

    @Step
    public void validarItemCancelado(){
        pageEditarItensOrdemDeServico.validarCancelamentoDeItem();
    }

    @Step
    public void validarAcaoItem(String acaoItem){
        assertTrue(pageEditarItensOrdemDeServico.validarAcaoItem(acaoItem));
    }

    @Step
    public void validarAlertaDataAnterior(String acaoItem){
        pageEditarItensOrdemDeServico.validarAlertaDataAnterior(acaoItem);
    }

    @Step
    public void validarAlteracaoSetorExecutante(String setorExecutante){
        pageEditarItensOrdemDeServico.validarAlteracaoSetorExecutante(setorExecutante);
    }

    @Step
    public void validarCancelamentoGuia(String acaoItem){
        pageEditarItensOrdemDeServico.inserirNovoProfissional();
        pageEditarItensOrdemDeServico.gravarOrdemDeServico();
        pageEditarItensOrdemDeServico.preencherMotivoAlteracao();
        pageEditarItensOrdemDeServico.clicarEditarItens();
        novoLancamentoItens();
        selecionarItem();
        acaoItem(acaoItem);
        confirmarCancelamentoWebService();
        pageEditarItensOrdemDeServico.validarCancelamentoGuia();
    }


    @Step
    public void validarMensagemNaoTemGuia(){
        pageEditarItensOrdemDeServico.validarMensagemNaoTemGuia();
    }

    @Step
    public void alterarQuantidadeItem(String quantidadeItem) {
        pageEditarItensOrdemDeServico.pegarValorItem();
        pageEditarItensOrdemDeServico.clicarLinkQuantidadeItem();
        pageEditarItensOrdemDeServico.escreverQuantidadeDoItem(quantidadeItem);
        pageEditarItensOrdemDeServico.clicarBotaoOkQuantidadeItem();
    }

    @Step
    public void validarValor() {
        pageEditarItensOrdemDeServico.validarValor();
    }

    @Step
    public void validarLinkNaoPodeAlterarPreco() {
        pageEditarItensOrdemDeServico.LinkNaoDisponivelParaAlterarPreco();
    }

    @Step
    public void alteracaoQuantidadeDesabilitado() {
        Assert.assertFalse(pageEditarItensOrdemDeServico.verificarAlteracaoQuantidade());
    }

    @Step
    public void alterarValorItemDesabilitado() {
        Assert.assertFalse(pageEditarItensOrdemDeServico.verificarAlteracaoValor());
    }

    @Step
    public void validarValorItemComDescontoAplicadoNoOrcamento() {
        pageEditarItensOrdemDeServico.validarValorTotal();
    }

}
