package com.pixeon.steps.serenity.atendimento.recebimentoDeAmostra;

import com.pixeon.pages.atendimento.recebimentoDeAmostra.PageRecebimentoDeAmostras;
import net.thucydides.core.annotations.Step;

public class ImpressaoOrdemServicoSteps {

    PageRecebimentoDeAmostras pageRecebimentoDeAmostras;

    @Step
    public void selecionarOrdemServico(){
        pageRecebimentoDeAmostras.clicarOrdemServico();
    }

    @Step
    public void validarImpressaoOrdemServicoPeloNome(String nomePaciente){
        pageRecebimentoDeAmostras.validarImpressaoOrdemServicoPeloNome(nomePaciente);
    }

}
