package com.pixeon.steps.serenity.atendimento.orcamentoEPreAtendimento;

import com.pixeon.pages.atendimento.orcamentoEPreAtendimento.PageOrcamentoEPreAtendimento;
import net.thucydides.core.annotations.Step;

public class AplicarDescontoSteps {

    PageOrcamentoEPreAtendimento pageOrcamentoEPreAtendimento;

    @Step
    public void selecionarDesconto(String desconto) {
        pageOrcamentoEPreAtendimento.selecionarDesconto(desconto);
    }

    @Step
    public void validarDescontoAplicado() {
        pageOrcamentoEPreAtendimento.validarDescontoAplicado();
    }

    @Step
    public void inserirPercentualDesconto(String percentual) {
        pageOrcamentoEPreAtendimento.inserirPercentualDesconto(percentual);
    }

}
