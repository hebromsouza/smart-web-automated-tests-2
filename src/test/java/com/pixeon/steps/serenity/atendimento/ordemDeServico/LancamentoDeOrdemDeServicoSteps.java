package com.pixeon.steps.serenity.atendimento.ordemDeServico;

import com.pixeon.datamodel.ordemDeServico.DataOrdemDeServico;
import com.pixeon.pages.atendimento.orcamentoEPreAtendimento.PageOrcamentoEPreAtendimento;
import com.pixeon.pages.atendimento.ordemDeServico.PageEditarItensOrdemDeServico;
import com.pixeon.pages.atendimento.ordemDeServico.PageLancamentoDeItens;
import com.pixeon.pages.atendimento.ordemDeServico.PageLancamentoDeOrdemDeServico;
import com.pixeon.pages.pesquisaDeAvaliacao.PageQuestionario;
import com.pixeon.util.SeleniumUtil;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class LancamentoDeOrdemDeServicoSteps {

    PageLancamentoDeOrdemDeServico pageLancamentoDeOrdemDeServico;
    PageLancamentoDeItens pageLancamentoDeItens;
    PageEditarItensOrdemDeServico pageEditarItensOrdemDeServico;
    PageQuestionario pageQuestionario;
    PageOrcamentoEPreAtendimento pageOrcamentoEPreatendimento;
    @Step
    public void clicarBotaoNovaOrdemDeServico() {
        pageLancamentoDeOrdemDeServico.fecharJanelaInstrucoes();
        pageLancamentoDeOrdemDeServico.clicarNoBotaoNovaOs();
        pageLancamentoDeOrdemDeServico.fecharJanelaInstrucoes();
    }

    @Step
    public void incluirNovaOrdemDeServico(){
        pageLancamentoDeOrdemDeServico.clicarNoBotaoNovaOs();
        pageLancamentoDeOrdemDeServico.preencherCampoGuia();
        pageLancamentoDeOrdemDeServico.clicarNoBotaoGravar();
        pageLancamentoDeOrdemDeServico.numeroOrdemServico();
    }

    @Step
    public void adicionar_uma_nova_ordem_de_serviço(){
        pageLancamentoDeOrdemDeServico.AdicionarNovaOrdemServico();
        pageLancamentoDeOrdemDeServico.clicarNoBotaoGravar();
        pageLancamentoDeOrdemDeServico.numeroOrdemServico();
    }

    @Step
    public void criar_uma_nova_ordem_de_serviço_com_codigo_convenio(String convenio){
        pageLancamentoDeOrdemDeServico.AdicionarNovaOrdemServico();
        pageLancamentoDeOrdemDeServico.clicarNoBotaoGravar();
        pageLancamentoDeOrdemDeServico.numeroOrdemServico();
    }


    @Step
    public void criarNovaOrdemDeServico( ){
        if(pageLancamentoDeOrdemDeServico.validarBotaoAvancarCadastroPaciente())
            pageLancamentoDeOrdemDeServico.clicarNoBotaoAvancar();

        //NOVA OS
        clicarBotaoNovaOrdemDeServico();
        pageLancamentoDeOrdemDeServico.clicarNoBotaoNovaOs();

        pageLancamentoDeOrdemDeServico.clicarNoBotaoGravar();
        pageLancamentoDeOrdemDeServico.numeroOrdemServico();
    }

    @Step
    public void criarNovaOrdemDeServicoSemGravar() {
        if(pageLancamentoDeOrdemDeServico.validarBotaoAvancarCadastroPaciente())pageLancamentoDeOrdemDeServico.clicarNoBotaoAvancar();
        clicarBotaoNovaOrdemDeServico();
    }

    @Step
    public void preencherDadosOrdemDeServico(List<DataOrdemDeServico> dataOrdemDeServico){
        pageLancamentoDeOrdemDeServico.preencherDadosOrdemDeServico(dataOrdemDeServico);
        pageLancamentoDeOrdemDeServico.clicarNoBotaoGravar();
        pageLancamentoDeOrdemDeServico.numeroOrdemServico();
    }

    @Step
    public void lancarItemServicoComInstrucao(String itemServico){
        final String instrucao = pageLancamentoDeItens.lancarItemServicoComInstrucaoSemConcluir(itemServico);
        if (SeleniumUtil.isAlertPresent()) {
            assertEquals("GLICO\n- Jejum não obrigatório;\n- Enviar cópia do documento do paciente;\n- Obrigatório Pedido Médico e Relatório Clínico;\n- Preencher questionário Formulário Requisição de Exame Translocação BCR-ABL;\n- Coleta somente de segunda a quarta nas unidades Pituba e Rio Vermelho;", instrucao);
        }
    }

    @Step
    public void incluir_o_item_na_os(String itemServico){
     //   pageLancamentoDeItens.IncluirItemNaOS(itemServico);
     //   pageOrcamentoEPreatendimento.AdicionarItemNaOS(itemServico);
        pageOrcamentoEPreatendimento.BuscarServico(itemServico);
        pageLancamentoDeItens.incluirItemServico();
        pageLancamentoDeItens.confirmaInclusaoItemNovamente();

    }


    @Step
    public void lancarItemOrdemDeServico(String item){
//        pageLancamentoDeItens.lancarItemOrdemDeServico(item);
        pageLancamentoDeItens.escreverItemServico(item);
        pageLancamentoDeItens.buscarItemServico();
        pageLancamentoDeItens.incluirItemServico();
        pageLancamentoDeItens.confirmaInclusaoItemNovamente();
        pageLancamentoDeItens.aceitarAlertaInstrucaoDoitem();
    }

    @Step
    public void lancamentoItemServicoSemInstrucao(String itemServico){
        pageLancamentoDeItens.lancarItemServicoSemInstrucao(itemServico);
    }

    @Step
    public void criarNovaOrdemDeServicoSelecionandoConvenio(String codigoConvenio){
//        if(pageLancamentoDeOrdemDeServico.validarBotaoAvancarCadastroPaciente()== true){
//            pageLancamentoDeOrdemDeServico.clicarNoBotaoAvancar();
//            pageLancamentoDeOrdemDeServico.fecharJanelaInstrucoes();
//        }

        pageLancamentoDeOrdemDeServico.clicarNoBotaoNovaOs();
    //    pageLancamentoDeOrdemDeServico.esperarMensagemAguarde();
        //pageLancamentoDeOrdemDeServico.clicarNoBotaoFechar();

        if(codigoConvenio.equals("1")){
            pageLancamentoDeOrdemDeServico.alterarConvenioParaParticular(codigoConvenio);
        }else{
            pageLancamentoDeOrdemDeServico.alterarParticularParaConvenio(codigoConvenio);
        }

        pageLancamentoDeOrdemDeServico.fecharJanelaInstrucoes();
        pageLancamentoDeOrdemDeServico.clicarNoBotaoGravar();
    }



    @Step
    public void novaOrdemServicoIcluindoItem(String item) throws InterruptedException {
        criarNovaOrdemDeServico();
        lancarItemOrdemDeServico(item);
        pageEditarItensOrdemDeServico.concluirLancamentoDeItem();
        pageEditarItensOrdemDeServico.clicarBotaoOkAutorizacao();
    }

    @Step
    public void gerarNovaOrdemDeServicoComItem(String item) throws InterruptedException {
        if(pageLancamentoDeOrdemDeServico.validarBotaoAvancarCadastroPaciente())
            pageLancamentoDeOrdemDeServico.clicarNoBotaoAvancar();

 //       criarNovaOrdemDeServico();
        pageLancamentoDeItens.buscarItem(item);
        pageLancamentoDeItens.incluirItemServico();
        pageLancamentoDeItens.confirmaInclusaoItemNovamente();
        pageLancamentoDeItens.aceitarPopUpInstrucaoItem();
        pageEditarItensOrdemDeServico.concluirLancamentoDeItem();
    }

    @Step
    public void validarNovaOrdemDeServico(String plano){
        pageLancamentoDeOrdemDeServico.validarNovaOrdemDeServico(plano);
    }

    @Step
    public void validarHerancaSetorSolicitante(String setor){
        pageLancamentoDeOrdemDeServico.validarHerancaSetorSolicitante(setor);
    }

    @Step
    public void validarBuscaOrdemServico(){
        pageLancamentoDeOrdemDeServico.validarBuscaOrdemServico();
    }

    @Step
    public void validarAusenciaOrdemServico(){
        pageLancamentoDeOrdemDeServico.validarAusenciaOrdemServico();
    }

    @Step
    public void gerarOrdensDeServicoComItem(int quantidade, String item) throws InterruptedException {
        for (int i=0; i<quantidade; i++){
            gerarNovaOrdemDeServicoComItem(item);
            pageQuestionario.excluirRespostaQuestionario();
            if (i != quantidade - 1) pageLancamentoDeOrdemDeServico.clicarLinkLancamentoDeOs();
        }
    }

    @Step
    public void informarCpfCnpjPrestadorServico(String cpfCnpj) {
        pageLancamentoDeOrdemDeServico.escreverCpfCnpjPrestador(cpfCnpj);
    }

    @Step
    public void gravarOrdemDeServico() {
        pageLancamentoDeOrdemDeServico.clicarBotaoGravar();
    }

    @Step
    public void validarInclusaoCpfCnpjPrestador(String cpfCnpj) {
    //    pageLancamentoDeOrdemDeServico.clicarLinkLancamentoDeOs();
        Assert.assertEquals(cpfCnpj, pageLancamentoDeOrdemDeServico.retornarValorCpfCnpjPrestador());
    }

    @Step
    public void validarMensagemPopUp(String mensagem) {
        pageLancamentoDeOrdemDeServico.validarMensagemAlert(mensagem);
    }

    @Step
    public void acessarItensDaOrdemDeServico() {
        pageEditarItensOrdemDeServico.clicarEditarItens();
    }

}
