package com.pixeon.steps.serenity.atendimento.orcamentoEPreAtendimento;

import com.pixeon.pages.atendimento.orcamentoEPreAtendimento.PageOrcamentoEPreAtendimento;
import net.thucydides.core.annotations.Step;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class OrcamentoEPreAtendimentoSteps {

    PageOrcamentoEPreAtendimento pageOrcamentoEPreatendimento;

    @Step
    public void clicarBotaoGravar(){
        pageOrcamentoEPreatendimento.clicarBtnGravar();
    }

    @Step
    public void escreverItem(String txtItem){
        pageOrcamentoEPreatendimento.escreverItem(txtItem);
    }

    @Step
    public void clicarBotaoBuscarItem(){
        pageOrcamentoEPreatendimento.clicarBtnBuscarServico();
    }

    @Step
    public void clicarBotaoIncluirItem(){
        pageOrcamentoEPreatendimento.clicarBtnIncluir();
    }

    @Step
    public void validaSubMenuClicado(String subMenu){
        pageOrcamentoEPreatendimento.validaSubMenu(subMenu);
    }

    @Step
    public void confirmarAlteracaoDeConvenio (String convenio){
        pageOrcamentoEPreatendimento.AlterarConvenio(convenio);
        pageOrcamentoEPreatendimento.confirmarAlerta();
    }

    @Step
    public void gerarOrdemDeServico(){
        pageOrcamentoEPreatendimento.clicarBotaoGerarOrdemDeServico();
    }

    @Step
    public void incluirItemNoSubMenu(String item, String subMenu){
        clicarBotaoGravar();
        validaSubMenuClicado(subMenu);
        escreverItem(item);
        clicarBotaoBuscarItem();
        clicarBotaoIncluirItem();
        clicarBotaoGravar();
    }

    @Step
    public void gerar_uma_nova_OS_para_o_paciente(String item){
        pageOrcamentoEPreatendimento.AdicionarNovaOS();
        pageOrcamentoEPreatendimento.AdicionarItemNaOS(item);
    }

    @Step
    public void validarOpcaoDeConvenio(String opcaoConvenio){
        if (opcaoConvenio.equals("PARTICULAR")){
            pageOrcamentoEPreatendimento.ConfirmarConvenio(opcaoConvenio);
        }else if (opcaoConvenio.equals("BRADESCO SAUDE")) {
            pageOrcamentoEPreatendimento.ConfirmarConvenio(opcaoConvenio);
          //  assertTrue(pageOrcamentoEPreatendimento.validarTxtDoLocatorVazio());
        }else if (opcaoConvenio.equals("SULAMERICA SAUDE")) {
            pageOrcamentoEPreatendimento.ConfirmarConvenio(opcaoConvenio);
            //  assertTrue(pageOrcamentoEPreatendimento.validarTxtDoLocatorVazio());
        }
        else if (opcaoConvenio.equals("SUS")) {
            pageOrcamentoEPreatendimento.ConfirmarConvenio(opcaoConvenio);
        }
        else if (opcaoConvenio.isEmpty()) {
            assertFalse(pageOrcamentoEPreatendimento.validarSeTxtConvenioEstaPresente());
            assertEquals("ATENÇÃO! O convênio não cobre este item (SGEECOAR).", pageOrcamentoEPreatendimento.obterTxtErro());
        }
    }

    @Step
    public void confirmarAlteracaoDeOrcamentoOuPreAtendimento(String convenio){
        pageOrcamentoEPreatendimento.selecionarConvenio(convenio);
        clicarBotaoGravar();
        confirmarAlteracaoDeConvenio(convenio);
    }

    @Step
    public void validarTabelaDePreco(String tabPreco){
        if (tabPreco.equals("contem preco diferente")){
            assertEquals("13,50", pageOrcamentoEPreatendimento.obterValueTxtValor());
            assertTrue(pageOrcamentoEPreatendimento.validarSeLblItemMarcadoEstaPresente());
        }
        else if (tabPreco.equals("eh sem preco")) {
            assertEquals("0,00", pageOrcamentoEPreatendimento.obterValueTxtValor());
            assertFalse(pageOrcamentoEPreatendimento.validarSeLblItemMarcadoEstaPresente());
        }else if (tabPreco.equals("nao contem preco")) {
            assertEquals("0,00", pageOrcamentoEPreatendimento.obterValueTxtValor());
            assertFalse(pageOrcamentoEPreatendimento.validarSeLblItemMarcadoEstaPresente());
        }
    }

    @Step
    public void gerarPreAtendimento() {
        pageOrcamentoEPreatendimento.clicarBotaoGerarPreAtendimento();
    }

    @Step
    public void validarAlteracaoParaPreAtendimento() {
        pageOrcamentoEPreatendimento.validarTelaPreAtendimento();
    }

    @Step
    public void validarOrcamentoPreAtendimento() {
        pageOrcamentoEPreatendimento.validarOrcamentoPreAtendimento();
    }

    @Step
    public void pegarNumeroOrdemServicoGeradaOrcamento() {
        pageOrcamentoEPreatendimento.pegarNumeroOrdemServicoGeradaOrcamento();
    }

    @Step
    public void acessarOrdemServicoPeloOrcamento() {
        pageOrcamentoEPreatendimento.clicarLinkOrdemDeServiço();
    }

}
