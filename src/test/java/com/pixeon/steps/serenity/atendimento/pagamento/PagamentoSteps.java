package com.pixeon.steps.serenity.atendimento.pagamento;

import com.pixeon.pages.atendimento.ordemDeServico.PageItensDaOrdemDeServico;
import com.pixeon.pages.atendimento.pagamento.PageNotaFiscal;
import com.pixeon.pages.atendimento.pagamento.PagePagamento;
import com.pixeon.pages.atendimento.pagamento.PageRecebimentos;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static org.junit.Assert.assertTrue;

public class PagamentoSteps {

    PagePagamento pagePagamento;
    PageItensDaOrdemDeServico pageItensDaOrdemDeServico;
    PageRecebimentos pageRecebimentos;
    PageNotaFiscal pageNotaFiscal;

    @Step
    public void concluirPagamento(){
        pageItensDaOrdemDeServico.clicarLinkPagamento();
        pagePagamento.finalizarPagamentoSemDesconto();
    }

    @Step
    public void concluirPagamentoSemFormaPagamento(){
        pageItensDaOrdemDeServico.clicarLinkPagamentoSemFormaPagamento();
    }

    @Step
    public void concluirPagamentoAplicandoDesconto(String valor, String tpDesconto){
        pageItensDaOrdemDeServico.clicarLinkPagamento();
        if (valor.isEmpty() && tpDesconto.isEmpty()){
            pagePagamento.finalizarPagamentoSemDescontoVerificandoValor();
        }else{
            pagePagamento.finalizarPagamentoComDesconto(valor, tpDesconto);
        }
    }

    @Step
    public void concluirPagamentoComValorParcial(String valor, String formaPagamento, String cartao, String vencimento, String validade){
        pageItensDaOrdemDeServico.clicarLinkPagamento();
        pagePagamento.finalizarPagamentoComValorParcial(valor, formaPagamento, cartao, vencimento, validade);
    }

    @Step
    public void concluirPagamentoComPacienteNaoResponsavel(String tipoPessoa, String cpfCnpj){
        pageItensDaOrdemDeServico.clicarLinkPagamento();
        pagePagamento.finalizarPagamentoTipoPessoaFisicaJuridica(tipoPessoa, cpfCnpj);
    }

    @Step
    public void selecionarOpcaoPagamento(){
        pageItensDaOrdemDeServico.clicarLinkPagamento();
    }

    @Step
    public void pagarOrdemDeServico(){
        pagePagamento.iniciarPagamento();
    }

    @Step
    public void iniciarPagamentoOrdemDeServico(){
        pagePagamento.clicarBotaoPagamento();
        pagePagamento.iniciarPagamento();
    }

    @Step
    public void selecionarFormaPagamento(String formaPagamento){
        pagePagamento.selecionarFormaPagamento(formaPagamento);
    }

    @Step
    public void selecionarBandeiraCartao(String bandeiraCartao){
        pagePagamento.selecionarCartao(bandeiraCartao);
    }

    @Step
    public void preencherDadosCartao(String vencimento, String validade){
        pagePagamento.digitarDataDeVencimento(vencimento);
        pagePagamento.digitarDataDeValidade(validade);
    }

    @Step
    public void preencherObservacaoFormaPagamento(String observacaoFormaPagamento){
        pagePagamento.escreverObservacaoFormaPagamento(observacaoFormaPagamento);
    }

    @Step
    public void informarQuantidadeParcelas(String parcelas){
        pagePagamento.clicarBotaoParcelar();
        pagePagamento.digitarQuantidadeDeParcelas(parcelas);
        pagePagamento.clicarBotaoOk();

    }

    @Step
    public void parcelarPagamentoPreencherDadosCartao(String quantidadeParcelas, String vencimento, String validade){
        informarQuantidadeParcelas(quantidadeParcelas);
        preencherDadosCartao(vencimento, validade);
        pagePagamento.digitarDataDeVencimentoSegundaParcela(vencimento);
        pagePagamento.digitarDataDeValidadeSegundaParcela(validade);
    }

    @Step
    public void gravarPagamento(){
        pagePagamento.clicarBotaoGravarPagamento();
       // pagePagamento.pegarValorTotalPagamento();
    }

    @Step
    public void fecharTelaPagamento(){
        pagePagamento.fecharTelaPagamento();
    }

    @Step
    public void abrirTelaPagamento(){
        pagePagamento.clicarBotaoPagamento();
    }

    @Step
    public void abrirTelaPagamentoAReceber(){
        pagePagamento.clicarBotaoPagamentoAReceber();
    }

    @Step
    public void receberPagamentoEmCheque(String numBanco, String numAgencia, String numConta, String numCheque, String praca){
        pageRecebimentos.clicarLinkNumeroRecebimento();
        pagePagamento.selecionarFormaPagamento("Cheque");
        preencherDadosCheque(numBanco, numAgencia, numConta, numCheque, praca);
        pagePagamento.clicarBotaoGravar();
    }

    @Step
    public void preencherDadosCheque(String numBanco, String numAgencia, String numConta, String numCheque, String praca){
        pagePagamento.escreverNumeroBanco(numBanco);
        pagePagamento.escreverNumeroAgencia(numAgencia);
        pagePagamento.escreverNumeroConta(numConta);
        pagePagamento.escreverNumeroCheque(numCheque);
        pagePagamento.selecionarPraca(praca);
    }

    @Step
    public void informarResponsavel(String responsavel){
        pagePagamento.digitarQuantidadeDeParcelas(responsavel);
        pagePagamento.clicarBotaoOk();
    }

    @Step
    public void imprimirRecibo(){
        pageRecebimentos.clicarBotaoImprimirRecibo();
    }

    @Step
    public void abrirTelaDeposito(){
        pagePagamento.clicarLinkDeposito();
    }

    @Step
    public void informarRecepcaoTipoCaucao(String recepcao, String tipoCaucao){
        pagePagamento.selecionarRecepcao(recepcao);
        pagePagamento.selecionarTipoCaucao(tipoCaucao);
    }

    @Step
    public void depositoComValorMenorDaOrdemDeServico(String valor){
        pagePagamento.escreverValorTotal(valor);
        informarRecepcaoTipoCaucao("CONSULTORIOS", "ADIANTAMENTO PARTICULAR");
        pagePagamento.gravarPagamento();
        selecionarFormaPagamento("Espécie");
        pagePagamento.clicarBotaoGravarPagamento();
    //    pagePagamento.clicarFecharPainel();
    }

    @Step
    public void validarValorDeposito(String valorDeposito){
        pagePagamento.validarValorDeposito(valorDeposito);
    }

    @Step
    public void adicionarNovaParcela(){
        pagePagamento.clicarBotaoNovaParcela();
    }

    @Step
    public void validarValorNovaParcela(){
        pagePagamento.validarValorRestanteDeposito();
    }

    @Step
    public void validarImpressaoRecibo(){
        pagePagamento.clicarBotaoImprimirRecibo();
        pagePagamento.closePanel();
        pagePagamento.closePanel();

    }

    @Step
    public void validarGeracaoNotaFiscalComValorDaOrdemDeServico(){
        pagePagamento.clicarLinkNotaFiscal();
        pageNotaFiscal.clicarBotaoGravar();
        pageNotaFiscal.validarValorBruto();
//        pageNotaFiscal.clicarBotaoImprimirNotaFiscalEletronica();
    }

    @Step
    public void excluirNotaFiscal(){
        pageNotaFiscal.clicarBotaoExcluirNotaFiscal();
        pageNotaFiscal.escreverMotivoDeCancelamento();
        pageNotaFiscal.clicarBotaoGravarCancelamento();
        pageNotaFiscal.aceitarPopUpCancelamento();
    }

    @Step
    public void validarExclusaoNotaFiscal(){
        excluirNotaFiscal();
        pagePagamento.closePanel();
        pagePagamento.clicarLinkNotaFiscal();
        Assert.assertEquals(pageNotaFiscal.pegarNumNotaFiscal(), "");
    }

    @Step
    public void validarValorParcelas(int quantidadeParcelas){
        pagePagamento.validarValorParcelas(quantidadeParcelas);
    }

    @Step
    public void validarValorParcial(){
        assertTrue(pagePagamento.validarValorParcial());
    }

    @Step
    public void validarSeItemDeOrdemDeServicoFoiFaturada(){
        assertTrue(pagePagamento.validarCheckDeItemFaturado());
    }

    @Step
    public void validarAplicacaoDeDesconto(){
        assertTrue(pagePagamento.validarDesconto());
    }

    @Step
    public void validarPagamento(){
        assertTrue(pagePagamento.validarLinkNotaFiscal());
    }

    @Step
    public void validarRecebimentoDePagamento(){
        pageRecebimentos.validarRecebimentoDePagamento();
    }

    @Step
    public void pagamentoOrdemDeServicoGerandoNotaFiscal() {
      pageItensDaOrdemDeServico.clicarLinkPagamento();
      pagePagamento.iniciarPagamento();
      pagePagamento.gravarPagamento();
      pagePagamento.clicarBotaoGravarPagamento();
      pagePagamento.clicarLinkNotaFiscal();
      pagePagamento.gravarNotaFiscal();
      pagePagamento.clicarFecharPainel();
      pagePagamento.clicarFecharPainel();
      pagePagamento.divAguarde();
    }

}
