package com.pixeon.steps.serenity.atendimento.entregaDeLaudos;

import com.pixeon.pages.atendimento.entregaDeLaudos.PageEntregaLaudos;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class EntregaDeLaudosSteps {

    PageEntregaLaudos pageEntregaLaudos;

    @Step
    public void imprimirEntregaLaudos(String nomePessoaEntrega){
        pageEntregaLaudos.verificarStatusExame("Liberado ");
        pageEntregaLaudos.clicarBotaoImprimirLaudos();
        pageEntregaLaudos.clicarBotaoImprimir();
        pageEntregaLaudos.digitarNomePessoaEntrega(nomePessoaEntrega);
        pageEntregaLaudos.clicarBotaoOk();
        pageEntregaLaudos.fecharTelaImpressao();
    }

    @Step
    public void registrarEntregaLaudos(String nomePessoaEntrega){
        pageEntregaLaudos.clicarLinkEntregaDeResultado();
        pageEntregaLaudos.verificarStatusExame("Entregue ");
        pageEntregaLaudos.clicarBotaoRegistrarEntrega();
        pageEntregaLaudos.digitarNomePessoaEntrega(nomePessoaEntrega);
        pageEntregaLaudos.clicarBotaoOk();
    }

    @Step
    public void validarEntregaLaudosPaciente(){
        pageEntregaLaudos.verificarStatusExame("Entregue ");
        Assert.assertTrue(pageEntregaLaudos.validarNomePessoaEntrega());
    }

}
