package com.pixeon.steps.serenity.atendimento.ordemDeServico.comunicacaoWS;

import com.pixeon.pages.atendimento.ordemDeServico.comunicacaoWS.PageComunicacaoWS;
import net.thucydides.core.annotations.Step;

public class ComunicacaoWSSteps {

    PageComunicacaoWS pageComunicacaoWS;

    @Step
    public void abrirXmlEnvio(){
        pageComunicacaoWS.clicarLinkXmlEnviado();
    }

    @Step
    public void abrirXmlRetorno(){
        pageComunicacaoWS.clicarLinkXmlRetornado();
    }

    @Step
    public void validarConexaoWebServiceLaboratorio(String wsLaboratorio){
        pageComunicacaoWS.validarConexaoWebServiceLaboratorio(wsLaboratorio);
    }

    @Step
    public void validarXmlEnvio(){
        pageComunicacaoWS.validarXmlEnvio();
    }

    @Step
    public void validarXmlRetorno(){
        pageComunicacaoWS.validarXmlRetorno();
    }

}
