package com.pixeon.steps.serenity.atendimento.ordemDeServico.questionario;

import com.pixeon.pages.atendimento.ordemDeServico.PageLancamentoDeOrdemDeServico;
import com.pixeon.pages.pesquisaDeAvaliacao.PageQuestionario;
import net.thucydides.core.annotations.Step;

public class ImpressaoQuestionariosSteps {

    PageLancamentoDeOrdemDeServico pageLancamentoDeOrdemDeServico;

    PageQuestionario pageQuestionario;

    @Step
    public void responderQuestionariosComTipo(String quantidade, String tipoQuestionario) {
        pageLancamentoDeOrdemDeServico.clicarLinkLancamentoDeOs();
        for (int i=0; i<Integer.parseInt(quantidade); i++) {
            pageLancamentoDeOrdemDeServico.clicarLinkQuestionario();
            pageQuestionario.clicarBotaoNovoQuestionario();
            pageQuestionario.selecionarQuestionario(tipoQuestionario);
            pageQuestionario.gravarQuestionario();
            pageQuestionario.responderQuestionario();
            pageQuestionario.gravarQuestionario();
            pageQuestionario.validarAlertaConceitoObtidoQuestionario("Bom");
        }
    }

    @Step
    public void imprimirQuestionarioOrdemDeServico() {
        pageLancamentoDeOrdemDeServico.clicarLinkQuestionario();
        pageQuestionario.clicarBotaoImprimir();
    }

    @Step
    public void validarImpressaoDeQuestionarios() {
        System.out.println("");
    }

}
