package com.pixeon.steps.serenity.atendimento.ordemDeServico;

import com.pixeon.pages.atendimento.ordemDeServico.PageItensDaOrdemDeServico;
import net.thucydides.core.annotations.Step;

public class ImprimirOrdemDeServicoSteps {

    PageItensDaOrdemDeServico pageItensDaOrdemDeServico;

    @Step
    public void imprimirOrdemDeServico(){
        pageItensDaOrdemDeServico.imprimirOrdemDeServico();
    }

    @Step
    public void fecharTelaImpressao(){
        pageItensDaOrdemDeServico.fecharTelaImpressao();
    }

}
