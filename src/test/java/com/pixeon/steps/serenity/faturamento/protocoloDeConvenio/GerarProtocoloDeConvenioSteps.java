package com.pixeon.steps.serenity.faturamento.protocoloDeConvenio;

import com.pixeon.pages.faturamento.protocoloDeConvenio.PageFechamentoProtocoloDeConvenio;
import com.pixeon.pages.faturamento.protocoloDeConvenio.PageLoteDeGuias;
import net.thucydides.core.annotations.Step;

public class GerarProtocoloDeConvenioSteps {

    PageFechamentoProtocoloDeConvenio pageFechamentoProtocoloDeConvenio;
    PageLoteDeGuias pageLoteDeGuias;

    @Step
    public void buscarOrdensDeServicoComStatus(String status) {
        pageFechamentoProtocoloDeConvenio.selecionarStatus(status);
        pageFechamentoProtocoloDeConvenio.clicarBotaoBuscar();
   //     pageFechamentoProtocoloDeConvenio.verificarFaturasNaoPagas();
    }

    @Step
    public void verificarque_apenas_os_comstatus_saoexibidas(String status) {
        pageFechamentoProtocoloDeConvenio.ValidarStatus(status);
    }


    @Step
    public void gerarProtocoloDeConvenio() {
        pageFechamentoProtocoloDeConvenio.clicarBotaoGerarLote();
        pageFechamentoProtocoloDeConvenio.aceitarPopUpConfirmarGerarLote();
    }

    @Step
    public void receberLoteDeGuias() {
        pageLoteDeGuias.clicarCheckRecebidos();
        pageLoteDeGuias.escreverObservacao();
        pageLoteDeGuias.clicarBotaoGravar();
    }

    @Step
    public void pagarOSComStatusAberta(String mensagem) {
        pageLoteDeGuias.pagarOSComStatusAberta(mensagem);
    }



    @Step
    public void buscarLotesDoDia() {
        pageLoteDeGuias.clicarLinkBuscarLote();
        pageLoteDeGuias.selecionarLoteRecebidos();
        pageLoteDeGuias.clicarBotaoBuscar();
    }

    @Step
    public void validarExistenciaLoteGerado() {
        pageLoteDeGuias.validarExistenciaLote();
    }

    @Step
    public void visualizo_as_OS_aberta_clico_na_OS_e_realizo_o_pagamento_da_OS() {
        pageLoteDeGuias.pagarOSDoFechamentoDeCaixaComStatusAberta();
    }



    @Step
    public void gerarProtocolo() {
        pageFechamentoProtocoloDeConvenio.clicarBotaoGerarLote();
    }
}
