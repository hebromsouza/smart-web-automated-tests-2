package com.pixeon.steps.serenity.faturamento.notaFiscal;

import com.pixeon.pages.faturamento.notaFiscal.PageFaturasNotaFiscal;
import net.thucydides.core.annotations.Step;

public class FiltrarNotaFiscalSteps {

    PageFaturasNotaFiscal pageFaturasNotaFiscal;

    @Step
    public void filtrarNotas() {
        pageFaturasNotaFiscal.periodoInicial("01062020");
        pageFaturasNotaFiscal.escreverDataFimHoje();
        pageFaturasNotaFiscal.clicarBotaoFiltrar();
    }


    @Step
    public void filtrarNotasPeloNumero() {
     //   pageFaturasNotaFiscal.escreverDataInicioHoje();
        pageFaturasNotaFiscal.periodoInicial("01062020");
        pageFaturasNotaFiscal.escreverNumeroNotaGerada();
        pageFaturasNotaFiscal.clicarBotaoFiltrar();
    }

    @Step
    public void validarExistenciaNotaFiscal(String status) {
        pageFaturasNotaFiscal.verificarExistenciaNumeroNotaFiscal(status);
    }

}
