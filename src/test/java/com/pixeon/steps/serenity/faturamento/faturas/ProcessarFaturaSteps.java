package com.pixeon.steps.serenity.faturamento.faturas;

import com.pixeon.pages.faturamento.faturas.PageFaturamento;
import net.thucydides.core.annotations.Step;

public class ProcessarFaturaSteps {

    PageFaturamento pageFaturamento;

    @Step
    public void processarFaturaEmpresaConvenioContratato(String empresaConvenio, String convenio, String contratado) {
        pageFaturamento.digitarDataEmissaoHoje();
        pageFaturamento.digitarDataVencimentohoje();
        pageFaturamento.selecionarEmpresaConvenio(empresaConvenio);
        pageFaturamento.selecionarConvenio(convenio);
        pageFaturamento.selecionarContratado(contratado);
        pageFaturamento.digitarPeriodoAtendimentoHoje();
        pageFaturamento.clicarBotaoProcessar();
    }

    @Step
    public void validarProcessamentoFatura() {
        pageFaturamento.validarExistenciaOrdemDeServicoFaturada();
    }

}
