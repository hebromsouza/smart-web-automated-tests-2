package com.pixeon.steps.serenity.faturamento.recepcaoCaixa;

import com.pixeon.pages.faturamento.recepcaoCaixa.PageFechamentoCaixa;
import net.thucydides.core.annotations.Step;

public class FechamentoCaixaSteps {

    PageFechamentoCaixa pageFechamentoCaixa;

    @Step
    public void fechamentoCaixa() {
        pageFechamentoCaixa.clicarBotaoFechamentoCaixa();
    }

    @Step
    public void validarExistenciaOrdemDeServico() {
        pageFechamentoCaixa.validarExisteciaOrdemDeServico();
    }

}
