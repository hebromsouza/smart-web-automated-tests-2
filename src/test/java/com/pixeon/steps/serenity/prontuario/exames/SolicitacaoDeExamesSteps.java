package com.pixeon.steps.serenity.prontuario.exames;

import com.pixeon.pages.prontuario.exames.PageSolicitacaoDeExames;
import net.thucydides.core.annotations.Step;

public class SolicitacaoDeExamesSteps {

    PageSolicitacaoDeExames pageSolicitacaoDeExames;

    @Step
    public void solicitacaoExame(){
        pageSolicitacaoDeExames.clicarBotaoNovaSolicitacaoDeExames();
    }

    @Step
    public void selecionarGrupo(String grupo){
//        pageSolicitacaoDeExames.clicarLinkGrupoExames(grupo);
        if(grupo.equals("Cardiologia")) {
            pageSolicitacaoDeExames.clicarGrupoCardiologia();
        }
    }

    @Step
    public void selecionarExamesCardiologia(){
//        pageSolicitacaoDeExames.clicarExames(exames);
        pageSolicitacaoDeExames.inserirAssisteVentMecani();
        pageSolicitacaoDeExames.inserirAtendDisfunSexual();
        pageSolicitacaoDeExames.inserirAvaliaNutricional();
    }

    @Step
    public void selecionarGrupoLaboratorioESubGrupoBacteriologia(){
//        pageSolicitacaoDeExames.clicarLinkGrupoExames(grupo);
//        pageSolicitacaoDeExames.clicarSubGruposExames(subGrupo);
        pageSolicitacaoDeExames.clicarGrupoLaboratorio();
        pageSolicitacaoDeExames.clicarSubGrupoBacteriologia();
    }

    @Step
    public void selecionarExamesGrupoLaboratorio(){
        pageSolicitacaoDeExames.clicarExameLaboratorioBaciloDiftericoP();
    }

    @Step
    public void avancarPreenchimentoDados(){
        pageSolicitacaoDeExames.clicarBotaoProximo();
    }

    @Step
    public void selecionarProfissional(String profissional){
        pageSolicitacaoDeExames.selecionarProfissional(profissional);
    }

    @Step
    public void selecionarTipoAtendimento(String tipoAtendimento){
        pageSolicitacaoDeExames.selecionarTipoAtendimento(tipoAtendimento);
    }

    @Step
    public void selecionarTipoSaida(String tipoSaida){
        pageSolicitacaoDeExames.selecionarTipoSaida(tipoSaida);
    }

    @Step
    public void concluirSolicitacao(){
        pageSolicitacaoDeExames.clicarBotaoConcluir();
    }

    @Step
    public void cancelarImpressaoSolicitacao(){
        pageSolicitacaoDeExames.validarMensagemDeImpressaoECancelar();
    }

    @Step
    public void validarPedidoExamesCardiologiaLaboratorio(){
        pageSolicitacaoDeExames.validarDataPedidoExame();
        pageSolicitacaoDeExames.validarPedidoExameCardiologiaLaboratorio();
    }

}
