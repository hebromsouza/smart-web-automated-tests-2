package com.pixeon.steps.serenity.prontuario.registro_clinico;


import com.pixeon.pages.common.PageActionCommon;
import com.pixeon.pages.prontuario.registro_clinico.PagePeriodoRegistroClinico;
import com.pixeon.util.SeleniumUtil;
import net.thucydides.core.annotations.Step;

public class PeriodoRegistroClinicoSteps {

    PagePeriodoRegistroClinico pagePeriodoRegistroClinico;
    PageActionCommon pageActionCommon;

    @Step
    public void informar_a_data_inicial_e_a_data_Final_na_tela_de_periodo_e_busca(String datainico, String dataFim) {
        pagePeriodoRegistroClinico.inseirDataEConfirmar(datainico,dataFim);
    }
    @Step
    public void fechar_a_tela_de_Periodo() {
        pagePeriodoRegistroClinico.clickFechar();
    }

    @Step
    public void confirmar_o_popoup(String mensagem) {
        pageActionCommon.validarMensagemAlertaPopUp(mensagem);
    }

    @Step
    public void o_sistema_valida_na_impressao_em_PDF_o_campo_na_tela_de_impresao_do_prontuario(String mesagem) {
        SeleniumUtil.validarPDF(mesagem);
    }


}
