package com.pixeon.steps.serenity.prontuario.registro_clinico.formularios;

import com.pixeon.pages.common.PageActionCommon;
import com.pixeon.pages.prontuario.registro_clinico.formularios.PageFormularioDeAnaminese;
import net.thucydides.core.annotations.Step;


public class FormularioDeAnamineseSteps {

    PageFormularioDeAnaminese pageFormularioDeAnaminese;
    PageActionCommon pageActionCommon;

    @Step
    public void inserirDadosTxtQueixaPrincipal( String queixaPrincipal) { pageFormularioDeAnaminese.inserirDadoQueixaPrincipal(queixaPrincipal); }

    @Step
    public void selecionaGravar() { pageFormularioDeAnaminese.clicarBtnGravar(); }

    @Step
    public void selecionaAssinar() {
        pageFormularioDeAnaminese.clicarbtnAssinar();
    }

    @Step
    public void fechar_a_tela_Formulario_de_anaminese() {
        pageFormularioDeAnaminese.clicarFechar();
    }
    @Step
    public void aprovar_da_tela_Formulario_de_anaminese() {
        pageFormularioDeAnaminese.clicarAprovar();
    }

    @Step
    public void e_apresentado_a_mensagem_e_confirma_o_popup_da_tela_Formulario_de_anaminese(String mensagem) {
        pageActionCommon.validarMensagemAlertaPopUp(mensagem);
    }
    @Step
    public void finalizar_na_tela_Formulario_de_anaminese() { pageFormularioDeAnaminese.clicarbtnFinalizar(); }

}
