package com.pixeon.steps.serenity.prontuario.registro_clinico;

import com.pixeon.pages.prontuario.registro_clinico.PageRegistrosClinicos;
import net.thucydides.core.annotations.Step;

public class RegistrosClinicosSteps {

    PageRegistrosClinicos pageRegistrosClinicos;

    @Step
    public void validarRegistroClinicoPorOrdemServico() {
        pageRegistrosClinicos.validarRegistroClinicoPorOrdemServico();
    }

}
