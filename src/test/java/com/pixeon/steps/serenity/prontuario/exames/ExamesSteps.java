package com.pixeon.steps.serenity.prontuario.exames;

import com.pixeon.pages.prontuario.exames.PageExames;
import net.thucydides.core.annotations.Step;

public class ExamesSteps {

    PageExames pageExames;

    @Step
    public void cancelarSolicitacaoExamesComMotivo(String motivo){
        pageExames.cancelarExames();
        pageExames.informarMotivoDeCancelamento(motivo);
        pageExames.validarCancelamentoDeExames();
    }

}
