package com.pixeon.steps.serenity.prontuario.selecao_aba_prontuario;

import com.pixeon.pages.prontuario.PageProntuarioHome;
import net.thucydides.core.annotations.Step;

public class SelecaoAbaProntuarioSteps {
    PageProntuarioHome pageProntuarioHome;


    @Step
    public void solecionarAbaRegistro(String abaProntuario) {
        pageProntuarioHome.clicarAbaRegistroClinico(abaProntuario);
    }

}
