package com.pixeon.steps.serenity.prontuario.registro_clinico;

import com.pixeon.pages.prontuario.registro_clinico.PageArquivos;
import net.thucydides.core.annotations.Step;

public class ArquivosSteps {
    PageArquivos pageArquivos;

    @Step
    public void adicionar_arquivos_na_tela_de_arquivos() {
        pageArquivos.clickAdicionarArquivos();
    }
    @Step
    public void o_sistema_valida_se_foi_anexado_o_arquivo_na_tela_de_arquivos() {
        pageArquivos.validarExistenciaDoArquivo();
    }
    @Step
    public void fechar_tela_de_arquivos() {
        pageArquivos.clickFechar();
    }
    @Step
    public void habilitar_o_Flash_na_tela_de_arquivo() {
        pageArquivos.clickLinkhabilitarFlash();
    }
}
