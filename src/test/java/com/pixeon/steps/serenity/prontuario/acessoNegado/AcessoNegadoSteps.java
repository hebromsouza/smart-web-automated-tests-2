package com.pixeon.steps.serenity.prontuario.acessoNegado;

import com.pixeon.pages.prontuario.acessoNegado.PageAcessoNegado;
import net.thucydides.core.annotations.Step;

public class AcessoNegadoSteps {

    PageAcessoNegado pageAcessoNegado;

    @Step
    public void é_apresentado_a_mensagem_da_tela_acesso_negado(String acessoNegado) {
        pageAcessoNegado.validarMensagemDeAcessoNegado(acessoNegado);
    }

}
