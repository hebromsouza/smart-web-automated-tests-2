package com.pixeon.steps.serenity.prontuario.registro_clinico;

import com.pixeon.pages.prontuario.registro_clinico.PageJustificativa;
import net.thucydides.core.annotations.Step;

public class JustificativaSteps {

    PageJustificativa pageJustificativa;

    @Step
    public void informar_a_Justificativa_e_confiramr_na_tela_de_Justificativa(String justificativa) {
        pageJustificativa.inseirJustificativaEConfirmar(justificativa);
    }
}
