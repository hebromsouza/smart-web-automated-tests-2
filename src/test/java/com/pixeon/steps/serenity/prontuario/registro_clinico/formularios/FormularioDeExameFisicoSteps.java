package com.pixeon.steps.serenity.prontuario.registro_clinico.formularios;

import com.pixeon.pages.prontuario.registro_clinico.formularios.PageFormularioDeExameFisico;
import net.thucydides.core.annotations.Step;

public class FormularioDeExameFisicoSteps {
    PageFormularioDeExameFisico pageFormularioDeExameFisico;

    @Step
    public void anexar_arquivos_na_tela_de_Formularios_de_exame_Fisico() {
        pageFormularioDeExameFisico.clickAnexarArquivos();
    }

    @Step
    public void informar_no_campo_exame_fisico_na_tela_de_Formularios_de_exame_Fisico(String exame_fisico) {
        pageFormularioDeExameFisico.informarExameFisico(exame_fisico);
    }
    @Step
    public void finalizaro_laudo_na_tela_de_Formularios_de_exame_Fisico() {
        pageFormularioDeExameFisico.clickFinalizar();
    }

    @Step
    public void fechar_formulário_na_tela_de_Formularios_de_exame_Fisico() {
        pageFormularioDeExameFisico.clickFechar();
    }
}
