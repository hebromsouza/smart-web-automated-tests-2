package com.pixeon.steps.serenity.prontuario.registro_clinico;

import com.pixeon.pages.prontuario.registro_clinico.PageNovoRegistroClinico;
import net.thucydides.core.annotations.Step;

public class NovoRegistroClinicoSteps {

    PageNovoRegistroClinico pageNovoRegistroClinico;

    @Step
    public void selecionaFormularioDeAnaminese() {
        pageNovoRegistroClinico.clicarNoFormularioDeAnaminese();
    }

    @Step
    public void infromar_o_registro_e_grava_na_tela_de_novo_registro(String registro) {
        pageNovoRegistroClinico.DotaRegistrodata();
        pageNovoRegistroClinico.txtRegistro(registro);
        pageNovoRegistroClinico.clicarBnGravar();
    }

    @Step
    public void validar_se_o_Status_do_para_o_atendimento_feito_na_tela_de_prontuário(String formulario) {
        pageNovoRegistroClinico.validarStatusdoFormulario(formulario);
    }

    @Step
    public void seleciono_o_último_FORMULÁRIO_ANAMNESE_Aguardando_aprovação_inserido_na_tela_de_novo_registro() {
        pageNovoRegistroClinico.selecioanrAprimeiraLinhaDataTabelaNomeDoformulario();
    }

    @Step
    public void o_sistema_valida_se_foi_informado_que_o_registro_clínico_esta_com_o_status_e_com_o_icone_de_anexo_na_prontuario_aba_registro_clinico(String formulario, String status) {
        pageNovoRegistroClinico.validarNomeStatusIconDoformulario(formulario,status);
    }

}
