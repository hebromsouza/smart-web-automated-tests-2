package com.pixeon.steps.serenity.prontuario.registro_clinico;


import com.pixeon.pages.prontuario.registro_clinico.PageImpressaoRegistroClinico;
import com.pixeon.util.SeleniumUtil;
import net.thucydides.core.annotations.Step;

public class ImpressaoRegistroClinicoSteps {

    PageImpressaoRegistroClinico pageImpressaoRegistroClinico;


    @Step
    public void selecionarNoAdicionarAOpcao(String item) {
        pageImpressaoRegistroClinico.clicarNoBtnAdiconarESelecionar0itemDDaPage(item);
    }

 //   @Step
 //   public void impimir_na_tela_de_Protuario() {
 //       prontuarioDoPacienteAbaRegistroClinico.clicarImprimir();
//    }

    @Step
    public void impimir_na_tela_de_Protuario_na_Aba_Registro_Clinico() {
        pageImpressaoRegistroClinico.clicarImprimir();
    }


    @Step
    public void validar_conteudo_no_PDF_nome_do_software_fornecedor_versão_e_build(String txtParavalidar) {
    	//prontuarioDoPacienteAbaRegistroClinico.validarPDF(txtParavalidar);
        SeleniumUtil.validarPDF(txtParavalidar);
    }
}
