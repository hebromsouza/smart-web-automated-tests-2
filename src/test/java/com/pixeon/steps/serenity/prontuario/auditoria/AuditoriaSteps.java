package com.pixeon.steps.serenity.prontuario.auditoria;

import com.pixeon.datamodel.busca_auditoria.DataBuscaAuditoria;
import com.pixeon.pages.prontuario.auditoria.PageAuditoria;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class AuditoriaSteps {

    PageAuditoria pageAuditoria;

    @Step
    public void que_preencho_dados_da_busca_na_tela_de_auditoria(List<DataBuscaAuditoria> dataBuscaAuditoria) {
        pageAuditoria.preencheCamposDeBuscaAuditoria(dataBuscaAuditoria);
    }

    @Step
    public void buscar_na_tela_de_auditoria() {
        pageAuditoria.clicaBtnBuscar();
    }

    @Step
    public void validar_o_que_na_tabela_contem_e_as(String data, String criticidade, String evento, String identificaco_do_componente, String dentificacao_do_usuario, String indicacacao_de_atividade, String identificador_do_registro) {
        pageAuditoria.validaPresencaDointemnaTela(data);
        pageAuditoria.validaPresencaDointemnaTela(evento);
        pageAuditoria.validaPresencaDointemnaTela(identificaco_do_componente);
        pageAuditoria.validaPresencaDointemnaTela(dentificacao_do_usuario);
        pageAuditoria.validaPresencaDointemnaTela(indicacacao_de_atividade);
        pageAuditoria.validaPresencaDointemnaTela(identificador_do_registro);
    }

}
