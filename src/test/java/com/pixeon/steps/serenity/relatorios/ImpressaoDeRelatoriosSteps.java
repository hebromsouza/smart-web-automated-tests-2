package com.pixeon.steps.serenity.relatorios;

import com.pixeon.pages.common.PageActionCommon;
import com.pixeon.pages.relatorios.*;
import com.pixeon.util.SeleniumUtil;
import net.thucydides.core.annotations.Step;

public class ImpressaoDeRelatoriosSteps {

    PageRelatorios pageRelatorios;

    PageRelatorioMovimentacaoCaixa pageRelatorioMovimentacaoCaixa;

    PageAtendimentosPorRecepcionista pageAtendimentosPorRecepcionista;

    PageAtendimentoPorColetaPostoOS pageAtendimentoPorColetaPostoOS;

    PageExamesPorDataDeEntrega pageExamesPorDataDeEntrega;

    PageInternacaoTempoPermanencia pageInternacaoTempoPermanencia;

    PageExamesRealizadosColeta pageExamesRealizadosColeta;

    PageActionCommon pageActionCommon;

    @Step
    public void acessarTelaEmissorDeRelatorios() {
        pageRelatorios.clicarBotaoEmissorDeRelatorios();
    }

    @Step
    public void acessarTelaRelatorio(String relatorio) {
        pageRelatorios.abrirRelatorio(relatorio);
    }

    @Step
    public void inserirDataPeriodolHoje(String relatorio) {
        String dtIni = SeleniumUtil.getDataAtualFormato("dd/MM/yyyy");
        String dtFim = SeleniumUtil.getDataAtualFormato("dd/MM/yyyy");
        String dtIniHr = SeleniumUtil.getDataAtualFormato("01"+"/MM/yyyy "+"00:00");
        String dtFimHr = SeleniumUtil.getDataAtualFormato("28"+"/MM/yyyy "+"23:59");

        if (relatorio.equals("Movimentação de Caixa")) {
            pageRelatorioMovimentacaoCaixa.escreverDataInicialHoje(dtIni);
        }else if (relatorio.equals("ATENDIMENTOS POR RECEPCIONISTAS")){
            pageAtendimentosPorRecepcionista.escreverDataInicio(dtIniHr);
            pageAtendimentosPorRecepcionista.escreverDataFim(dtFimHr);
        }else if (relatorio.equals("RELATÓRIO DE ATENDIMENTO POR POSTO DE COLETA - ORDENADO POR OS")){
            pageAtendimentoPorColetaPostoOS.escreverDataInicio(dtIni);
            pageAtendimentoPorColetaPostoOS.escreverDataFim(dtFim);
        }else if (relatorio.equals("EXAMES POR DATA DE ENTRADA")){
            pageExamesPorDataDeEntrega.escreverDataInicio(dtIni);
            pageExamesPorDataDeEntrega.escreverDataFim(dtFim);
        }else if (relatorio.equals("Internação - Tempo Permanência")){
            pageInternacaoTempoPermanencia.escreverDataInicio(dtIni);
            pageInternacaoTempoPermanencia.escreverDataFim(dtFim);
        }else if (relatorio.equals("AREA TECNICA - EXAMES REALIZADOS NA COLETA (TSG,TCO,COAG,PPD,IDRE)")){
            pageExamesRealizadosColeta.escreverDataInicial(dtIniHr);
            pageExamesRealizadosColeta.escreverDataFinal(dtFimHr);
        }

    }

    @Step
    public void selecionarRecepcao(String recepcao) {
        pageRelatorioMovimentacaoCaixa.selecionarRecepcao(recepcao);
    }

    @Step
    public void selecionarUsuario(String usuario) {
        pageRelatorioMovimentacaoCaixa.selecionarUsuario(usuario);
    }

    @Step
    public void buscarRelatorio() {
        pageRelatorioMovimentacaoCaixa.clicarBotaoOk();
    }

    @Step
    public void inserirSetorSolicitante(String setorSolicitante) {
        pageAtendimentosPorRecepcionista.escreverSetorSolicitante(setorSolicitante);
    }

    @Step
    public void emitirRelatorio() {
        pageAtendimentosPorRecepcionista.clicarBotaoEmitirRelatorio();
    }

    @Step
    public void validarImpressaoRelatorio() {
        pageRelatorios.closePanel();
    }

    @Step
    public void limparCampoTempoPermanencia() {
        pageInternacaoTempoPermanencia.limparCampoTempoPermanencia();
    }

    @Step
    public void limparCampoDataInicial() {
        pageInternacaoTempoPermanencia.limparCampoDataInicio();
    }

    @Step
    public void limparCampoDataFinal() {
        pageInternacaoTempoPermanencia.limparCampoDataFim();
    }

    @Step
    public void inserirSetorSolicitanteTelaAreaTecnica(String setorSolicitante) {
        pageExamesRealizadosColeta.escreverSetorSolicitante(setorSolicitante);
    }

    @Step
    public void limparCampoStatusExecucao() {
        pageExamesRealizadosColeta.limparCampoStatusExecucao();
    }

    @Step
    public void limparCampoDataInicialTelaAreaTecnica() {
        pageExamesRealizadosColeta.limparCampoDataInicial();
    }

    @Step
    public void limparCampoDataFinalTelaAreaTecnica() {
        pageExamesRealizadosColeta.limparCampoDataFinal();
    }

    @Step
    public void limparCampoCodigoItemTelaAreaTecnica() {
        pageExamesRealizadosColeta.limparCampoCodigoItem();
    }

}