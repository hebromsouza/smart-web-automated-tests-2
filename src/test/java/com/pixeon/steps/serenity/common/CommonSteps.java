package com.pixeon.steps.serenity.common;

import com.pixeon.pages.common.PageActionCommon;
import com.pixeon.util.SeleniumUtil;
import net.thucydides.core.annotations.Step;

public class CommonSteps {

    PageActionCommon pageActionCommon;

    @Step
    public void e_apresentado_a_mensagem_e_confirma_o_popup(String mensagem) {
        pageActionCommon.validarMensagemAlertaPopUp(mensagem);
    }


    @Step
    public void informo_o_caminho_do_arquivo_na_janela_do_windows_e_abrir(String texto) {
        SeleniumUtil.informarTextoEmJanelaDoWindowsEconfiramr(texto);
    }

    @Step
    public void abrirArquivoInformandoCaminhoNaJanelaDoWindows(String caminho){
        SeleniumUtil.informarTextoEmJanelaDoWindowsEconfiramr(caminho);
    }

    @Step
    public void selecionarAba(String aba) {
        pageActionCommon.clicarNaAba(aba);
    }

    @Step
    public void closePainel(){
        pageActionCommon.closePanel();
    }

    @Step
    public void validarPopUpConfirmar(String mensagem) {
        pageActionCommon.validarMensagemAlertaPopUp(mensagem);
    }

}
