package com.pixeon.steps.serenity.motivoCancelamento;

import com.pixeon.pages.motivoCancelamento.PageMotivoCancelamento;
import net.thucydides.core.annotations.Step;

public class MotivoCancelamentoSteps {

    PageMotivoCancelamento pageMotivoCancelamento;

    @Step
    public void informarMotivoCancelamentoObservacao(String motivo, String obs) {
        pageMotivoCancelamento.selecionarMotivoCancelamento(motivo);
        pageMotivoCancelamento.escreverObservacao(obs);
        pageMotivoCancelamento.clicarBotaoOkMotivoCancelamento();
    }
}
