package com.pixeon.steps.serenity.presquisaDeAvaliacao;

import com.pixeon.pages.pesquisaDeAvaliacao.PageQuestionario;
import net.thucydides.core.annotations.Step;

public class PesquisaDeAvaliacaoSteps {

    PageQuestionario pageQuestionario;

    @Step
    public void responderQuestionario(){
        pageQuestionario.responderQuestionario();
    }

    @Step
    public void aplicarNovoQuestionario(){
        pageQuestionario.clicarBotaoNovoQuestionario();
    }

    @Step
    public void selecionarQuestionario(String questionario){
        pageQuestionario.selecionarQuestionario(questionario);
    }

    @Step
    public void gravarQuestionario(){
        pageQuestionario.gravarQuestionario();
    }

    @Step
    public void excluirRespostaQuestionario(){
        pageQuestionario.clicarBotaoExcluir();
    }

    @Step
    public void excluirRespostaQuestionarioAvaliacao(){
        pageQuestionario.clicarBotaoExcluir();
    }

    public void continuaExclusaoQuestionario(){
        pageQuestionario.clicarOkAlerta();
    }

    @Step
    public void aceitarAlertaConceitoObtido(){
        pageQuestionario.clicarOkAlerta();
    }

    @Step
    public void abrirQuestionarioRespondido(){
        pageQuestionario.clicarLinkQuestionario();
    }


    @Step
    public void clicoOk_or_Enter_sair(){
        pageQuestionario.ClicarOkEnterSair();
    }

    @Step
    public void imprimirQuestionario(){
        pageQuestionario.clicarBotaoImprimir();
    }

    @Step
    public void validarMensagemAlertaQuestionario(String mensagemAlerta){
        pageQuestionario.validarMensagemAlertaQuestionario(mensagemAlerta);
    }

    @Step
    public void validaImpressaoQuestionario(){
        pageQuestionario.validarImpressaoQuestionario();
    }
}
