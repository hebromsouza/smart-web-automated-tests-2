package com.pixeon.steps.serenity.diligenciamento.rastreabilidade;

import com.pixeon.pages.diligenciamento.rastreabilidade.PageRastreabilidadeExames;
import net.thucydides.core.annotations.Step;

public class RastreabilidadeExamesSteps {

    PageRastreabilidadeExames pageRastreabilidadeExames;

    @Step
    public void validarNumeroAmostraRastreabilidade() {
        pageRastreabilidadeExames.validarNumeroAmostraRastreabilidade();
    }

}
