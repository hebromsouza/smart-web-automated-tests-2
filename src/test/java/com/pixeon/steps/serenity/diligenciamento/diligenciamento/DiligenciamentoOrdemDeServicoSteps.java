package com.pixeon.steps.serenity.diligenciamento.diligenciamento;

import com.pixeon.pages.diligenciamento.diligenciamento.PageDiligenciamentoOrdemDeServico;
import net.thucydides.core.annotations.Step;

public class DiligenciamentoOrdemDeServicoSteps {

    PageDiligenciamentoOrdemDeServico pageDiligenciamentoOrdemDeServico;

    @Step
    public void abrirDiligenciamento(){
        pageDiligenciamentoOrdemDeServico.clicarBotaoDiligenciamento();
    }

    @Step
    public void novoDiligenciamento(){
        pageDiligenciamentoOrdemDeServico.clicarBotaoNovo();
    }

    @Step
    public void selecionarEvento(String evento){
        pageDiligenciamentoOrdemDeServico.selecionarEvento(evento);
    }

    @Step
    public void incluirObservacao(){
        pageDiligenciamentoOrdemDeServico.incluirObservacao();
    }

    @Step
    public void gravarDiligenciamento(){
        pageDiligenciamentoOrdemDeServico.clicarBotaoGravar();
    }

    @Step
    public void excluirDiligenciamento(){
        pageDiligenciamentoOrdemDeServico.clicarExcluir();
    }

    @Step
    public void validarMensagemErroExclusao(String mensagemErro){
        pageDiligenciamentoOrdemDeServico.validarMensagemErroExclusao(mensagemErro);
    }


    @Step
    public void o_deligenciamentoè_excluido_comSucesso(){
        pageDiligenciamentoOrdemDeServico.validarExclusaoDiligenciamento();
    }


    @Step
    public void validarExclusaoDiligenciamento(){
        pageDiligenciamentoOrdemDeServico.validarExclusaoDiligenciamento();
    }

    @Step
    public void validarInclusaoDiligenciamento(String evento){
        pageDiligenciamentoOrdemDeServico.validarInclusaoDiligenciamento(evento);
    }

}
