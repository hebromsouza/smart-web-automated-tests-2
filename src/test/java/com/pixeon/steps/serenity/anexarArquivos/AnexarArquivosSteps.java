package com.pixeon.steps.serenity.anexarArquivos;

import com.pixeon.pages.anexarArquivos.PageAnexarArquivos;
import net.thucydides.core.annotations.Step;

import java.awt.*;

public class AnexarArquivosSteps {

    PageAnexarArquivos pageAnexarArquivos;

    @Step
    public void abrirTelaArquivos(){
        try {
            pageAnexarArquivos.clicarBotaoAnexarArquivos();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        pageAnexarArquivos.habilitarFlash();
    }

    @Step
    public void adicionarArquivos(){
        pageAnexarArquivos.clicarAdicionarArquivos();
    }

    @Step
    public void validarArquivoAnexado(){
        pageAnexarArquivos.validarArquivoAnexado();
    }

}