package com.pixeon.steps.serenity.login.versao_software;

import com.pixeon.pages.login.PageLogin;
import net.thucydides.core.annotations.Step;

public class VersaoDoSoftwareSteps {

    PageLogin pageLogin;

    @Step
    public void validaVersaoDoSoftware(String nomeSoftware, String nomeFornecedor, String versaoSoftware, String buildSoftware) {
        pageLogin.versaoSoftwareTelaLogin(nomeSoftware, nomeFornecedor, versaoSoftware, buildSoftware);
    }

}
