package com.pixeon.steps.serenity.login.usuario;

import com.pixeon.datamodel.login.DataBuscaUsusario;
import com.pixeon.pages.login.usuario.PageBuscaUsuario;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class BuscaUsuarioSteps {
    PageBuscaUsuario pageBuscaUsuario;

    @Step
    public void informar_de_busca_da_tela_de_Usuários(List<DataBuscaUsusario> dataBuscaUsusario) {
        pageBuscaUsuario.preencheCamposDeBuscaUsuario(dataBuscaUsusario);
    }

    @Step
    public void filtrar_na_tela_de_Usuários() {
        pageBuscaUsuario.clickFiltrar();
    }

    @Step
    public void selecionar_o_nome_na_tela_de_Usuários(String nome) {
        pageBuscaUsuario.selecionarOUsuario(nome);
    }


}
