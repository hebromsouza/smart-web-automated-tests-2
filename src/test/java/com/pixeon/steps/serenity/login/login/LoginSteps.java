package com.pixeon.steps.serenity.login.login;

import com.pixeon.datamodel.login.DataLogin;
import com.pixeon.pages.home.PageHome;
import com.pixeon.pages.login.PageLogin;
import com.pixeon.steps.definitionSteps.login.login.DefinitionStepsLogin;
import net.thucydides.core.annotations.Step;

import java.util.List;

import static com.pixeon.pages.BasePage.waitHelper;

public class LoginSteps {

    PageLogin pageLogin;
    PageHome pageHome;

    String mensagemAlerta = null;

    @Step
    public void efetuarLoginSmartWeb(String usuario, String senha) throws InterruptedException {

        try {
            pageLogin.open();
            pageLogin.informarUsuarioSenhaSmartweb(usuario, senha);


            Thread.sleep(1000);
            boolean alert = waitHelper.alertGetText().contains("O usuário MEDICWARE já está logado no sistema em outra sessão.");

            if(alert==true){
                waitHelper.waitAlertOk();
            }
        } catch (Exception e) {
            System.out.println("########## LOGIN REALIZADO COM SUCESSO! INICIANDO O CENÁRIO DE TESTE ##############");
        }
        pageLogin.preencheRecepcao("CONSULTORIOS");

    }

    @Step
    public void carregarTelaInicial() {
        pageHome.carregarTelaInicial();
    }

    @Step
    public void deslogarSmartweb() {
        pageLogin.efetuarLogoutSmartweb();
    }


    @Step
    public void abreSmartWeb() {
        pageLogin.open();
    }

    @Step
    public void preencherLoginESenha(List<DataLogin> dataLogin) {
        pageLogin.preencheCamposLogin(dataLogin);
    }

    @Step
    public void realizaLogin() {
        pageLogin.clicaBtnLogin();
    }

    //Step deve ser retirado quando ajustar login do SmartPEP
    @Step
    public void realizaLoginSmartPEP() {
        pageLogin.clicaBtnLogin();
        //essa parte do bloco deve ser removida deposi de ajusta o login
        try {
            Thread.sleep(1000);
            mensagemAlerta = waitHelper.alertGetText();
            Thread.sleep(3000);
            if (mensagemAlerta.contains("já está logado no sistema em outra sessão.")) {
                waitHelper.alertOk();
                Thread.sleep(1000);
                pageLogin.preencheCamposLogin(DefinitionStepsLogin.dataLogihome);
                Thread.sleep(3000);
                pageLogin.clicaBtnLogin();
            }
        } catch (Exception e) {
            System.out.println("LoginSteps.realizaLogin");
        }
    }

    @Step
    public void informar_usuario_senha(String usuario, String senha) {

        pageLogin.inserirUsuario(usuario);
        pageLogin.inserirSenha(senha);
    }

    @Step
    public void logout() {
        pageLogin.clicarDeslogarSeExistit();

    }


    @Step
    public void quefacologout_na_app_ignorandologindo_hooks() {
        pageLogin.efetuarLogoutSmartweb();
    }

    @Step
    public void abrir_tela_login() {
        pageLogin.open();
    }

    @Step
    public void solicitaPreenchimentoRecepcao() {
        pageLogin.validaSolicitouPreenchimentoRecepcao();

    }

    @Step
    public void selecionaRecepcao(String recepcao) {
        pageLogin.preencheRecepcao(recepcao);
        pageLogin.clicaBtnOkRecepcao();
        pageHome.carregarTelaInicial();
    }

    @Step
    public void exibeMensagemLoginInvalido() {
        pageLogin.validaLoginInvalido();
    }

    @Step
    public void espera_minuto_e_validar_se_o_sistema_apresenta_a_mensagem_na_tela_de_login(String tempo, String mensagem) {
        pageLogin.validarTempoDaSecao(tempo,mensagem);
    }

    @Step
    public void for_selecionada_o_Grupo_de_usuário(String grupoDeUsuario) {
        pageLogin.preencheGrupoDeUsuario(grupoDeUsuario);
        pageLogin.clicaBtnOkRecepcao();
    }

    @Step
    public void o_sistema_apresenta_a_mensagem_na_tela_de_login(String mensagem) {
        pageLogin.validarMensagemDeErroNaTelaDeLogin(mensagem);
    }

}
