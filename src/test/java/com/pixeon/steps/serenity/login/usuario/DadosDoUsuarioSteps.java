package com.pixeon.steps.serenity.login.usuario;

import com.pixeon.pages.login.usuario.PageDadosDoUsuario;
import com.pixeon.util.SeleniumUtil;
import net.thucydides.core.annotations.Step;

public class DadosDoUsuarioSteps {

    PageDadosDoUsuario pageDadosDoUsuario;

    @Step
    public void eu_disativo_a_selcao_do_usuario_Ativo_da_tela_dados_do_usuario() {
        pageDadosDoUsuario.desativarMarcarcaoChkAtivo();
    }

    @Step
    public void gravar_na_tela_dados_do_usuario() {
        pageDadosDoUsuario.clickGravar();
    }

    @Step
    public void eu_ativar_a_opcao_Ativo_na_tela_dados_do_usuario() {
        pageDadosDoUsuario.ativoMarcarcaoChkAtivo();
    }

    @Step
    public void eu_disativo_a_selecao_sem_prazo_na_tela_dados_do_ususrio() {
        pageDadosDoUsuario.desativarMarcarcaoChkSemPrazo();
    }

    @Step
    public void eu_ativar_a_selcao_sem_prazo_na_tela_dados_do_usuario() {
        pageDadosDoUsuario.ativoMarcarcaoChkSemPrazo();
    }

    @Step
    public void informa_a_data_atual_na_tela_dados_do_usuario() {
        String data = SeleniumUtil.getDataAtual();
        pageDadosDoUsuario.informarTxtPrazo(data);
    }
    @Step
    public void fechar_tela_dados_do_usuario() {
        pageDadosDoUsuario.btnFechar();
    }


}
