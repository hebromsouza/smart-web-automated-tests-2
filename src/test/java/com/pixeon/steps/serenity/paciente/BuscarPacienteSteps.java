package com.pixeon.steps.serenity.paciente;

import com.pixeon.pages.paciente.PageBuscarPaciente;
import net.thucydides.core.annotations.Step;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BuscarPacienteSteps {

    PageBuscarPaciente pageBuscarPaciente;

    @Step
    public void buscarPacientePeloNome(String nome){
        pageBuscarPaciente.digitarNomePaciente(nome);
        pageBuscarPaciente.clicarBotaoBuscar();
       // pageBuscarPaciente.clicarLinkNomePaciente(nome);
       // pageBuscarPaciente.clicarBotaoAvancar();
    }

    @Step
    public void buscarPacientePeloNomeSemAvancar(String nome){
        pageBuscarPaciente.digitarNomePaciente(nome);
        pageBuscarPaciente.clicarBotaoBuscar();
        pageBuscarPaciente.clicarLinkNomePaciente(nome);
    }

    @Step
    public void SelecionarPacientePeloNome(String nome){
        pageBuscarPaciente.selecionarPacienteLuana();
    }


    @Step
    public void buscarPacienteComDadosDeBusca(String formaDeBusca, String busca){
        if(formaDeBusca.equals("ordemServico")) {
            buscarPacienteComOrdemServico();
        }else{
            pageBuscarPaciente.buscarPacienteComDadosDeBusca(formaDeBusca, busca);
        }
    }

    @Step
    public void buscarPacienteComDadosDeBusca(String tipoBusca, String busca, String nomePaciente) {
        pageBuscarPaciente.digitarNomePaciente(nomePaciente);
        pageBuscarPaciente.escreverDadosCampoBusca(tipoBusca, busca);
        pageBuscarPaciente.clicarBotaoBuscar();
    }

    @Step
    public void buscarPacienteComDadosDeBuscaEAvancar(String formaDeBusca, String busca){
//        if(formaDeBusca.equals("ordemServico")) {
//            buscarPacienteComOrdemServico();
//        }else{
            pageBuscarPaciente.buscarComDadosBusca(formaDeBusca, busca);
//        }
    }

    @Step
    public void buscarOrdemServicoComCodigoAmostra(String codigoAmostra){
        pageBuscarPaciente.digitarCodigoAmostra(codigoAmostra);
        pageBuscarPaciente.clicarBotaoBuscar();
    }

    @Step
    public void buscarPacienteComOrdemServico(){
        pageBuscarPaciente.buscarPacienteComOrdemServico();
    }

    @Step
    public void pegarValorCamposOrdemServico(String formaDeBusca){
        pageBuscarPaciente.pegarValorCamposOrdemServico(formaDeBusca);
    }

    @Step
    public void pegarNumeroOrcamentoPreAtendimento(){
        pageBuscarPaciente.pegarNumeroOrcamentoPreAtendimento();
    }

    @Step
    public void buscarOrcamentoPreAtendimento(){
        pageBuscarPaciente.buscarPacienteComNumeroOrcamentoPreAtendimento();
    }

    @Step
    public void validarBuscaPaciente(String formaDeBusca, String busca){
        if(formaDeBusca.equals("ordemServico")){
            assertTrue(pageBuscarPaciente.validarBuscaPacienteComOrdemServico());
        }else if(formaDeBusca.equals("amostra")){
            assertTrue(pageBuscarPaciente.validarBuscaPacienteComAmostra());
        }else {
            assertEquals(busca, pageBuscarPaciente.validarBuscaPaciente(formaDeBusca));
        }
    }

    @Step
    public void validarBuscaPacienteComOrcamentoPreAtendimento(String matricula){
        assertTrue(pageBuscarPaciente.validarBuscaPacienteComOrcamentoPreAtendimento(matricula));
    }

}
