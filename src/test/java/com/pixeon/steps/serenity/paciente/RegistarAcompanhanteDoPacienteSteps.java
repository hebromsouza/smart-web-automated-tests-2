package com.pixeon.steps.serenity.paciente;

import com.pixeon.pages.paciente.PageRegistrarAcompanhanteDoPaciente;
import net.thucydides.core.annotations.Step;
import static org.junit.Assert.assertTrue;

public class RegistarAcompanhanteDoPacienteSteps {

    PageRegistrarAcompanhanteDoPaciente pageRegistrarAcompanhanteDoPaciente;

    @Step
    public void fecharPainel(){
        pageRegistrarAcompanhanteDoPaciente.fecharPainel();
    }

    @Step
    public void acessarTelaAcompanhante(){
        pageRegistrarAcompanhanteDoPaciente.clicarBotaoAcompanhante();
    }

    @Step
    public void excluirAcompanhanteExistente(){
        pageRegistrarAcompanhanteDoPaciente.excluirAcompanhanteExistente();
    }

    @Step
    public void novoAcompanhanteComParentesco(String acompanhante, String parentesco){
        pageRegistrarAcompanhanteDoPaciente.clicarBotaoNovoAcompanhante();
        pageRegistrarAcompanhanteDoPaciente.selecionarParentesco(parentesco);
        pageRegistrarAcompanhanteDoPaciente.digitarNomeAcompanhante(acompanhante);
        pageRegistrarAcompanhanteDoPaciente.digitarObservacaoAcompanhante();
        pageRegistrarAcompanhanteDoPaciente.gravarRegistroDeAcompanhante();
    }

    @Step
    public void abaAcompanhantePacientePorPaciente(){
        pageRegistrarAcompanhanteDoPaciente.clicarAbaFamilia();
    }

    @Step
    public void registrarNovoAcompanhanteBuscandoPorRegistroParentesco(String registro, String parentesco){
        pageRegistrarAcompanhanteDoPaciente.clicarBotaoNovoAcompanhante();
        pageRegistrarAcompanhanteDoPaciente.buscarAcompanhantePorRegistro(registro);
        pageRegistrarAcompanhanteDoPaciente.informarParentescoDeAcompanhanteFamilia(parentesco);
        pageRegistrarAcompanhanteDoPaciente.digitarObservacaoAcompanhanteFamilia();
        pageRegistrarAcompanhanteDoPaciente.gravarRegistroDeAcompanhanteFamilia();
    }

    @Step
    public void validaRegistroAcompanhante(String acompanhante){
        pageRegistrarAcompanhanteDoPaciente.fecharPainel();
        pageRegistrarAcompanhanteDoPaciente.clicarBotaoAcompanhante();
        assertTrue(pageRegistrarAcompanhanteDoPaciente.validarRegistroDeAcompanhante(acompanhante));
    }

    @Step
    public void validaRegistroAcompanhantePacientePorPaciente(String registro){
        pageRegistrarAcompanhanteDoPaciente.fecharPainel();
        pageRegistrarAcompanhanteDoPaciente.clicarBotaoAcompanhante();
        abaAcompanhantePacientePorPaciente();
        assertTrue(pageRegistrarAcompanhanteDoPaciente.validarRegistroDeAcompanhanteFamilia(registro));
    }

}
