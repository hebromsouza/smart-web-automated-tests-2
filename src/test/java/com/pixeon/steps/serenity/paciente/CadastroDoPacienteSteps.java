package com.pixeon.steps.serenity.paciente;

import com.pixeon.pages.paciente.PageBuscarPaciente;
import com.pixeon.pages.paciente.PageCadastroDoPaciente;
import net.thucydides.core.annotations.Step;

public class CadastroDoPacienteSteps {

    PageCadastroDoPaciente pageCadastroDoPaciente;
    PageBuscarPaciente pageBuscarPaciente;

    @Step
    public void avancarCadastroPaciente(){
        pageBuscarPaciente.validarAlertaAniversario();
        pageCadastroDoPaciente.clicarNoBotaoAvancar();
    }

}
