package com.pixeon.steps.serenity.paciente;

import com.pixeon.datamodel.cadastros.DataModelCadastroPacientes;
import com.pixeon.datamodel.ordemDeServico.DataOrdemDeServico;
import com.pixeon.pages.common.PageActionCommon;
import com.pixeon.pages.paciente.PageBuscaPaciente;
import com.pixeon.pages.paciente.PageBuscarPaciente;
import com.pixeon.pages.paciente.PageCadastroDoPaciente;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class PacienteSteps {

    PageBuscaPaciente pageBuscaPaciente;
    PageBuscarPaciente pageBuscarPaciente;
    PageCadastroDoPaciente pageCadastroDoPaciente;
    PageActionCommon pageActionCommon;

    @Step
    public void informo_paciente_aleatorio(String nome){
        pageCadastroDoPaciente.InformarDadosPacienteAleatorio(nome);
    }

    @Step
    public void clico_no_botao_comum(String btComum){
        pageActionCommon.clicarNoBotao(btComum);
    }

    @Step
    public void confirmo_em_OK_o_desejo_de_incluir_um_novo_paciente(){
        pageCadastroDoPaciente.ConfirmarInclusaoNovoPaciente();
    }

    @Step
    public void preencho_os_dados_basicos_do_cadastro_do_paciente(List<DataModelCadastroPacientes>  dadosBasicosPaciente){
        pageCadastroDoPaciente.InformarDadosBasicosDoPaciente(dadosBasicosPaciente);
    }

    @Step
    public void preencho_os_dados_da_operadora_de_saude_do_paciente(List<DataModelCadastroPacientes> dadosOperadoraSaude){
        pageCadastroDoPaciente.InformarDadosOperadoraSaudePaciente(dadosOperadoraSaude);
    }

    @Step
    public void preencho_os_dados_de_contato_do_paciente(List<DataModelCadastroPacientes> dadosContatoPaciente){
        pageCadastroDoPaciente.InformarDadosContatoPaciente(dadosContatoPaciente);
    }

    @Step
    public void buscarPacientePorRegistro(String registroPaciente){
        pageBuscaPaciente.escreverNoCampoRegistro(registroPaciente);
        pageBuscaPaciente.clicarNoBotaoBuscar();
    }

    @Step
    public void buscarPacientePorMatricula(String matriculaPaciente){
        pageBuscaPaciente.escreverNoCampoMadricula(matriculaPaciente);
        pageBuscaPaciente.clicarNoBotaoBuscar();
        pageBuscaPaciente.clicarNomePacienteTelaBusca(matriculaPaciente);
    }


    @Step
    public void informar_o_cpf_cnpj(String cpfcnpj){
        pageCadastroDoPaciente.InformarCPFCNPJ(cpfcnpj);
        pageCadastroDoPaciente.clicarNoBotaoAvancar();
    }

    @Step
    public void informo_motivo_alteracao(){
        pageCadastroDoPaciente.InformarMotivoAlteracao();
    }

    @Step
    public void é_apresentado_a_mensagem_de_alerta(String msg){
        pageCadastroDoPaciente.ValidarMsgAlertaCpfCnpj(msg);
    }

    @Step
    public void buscarPacienteComNumeroDeRegisto(String regPaciente){
        buscarPacientePorRegistro(regPaciente);
        pageBuscarPaciente.validarAlertaAniversario();
        pageCadastroDoPaciente.clicarNoBotaoAvancar();
        pageCadastroDoPaciente.fecharJanelasAbertas();
      //  pageCadastroDoPaciente.clicarNoBotaoGravar();
    }

    @Step
    public void clicar_no_botao_avancar(){
        pageBuscarPaciente.validarAlertaAniversario();
        pageCadastroDoPaciente.clicarNoBotaoAvancar();
        pageCadastroDoPaciente.fecharJanelasAbertas();
        pageCadastroDoPaciente.clicarNoBotaoGravar();
        //comentar talvez a acao de gravar
    }


    @Step
    public void efetuo_o_pagamento_da_OS(){
        pageCadastroDoPaciente.EfetuarPagamentoOS();
    }


    @Step
    public void buscarPacienteComNumeroDeMatriculaEAvancar(String matPaciente){
        buscarPacientePorMatricula(matPaciente);
        pageCadastroDoPaciente.clicarNoBotaoAvancar();
        pageCadastroDoPaciente.fecharJanelasAbertas();
    }




    @Step
    public void criar_uma_nova_ordem_de_serviço(){
        pageCadastroDoPaciente.CriarNovaOrdemServico();
    }

    @Step
    public void criar_uma_nova_ordem_de_serviço_sem_gravar(){
        pageCadastroDoPaciente.CriarNovaOrdemServicoSemGravar();
    }

    @Step
    public void preencher_dados_da_ordem_de_serviço(List<DataOrdemDeServico> dataOrdemDeServico){
        pageCadastroDoPaciente.PreencherDadosDaOS(dataOrdemDeServico);
    }


    @Step
    public void lancar_item_para_os_criada(String item){
      //  pageCadastroDoPaciente.LancarItemParaOS(item);
        pageCadastroDoPaciente.buscarServico(item);
        pageCadastroDoPaciente.incluirServico();
        pageCadastroDoPaciente.confirmarServico();
        pageCadastroDoPaciente.confirmarAlertaGlico();
        pageCadastroDoPaciente.concluirServico();
        pageCadastroDoPaciente.autorizacaoOnLineWebService();
       // pageCadastroDoPaciente.excluirRespostaQuestionario();
    }


    @Step
    public void criar_uma_nova_ordem_de_serviço_com_codigo_convenio(String convenio){
        pageCadastroDoPaciente.CriarNovaOrdemServicoConvenio(convenio);
    }

    @Step
    public void criar_uma_nova_ordem_de_serviço_com_o_plano(String plano){
        pageCadastroDoPaciente.CriarNovaOrdemServicoPlano(plano);
    }

}
