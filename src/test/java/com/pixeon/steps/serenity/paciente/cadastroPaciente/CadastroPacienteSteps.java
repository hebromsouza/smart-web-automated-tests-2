package com.pixeon.steps.serenity.paciente.cadastroPaciente;

import com.pixeon.pages.paciente.PageCadastroDoPaciente;
import net.thucydides.core.annotations.Step;

public class CadastroPacienteSteps {

    PageCadastroDoPaciente pageCadastroDoPaciente;

    @Step
    public void inserirCpfPaciente(String cpf) {
        pageCadastroDoPaciente.escreverCpf(cpf);
    }

    @Step
    public void gravarCadastroDoPaciente() {
        pageCadastroDoPaciente.clicarBotaoAvançar();
    }

    @Step
    public void validarInclusaoCpf() {
        pageCadastroDoPaciente.validarCPFValido();
    }

    @Step
    public void validarInclusaoCpfSemPontuacao() {
        pageCadastroDoPaciente.validarCpfSemPontuacao();
    }

    @Step
    public void imprimirEtiquetaPaciente() {
        pageCadastroDoPaciente.clicarBotaoProntuario();
        pageCadastroDoPaciente.clicarLinkImprimirEtiqueta();
    }

}
