package com.pixeon.steps.serenity.paciente;

import com.pixeon.datamodel.paciente.DataModelBuscaDePacienteProntuario;
import com.pixeon.pages.paciente.PageBuscaDePacienteProntuario;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class BuscaDePacienteProntuarioSteps {

    PageBuscaDePacienteProntuario pageBuscaDePacienteProntuario;

    @Step
    public void preencherbuscaDePacienteProntuario(List<DataModelBuscaDePacienteProntuario> dataBuscaDePacienteProntuario) {
        pageBuscaDePacienteProntuario.preencheCamposDeBuscaProntuarioDoPaciente(dataBuscaDePacienteProntuario);
    }

    @Step
    public void buscarPacienteProntuario() {
        pageBuscaDePacienteProntuario.clicaBtnBuscar();
    }
}
