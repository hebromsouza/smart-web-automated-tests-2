package com.pixeon.steps.serenity.paciente;

import com.pixeon.pages.paciente.PageBuscarPaciente;
import net.thucydides.core.annotations.Step;

public class CriarPacienteSteps {

    PageBuscarPaciente pageBuscarPaciente;

    @Step
    public void criarPacienteComNome(String paciente) {
        pageBuscarPaciente.escreverDadosCampoBusca("nome", paciente);
        pageBuscarPaciente.clicarBotaoBuscarSemEspera();
        pageBuscarPaciente.aceitarPopUpNovoPaciente();
    }

}
