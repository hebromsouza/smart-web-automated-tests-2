package com.pixeon.steps.serenity.financeiro;

import com.pixeon.datamodel.financeiro.DataBaixaCompromisso;
import com.pixeon.pages.financeiro.PageBaixaCompromisso;
import com.pixeon.pages.financeiro.PageCompromissoAPagar;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.util.List;

public class BaixaCompromissoSteps {

    PageBaixaCompromisso pageBaixaCompromisso;

    @Step
    public void buscarBaixaCompromissoComDados(List<DataBaixaCompromisso> dataBaixaCompromisso) {
        pageBaixaCompromisso.buscarBaixaCompromissoComDados(dataBaixaCompromisso);
        pageBaixaCompromisso.clicarBotaoBuscar();
    }

    @Step
    public void validarValorCompromissoGerado(){
        Assert.assertEquals(PageCompromissoAPagar.valorSerieNumeroCompromissoAPagar, pageBaixaCompromisso.retornarValorCompromisso());
    }

    @Step
    public void validarExistenciaCompromissoRegistradoGerado() {
        Assert.assertTrue(pageBaixaCompromisso.retornaExistenciaCompromissoRegistrado(PageCompromissoAPagar.serieNumeroCompromissoAPagar));
    }

    @Step
    public void validarExistenciaCompromissoPagoGerado() {
        Assert.assertTrue(pageBaixaCompromisso.retornaExistenciaCompromissoPago(
                PageCompromissoAPagar.serieNumeroCompromissoAPagar));
    }

    @Step
    public void pagarCompromissoGerado() {
        pageBaixaCompromisso.clicarBotaoPagarCompromissos();
    }

    @Step
    public void validarInexistenciaCompromissoRegistradoGerado() {
        Assert.assertFalse(pageBaixaCompromisso.retornaExistenciaCompromissoRegistrado(PageCompromissoAPagar.serieNumeroCompromissoAPagar));
    }

    @Step
    public void validarInexistenciaCompromissoPagoGerado() {
        Assert.assertFalse(pageBaixaCompromisso.retornaExistenciaCompromissoPago(PageCompromissoAPagar.serieNumeroCompromissoAPagar));
    }

}
