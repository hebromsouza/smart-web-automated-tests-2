package com.pixeon.steps.serenity.financeiro;

import com.pixeon.datamodel.financeiro.DataCompromissoAPagar;
import com.pixeon.pages.atendimento.ordemDeServico.PageBuscaDeProfissional;
import com.pixeon.pages.financeiro.PageCompromissoAPagar;
import com.pixeon.util.DataHelper;
import net.thucydides.core.annotations.Step;

import java.util.List;

public class CompromissoAPagarSteps {

    PageCompromissoAPagar pageCompromissoAPagar;
    PageBuscaDeProfissional pageBuscaDeProfissional;

    @Step
    public void buscarCredor(String fornecedor){
        pageCompromissoAPagar.clicarBotaoBuscarFornecedor();
        pageCompromissoAPagar.digitarNomeMedico(fornecedor);
        pageCompromissoAPagar.clicarBotaoBuscar();
        pageBuscaDeProfissional.clicarLinkNomeMedico(fornecedor);
    }

    @Step
    public void gravarCompromissoPreenchendoComDados(List<DataCompromissoAPagar> dataCompromissoAPagar) {
        DataCompromissoAPagar data = DataHelper.getData(dataCompromissoAPagar);
        buscarCredor(data.getCredor());
        pageCompromissoAPagar.validarAutoCompleteCpfCnpjCredorBuscado(dataCompromissoAPagar);
        pageCompromissoAPagar.preencherCompromissoAPagarComDados(dataCompromissoAPagar);
        pageCompromissoAPagar.pegarSerieNumeroDoCompromissoAPagar();
        pageCompromissoAPagar.clicarBotaoGravar();
    }

    @Step
    public void informarValorTotalComClasse(String valorTotal, String classe) {
        pageCompromissoAPagar.digitarValorTotal(valorTotal);
        pageCompromissoAPagar.selecionarClasse(classe);
        pageCompromissoAPagar.clicarBotaoGravarValor();
    }

    @Step
    public void validarLancamentoComValorVencimentoMesmoDia(String valor) {
        pageCompromissoAPagar.validarLancamentoComValor(valor);
        pageCompromissoAPagar.validarPagamentoMesmoDia();
    }

}
