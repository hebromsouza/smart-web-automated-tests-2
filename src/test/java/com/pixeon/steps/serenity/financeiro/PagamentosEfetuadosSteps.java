package com.pixeon.steps.serenity.financeiro;

import com.pixeon.pages.financeiro.PageCompromissoAPagar;
import com.pixeon.pages.financeiro.PagePagamentosEfetuados;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class PagamentosEfetuadosSteps {
    
    PagePagamentosEfetuados pagePagamentosEfetuados;
    
    @Step
    public void buscarPagamentosEfetuadosComEmpresa(String empresa) {
        pagePagamentosEfetuados.selecionarEmpresa(empresa);
        pagePagamentosEfetuados.clicarBotaoBuscar();
    }

    @Step
    public void validarPagamentosEfetuadosCompromissoGerado() {
        pagePagamentosEfetuados.validarPagamentosEfetuadosCompromissoGerado();
      //  pagePagamentosEfetuados.validarPagamentosEfetuadosCompromissoGerado(PageCompromissoAPagar.serieNumeroCompromissoAPagar);
    }

    @Step
    public void imprimirReciboCompromissoGerado() {
        //pagePagamentosEfetuados.clicarCompromissoGeradoPagamentosEfetuados(PageCompromissoAPagar.serieNumeroCompromissoAPagar);
        pagePagamentosEfetuados.clicarCompromissoGeradoPagamentosEfetuados();
        pagePagamentosEfetuados.clicarImprimirRecibo();
    }

    @Step
    public void validarReciboDePagamentosEfetuadosCompromissoGerado() {
        pagePagamentosEfetuados.closePanel();
    }

    @Step
    public void cancelarCompromisso() {
        pagePagamentosEfetuados.clicarBotaoCancelarDaBaixaDeCompromissoGerado(PageCompromissoAPagar.serieNumeroCompromissoAPagar);
    }

    @Step
    public void validarInexistenciaCompromissoGerado() {
        //Assert.assertFalse(pagePagamentosEfetuados.visibilidadeBaixaCompromissoGerado(PageCompromissoAPagar.serieNumeroCompromissoAPagar));
        Assert.assertFalse(pagePagamentosEfetuados.visibilidadeBaixaCompromissoGerado(PageCompromissoAPagar.serieNumeroCompromissoAPagar));
    }

}