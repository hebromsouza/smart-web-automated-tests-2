package com.pixeon.steps.serenity.ajudaOnline;

import com.pixeon.pages.ajudaOnline.PageAjudaOnline;
import com.pixeon.pages.relatorios.PageRelatorios;
import net.thucydides.core.annotations.Step;

public class AjudaOnlineSteps {

    PageRelatorios pageRelatorios;

    PageAjudaOnline pageAjudaOnline;

    @Step
    public void acessarPaginaAjudaOnline(){
        pageRelatorios.clicarIconeAjudaOnline();
    }

    @Step
    public void validarPaginaAjudaOnline(){
        pageAjudaOnline.validarPaginaAjudaOnline();
    }

}
