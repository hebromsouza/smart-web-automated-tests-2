package com.pixeon.steps.serenity.menu;

import com.pixeon.pages.menu.PageMenu;
import net.thucydides.core.annotations.Step;

import java.util.ArrayList;
import java.util.List;

public class MenuSteps {

    PageMenu pageMenu;


    @Step
    public void selecionarMenu(String menu, String subMenu){
        List<String> menuList = new ArrayList<>();
        menuList.add(menu);
        menuList.add(subMenu);

        pageMenu.clicarMenu(menuList);
    }

    @Step
    public void selecionarMenuOnly(String menu){
        List<String> menuList = new ArrayList<>();
        menuList.add(menu);

        pageMenu.clicarMenuOnly(menuList);
    }

    @Step
    public void seleciono_o_setorNaoEspecificado(){
        pageMenu.setorNaoEspecificado();
    }

    @Step
    public void que_escolho_o_setor_desejado(String setor){
        pageMenu.EscolherSetorDesejado(setor);
    }


    @Step
    public void filtrar_pelo_periodo_informado(String dtInicial){
        pageMenu.FiltrarPeloPeriodo(dtInicial);
    }

}
