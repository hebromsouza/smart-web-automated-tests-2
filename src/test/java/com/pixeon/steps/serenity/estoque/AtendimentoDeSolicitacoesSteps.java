package com.pixeon.steps.serenity.estoque;

import com.pixeon.pages.common.PageActionCommon;
import com.pixeon.pages.estoque.PageAtendimentoDeSolicitacoes;
import net.thucydides.core.annotations.Step;

import static com.pixeon.pages.estoque.PageSolicitacaoDeTransferenciaDeMateriais.serieNumeroSolicitacao;

public class AtendimentoDeSolicitacoesSteps {

    PageAtendimentoDeSolicitacoes pageAtendimentoDeSolicitacoes;
    PageActionCommon pageActionCommon;

    @Step
    public void selecionarSubAlmoxarifadoTelaAtendimentoSolicitacao(String subalmoxarifado) {
        pageAtendimentoDeSolicitacoes.selecionarCentroEstocador(subalmoxarifado);
    }

    @Step
    public void selecionarTipoSolicitacaoMateriaisTelaAtendimentoSolicitacoes(String tipoSolicitacao) {
        pageAtendimentoDeSolicitacoes.selecionarSolicitacao(tipoSolicitacao);
    }

    @Step
    public void filtrarAtendimentoSolicitacoes() {
        pageAtendimentoDeSolicitacoes.clicarBotaoFiltrar();
    }

    @Step
    public void abrirAtendimentoSolicitacaoPelaSerieNumeroGeradosTelaSolicitacaoTransferencia() {
        pageAtendimentoDeSolicitacoes.clicarLinkNumeroSolicitacao(serieNumeroSolicitacao);
    }

    @Step
    public void incluirQuantidadeParaBaixa(String quantidade) {
        pageAtendimentoDeSolicitacoes.digitarQuantidadeBaixa(quantidade);
    }

    @Step
    public void imprimirAtendimentoSolicitacao() {
        pageAtendimentoDeSolicitacoes.clicarBotaoImprimir();
    }

    @Step
    public void validarImpressaoAtendimentoSolicitacoes() {
        pageAtendimentoDeSolicitacoes.validarImpressaoAtendimentoSolicitacoes();
        pageAtendimentoDeSolicitacoes.clicarFecharPainel2();
    }

    @Step
    public void gravarAtendimentoSolicitacoes() {
        pageAtendimentoDeSolicitacoes.clicarBotaoGravar();
    }

    @Step
    public void validarAtendimentoSolicitacao() {
        pageAtendimentoDeSolicitacoes.aceitarPopUpAtendimentoParcial();
        pageAtendimentoDeSolicitacoes.aceitarPopUpAtendimento();
    }

    @Step
    public void validarAusenciaSolicitacaoTelaAtendimentoSolicitacoesPendentes() {
        pageAtendimentoDeSolicitacoes.validarAusenciaSolicitacaoAtendimentoPendente();
    }

    @Step
    public void validarPresencaSolicitacao() {
        pageAtendimentoDeSolicitacoes.clicarCheckPendentes();
        pageAtendimentoDeSolicitacoes.clicarBotaoFiltrar();
        pageAtendimentoDeSolicitacoes.validarPresencaSolicitacaoAtendimentoSemPendencia();
    }
}
