package com.pixeon.steps.serenity.estoque;

import com.pixeon.pages.estoque.PageBuscaDeSolicitações;
import com.pixeon.pages.estoque.PageSolicitacaoDeTransferenciaDeMateriais;
import net.thucydides.core.annotations.Step;

public class SolicitacaoDeTransferenciaDeMateriaisSteps {

    PageSolicitacaoDeTransferenciaDeMateriais pageSolicitacaoDeTransferenciaDeMateriais;
    PageBuscaDeSolicitações pageBuscaDeSolicitações;

    @Step
    public void selecionarSetorSolicitacaoMaterial(String setorSolicitante){
        pageSolicitacaoDeTransferenciaDeMateriais.selecionarSetorSolicitante(setorSolicitante);
    }

    @Step
    public void selecionarSubAlmoxarifadoSolicitacaoMaterial(String subalmoxarifado){
        pageSolicitacaoDeTransferenciaDeMateriais.selecionarSubAlmoxarifado(subalmoxarifado);
    }

    @Step
    public void selecionarSubAlmoxarifadoParaTransferencia(String subalmoxarifadoTransf){
        pageSolicitacaoDeTransferenciaDeMateriais.selecionarSubAlmoxarifadoParaTransferencia(subalmoxarifadoTransf);
    }

    @Step
    public void informarObservacaoSolicitacaoTransferencia(String observacao){
        pageSolicitacaoDeTransferenciaDeMateriais.digitarObservacao(observacao);
    }

    @Step
    public void gravarSolicitacaoTransferenciaMaterialPegandoSerieNumero(){
        pageSolicitacaoDeTransferenciaDeMateriais.clicarBotaoGravar();
        pageSolicitacaoDeTransferenciaDeMateriais.capturarSerieNumeroSolicitacao();
    }

    @Step
    public void buscarItemTelaSolicitacaoTransferenciaMaterial(String item){
        pageSolicitacaoDeTransferenciaDeMateriais.digitarItem(item);
        pageSolicitacaoDeTransferenciaDeMateriais.clicarBotaoBuscar();
    }

    @Step
    public void adicionarItemSelecionandoQuantidade(String quantidade){
        pageSolicitacaoDeTransferenciaDeMateriais.digitarQuantidadeMaterial(quantidade);
        pageSolicitacaoDeTransferenciaDeMateriais.clicarBotaoAdicionar();
    }

    @Step
    public void buscarSolicitacoesDeTransferenciaDeMateriais(){
        pageSolicitacaoDeTransferenciaDeMateriais.clicarIconeBuscarSolicitacoes();
        pageBuscaDeSolicitações.clicarBotaoBuscar();
    }

    @Step
    public void validarSolicitacaoDeTransferenciaDeMateriais(){
        pageBuscaDeSolicitações.validarBuscaDeNumeroDeSolicitacaoDeTransferenciaDeMateriais();
    }

    @Step
    public void validarNaTelaDeSolicitacaoDeTransferenciaDeMateriaisNumeroDaSolicitacao(){
        pageSolicitacaoDeTransferenciaDeMateriais.validarNumeroDaSolicitacaoDeTransferenciaDeMateriais();
    }

    @Step
    public void selecionarItem() {
        pageSolicitacaoDeTransferenciaDeMateriais.clicarCheckItem();
    }

    @Step
    public void cancelarItem() {
        pageSolicitacaoDeTransferenciaDeMateriais.selecionarAcaoCancelar();
        pageSolicitacaoDeTransferenciaDeMateriais.aceitarPopUpCancelamento();
    }

    public void validarAusenciaNumeroSolicitacoesRecentes() {
        pageSolicitacaoDeTransferenciaDeMateriais.validarItemNotVisible();
    }

    @Step
    public void validarCancelamentoDoItem() {
        pageSolicitacaoDeTransferenciaDeMateriais.clicarChekListarCanceladas();
        pageSolicitacaoDeTransferenciaDeMateriais.validarItemVisivel();
        pageSolicitacaoDeTransferenciaDeMateriais.validarQuantidadeItemZerado();
    }

    @Step
    public void cancelarSolicitacao() {
        pageSolicitacaoDeTransferenciaDeMateriais.clicarIconeCancelarSolicitacao();
        pageSolicitacaoDeTransferenciaDeMateriais.aceitarPopUpCancelamentoSolicitacao();
    }

    @Step
    public void validarStatusSolicitacao(String status) {
        pageSolicitacaoDeTransferenciaDeMateriais.validarStatusSolicitacao(status);
    }
}
