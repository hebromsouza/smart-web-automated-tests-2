package com.pixeon.steps.serenity.home;

import com.pixeon.pages.home.PageHome;
import net.thucydides.core.annotations.Step;

public class HomeSteps {

    PageHome pageHome;

    @Step
    public void validaVersaoDoSoftware(String nomeSoftware_versaoSoftware, String buildSoftware, String nomeFornecedor) {
        pageHome.validaVersaoSoftwareTelaHome(nomeSoftware_versaoSoftware, buildSoftware, nomeFornecedor);
    }

    @Step
    public void realizaLOginSucesso() {
        pageHome.validaPageHome();
        pageHome.carregarTelaInicial();
    }

    @Step
    public void realizaLoginSucessoCarregandoTelaInicial(){
        pageHome.carregarTelaInicial();
        pageHome.validaPageHome();
    }

    @Step
    public void deslogar() {
        pageHome.clicarDeslogar();
    }

    @Step
    public void o_menu_nao_devera_esta_presente_na_tela_Home(String menuNaoPresetne) {
        pageHome.validarQueMenuNaoEstaPresente(menuNaoPresetne);
    }

    @Step
    public void os_sistema_apresenta_a_tela_Home_da_aplicacao() {
        pageHome.validarTelaHomePresente();
    }

}
