package com.pixeon.steps.serenity.laboratorio;

import com.pixeon.pages.laboratorio.PageOrdemDeServico;
import net.thucydides.core.annotations.Step;

public class OrdemDeServicoSteps {

    PageOrdemDeServico pageOrdemDeServico;

    @Step
    public void clicarNaOrdemDeServico(){
        pageOrdemDeServico.clicarNaOrdemDeServico();
    }

    @Step
    public void clicarLinkOrdemDeServicoNaLista(String lista){
        pageOrdemDeServico.clicarLinkOrdemDeServico(lista);
    }

    @Step
    public void clicarOrdemDeServicoGeradaNoOrcamento() {
        pageOrdemDeServico.clicarLinkOrdemDeServicoGeradaNoOrcamento();
    }

    @Step
    public void verificarseResultadoEstaDisponivel() {
        pageOrdemDeServico.VerificarResultadoAberto();
    }

    @Step
    public void clicarNaOrdemDeServicoGerada() {
        pageOrdemDeServico.clicarLinkOrdemDeServicoGerada();
    }

}
