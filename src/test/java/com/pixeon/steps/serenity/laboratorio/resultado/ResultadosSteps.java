package com.pixeon.steps.serenity.laboratorio.resultado;

import com.pixeon.datamodel.laboratorio.DataResultadoEritrograma;
import com.pixeon.datamodel.laboratorio.DataResultadoLeucograma;
import com.pixeon.pages.laboratorio.resultado.PagePainelResultadoExame;
import com.pixeon.pages.laboratorio.resultado.PageResultados;
import net.thucydides.core.annotations.Step;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class ResultadosSteps {

    PageResultados pageResultados;
    PagePainelResultadoExame pagePainelResultadoExame;

    @Step
    public void gravarResultado(String resultado){
        pageResultados.gravarResultado(resultado);
    }

    @Step
    public void gravarResultadoComStatus(String resultado, String status){

        pageResultados.gravarResultado(resultado);

        if (status.equals("Exec (Av) ")) {
            pageResultados.avaliarResultadoExecutado(resultado,"Teste Avaliar " + resultado );
        } else if (status.equals("Liberado ")){
            pageResultados.avaliarResultadoExecutado(resultado,"Teste Avaliar " + resultado );
            pageResultados.liberarResultado();
        } else if (status.equals("Impresso ")){
            pageResultados.clicarBotaoGravar();
            pageResultados.avaliarResultadoExecutado(resultado,"Teste Avaliar " + resultado );
            pageResultados.liberarResultado();
            pageResultados.clicarBotaoVisualizarImprimirLaudo();
            pageResultados.clicarCheckBoxExame();
            pageResultados.clicarBotaoVisualizarImprimirLaudo();
            pageResultados.closePanel();
            pageResultados.clicarLinkResultados();
        }

        validarStatus(status);
    }

    @Step
    public void inserirResultadoDoExame(String resultado, String resultado2){
        if(!resultado.isEmpty()){pageResultados.escreverResultado(resultado);}
        if(!resultado2.isEmpty()){pageResultados.digitarResultado(resultado2);}
    }

    @Step
    public void gravarResultadoDoExame(){
        pageResultados.clicarBotaoGravar();
    }

    @Step
    public void gravarResultadoExameComValorDeReferencia(String valorReferencia){
        pageResultados.escreverResultado(valorReferencia);
        pageResultados.clicarBotaoGravar();
    }

    @Step
    public void confirmarResultados(){
        pageResultados.clicarBotaoConfirmarResultaods();
    }

    @Step
    public void liberarExame(){
        pageResultados.clicarExame();
        pageResultados.clicarLinkLiberar();
    }

    @Step
    public void validarMensagemPopUp(String mensagemPopUp){
        pageResultados.validarMensagemPopUp(mensagemPopUp);
    }

    @Step
    public void validarStatus(String status){
        if (status.equals("Executado ")){
            validarStatusExecutado(status);
        }else if(status.equals("Exec (Av) ")){
            validarStatusExecutadoAvaliado(status);
        }else if(status.equals ("Liberado ")){
            validarStatusLiberado(status);
        }
    }

    @Step
    public void validarValorGravadoDeResultado(String resultado){
        assertTrue(pageResultados.validarValorDeReferencia(resultado));
    }

    @Step
    public void validarStatusExecutado(String status){
      //  assertEquals(status, pageResultados.obterTextoStatus());
        pageResultados.obterTextoStatus();
    }

    @Step
    public void clico_para_visualizar_lauro(){
        pageResultados.VisualizarLaudo();
    }

    @Step
    public void validarStatusExecutadoAvaliado(String status){
       // assertEquals(status, pageResultados.obterTextoStatus());
        pageResultados.obterTextoStatus();
    }

    @Step
    public void validarStatusLiberado(String status){
       // assertEquals(status, pageResultados.obterTextoStatus());
        pageResultados.obterTextoStatus();
    }

    @Step
    public void abrirPainelResultadosExame() {
        pageResultados.clicarLupaResultadosExame();
    }

    @Step
    public void validarErroPainel() {
        pageResultados.ValidarValorReferencia();
    }

    @Step
    public void preencherResultadoEritrograma(List<DataResultadoEritrograma> dataResultadoEritrograma) {
        pagePainelResultadoExame.preencherCamposResultadoEritrograma(dataResultadoEritrograma);
    }

    @Step
    public void preencherResultadoLeucograma(List<DataResultadoLeucograma> dataResultadoLeucograma) {
        pagePainelResultadoExame.preencherCamposResultadoLeucograma(dataResultadoLeucograma);
    }

    @Step
    public void gravarResultadoHemograma() {
        pagePainelResultadoExame.clicarBotaoGravar();
    }

    @Step
    public void validarResultadoNaoEditavel(String resultado) {
        pageResultados.validarResultadoNaoEditavel(resultado);
    }

}
