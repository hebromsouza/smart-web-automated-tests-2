package com.pixeon.steps.serenity.laboratorio.folhasDeTrabalho;

import com.pixeon.pages.laboratorio.folhasDeTrabalho.PageBuscarFolhasDeTrabalho;
import com.pixeon.pages.laboratorio.folhasDeTrabalho.PageGerarFolhasDeTrabalho;
import com.pixeon.pages.laboratorio.folhasDeTrabalho.PageImpressaoDaFolha;
import net.thucydides.core.annotations.Step;

public class GerarFolhasDeTrabalhoSteps {

    PageBuscarFolhasDeTrabalho pageBuscarFolhasDeTrabalho;
    PageGerarFolhasDeTrabalho pageGerarFolhasDeTrabalho;
    PageImpressaoDaFolha pageImpressaoDaFolha;

    @Step
    public void selecionarOpcaoGerarFolhas(){
        pageBuscarFolhasDeTrabalho.clicarGerarFolhas();
    }

    @Step
    public void selecionarSetor(String setor){
        pageGerarFolhasDeTrabalho.selecionarSetor(setor);
    }

    @Step
    public void selecionarBancada(String bancada){
        pageGerarFolhasDeTrabalho.selecionarbancada(bancada);
    }

    @Step
    public void buscarFolhas(){
        pageGerarFolhasDeTrabalho.clicarBuscar();
    }

    @Step
    public void selecionarFolhas(){
        pageGerarFolhasDeTrabalho.clicarFolhasDeTrabalho();
        pageGerarFolhasDeTrabalho.clicarSelecionarFolhas();
    }

    @Step
    public void gerarFolhas(){
        pageGerarFolhasDeTrabalho.clicarGerarFolhas();
    }

    @Step
    public void concluirGeracaoFolhas(){
        pageImpressaoDaFolha.clicarConcluir();
    }

    @Step
    public void validarGeracaoDeFolhas(String bancada){
        pageGerarFolhasDeTrabalho.validarGeracaoDaFolhaDeTrabalho(bancada);
    }

}
