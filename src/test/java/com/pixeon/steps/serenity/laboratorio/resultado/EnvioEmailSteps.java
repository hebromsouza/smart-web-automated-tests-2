package com.pixeon.steps.serenity.laboratorio.resultado;

import com.pixeon.pages.atendimento.ordemDeServico.PageLancamentoDeOrdemDeServico;
import com.pixeon.pages.laboratorio.resultado.PageEnvioEmail;
import net.thucydides.core.annotations.Step;

public class EnvioEmailSteps {

    PageEnvioEmail pageEnvioEmail;

    @Step
    public void abrirEmailOutlook365(){
        pageEnvioEmail.open();
    }

    @Step
    public void realizarLoginEmail(String email, String senha) {
        pageEnvioEmail.realizarLoginEmail(email, senha);
    }

    @Step
    public void validarEnvioEmail(String nomePaciente) {
        pageEnvioEmail.clicarEmailRecebidoComOSGerada(PageLancamentoDeOrdemDeServico.numeroOS);
        pageEnvioEmail.validarEnvioEmailPaciente(nomePaciente);
    }

}
