package com.pixeon.steps.serenity.laboratorio.resultado;

import com.pixeon.pages.laboratorio.PageImpressaoDeLaudos;
import com.pixeon.pages.laboratorio.resultado.PageResultados;
import net.thucydides.core.annotations.Step;

public class EnviarLaudoPorEmailSteps {

    PageResultados pageResultados;
    PageImpressaoDeLaudos pageImpressaoDeLaudos;

    @Step
    public void enviarLaudoPorEmail() {
        pageResultados.clicarBotaoVisualizarImprimirLaudo();
        pageImpressaoDeLaudos.clicarRadioDestino();
        pageImpressaoDeLaudos.clicarBotaoEnviarPorEmail();
        pageImpressaoDeLaudos.validarMensagemAlertaPopUp("Não foi possível enviar o laudo via e-mail para o paciente TESTE ADRIANO.\nE-mail não cadastrado.");
        pageImpressaoDeLaudos.waitHelper.alertOk();
    }

    @Step
    public void enviarLaudoParaOutroEmail(String email) {
        pageImpressaoDeLaudos.clicarBotaoEnviarParaOutroEmail();
        pageImpressaoDeLaudos.escreverOutroEmail(email);
        pageImpressaoDeLaudos.clicarBotaoEnviar();
    }

}