package com.pixeon.steps.serenity.laboratorio.folhasDeTrabalho;

import com.pixeon.pages.laboratorio.folhasDeTrabalho.PageBuscarFolhasDeTrabalho;
import net.thucydides.core.annotations.Step;

import org.junit.Assert;

public class BuscarFolhasDeTrabalhoSteps {

    PageBuscarFolhasDeTrabalho pageBuscarFolhasDeTrabalho;

    private String numeroPrimeiraFolhaEmitida;
    private String numeroPrimeiraFolhaEmitidaCancelado;

    @Step
    public void buscarFolhasEmitidas(String bancada){
        pageBuscarFolhasDeTrabalho.selecionarBancada(bancada);
        pageBuscarFolhasDeTrabalho.clicarBuscarFolhasEmitidas();
    }

    @Step
    public void numeroDaPrimeiraFolhaEmitida(){
        numeroPrimeiraFolhaEmitida = pageBuscarFolhasDeTrabalho.copiarNumeroDaFolhaDeTrabalho();
    }

    @Step
    public void checarFolhaEmitida(){
        pageBuscarFolhasDeTrabalho.checarPrimeiraFolhaEmitidaDaBusca();
    }

    @Step
    public void cancelarFolhaSelecionada(){
        pageBuscarFolhasDeTrabalho.cancelarFolhasSelecionadas();
    }

    @Step
    public void validarCancelamentoDaFolhaSelecionada(){
        if(pageBuscarFolhasDeTrabalho.validarCancelamentoFolhaSelecionada()){
            numeroPrimeiraFolhaEmitidaCancelado = pageBuscarFolhasDeTrabalho.copiarNumeroDaFolhaDeTrabalho();

            Assert.assertNotEquals(numeroPrimeiraFolhaEmitidaCancelado, numeroPrimeiraFolhaEmitida);
        }else{
            Assert.assertEquals("", "");
        }

    }

}
