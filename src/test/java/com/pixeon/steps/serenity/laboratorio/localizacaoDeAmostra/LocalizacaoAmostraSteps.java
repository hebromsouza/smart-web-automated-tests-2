package com.pixeon.steps.serenity.laboratorio.localizacaoDeAmostra;

import com.pixeon.pages.atendimento.ordemDeServico.PageInserirItensOrdemDeServico;
import com.pixeon.pages.laboratorio.localizacaoDeAmostra.PageLocalizacaoAmostra;
import com.pixeon.util.SeleniumUtil;
import net.thucydides.core.annotations.Step;

public class LocalizacaoAmostraSteps {

    PageLocalizacaoAmostra pageLocalizacaoAmostra;

    @Step
    public void localizarAmostraDoItemLancado() {
        pageLocalizacaoAmostra.escreverNumeroAmostra(PageInserirItensOrdemDeServico.numeroAmostraItem);
        pageLocalizacaoAmostra.clicarBotaoLocalizar();
        if (SeleniumUtil.isAlertPresent()) pageLocalizacaoAmostra.aceitarPopUpLocalizarAmostraNovamente();
        pageLocalizacaoAmostra.divAguarde();
        validarLocalizacaoAmostra();
    }

    @Step
    public void selecionarOpcaoInformarVolumeUrinarioLinfocito() {
        pageLocalizacaoAmostra.clicarCheckInformarVolumeUrinarioLinfocito();
    }

    @Step
    public void informarTempoVolume(String tempo, String volume) {
        pageLocalizacaoAmostra.escreverQuantidadeHoras(tempo);
        pageLocalizacaoAmostra.escreverQuantidadeVolume(volume);
        pageLocalizacaoAmostra.clicarBotaoGravar();
    }

    @Step
    public void validarAdicaoVolumeUrinarioTempoVolume(String tempo, String volume) {
        pageLocalizacaoAmostra.abrirPainelDeAmostra(PageInserirItensOrdemDeServico.numeroAmostraItem);
        pageLocalizacaoAmostra.validarTempoVolumeUrinario(tempo);
        pageLocalizacaoAmostra.validarVolumeDoVolumeUrinario(volume);
    }

    @Step
    public void informarLinfocitos(String linfocitos) {
        pageLocalizacaoAmostra.escreverQuantidadeLinfocitos(linfocitos);
        pageLocalizacaoAmostra.clicarBotaoGravar();
    }

    @Step
    public void validarAdicaoLinfocitos(String linfocitos) {
        pageLocalizacaoAmostra.abrirPainelDeAmostra(PageInserirItensOrdemDeServico.numeroAmostraItem);
        pageLocalizacaoAmostra.validarLinfocitos(linfocitos);
    }

    @Step
    public void validarLocalizacaoAmostra() {
        pageLocalizacaoAmostra.validarLocalizacaoAmostra(PageInserirItensOrdemDeServico.numeroAmostraItem);
    }

}
