package com.pixeon.pages.paciente;

import com.pixeon.pages.BasePage;
import com.pixeon.util.SeleniumUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class PageBuscarPaciente extends BasePage {

    String serieOsm;
    String numOsm;
    public static String numeroOrp;
    String getCodigoAmostra;
    WebElementFacade nomeCampo;

    @FindBy(name = "pac_nome_0")
    private WebElementFacade nomePaciente;

    @FindBy(name = "pac_pac_nome_0")
    private WebElementFacade lnkNomePaciente;

    @FindBy(name = "pac_reg_0")
    private WebElementFacade registroPaciente;

    @FindBy(name = "pac_pront_0")
    private WebElementFacade prontuarioPaciente;

    @FindBy(name = "pac_numcpf_0")
    private WebElementFacade cpfPaciente;

    @FindBy(name = "pac_numrg_0")
    private WebElementFacade rgPaciente;

    @FindBy(name = "pac_nasc_0")
    private WebElementFacade nascimentoPaciente;

    @FindBy(name = "pac_mcnv_0")
    private WebElementFacade matriculaPaciente;

    @FindBy(name = "pac_fone_0")
    private WebElementFacade telefonePaciente;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnBuscar;

    @FindBy(xpath = "//*[@id=\'dw_pacLista_detail_2\']/span[8]/a/div")
    private WebElementFacade pacienteLuana;


    @FindBy(name = "guia_num_0")
    private WebElementFacade numeroGuia;

    @FindBy(name = "osm_osm_ctle_cnv_0")
    private WebElementFacade numeroGuiaOrdemServico;

    @FindBy(name = "pac_cartao_sus_0")
    private WebElementFacade numeroCns;

    @FindBy(name = "pac_pac_cartao_sus_0")
    private WebElementFacade numeroCnsPaciente;

    @FindBy(name = "smm_cod_amostra_0")
    private WebElementFacade numeroAmostra;

    @FindBy(name = "smm_smm_cod_amostra_0")
    private WebElementFacade numeroAmostraPaciente;

    @FindBy(id = "msgBox")
    private WebElementFacade divAguardeBuscando;

    @FindBy(id = "bt_next")
    private WebElementFacade btnAvancar;

    @FindBy(id = "a_edit-itens")
    private WebElementFacade btnEditarItens;

    @FindBy(name = "smk_rot_0")
    private WebElementFacade detalheExame;

    @FindBy(name = "numero_da_os_1")
    private WebElementFacade escolherOs;

    @FindBy(name = "osm_serie_0")
    private WebElementFacade osmSerie;

    @FindBy(name = "osm_num_0")
    private WebElementFacade osmNum;

    @FindBy(xpath = "//*[@id=\"dw_dados_os_pac_header\"]/span[10]/a")
    private WebElementFacade lnkLancamentoDeOs;

    @FindBy(linkText = "Lançamento de OS")
    private WebElementFacade lnkLancamentoOS;

    @FindBy(xpath = "//*[@id=\"dw_orp_detail_0\"]/span[6]")
    private WebElementFacade numeroOrcamento;

    @FindBy(id = "dw_orp_lista_detail_0")
    private WebElementFacade listaOrcamentosAnteriores;

    @FindBy(name = "orp_num_0")
    private WebElementFacade numeroOrcamentoPreAtendimento;

    @FindBy(xpath = "//*[@id=\"bd-menu\"]/div[2]/ul/li/a")
    private WebElementFacade orcamentosAnteriores;

    @FindBy(className = "container-close")
    private WebElementFacade btnClose;

    @FindBy(name = "t_7_0")
    private WebElementFacade descricaoExame;

    @FindBy(name = "smm_smm_cod_amostra_0")
    private WebElementFacade codigoAmostra;

    @FindBy(linkText = "ÍVARR BEINLAUSI")
    private WebElementFacade lnkBuscaPaciente;

    @FindBy(xpath = "//*[@id=\'bd-main']/h2[contains(text(), \'Busca de Pacientes\')]")
    private WebElementFacade lnkNomeBuscaPaciente;

    @FindBy(name = "compute_3_0")
    private WebElementFacade lnkNomesPacientes;

    public void digitarNomePaciente(String nome){
        if(nomePaciente.isPresent() && nomePaciente.isVisible()) {
            nomePaciente.waitUntilClickable().click();
            nomePaciente.type(nome);
        }
    }

    public void clicarLinkNomePaciente(String nome){
        if(lnkNomePaciente.isPresent() && lnkNomeBuscaPaciente.isPresent()){
            getDriver().findElement(By.linkText(nome)).click();
        }
    }

    public void clicarBotaoAvancar(){
        if(btnAvancar.isPresent() && btnAvancar.isVisible()){
            divAguarde();
            btnAvancar.click();
        }
    }


    public WebElementFacade pegarFormaBusca(String formaDeBusca){
        switch (formaDeBusca){
            case "nome":
                nomeCampo = nomePaciente;
                break;
            case "registro":
                nomeCampo = registroPaciente;
                break;
            case "prontuario":
                nomeCampo = prontuarioPaciente;
                break;
            case "cpf":
                nomeCampo = cpfPaciente;
                break;
            case "rg":
                nomeCampo = rgPaciente;
                break;
            case "nascimento":
                nomeCampo = nascimentoPaciente;
                break;
            case "matricula":
                nomeCampo = matriculaPaciente;
                break;
            case "telefone":
                nomeCampo = telefonePaciente;
                break;
            case "guia":
                nomeCampo = numeroGuia;
                break;
            case "cns":
                nomeCampo = numeroCns;
                break;
            case "amostra":
                nomeCampo = numeroAmostra;
                break;
            case "orcamentoPreAtendimento":
                nomeCampo = numeroOrcamentoPreAtendimento;
                break;
        }
        return nomeCampo;
    }

    public void selecionarPacienteDaLista(){
        if (lnkNomesPacientes.isVisible()){
            lnkNomesPacientes.click();
            divAguarde();
            divAguardeBuscando.waitUntilNotVisible();
        }

    }

    public void validarAlertaAniversario(){
        if (SeleniumUtil.isAlertPresent()) if (waitHelper.waitAlertGetText().contains(" o aniversário do paciente")) waitHelper.alertOk();
        if (SeleniumUtil.isAlertPresent()) if (waitHelper.waitAlertGetText().contains("O aniversário do paciente foi há")) waitHelper.alertOk();
        if (SeleniumUtil.isAlertPresent()) if (waitHelper.waitAlertGetText().contains("ONTEM foi o aniversário do paciente")) waitHelper.alertOk();
    }

    public void escreverDadosCampoBusca(String tipoBusca, String busca){
        WebElementFacade nomeCampoBusca = pegarFormaBusca(tipoBusca);
        nomeCampoBusca.waitUntilEnabled().click();

        nomeCampoBusca.sendKeys(busca);
    }

    public void buscarComDadosBusca(String formaDeBusca, String busca){
        WebElementFacade nomeCampoBusca = pegarFormaBusca(formaDeBusca);
        nomeCampoBusca.waitUntilEnabled().click();

        if(formaDeBusca.equals("amostra")) {
            nomeCampoBusca.sendKeys(getCodigoAmostra);
        }else{
            nomeCampoBusca.sendKeys(busca);
        }

        btnBuscar.waitUntilClickable().click();
        divAguardeBuscando.waitUntilNotVisible();
        divAguarde();

        selecionarPacienteDaLista();
        validarAlertaAniversario();
    }

    public void buscarPacienteComDadosDeBusca(String formaDeBusca, String busca){
        buscarComDadosBusca(formaDeBusca, busca);
        divAguarde();

        if( lnkBuscaPaciente.isVisible() ){
            lnkBuscaPaciente.click();
            divAguarde();
            validarAlertaAniversario();
        }

    }

    public void pegarValorCamposOrdemServico(String formaDeBusca){

        if(lnkLancamentoDeOs.isPresent()) lnkLancamentoDeOs.waitUntilClickable().click();
        if(lnkLancamentoOS.isPresent()) lnkLancamentoOS.waitUntilClickable().click();

        if(formaDeBusca.equals("ordemServico")) {
            osmSerie.waitUntilPresent();
            serieOsm = osmSerie.getValue();
            numOsm = osmNum.getValue();
        }else if(formaDeBusca.equals("amostra")){
            descricaoExame.waitUntilClickable().click();
            getCodigoAmostra = codigoAmostra.getValue();
            btnClose.waitUntilClickable().click();
        }
    }

    public void buscarPacienteComOrdemServico(){
        osmSerie.waitUntilClickable().click();
        osmSerie.type(serieOsm);
        osmNum.waitUntilClickable().click();
        osmNum.type(numOsm);
        btnBuscar.waitUntilClickable().click();
        divAguardeBuscando.waitUntilNotVisible();
        divAguarde();

        validarAlertaAniversario();
    }

    public void pegarNumeroOrcamentoPreAtendimento(){
        orcamentosAnteriores.waitUntilClickable().click();
        listaOrcamentosAnteriores.waitUntilPresent();
        numeroOrp = numeroOrcamentoPreAtendimento.getText();
        btnClose.waitUntilClickable().click();
    }

    public void buscarPacienteComNumeroOrcamentoPreAtendimento(){
        buscarPacienteComDadosDeBusca("orcamentoPreAtendimento", numeroOrp);
    }

    public void digitarCodigoAmostra(String codAmostra){
        numeroAmostra.waitUntilClickable().click();
        numeroAmostra.type(codAmostra);
    }

    public void clicarBotaoBuscar(){
        if(btnBuscar.isPresent()) {
            btnBuscar.click();
            divAguarde();
        }
    }

    public void selecionarPacienteLuana(){
        pacienteLuana.click();

    }


    public void clicarBotaoBuscarSemEspera(){
        btnBuscar.click();
    }

    public String validarBuscaPaciente(String formaDeBusca){
        WebElementFacade nomeCampoBusca = null;
                
        if(formaDeBusca.equals("guia")){
            validarAlertaAniversario();
            if(btnAvancar.isPresent()){
                btnAvancar.waitUntilClickable().click();
            }
            divAguarde();
            nomeCampoBusca = numeroGuiaOrdemServico;
        }else if(formaDeBusca.equals("cns")) {
            nomeCampoBusca = numeroCnsPaciente;
        }else{
            nomeCampoBusca = pegarFormaBusca(formaDeBusca);
        }

        return nomeCampoBusca.getValue();
    }

    public boolean validarBuscaPacienteComAmostra(){
        if(btnAvancar.isPresent()){
            btnAvancar.waitUntilClickable().click();
        }
        divAguarde();
        escolherOs.waitUntilClickable().click();
        btnEditarItens.waitUntilClickable().click();
        detalheExame.waitUntilClickable().click();
        if(numeroAmostraPaciente.getValue().equals(getCodigoAmostra)){
            return true;
        }else{
            return false;
        }
    }

    public boolean validarBuscaPacienteComOrdemServico(){
        if(osmSerie.getValue().equals(serieOsm) && osmNum.getValue().equals(numOsm)){
            return true;
        }else{
            return false;
        }
    }

    public boolean validarBuscaPacienteComOrcamentoPreAtendimento(String matricula){
        if(matriculaPaciente.waitUntilPresent().getValue().equals(matricula)) {
            return true;
        }else{
            return false;
        }
    }

    public void aceitarPopUpNovoPaciente() {
        if (waitHelper.waitAlertGetText().equals("Paciente não encontrado!\nDeseja incluí-lo?")){
            waitHelper.alertOk();
            divAguarde();
        }
    }

}