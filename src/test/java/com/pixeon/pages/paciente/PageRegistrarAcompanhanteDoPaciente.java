package com.pixeon.pages.paciente;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageRegistrarAcompanhanteDoPaciente extends BasePage {

    @FindBy(id = "a_acompanhante")
    private WebElementFacade IconAcompanhante;

    @FindBy(name = "t_4_0")
    private WebElementFacade excluirAcompanhante;

    @FindBy(name = "t_1_0")
    private WebElementFacade excluirAcompanhanteFamilia;

    @FindBy(id = "msgBoxDiv")
    private WebElementFacade divAguarde;

    @FindBy(id = "btp_novo")
    private WebElementFacade novoAcompanhante;

    @FindBy(id = "btp_novo_fma")
    private WebElementFacade novoAcompanhanteFamilia;

    @FindBy(name = "acp_parentesco_0")
    private WebElementFacade botaoParentesco;

    @FindBy(name = "fma_parentesco_0")
    private WebElementFacade botaoParentescoFamilia;

    @FindBy(name = "acp_nome_0")
    private WebElementFacade nomeAcompanhante;

    @FindBy(name = "acp_obs_0")
    private WebElementFacade observacaoAcompanhante;

    @FindBy(name = "fma_obs_0")
    private WebElementFacade observacaoAcompanhanteFamilia;

    @FindBy(id = "btp_gravar")
    private WebElementFacade botaoGravar;

    @FindBy(id = "btp_gravar_fma")
    private WebElementFacade botaoGravarFamilia;

    @FindBy(className = "container-close")
    private WebElementFacade botaoFechar;

    @FindBy(linkText = "Família (Relação Paciente x Paciente)")
    private WebElementFacade acompanhanteFamilia;

    @FindBy(name = "fma_pac_reg_destino_0")
    private WebElementFacade registroPacienteFamilia;

    public void fecharPainel(){
        if(botaoFechar.isPresent()) {
            botaoFechar.waitUntilClickable().click();
        }
    }

    public void clicarBotaoAcompanhante(){
        if(IconAcompanhante.isPresent()&& IconAcompanhante.isEnabled()){
            IconAcompanhante.click();
        }
    }

    public void excluirAcompanhanteExistente(){
        if(excluirAcompanhante.isVisible() && excluirAcompanhante.isPresent() && excluirAcompanhante.isEnabled() ){
            excluirAcompanhante.click();
        }else{
            excluirAcompanhanteFamilia.waitUntilVisible().click();
        }
        divAguarde.isPresent();
        divAguarde.waitUntilNotVisible();
    }

    public void clicarBotaoNovoAcompanhante(){
        if(novoAcompanhante.isVisible()) {
            novoAcompanhante.click();
        }else{
            novoAcompanhanteFamilia.click();
        }

        divAguarde.waitUntilNotVisible();
    }

    public void selecionarParentesco(String parentesco){
        if(botaoParentesco.isPresent()) {
            botaoParentesco.click();
            botaoParentesco.selectByVisibleText(parentesco);
        }
    }

    public void digitarNomeAcompanhante(String acompanhante){
        if(nomeAcompanhante.isPresent()) {
            nomeAcompanhante.click();
            nomeAcompanhante.type(acompanhante);
        }
    }

    public void digitarObservacaoAcompanhante(){
        if(observacaoAcompanhante.isPresent()) {
            observacaoAcompanhante.click();
            observacaoAcompanhante.type("OBSERVAÇÃO DO ACOMPANHATE DO PACIENTE");
        }
    }

    public void gravarRegistroDeAcompanhante(){
        if(botaoGravar.isPresent()) {
            botaoGravar.click();
        }
    }

    public void clicarAbaFamilia(){
        if(acompanhanteFamilia.isPresent()) {
            acompanhanteFamilia.click();
        }
    }

    public void buscarAcompanhantePorRegistro(String registro){
        registroPacienteFamilia.waitUntilClickable().click();
        registroPacienteFamilia.typeAndTab(registro);
        divAguarde.waitUntilNotVisible();
    }

    public void informarParentescoDeAcompanhanteFamilia(String parentesco){
        botaoParentescoFamilia.waitUntilClickable().click();
        botaoParentescoFamilia.selectByVisibleText(parentesco);
    }

    public void digitarObservacaoAcompanhanteFamilia(){
        observacaoAcompanhanteFamilia.waitUntilClickable().click();
        observacaoAcompanhanteFamilia.type("OBSERVAÇÃO DO ACOMPANHATE DO PACIENTE DA FAMÍLIA");
    }

    public void gravarRegistroDeAcompanhanteFamilia(){
        botaoGravarFamilia.waitUntilClickable().click();
    }

    public boolean validarRegistroDeAcompanhante(String acompanhante){
        if(nomeAcompanhante.getValue().equals(acompanhante)){
            return true;
        }else{
            return false;
        }
    }

    public boolean validarRegistroDeAcompanhanteFamilia(String registro){
        if(registroPacienteFamilia.getValue().equals(registro)){
            return true;
        }else{
            return false;
        }
    }

}
