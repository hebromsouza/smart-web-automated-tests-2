package com.pixeon.pages.paciente;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class PageBuscaPaciente extends BasePage {

    @FindBy(name = "pac_reg_0")
    private WebElementFacade registroPaciente;

    @FindBy(name = "pac_mcnv_0")
    private WebElementFacade matriculaPaciente;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade botaoBuscar;

    @FindBy(name = "pac_nome_0")
    private WebElementFacade lblNomePaciente;

    @FindBy(name = "pac_pac_nome_0")
    private WebElementFacade lnkNomePaciente;

    @FindBy(name = "pac_pac_nome_1")
    private WebElementFacade lnkSegundoNomePaciente;

    @FindBy(name = "compute_3_0")
    private WebElementFacade lnkNomePacienteAtendimento;

    @FindBy(name = "compute_3_1")
    private WebElementFacade lnkSegundoNomePacienteAtendimento;

    public void clicarNoCampoRegistro() {
        registroPaciente.click();
    }

    public void clicarNoCampoMatricula() {
        matriculaPaciente.click();
    }

    public void escreverNoCampoRegistro(String regPaciente) {
        if(regPaciente != null) {
            registroPaciente.waitUntilVisible().type(regPaciente);
        }

    }

    public void escreverNoCampoMadricula(String matPaciente) {

        if(matriculaPaciente.isPresent()) {
            matriculaPaciente.click();
            matriculaPaciente.type(matPaciente);
        }

    }

    public void clicarNomePacienteTelaBusca(String paciente){
        if ((lnkNomePaciente.isPresent() || lnkNomePacienteAtendimento.isPresent()) && (lnkSegundoNomePaciente.isPresent() || lnkSegundoNomePacienteAtendimento.isPresent())) {
            if (paciente.equals("7445390024960012")) {
                getDriver().findElement(By.linkText("LUANA OLIVEIRA DE SOUZA")).click();
            }
        }
    }

    public void InformarCPFCNPJ(String cpfCnoj){

    }




    public void clicarNoBotaoBuscar() {
        if(botaoBuscar.isPresent()) {
            botaoBuscar.click();
            divAguarde();
        }
    }

    public void buscarPacientePorRegistro(String regPaciente) {
        clicarNoCampoRegistro();
        escreverNoCampoRegistro(regPaciente);
        clicarNoBotaoBuscar();
    }

    public void buscarPacientePorMatricula(String matPaciente) {
        clicarNoCampoMatricula();
        escreverNoCampoMadricula(matPaciente);
        clicarNoBotaoBuscar();
    }



}