package com.pixeon.pages.paciente;

import com.pixeon.datamodel.paciente.DataModelBuscaDePacienteProntuario;
import com.pixeon.pages.BasePage;
import com.pixeon.util.DataHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class PageBuscaDePacienteProntuario extends BasePage {

    //nome
    @FindBy(xpath = "//*[@name='pac_nome_0']")
    private WebElementFacade txtNome;
    //registro
    @FindBy(xpath = "//*[@name='pac_reg_0']")
    private WebElementFacade txtRegistro;
   //prontuario
    @FindBy(xpath = "//*[@name='pac_pront_0']")
    private WebElementFacade txtProntuario;
    //CPF
    @FindBy(xpath = "//*[@name='pac_numcpf_0']")
    private WebElementFacade txtCPF;
    //RG
    @FindBy(xpath = "//*[@name='pac_numrg_0']")
    private WebElementFacade txtRG;
    //nascimento (data)
    @FindBy(xpath = "//*[@name='pac_nasc_0']")
    private WebElementFacade txtNascimento;

    //botão buscar
    @FindBy(xpath = "//*[@type='submit' and @value='Buscar' and @name='cb_submit_0']")
    private WebElementFacade btnBuscar;



    public void preencheCamposDeBuscaProntuarioDoPaciente(List<DataModelBuscaDePacienteProntuario> dataBuscaDePacienteProntuarioLogin) {


        DataModelBuscaDePacienteProntuario data = DataHelper.getData(dataBuscaDePacienteProntuarioLogin);
       //get nome
        if (data.getNome() != null) {
            txtNome.waitUntilEnabled();
            txtNome.waitUntilPresent().type(data.getNome());
        }
        //get nome
        if (data.getRegistro() != null) {
            txtRegistro.waitUntilPresent().type(data.getRegistro());
        }
        //get Prontuario
        if (data.getProntuario() != null) {
            txtProntuario.waitUntilPresent().type(data.getProntuario());
        }
        //get CPF
        if (data.getCPF() != null) {
            txtCPF.waitUntilPresent().type(data.getCPF());
        }
        //get RG
        if (data.getRG() != null) {
            txtRG.waitUntilPresent().type(data.getRG());
        }
        //get nascimento
        if (data.getNacimento() != null) {
            txtNascimento.waitUntilPresent().type(data.getNacimento());
        }
    }

   //clicar no botão Buscar
    public void clicaBtnBuscar() {
        btnBuscar.click();
    }

}
