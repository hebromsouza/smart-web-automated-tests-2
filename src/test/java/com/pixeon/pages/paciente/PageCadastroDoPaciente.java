package com.pixeon.pages.paciente;

import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import com.pixeon.datamodel.cadastros.DataModelCadastroPacientes;
import com.pixeon.datamodel.ordemDeServico.DataOrdemDeServico;
import com.pixeon.pages.BasePage;
import com.pixeon.pages.common.Utils;
import com.pixeon.util.DataHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.sikuli.script.Key;

import java.util.List;

public class PageCadastroDoPaciente extends BasePage {

    @FindBy(id = "bt_next")
    private WebElementFacade btnAvancar;


    @FindBy(xpath = "//*[@id=\"a_novo\"]/div")
    private WebElementFacade iconeNovaOs;


    @FindBy(xpath = "//*[@id='dw_osm_detail_0']/select[@name='osm_str_0']")
    private WebElementFacade comboSetor;

    @FindBy(linkText = "JOAO DO FECHAMENTO DE CAIXA")
    private WebElementFacade pacienteJoaoFechamentoCaixa;


    @FindBy(xpath = "//select[@name='osm_str_0']/option[@value='FAT']")
    private WebElementFacade setorFaturamento;

    @FindBy(xpath = "//*[@name='osm_str_0']/option[@value='CON']")
    private WebElementFacade cboSetorSolicitante;

    @FindBy(xpath = "//*[@id='bt_gravar']")
    private WebElementFacade btnGravar;

    @FindBy(name = "pac_numcpf_0")
    private WebElementFacade lblNumeroCpf;

    @FindBy(name = "pac_numcpf_0")
    private WebElementFacade FieldCpfCnpj;

    @FindBy(name = "mot_cod_0")
    private WebElementFacade comboMotivo;

    @FindBy(xpath = "//*[@id='dwp_mot_detail_0']/select/option[2]")
    private WebElementFacade alteracoesGerais;

    @FindBy(name = "smk_cod_filtro_0")
    private WebElementFacade fieldBuscaServico;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnBuscar;

    @FindBy(name = "smk_smk_ind_usual_0")
    private WebElementFacade item0;

    @FindBy(name = "smk_smk_ind_usual_1")
    private WebElementFacade item1;

    @FindBy(xpath = "//*[@id='btnIncluirSmk']/div")
    private WebElementFacade btnIncluir;

    @FindBy(xpath = "//*[@id='cheqpaipend']")
    private WebElementFacade checkAllListaItens;

    @FindBy(xpath = "//*[@id='btnConfirmar']")
    private WebElementFacade btnConfirmar;

    @FindBy(xpath = "//*[@id='ck_smm_all']")
    private WebElementFacade checkAllItensSelecionados;

    @FindBy(xpath = "//*[@id='secDadosSmm']/div[2]/span/button[1]/div")
    private WebElementFacade btnConcluir;

    @FindBy(xpath = "//div[@class='hd'][contains(text(),'ERRO NA AUTORIZAÇÃO ONLINE')]")
    private WebElementFacade ErroAutorizacaoOnline;

    @FindBy(id = "bt_excluir")
    private WebElementFacade btnExcluirQuestionario;

    @FindBy(xpath = "//*[@id='bt_button_ok']/div")
    private WebElementFacade btnOkAutorizacao;

    @FindBy(name = "osm_num_0")
    private WebElementFacade lblNumeroOS;

    @FindBy(name = "pac_pront_0")
    private WebElementFacade btnProntuario;

    @FindBy(linkText = "Imprimir Etiqueta")
    private WebElementFacade lnkImprimirEtiqueta;

    @FindBy(xpath = "//*[contains(text(),'Close')]")
    private WebElementFacade closeJanelas;

    @FindBy(id = "a_pagamento")
    private WebElementFacade pagamento;

    @FindBy(xpath = "//*[@id='dw_pend_footer']/input")
    private WebElementFacade btnPagar;

    @FindBy(name = "cb_submit_rdi")
    private WebElementFacade btnGravarFormaPagamento;

    @FindBy(xpath = "//*[@id='dw_osm_detail_0']/select[1]")
    private WebElementFacade comboConvenio;

    @FindBy(xpath = "//*[@id='dw_osm_detail_0']/select[@name='osm_osm_pln_cod_0']")
    private WebElementFacade comboPlano;

    @FindBy(xpath = "//*[@id='dw_osm_detail_0']//option[@value='MED']")
    private WebElementFacade valorMedPlano;

    @FindBy(xpath = "//*[@id='dw_osm_detail_0']/select[11]/option[@value='SG1']")
    private WebElementFacade valorSGEApartamentoPlano;

    @FindBy(xpath = "//*[@id='dw_osm_detail_0']/select[@name='osm_osm_pln_cod_0']/option[@value='BRA']")
    private WebElementFacade valorPlanoBradesco;

    @FindBy(xpath = "//*[@id='dw_osm_detail_0']/select[1]/option[@value='1']")
    private WebElementFacade particularConvenio;

    @FindBy(xpath = "//*[@id='dwp_mot_detail_0']/select")
    private WebElementFacade combomotivoAlteracao;

    @FindBy(xpath = "//*[@id='dwp_mot_detail_0']/select/option[2]")
    private WebElementFacade MotivoAlteracoesGerais;

    @FindBy(xpath = "//*[@id='bt_mot_ok']/div")
    private WebElementFacade btnOKMotivoAlteracao;

    @FindBy(name = "pac_nome_0")
    private WebElementFacade fieldNomePaciente;

    @FindBy(name = "pac_nome_mae_0")
    private WebElementFacade fieldNomeMae;

    @FindBy(name = "pac_nasc_0")
    private WebElementFacade fieldDtNascimento;

    @FindBy(name = "pac_nome_social_0")
    private WebElementFacade fieldNomeSocial;

    @FindBy(name = "pac_numrg_0")
    private WebElementFacade fieldRG;

    @FindBy(name = "pac_numcpf_0")
    private WebElementFacade fieldCPF;

    @FindBy(name = "pac_sexo_0")
    private WebElementFacade fieldComboSexo;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']//option[@value='M']")
    private WebElementFacade fieldSexoM;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']//option[@value='F']")
    private WebElementFacade fieldSexoF;

    @FindBy(name = "pac_est_civil_0")
    private WebElementFacade fieldComboEstadoCivil;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']//option[@value='S']")
    private WebElementFacade fieldEstadoCivilSolteiro;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']//option[@value='C']")
    private WebElementFacade fieldEstadoCivilCasado;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']//option[@value='V']")
    private WebElementFacade fieldEstadoCivilViuvo;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']//option[@value='D']")
    private WebElementFacade fieldEstadoCivilDesquitado;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']/select[5]")
    private WebElementFacade fieldEstadoCivilDivorciado;

    @FindBy(name = "pac_cnv_0")
    private WebElementFacade fieldComboOperadora;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']//option[@value='AML']")
    private WebElementFacade fieldOperadoraAmil;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']//option[@value='1']")
    private WebElementFacade fieldOperadoraParticular;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']/select[1]/option[2]")
    private WebElementFacade fieldOperadoraBradesco;

    @FindBy(name = "pac_dt_valid_0")
    private WebElementFacade fieldtValidade;

    @FindBy(name = "pac_pac_cle_cod_0")
    private WebElementFacade fieldComboAcomodacao;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']/select[3]/option[2]")
    private WebElementFacade fieldAcomodacaoApartamento;

    @FindBy(name = "pac_mcnv_0")
    private WebElementFacade fieldMatricula;

    @FindBy(name = "pac_pln_cod_0")
    private WebElementFacade fieldComboPlano;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']/select[2]/option[2]")
    private WebElementFacade fieldPlanoBradesco;

    @FindBy(linkText = "Contatos")
    private WebElementFacade tabContatos;

    @FindBy(linkText = "Dados Médicos")
    private WebElementFacade tabDadosMedicos;

    @FindBy(linkText = "Complementos")
    private WebElementFacade tabComplementos;

    @FindBy(linkText = "Outros Documentos")
    private WebElementFacade tabOutrosDocumentos;

    @FindBy(linkText = "Indicação")
    private WebElementFacade tabIndicação;

    @FindBy(linkText = "Observação")
    private WebElementFacade tabObservacao;

    @FindBy(linkText = "Domicílio")
    private WebElementFacade tabDomicílio;

    @FindBy(name = "pac_cep_0")
    private WebElementFacade fieldCep;

    @FindBy(xpath = "//*[@id='dwp_end_detail_0']/span[6]/a/div[@title='Pesquisar CEP']")
    private WebElementFacade lupaPesquisarCep;

    @FindBy(name = "cep_0")
    private WebElementFacade fieldCEPBuscarEnderecoPopup;

    @FindBy(xpath = "//*[@id='dwp_cep_busca_detail_0']/input[@value='Buscar']")
    private WebElementFacade btnBuscarEnderecoPopup;


    @FindBy(xpath = "//*[@id='dwp_end_detail_0']/span[1]/a/div[@title='Selecionar Tipo Logradouro...']")
    private WebElementFacade lupaPesquisarTipoLog;

    @FindBy(xpath = "//*[@id='sle_log-desc']")
    private WebElementFacade fieldBuscarTipoLog;

    @FindBy(xpath = "//*/button")
    private WebElementFacade fieldBtnBuscarTipoLog;



    @FindBy(name = "pac_end_0")
     private WebElementFacade txtlogadouroRua;



    @FindBy(xpath = "//*[@id=\"_panel100\"]//button[@type=\"Submit\"]")
    private WebElementFacade btnBuscarTipoLogradouro;

    @FindBy(xpath = "//*[@id='dwp_log_detail_2']/span/a")
    private WebElementFacade fieldlinkTipoRua;

    @FindBy(name = "pac_end_0")
    private WebElementFacade fieldLogradouro;

    @FindBy(name = "cde_cde_nome_0")
    private WebElementFacade fieldCidade;

    @FindBy(name = "pac_end_num_0")
    private WebElementFacade fieldNumero;

    @FindBy(name = "pac_uf_0")
    private WebElementFacade fieldUF;

    @FindBy(xpath = "//*[@id='dwp_end_detail_0']/span[17]/a/div[@title='Buscar cidade...']")
    private WebElementFacade lupaPesquisarCidade;

    @FindBy(id = "sle_cdenome")
    private WebElementFacade fieldBuscarCidade;

    @FindBy(xpath = "//*[@id='_panel104']//div[@class='search']")
    private WebElementFacade btnBuscarCidade;

    @FindBy(xpath = "//*[@id='dwp_cde_detail_0']//a[@name='cde_nome_0']")
    private WebElementFacade btnBuscarCidadeSalvador;



    @FindBy(name = "pac_comp_0")
    private WebElementFacade fieldBairro;

    @FindBy(name = "pac_pac_comp_extra_0")
    private WebElementFacade fieldComplemento;

    @FindBy(name = "pac_pac_email_0")
    private WebElementFacade fieldEmail;

    @FindBy(name = "pac_fone_0")
    private WebElementFacade fieldTelefone;

    @FindBy(name = "pac_celular_0")
    private WebElementFacade fieldCelular;

    @FindBy(name = "pac_pac_ind_aceita_sms_0")
    private WebElementFacade checkSMS;

    @FindBy(name = "pac_pac_ind_whatsapp_0")
    private WebElementFacade checkWhatsApp;

    @FindBy(name = "pdc_pdc_contato_0")
    private WebElementFacade fieldContato;

    @FindBy(name = "pdc_pdc_contato_fone_0")
    private WebElementFacade fieldTelefoneContato;

    @FindBy(name = "pac_fone2_0")
    private WebElementFacade fieldOutroTelefone;









    //UtilSmartWeb utils = new UtilSmartWeb();
    Faker faker = new Faker();
    static public String nomePaciente;
    static public String nomePac;

    public void InformarDadosPacienteAleatorio(String nome) {

        if(nome.equals("nomePacienteAleatorio")){
            nomePaciente = "";
            Name nomePaciente = faker.name();
            //   FunnyName sobrenomePaciente = faker.funnyName();
            //   Shakespeare terceiroNome = faker.shakespeare();
            //   nomePaciente = "ROBO " + utils.nomeAleatorio(6) + " " + utils.nomeAleatorio(4) + " AUTOMATION " + utils.nomeAleatorio(5) + " DA SILVA ";
            nomePac = nomePaciente.firstName().concat(" ").concat(nomePaciente.lastName()).concat(" ").concat(nomePaciente.fullName());

            // INFORMAR NOME DO PACIENTE NO FIELD NOME
            if(fieldNomePaciente.isPresent()){
                fieldNomePaciente.sendKeys(nomePac);
            }
        }
    }

    public void InformarDadosBasicosDoPaciente(List<DataModelCadastroPacientes>  dadosBasicosPaciente) {

        DataModelCadastroPacientes data = DataHelper.getData(dadosBasicosPaciente);

        if(data.getNomeSocial() != null) {
            fieldNomeSocial.type(data.getNomeSocial());
        }

        if(data.getNomeMae() != null) {
            fieldNomeMae.type(data.getNomeMae());
        }

        if(data.getDtNascimento() != null) {
            fieldDtNascimento.click();
            fieldDtNascimento.sendKeys(data.getDtNascimento());
        }
        Utils utils = new Utils();

        if(data.getRg() != null) {
            if(data.getRg().equals("RG_Random")){
                fieldRG.click();
                fieldRG.type(utils.geraRG());
            }
        }



        if(data.getCpf() != null) {
           if(data.getCpf().equals("CPF_Gerado_Aleatorio")){
               fieldCPF.click();
               fieldCPF.type(utils.geraCPF());
           }
        }


        if(data.getSexo() != null && data.getSexo().equals("masculino")) {
            fieldComboSexo.click();
            fieldSexoM.click();
        } else{
            fieldComboSexo.click();
            fieldSexoF.click();
        }

        if(data.getEstadoCivil() != null && data.getEstadoCivil().equals("solteiro")) {
            fieldComboEstadoCivil.click();
            fieldEstadoCivilSolteiro.click();
        }
        else if(data.getEstadoCivil() != null && data.getEstadoCivil().equals("casado")){
            fieldComboEstadoCivil.click();
            fieldEstadoCivilCasado.click();
        }
        else if(data.getEstadoCivil() != null && data.getEstadoCivil().equals("viuvo")){
            fieldComboEstadoCivil.click();
            fieldEstadoCivilViuvo.click();
        }
        else if(data.getEstadoCivil() != null && data.getEstadoCivil().equals("divorciado")){
            fieldComboEstadoCivil.click();
            fieldEstadoCivilDivorciado.click();
        }
        else{
            fieldComboEstadoCivil.click();
            fieldEstadoCivilDesquitado.click();
        }
    }

    public void InformarDadosOperadoraSaudePaciente(List<DataModelCadastroPacientes> dadosOperadoraSaude) {

        DataModelCadastroPacientes data = DataHelper.getData(dadosOperadoraSaude);

        if(data.getOperadora() != null) {
            fieldComboOperadora.click();
            fieldOperadoraBradesco.click();
        }

        if(data.getPlano() != null) {
            fieldComboPlano.click();
            fieldPlanoBradesco.click();
        }

        if(data.getMatricula() != null) {
            fieldMatricula.click();
            fieldMatricula.sendKeys(data.getMatricula());
        }

        if(data.getValidade() != null) {
            fieldtValidade.click();
            fieldtValidade.sendKeys(data.getValidade());
        }

        if(data.getAcomodacao() != null) {
            fieldComboAcomodacao.click();
            fieldAcomodacaoApartamento.click();
        }

    }

    public void InformarDadosContatoPaciente(List<DataModelCadastroPacientes> dadosContatoPaciente) {

        DataModelCadastroPacientes data = DataHelper.getData(dadosContatoPaciente);

        if(tabContatos.isPresent()){
            tabContatos.click();
            divAguarde();
        }

        if(data.getCep() != null) {
            fieldCep.click();
            fieldCep.sendKeys(data.getCep());
        }

        if(data.getLogradouro() != null) {
            if(lupaPesquisarTipoLog.isPresent()){
               lupaPesquisarTipoLog.click();
                divAguarde();
               fieldBuscarTipoLog.waitUntilClickable().waitUntilVisible().sendKeys("Rua");
                divAguarde();
                try {
                    fieldBtnBuscarTipoLog.waitUntilClickable().click();
                    divAguarde();
                   // Thread.sleep(900);
                    fieldlinkTipoRua.waitUntilClickable().click();
                    Thread.sleep(100);
                    divAguarde();
                }catch (Exception e) {
                    Assert.assertTrue("Falha ao tenta localizar o link rua", false);
                }
            }

            fieldLogradouro.click();
            fieldLogradouro.sendKeys(data.getLogradouro());
        }

        if(data.getNumero() != null) {
            fieldNumero.click();
            fieldNumero.sendKeys(data.getNumero());
        }

        if(data.getCidade() != null) {

            if(lupaPesquisarCidade.isPresent()){
                lupaPesquisarCidade.click();
                fieldBuscarCidade.sendKeys(data.getCidade());
                fieldBuscarCidade.typeAndEnter(Key.ENTER);
            //    fieldBuscarCidade.sendKeys(Key.ENTER);
              //  btnBuscarCidade.click();
                btnBuscarCidadeSalvador.click();
            }
        }



        if(data.getComplemento() != null) {
            fieldComplemento.click();
            fieldComplemento.sendKeys(data.getComplemento());
        }

        if(data.getEmail() != null) {
            fieldEmail.click();
            fieldEmail.sendKeys(data.getEmail());
        }

        if(data.getTelefone() != null) {
            fieldTelefone.click();
            fieldTelefone.sendKeys(data.getTelefone());
        }

        if(data.getCelular() != null) {
            fieldCelular.click();
            fieldCelular.sendKeys(data.getCelular());
        }

        if(checkWhatsApp.isPresent() && checkWhatsApp != null){
            checkWhatsApp.click();
            checkSMS.click();
        }

        if(data.getContato() != null) {
            fieldContato.click();
            fieldContato.sendKeys(data.getContato());
        }

        if(data.getOutrotelefone() != null) {
            fieldOutroTelefone.click();
            fieldOutroTelefone.sendKeys(data.getOutrotelefone());
        }

    }

    public void ConfirmarInclusaoNovoPaciente() {
        try {
            Thread.sleep(500);
           // divAguarde();
            boolean alert = waitHelper.alertGetText().contains("Paciente não encontrado!");
         //   divAguarde();

            if(alert==true){
                waitHelper.waitAlertOk();
            }
        } catch (Exception e) {
            Assert.fail("TELA CONTENDO o ALERTA COM A INFORMAÇÃO: 'Paciente não encontrado! Deseja incluí-lo' não foi encontrada ou não exibida");
        }
    }

    public void InformarNomeSocialPaciente(String nomeSocial) {
        divAguarde();
        if(!nomeSocial.equals("") && fieldNomeSocial.isPresent()){
            fieldNomeSocial.sendKeys(nomeSocial);
        }else{
            Assert.assertFalse("Campo Nome Social - Não Encontrado",true);
        }
    }

    public void InformarNomeMaePaciente(String nomeMae) {
        divAguarde();
        if(!nomeMae.equals("") && fieldNomeSocial.isPresent()){
            fieldNomeMae.sendKeys(nomeMae);
        }else{
            Assert.assertFalse("Campo Nome Mãe - Não Encontrado",true);
        }
    }

    public void InformarRGPaciente(String rg) {
        divAguarde();
        if(!rg.equals("") && fieldRG.isPresent()){
            fieldRG.sendKeys(rg);
        }else{
            Assert.assertFalse("Campo RG - Não Encontrado",true);
        }
    }

    public void InformarCPFPaciente(String cpf) {
        divAguarde();
        if(!cpf.equals("") && fieldCPF.isPresent()){
            fieldCPF.sendKeys(cpf);
        }else{
            Assert.assertFalse("Campo RG - Não Encontrado",true);
        }
    }

    public void InformarSexoPaciente(String sexo){
        divAguarde();
        if(!sexo.equals("") && fieldComboSexo.isPresent()){
            fieldComboSexo.click();

            if(sexo.equals("masculino")){
                fieldSexoM.click();
            }else{
                fieldSexoF.click();
            }
        }else{
            Assert.assertFalse("Campo Sexo - Não Encontrado",true);
        }

    }

    public void InformarEstadoCivilPaciente(String estadoCivil) {

            divAguarde();
            if(!estadoCivil.equals("") && fieldComboEstadoCivil.isPresent()){
                fieldComboEstadoCivil.click();

                if(estadoCivil.equals("solteiro")){
                    fieldEstadoCivilSolteiro.click();
                }
                else if(estadoCivil.equals("casado")){
                    fieldEstadoCivilCasado.click();
                }
                else if(estadoCivil.equals("viuvo")){
                    fieldEstadoCivilViuvo.click();
                }
                else if(estadoCivil.equals("divorciado")){
                    fieldEstadoCivilDivorciado.click();
                }
                else{
                    fieldEstadoCivilDesquitado.click();
                }
            }else{
                Assert.assertFalse("Campo ESTADO CIVIL - Não Encontrado",true);
            }


    }


    public void PagarOS() {
        if (pagamento.isPresent()) {
            pagamento.click();
            divAguarde();
        }

        fecharJanelasAbertas();

    }

    public void EfetuarPagamentoOS() {
        PagarOS();

        if (pagamento.isPresent()) {
            pagamento.click();
            divAguarde();
        }

        //    if(btnPagar.isDisplayed()){
        //        btnPagar.click();

        //        divAguarde();

        if (btnGravarFormaPagamento.isPresent()) {
            btnGravarFormaPagamento.click();
            divAguarde();
        }
        //       }

        fecharJanelasAbertas();

    }


    public void fecharJanelasAbertas() {

        if (closeJanelas.isPresent() && closeJanelas.isVisible() && closeJanelas.isDisplayed()) {

            if(closeJanelas.isPresent()) {
                closeJanelas.click();
            }

            if (closeJanelas.isPresent()) {
                closeJanelas.click();
            }
        }
    }


    public void clicarNoBotaoAvancar() {
        if (btnAvancar.isPresent()) {
            btnAvancar.click();
        }

    }

    public void InformarCPFCNPJ(String cpfCnpj) {
        if (FieldCpfCnpj.isPresent()) {
            FieldCpfCnpj.clear();
            FieldCpfCnpj.type(cpfCnpj);
        }
    }


    public void alterarConvenio(String convenio) {

        if (convenio.equals("1")) {
            comboConvenio.click();
            particularConvenio.click();
            fecharJanelasAbertas();

            divAguarde();

            if (combomotivoAlteracao.isPresent()) {
                combomotivoAlteracao.click();
                MotivoAlteracoesGerais.click();
                btnOKMotivoAlteracao.click();
            }
        } else if (convenio.equals("HAV")) {
            //Não faz nada, mantem o mesmo convenio
        }

    }

    public void selecionarPlano(String plano) {
        if (closeJanelas.isPresent()) {
            closeJanelas.click();
        }

        if (plano.equals("MEDIO")) {
            divAguarde();
            if (comboPlano.isVisible() && comboPlano.containsText("MEDIO")) {
                comboPlano.click();

                if(valorMedPlano.isPresent()) {
                    valorMedPlano.click();
                }
            }

        } else if (plano.equals("SGE APARTAMENTO")) {
            divAguarde();
            if (comboPlano.isVisible()) {
                comboPlano.click();
                if(valorSGEApartamentoPlano.isPresent()) {
                    valorSGEApartamentoPlano.click();
                }
            }
        } else if (plano.equals("BRADESCO TOP")) {
            divAguarde();

            if (comboPlano.isPresent()) {
                comboPlano.click();

                if(valorPlanoBradesco.isPresent()) {
                    valorPlanoBradesco.click();
                }
            }
        }

    }

    public void CriarNovaOrdemServicoPlano(String plano) {
        selecionarPlano(plano);
        clicarNoBotaoGravar();

        try {
            if (iconeNovaOs.isPresent()) {
                iconeNovaOs.click();
                iconeNovaOs.click();
                divAguarde();
                fecharJanelasAbertas();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        selecionarPlano(plano);
        clicarNoBotaoGravar();
        //divAguarde();
    }

    public void CriarNovaOrdemServicoConvenio(String convenio) {
        alterarConvenio(convenio);

        try {
            if (iconeNovaOs.isPresent()) {
                iconeNovaOs.click();
                iconeNovaOs.click();
                fecharJanelasAbertas();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        clicarNoBotaoGravar();
//        divAguarde();
    }

    public void buscarServico(String item){
        if (fieldBuscaServico.isPresent()) {
            fieldBuscaServico.typeAndEnter(item);
        }
    }

    public void incluirServico(){
        if(item0.isPresent() && item1.isPresent()){
            if(item0.isPresent()) {
                //Desmarca o ITEM
                item0.click();
                //divAguarde();
                //Marca o Item
                item0.click();
            }
            if(item1.isPresent()) {
                //Mara o ITEM que vem desmarcado
                item1.click();
               // divAguarde();
            }
        }

        if (btnIncluir.isPresent()) {
            btnIncluir.click();
        }
    }

    public void confirmarServico(){
        confirmarAlertaGlico();
        if (checkAllListaItens.isPresent()) {
          //  divAguarde();divAguarde();
            checkAllListaItens.click();

            if (btnConfirmar.isPresent() && btnConfirmar.isVisible()) {
                btnConfirmar.click();
            }
        }
    }

    public void concluirServico(){
        confirmarAlertaGlico();
        if (checkAllItensSelecionados.isPresent()) {
            checkAllItensSelecionados.click();

            if (btnConcluir.isVisible() && btnConcluir.isPresent()) {
                btnConcluir.click();
            }
        }
    }

    public void excluirRespostaQuestionario() {
        if (btnExcluirQuestionario.isPresent() ) {

            btnExcluirQuestionario.click();

            try {
                Thread.sleep(1000);
                boolean alert = waitHelper.alertGetText().contains("Confirma a exclusão do formulário?");

                if(alert==true){
                    waitHelper.waitAlertOk();
                }
            } catch (Exception e) {
                System.out.println("Exclusão do formulário Não Realizada.");
            }

        }
    }

   public void autorizacaoOnLineWebService(){
       if(ErroAutorizacaoOnline.isPresent() && ErroAutorizacaoOnline.isVisible()){
           btnOkAutorizacao.click();
           divAguarde();
           divAguarde();
       }

       if (closeJanelas.isVisible() && closeJanelas.isPresent() && closeJanelas.isDisplayed()) {
           closeJanelas.click();
           divAguarde();
       }


   }
    public void LancarItemParaOS(String item) {

        if (fieldBuscaServico.isPresent()) {
            fieldBuscaServico.typeAndEnter(item);

            if(item0.isPresent() && item1.isPresent()){
                if(item0.isPresent()) {
                    //Desmarca o ITEM
                    item0.click();
                    //divAguarde();
                    //Marca o Item
                    item0.click();
                }
                if(item1.isPresent()) {
                    //Mara o ITEM que vem desmarcado
                    item1.click();
                    divAguarde();
                }
            }

            if (btnIncluir.isPresent()) {
                btnIncluir.click();
            }

            if (checkAllListaItens.isPresent() || checkAllListaItens.isVisible()) {
                checkAllListaItens.click();
            }

            divAguarde();
            if (btnConfirmar.isPresent() && btnConfirmar.isVisible()) {
                btnConfirmar.click();
            }
            confirmarAlertaGlico();
//            divAguarde();
            if (checkAllItensSelecionados.isPresent()) {
               // divAguarde();
                checkAllItensSelecionados.click();
               // divAguarde();

                if (btnConcluir.isVisible() && btnConcluir.isPresent()) {
                    btnConcluir.click();
                    divAguarde();
                }
                //divAguarde();
                if(ErroAutorizacaoOnline.waitUntilPresent().isPresent() && ErroAutorizacaoOnline.isVisible()){
                    btnOkAutorizacao.click();
                }

                if (closeJanelas.isVisible() && closeJanelas.isPresent() && closeJanelas.isDisplayed()) {
                    closeJanelas.click();
                }
            }

        }
    }


    public void CriarNovaOrdemServico() {
        try {
            if (iconeNovaOs.isPresent()) {
                iconeNovaOs.click();
                iconeNovaOs.click();
                divAguarde();

                fecharJanelasAbertas();
                selecionarSetor();
                clicarNoBotaoGravar();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }

    public void CriarNovaOrdemServicoSemGravar() {
        clicarBotaoAvançar();
        fecharJanelasAbertas();

        try {
            if (iconeNovaOs.isPresent()) {
                iconeNovaOs.click();
                iconeNovaOs.click();
                fecharJanelasAbertas();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    public void PreencherDadosDaOS(List<DataOrdemDeServico> dataOrdemDeServico) {
        DataOrdemDeServico data = DataHelper.getData(dataOrdemDeServico);

        if (dataOrdemDeServico.equals("CONSULTORIOS")) {
            if (data.getSetorSolicitante() != "" && data.getSetorSolicitante() != null) {
                if (comboSetor.isPresent()) {
                    comboSetor.click();
                    cboSetorSolicitante.click();
                }
            }
        }

        if (dataOrdemDeServico.equals("SGE - RECEPÇÃO")) {
            System.out.println("GOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOLGOL");
        }

    }

    public void selecionarSetor() {
        if(pacienteJoaoFechamentoCaixa.isPresent()){
            setorFaturamento.click();
        } else {
            comboSetor.click();
            cboSetorSolicitante.click();
        }

    }


    public void clicarNoBotaoGravar() {
        divAguarde();
        if(btnGravar.isVisible() && btnGravar.isPresent()) {
            btnGravar.click();

            InformarMotivoAlteracao();
            msgOSLiberadaPagamento();
        }
       // divAguarde();
    }

    public void msgOSLiberadaPagamento(){
        try {
            Thread.sleep(1000);
            boolean alert = waitHelper.alertGetText().contains("Esta OS já foi liberada para pagamento.");

            if(alert==true){
                waitHelper.waitAlertOk();
            }
        } catch (Exception e) {
            //Excessão de continuidade
            System.out.println("");
        }
    }

    public void InformarMotivoAlteracao() {
        divAguarde();
        if(comboMotivo.isPresent()){
           // divAguarde();
            comboMotivo.click();
            alteracoesGerais.click();
            btnOKMotivoAlteracao.click();
        }
    }

    public void ValidarMsgAlerta(String msg) {
        try {
            Thread.sleep(1000);
            boolean alert = waitHelper.alertGetText().contains("Não foi possível completar a operação.");

                if(alert==true){
                    waitHelper.waitAlertOk();

                    if(closeJanelas.isPresent()){
                        closeJanelas.click();
                    }
                    Thread.sleep(500);

                    boolean motivo = waitHelper.alertGetText().equals("É necessário informar o motivo.");

                    if(motivo==true){
                        Thread.sleep(500);
                        waitHelper.waitAlertOk();
                    }
                }
            } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
        }

    }

    public void ValidarMsgAlertaCpfCnpj(String msg) {
        try {
            Thread.sleep(500);
            boolean alert = waitHelper.alertGetText().contains("ERRO: CPF inválido");

            if(alert==true){
                waitHelper.alertGetText().contains("ERRO: CPF inválido");
                waitHelper.waitAlertOk();
            }
        } catch (Exception e) {
            Assert.fail("FALHA ENCONTRADA OU POSSIVEL BUG ENCONTRADO. FAVOR, Analise");
        }

    }

    public void confirmarAlertaGlico() {
        try {
            Thread.sleep(1000);
            boolean alert = waitHelper.waitAlertGetText().contains("GLICO");

            if(alert==true){
                waitHelper.alertOk();
            }
        } catch (Exception e) {
            //System.out.println("Alerta nao exibido");
            //Segue o fluxo pois tela de glico nao foi exibida, nao é necessário a excessão
        }
    }

    public void clicarBotaoAvançar(){
        if(btnAvancar.isPresent() && btnAvancar.isVisible()) {
            btnAvancar.click();

            if(btnAvancar.isPresent()&& btnAvancar.isVisible()){
                btnAvancar.click();
            }

        }else{
            Assert.assertTrue("btn Avancar não encontrado",false);
        }
    }

    public void escreverCpf(String cpf) {
        if(lblNumeroCpf.isPresent()) {
            lblNumeroCpf.clear();
            lblNumeroCpf.type(cpf);
        }
    }

    public boolean validarCpfSemPontuacao() {
        return lblNumeroOS.isVisible();
    }


    public void validarCPFValido() {

        String mensagemAlerta = waitHelper.waitAlertGetText();
        Assert.assertTrue(mensagemAlerta.contains("ERRO: CPF inválido. (778.945.740)") || mensagemAlerta.contains("77894574031"));

        try {
            Thread.sleep(300);
            boolean alert = waitHelper.alertGetText().contains("ERRO: CPF inválido");

            if(alert==true){
                waitHelper.waitAlertOk();
            }
        } catch (Exception e) {
            Assert.fail("BUG ENCONTRADO, CPF não VALIDADO ou MENSAGEM ESPERADA NÃO EXIBIDA. FAVOR ANALISE");
        }
    }


    public void clicarBotaoProntuario() {
        if(btnProntuario.isPresent()) {
            btnProntuario.waitUntilPresent().click();
        }
    }

    public void clicarLinkImprimirEtiqueta() {
        if(lnkImprimirEtiqueta.isPresent()) {
            lnkImprimirEtiqueta.waitUntilPresent().click();
            divAguarde();
        }
    }

}
