package com.pixeon.pages.agenda;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageConfirmacaoDeAgendamento extends BasePage {

    @FindBy(name = "procedimento_0")
    private WebElementFacade cboProcedimento;

    @FindBy(name = "observacao_0")
    private WebElementFacade lblObservacao;

    @FindBy(id = "bt_confirmar")
    private WebElementFacade btnConfirmar;

    @FindBy(id = "msgBoxDiv")
    private WebElementFacade divAguarde;

    public void selecionarProcedimento(String procedimento){
        if(cboProcedimento.isPresent()) {
            cboProcedimento.waitUntilClickable().click();
            cboProcedimento.selectByVisibleText(procedimento);
        }
    }

    public void digitarObservacao(){
        if(lblObservacao.isPresent()) {
            lblObservacao.type("TESTE OBSERVAÇÃO MARCAÇÃO CONFIRMAÇÃO PROCEDIMENTO");
        }
    }

    public void alertaPacienteConsultaNaMesaDataHora(){
        try {
            Thread.sleep(1000);
            boolean alert = waitHelper.alertGetText().contains("Este paciente já possui uma outra consulta ou exame na mesma data");

            if(alert==true){
                waitHelper.waitAlertOk();
            }
        } catch (Exception e) {
            System.out.println("########## Alerta (Este paciente já possui uma outra consulta ou exame na mesma data...) não exibido ##############");
        }
       divAguarde();
    }

    public void clicarBotaoConfirmar(){

        if(btnConfirmar.isPresent()) {
            btnConfirmar.waitUntilClickable().click();
        }

        //Verifica Alerta Após clicar no botão confirmar... Se exibido, clica no OK do Alerta.
        alertaPacienteConsultaNaMesaDataHora();
    }

    public void aceitarPopUpConfirmacaoAgendamento(){
        if(waitHelper.waitAlertGetText().contains("Confirma a marcação do paciente?")){
            waitHelper.alertOk();
        }
    }

    public void aceitarPopUpPrazoConsulta(){
        if(waitHelper.waitAlertGetText().contains("Paciente possui uma consulta marcada num prazo menor do que 15 dias.")){
            waitHelper.alertOk();
        }
    }

    public void aceitarPopUpMarcacaoMesmoMedico(){
        if(waitHelper.waitAlertGetText().contains("Este paciente já possui um(a) MARCAÇÃO WEB marcado(a) para o mesmo médico. \n")){
            waitHelper.alertOk();
        }
    }

}
