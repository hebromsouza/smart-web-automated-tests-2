package com.pixeon.pages.agenda;

import com.pixeon.pages.BasePage;
import com.pixeon.pages.motivoCancelamento.PageMotivoCancelamento;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;

public class PageAgendamento extends BasePage {


    @FindBy(name = "esp_cod_0")
    private WebElementFacade cboEspecialidade;

    @FindBy(xpath = "//*[@name=\"esp_cod_0\"]/option[@value=\"CM\"]")
    private WebElementFacade cboEspecialidadeClinicaMedica;

    @FindBy(name = "psv_cod_0")
    private WebElementFacade cboMedico;

    @FindBy(name = "str_cod_0")
    private WebElementFacade cboUnidade;

    @FindBy(xpath = "//div[@class='t' and span[@title='das 12 às 18 horas']]/div[@class='h']/span[@class='lv']/a[contains(text(),'Livre')][1]")
    private WebElementFacade lnkDiaLivre;

    @FindBy(xpath = "//div[@class='m' and @idx='16']/span[@class='lv']/a[contains(text(),'Horário Disponível')]")
    private WebElementFacade lnkHorarioDisponivel;

    @FindBy(xpath="//span[@class=\"lv\"]/a[@href=\"#\"]")
    private WebElementFacade TextLivre;

    @FindBy(id="bt_proximo")
    private WebElementFacade BotaoProximaSemana;


    @FindBy(xpath="//div[@class=\"ms\"]//../..//span[contains(text(),'06:')]/..//a[contains(text(),'Horário Disponível')]")
    private WebElementFacade HorarioDisponivel_6Hs;

    @FindBy(xpath="//div[@class=\"ms\"]//../..//span[contains(text(),'07:')]/..//a[contains(text(),'Horário Disponível')]")
    private WebElementFacade HorarioDisponivel_7Hs;

    @FindBy(xpath="//div[@class=\"ms\"]//../..//span[contains(text(),'08:')]/..//a[contains(text(),'Horário Disponível')]")
    private WebElementFacade HorarioDisponivel_8Hs;

    @FindBy(xpath="//div[@class=\"ms\"]//../..//span[contains(text(),'09:')]/./..//span[contains(text(),'00')]/..//a[contains(text(),'Horário Disponível')]")
    private WebElementFacade HorarioDisponivel_9Hs;

    @FindBy(xpath="//div[@class=\"ms\"]//../..//span[contains(text(),'09:')]/./..//span[contains(text(),'10')]/..//a[contains(text(),'Horário Disponível')]")
    private WebElementFacade HorarioDisponivel_9e10Hs;

    @FindBy(xpath="//div[@class=\"ms\"]//../..//span[contains(text(),'09:')]/./..//span[contains(text(),'15')]/..//a[contains(text(),'Horário Disponível')]")
    private WebElementFacade HorarioDisponivel_9e15Hs;

    @FindBy(xpath="//div[@class=\"ms\"]//../..//span[contains(text(),'09:')]/./..//span[contains(text(),'20')]/..//a[contains(text(),'Horário Disponível')]")
    private WebElementFacade HorarioDisponivel_9e20Hs;

    @FindBy(xpath="//div[@class=\"ms\"]//../..//span[contains(text(),'10:')]/./..//span[contains(text(),'00')]/..//a[contains(text(),'Horário Disponível')]")
    private WebElementFacade HorarioDisponivel_10Hs;

    @FindBy(xpath="//div[@class=\"ms\"]//../..//span[contains(text(),'11:')]/./..//span[contains(text(),'00')]/..//a[contains(text(),'Horário Disponível')]")
    private WebElementFacade HorarioDisponivel_11Hs;

    @FindBy(xpath="//div[@class='m']/span[@class='pc']/a[contains(text(),'ÍVARR BEINLAUSI')]")
    private WebElementFacade PacienteAgendado;

    @FindBy(linkText="ÍVARR BEINLAUSI")
    private WebElementFacade PacienteAgendadoLink;

    @FindBy(xpath="//span[@class=\'sr\']/./..//span[contains(text(),'SURDEZ CONG MUT 35 D')]")
    private WebElementFacade ServicoAgendado;

    @FindBy(linkText = "Agenda")
    private WebElementFacade lnkAgenda;

    @FindBy(id = "p1")
    private WebElementFacade divNomeMedico;

    @FindBy(xpath = "//*[@id=\"dwp_mot_detail_0\"]/select")
    private WebElementFacade comboMotivo;

    @FindBy(linkText = "ÍVARR BEINLAUSI")
    private WebElementFacade pacienteÍVARRBEINLAUSI;

    @FindBy(xpath = "//*[@id=\"dwp_mot_detail_0\"]/select/option[3]")
    private WebElementFacade motivoAlteracaoNomePac;

    @FindBy(xpath = "//*[@id=\"bt_mot_ok\"]/div")
    private WebElementFacade botaoOkMotivo;

    @FindBy(id = "bt_voltar")
    private WebElementFacade btnVoltarAgenda;

    @FindBy(id = "bt_cancelar")
    private WebElementFacade btnCancelarAgendamento;

    @FindBy(linkText = "Cancelar marcação")
    private WebElementFacade OpCancelarMarcacao;



    public void selecionarEspecialidade(String especialidade){
        if(cboEspecialidadeClinicaMedica.isVisible()) {
            cboEspecialidadeClinicaMedica.click();
        }
//        divAguarde();
    }

    public void selecionarMedico(String medico){
        if(cboMedico.isPresent()) {
            cboMedico.waitUntilClickable().click();
            cboMedico.selectByVisibleText(medico);
            divAguarde();
        }
    }

    public void selecionarunidade(String unidade){
        cboUnidade.waitUntilClickable().click();
        cboUnidade.selectByVisibleText(unidade);
        divAguarde();
    }

    public void clicarDiaLivre(){
        if(lnkDiaLivre.isPresent()) {
            lnkDiaLivre.click();
            divAguarde();
        } else{
            System.out.println("Possível bug encontrado, Nenhum Resultado Exibido na Agenda, mesmo tendo valores.");
        }
    }

    public void buscarHorarioLivre(){
        if (HorarioDisponivel_6Hs.isVisible()){
            HorarioDisponivel_6Hs.waitUntilClickable().click();
        } else if(HorarioDisponivel_7Hs.isVisible()){
            HorarioDisponivel_7Hs.waitUntilClickable().click();
        }else if(HorarioDisponivel_8Hs.isVisible()){
            HorarioDisponivel_8Hs.waitUntilClickable().click();
        }else if(HorarioDisponivel_9Hs.isVisible()){
            HorarioDisponivel_9Hs.waitUntilClickable().click();
        }else if(HorarioDisponivel_10Hs.isVisible()){
            HorarioDisponivel_10Hs.waitUntilClickable().click();
        }
        else {
            int idx = 16;
            while (!waitHelper.existsElement(By.xpath("//div[@class='m' and @idx='" + idx + "']/span[@class='lv']/a[contains(text(),'Horário Disponível')]"))){
                idx++;
                if (waitHelper.existsElement(By.xpath("//div[@class='m' and @idx='" + idx + "']/span[@class='lv']/a[contains(text(),'Horário Disponível')]"))){
                    getDriver().findElement(By.xpath("//div[@class='m' and @idx='" + idx + "']/span[@class='lv']/a[contains(text(),'Horário Disponível')]")).click();
                    idx++;
                }
            }
        }
    }

    public void clicarHorarioDisponivel(){
        buscarHorarioLivre();
        divAguarde();
    }

    public void clicarPacienteAgendado(String paciente){
        divAguarde();
        //Verifica se existe o paciente=ÍVARR BEINLAUSI. Se existir Cancela a marcação... e Verifica novamente
        while (PacienteAgendadoLink.isPresent() ){
            //Clica sobre o nome do paciente
            PacienteAgendadoLink.click();

            //Chama método para Cancelar Marcacao e Confirma Popup
            clicarAcaoAgenda("Cancelar marcação");

            //Informa o motivo do Cancelamento
            pageMotivoCancelamento.selecionarMotivoCancelamento("ALTERAÇÃO NO NOME DO MÉDICO");
            pageMotivoCancelamento.escreverObservacao("Teste observação cancelamento");
            pageMotivoCancelamento.clicarBotaoOkMotivoCancelamento();
        }
    }

    PageMotivoCancelamento pageMotivoCancelamento;

    public void verificarPacienteBÍVARR_BEINLAU(){
        //Verifica se existe o paciente=ÍVARR BEINLAUSI. Se existir Cancela a marcação... e Verifica novamente
        while (PacienteAgendado.isPresent()){
            //Clica sobre o nome do paciente
            PacienteAgendado.click();

            //Chama método para Cancelar Marcacao e Confirma Popup
            clicarAcaoAgenda("Cancelar marcação");

            //Informa o motivo do Cancelamento
            pageMotivoCancelamento.selecionarMotivoCancelamento("ALTERAÇÃO NO NOME DO MÉDICO");
            pageMotivoCancelamento.escreverObservacao("Teste observação cancelamento");
            pageMotivoCancelamento.clicarBotaoOkMotivoCancelamento();
        }
    }

    public void clicarAcaoAgenda(String acaoAgenda){
        divAguarde();
        //Verifica Alerta Após clicar no botão confirmar... Se exibido, clica no OK do Alerta.
       // alertaPacienteConsultaNaMesaDataHora();

        if(OpCancelarMarcacao.isPresent()){
            OpCancelarMarcacao.click();

            confirmarPopUpCancelamentoMarcacao();
        }
    }

    public void clicarBotaoVoltarParaAgenda() {
        btnVoltarAgenda.click();
    }

    public void confirmarPopUpRetornarParaTeste(){
        if (waitHelper.waitAlertGetText().equals("Confirma retornar para tela de marcação?\nAtenção! O processo atual será interrompido.")){
            waitHelper.alertOk();
        }
    }

    public void confirmarPopUpCancelamentoMarcacao(){
        divAguarde();
        try {
            Thread.sleep(1000);
            boolean alert = waitHelper.alertGetText().contains("Confirma o cancelamento do horário?");

            if(alert==true){
                waitHelper.waitAlertOk();

                motivoCancelamento();

            }
        } catch (Exception e) {
            System.out.println(" ");
        }

    }

    public void motivoCancelamento() {
       if(comboMotivo.isPresent()){
           comboMotivo.click();

           if(motivoAlteracaoNomePac.isPresent()){
               motivoAlteracaoNomePac.click();
           }

           if(botaoOkMotivo.isPresent()){
               botaoOkMotivo.click();
               divAguarde();
           }
       }
    }


    public void validarAgendaMedico(String medico) {
        Assert.assertEquals(divNomeMedico.getText(), medico);
    }

    public void clicarBotaoCancelarMarcacao() {
        btnCancelarAgendamento.click();
    }

    public void validarCancelamentoAgenda() {
        //Verificar na tela de Agendamento que o paciente agendado anteriormente não é mais exibido, pois o cancelamento foi realizado.
        divAguarde();
        if(pacienteÍVARRBEINLAUSI.isPresent()){
            divAguarde();
            Assert.fail("BUG ENCONTRADO. PACIENTE NÃO DEVE ESTAR NA AGENDA, POIS O CANCELAMENTO FOI REALIZADO, ou o PACIENTE FOI AGENDADO MAIS DE UMA VEZ NO MESMO DIA");
        } else {
            Assert.assertFalse(pacienteÍVARRBEINLAUSI.isPresent());
        }

    }


    public void VerificarSePacienteFoiAgendado(String paciente)  {
        divAguarde(); divAguarde();
      if(pacienteÍVARRBEINLAUSI.isPresent()){
          Assert.assertTrue(pacienteÍVARRBEINLAUSI.isPresent());
          Assert.assertTrue("Paciente Presente", pacienteÍVARRBEINLAUSI.isPresent());
      } else {
          Assert.fail("PACIENTE IVAR Não foi Agedado ou não está presente.");
      }


    }
    public void validarPacienteAgendado(String paciente) throws InterruptedException {
        Thread.sleep(1000);
        try {
            boolean alert = waitHelper.alertGetText().contains("Este paciente já possui uma outra consulta ou exame na mesma data e hora na qual está sendo marcado");
            if(alert==true){
                waitHelper.waitAlertOk();
            }

        } catch (Exception e){
            String textPacienteAgendado = PacienteAgendado.getText();
            if(textPacienteAgendado.equals(paciente)){
                System.out.println("Nada Faz, pois o Paciente foi Agendado com sucesso e ele existe");
            } else {
                Assert.fail("BUG ENCONTRADO: PACIENTE NÃO AGENDADO. O RESULTADO ESPERADO É DIFERENTE DO RESULTADO ENCONTRADO");
            }
        }

        Thread.sleep(1000);
    }

    public void VerificarEValidarPacienteAgendado(String paciente){
        divAguarde();
        String textPacienteAgendado = PacienteAgendado.getText();

        if(textPacienteAgendado.equals(paciente)){
            //System.out.println("Nada Faz, pois o Paciente foi Agendado com sucesso e ele existe");
        } else {
            Assert.fail("BUG ENCONTRADO: PACIENTE NÃO AGENDADO. O RESULTADO ESPERADO É DIFERENTE DO RESULTADO ENCONTRADO");
        }

    }

    public void SelecionarHorarioPosteriorDataAtual(){

        if (BotaoProximaSemana.isPresent()) {
            BotaoProximaSemana.click();

            if(TextLivre.isPresent()){
                TextLivre.click();
            }


            if(HorarioDisponivel_9Hs.isPresent()){
                HorarioDisponivel_9Hs.click();
            } else if (HorarioDisponivel_8Hs.isPresent()){
                HorarioDisponivel_8Hs.click();
            } else if (HorarioDisponivel_9e10Hs.isPresent()){
                HorarioDisponivel_9e10Hs.click();
            }else if (HorarioDisponivel_9e15Hs.isPresent()){
                HorarioDisponivel_9e15Hs.click();
            } else if (HorarioDisponivel_9e20Hs.isPresent()){
                HorarioDisponivel_9e20Hs.click();
            } else if (HorarioDisponivel_10Hs.isPresent()){
                HorarioDisponivel_10Hs.click();
            } else if (HorarioDisponivel_11Hs.isPresent()){
                HorarioDisponivel_11Hs.click();
            }

        } else {
            Assert.assertFalse("Erro ao selecionar um horario posterior a data atual, favor tente novamente",true);
        }

    }

}
