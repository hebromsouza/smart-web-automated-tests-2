package com.pixeon.pages.financeiro;

import com.pixeon.datamodel.financeiro.DataBaixaCompromisso;
import com.pixeon.pages.BasePage;
import com.pixeon.util.DataHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import java.util.List;

public class PageBaixaCompromisso extends BasePage {

    @FindBy(name = "cpg_status_0")
    private WebElementFacade cboStatusCompromisso;

    @FindBy(name = "cpg_gcc_cod_0")
    private WebElementFacade cboEmpresa;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnBuscar;

    @FindBy(name = "b_1")
    private WebElementFacade btnPagarCompromissos;

    @FindBy(id = "val_ipg")
    private WebElementFacade lnkValorCompromisso;

    @FindBy(name = "marcado_0")
    private WebElementFacade chkCompromisso;

    @FindBy(name = "marcado_1")
    private WebElementFacade chkCompromisso1;

    @FindBy(name = "marcado_2")
    private WebElementFacade chkCompromisso2;

    @FindBy(name = "marcado_3")
    private WebElementFacade chkCompromisso3;

    @FindBy(name = "marcado_4")
    private WebElementFacade chkCompromisso4;

    @FindBy(partialLinkText = "120")
    private WebElementFacade linkCompromissos;

    @FindBy(linkText = "Próxima página")
    private WebElementFacade lnkProximaPagina;

    public void buscarBaixaCompromissoComDados(List<DataBaixaCompromisso> dataBaixaCompromisso) {
        DataBaixaCompromisso data = DataHelper.getData(dataBaixaCompromisso);

        if (data.getStatus() != null){
            cboStatusCompromisso.waitUntilClickable().click();
            cboStatusCompromisso.selectByVisibleText(data.getStatus());
        }

        if (data.getEmpresa() != null){
            cboEmpresa.waitUntilClickable().click();
            cboEmpresa.selectByVisibleText(data.getEmpresa());
        }

    }

    public void clicarBotaoBuscar() {
        btnBuscar.waitUntilClickable().click();
        divAguarde();
    }

    public boolean retornaExistenciaCompromissoRegistrado(String serieNumeroCompromisso){
        divAguarde();
        if (waitHelper.existsElement(By.xpath("//span/a[contains(text(), '"+ serieNumeroCompromisso +"')]"))) {
            divAguarde();
            return waitHelper.existsElement(By.xpath("//span/a[contains(text(), '"+ serieNumeroCompromisso +"')]"));
        }else{
            if (lnkProximaPagina.isVisible())lnkProximaPagina.click();
            divAguarde();
            return waitHelper.existsElement(By.xpath("//span/a[contains(text(), '"+ serieNumeroCompromisso +"')]"));
        }
    }

    public boolean retornaExistenciaCompromissoPago(String serieNumeroCompromisso){
        divAguarde();
        if (waitHelper.existsElement(By.xpath("//span[contains(text(), '"+ serieNumeroCompromisso +"')]"))){
            return waitHelper.existsElement(By.xpath("//span[contains(text(), '"+ serieNumeroCompromisso +"')]"));
        }else{
            if (lnkProximaPagina.isVisible())lnkProximaPagina.click();
            divAguarde();
            return waitHelper.existsElement(By.xpath("//span[contains(text(), '"+ serieNumeroCompromisso +"')]"));
        }
    }

    public String retornarValorCompromisso(){
        return lnkValorCompromisso.getValue();
    }

    public void clicarBotaoPagarCompromissos() {
        if(chkCompromisso.waitUntilPresent().isPresent() ){
            //Selecionar os itens.
            if(chkCompromisso.isPresent()) {
                chkCompromisso.waitUntilClickable().click();
            }

            if(chkCompromisso1.isPresent()){
                chkCompromisso1.waitUntilClickable().click();
            }

            if(chkCompromisso2.isPresent()){
                chkCompromisso2.waitUntilClickable().click();
            }

            if(chkCompromisso3.isPresent()){
                chkCompromisso3.waitUntilClickable().click();
            }
            if(chkCompromisso4.isPresent()){
                chkCompromisso4.waitUntilClickable().click();
            }


            //Clicar no botão
            btnPagarCompromissos.click();

        }


    }

}