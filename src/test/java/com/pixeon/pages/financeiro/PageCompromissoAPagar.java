package com.pixeon.pages.financeiro;

import com.pixeon.datamodel.financeiro.DataCompromissoAPagar;
import com.pixeon.pages.BasePage;
import com.pixeon.util.DataHelper;
import com.pixeon.util.SeleniumUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.util.List;

public class PageCompromissoAPagar extends BasePage {

    @FindBy(name = "cpg_gcc_cod_0")
    private WebElementFacade cboEmpresa;

    @FindBy(name = "cpg_tot_parc_0")
    private WebElementFacade lblLancamentos;

    @FindBy(name = "cpg_doc_0")
    private WebElementFacade lblDocumento;

    @FindBy(name = "cpg_cic_rg_0")
    private WebElementFacade lblCpfCnpj;

    @FindBy(name = "cpg_fone_0")
    private WebElementFacade lblTelefone;

    @FindBy(name = "cpg_obs_0")
    private WebElementFacade lblObservacao;

    @FindBy(name = "valor_total_0")
    private WebElementFacade lblValorTotal;

    @FindBy(name = "cfo_cod_0")
    private WebElementFacade cboClasse;

    @FindBy(linkText = "Buscar Fornecedor...")
    private WebElementFacade btnBuscarFornecedor;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnGravar;

    @FindBy(className = "save")
    private WebElementFacade btnGravarValor;

    @FindBy(id = "sle_fnenome")
    private WebElementFacade lblNomePrestador;

    @FindBy(className = "bt")
    private WebElementFacade btnBuscar;

    @FindBy(name = "ipg_dt_pgto_0")
    private WebElementFacade txtDataPagamento;

    @FindBy(name = "cpg_serie_0")
    private WebElementFacade lblSerieCompromissoAPagar;

    @FindBy(name = "cpg_num_0")
    private WebElementFacade lblNumCompromissoAPagar;

    @FindBy(name = "ipg_ipg_valor_0")
    private WebElementFacade txtValorBrutoCompromisso;

    public static String serieNumeroCompromissoAPagar;
    public static String valorSerieNumeroCompromissoAPagar;

    public void preencherCompromissoAPagarComDados(List<DataCompromissoAPagar> dataCompromissoAPagar) {
        DataCompromissoAPagar data = DataHelper.getData(dataCompromissoAPagar);

        if (data.getEmpresa() != null) {
            cboEmpresa.click();
            cboEmpresa.selectByVisibleText(data.getEmpresa());
        }
        if (data.getLancamentos() != 0) lblLancamentos.type(String.valueOf(data.getLancamentos()));
        if (data.getDocumento() != null) lblDocumento.type(data.getDocumento());
        if (data.getTelefone() != null) lblTelefone.type(data.getTelefone());
        if (data.getObservacao() != null) lblObservacao.type(data.getObservacao());

    }

    public void clicarBotaoGravar() {
        btnGravar.click();
        divAguarde();
    }

    public void clicarBotaoBuscarFornecedor() {
        btnBuscarFornecedor.waitUntilClickable().click();
    }

    public void digitarValorTotal(String valorTotal) {
        lblValorTotal.type(valorTotal);
    }

    public void selecionarClasse(String classe) {
        cboClasse.click();
        cboClasse.selectByVisibleText(classe);
    }

    public void clicarBotaoGravarValor() {
        btnGravarValor.click();
    }

    public void digitarNomeMedico(String fornecedor) {
        lblNomePrestador.type(fornecedor);
    }

    public void clicarBotaoBuscar() {
        btnBuscar.click();
        divAguarde();
    }

    public void validarLancamentoComValor(String valor) {
        Assert.assertTrue(waitHelper.existsElement(By.xpath("//*[@id='dw_ipg_footer']/span[contains(text(), '"+ valor +"')]")));
    }

    public void validarPagamentoMesmoDia() {
        Assert.assertEquals(SeleniumUtil.getDataAtualFormato("dd/MM/yyyy"), txtDataPagamento.getValue());
    }

    public void validarAutoCompleteCpfCnpjCredorBuscado(List<DataCompromissoAPagar> dataCompromissoAPagar) {
        DataCompromissoAPagar data = DataHelper.getData(dataCompromissoAPagar);
        Assert.assertEquals(" " + data.getCpfCnpj(), lblCpfCnpj.getValue());
    }

    public void pegarSerieNumeroDoCompromissoAPagar() {
        serieNumeroCompromissoAPagar = lblSerieCompromissoAPagar.getValue() + "." + lblNumCompromissoAPagar.getValue();
    }

    public void pegarValorSerieNumeroDoCompromissoAPagar(){
        valorSerieNumeroCompromissoAPagar = txtValorBrutoCompromisso.getValue();
    }

}