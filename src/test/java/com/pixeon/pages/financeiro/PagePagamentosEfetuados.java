package com.pixeon.pages.financeiro;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;

public class PagePagamentosEfetuados extends BasePage {

    @FindBy(name = "gcc_cod_0")
    private WebElementFacade cboEmpresa;

    @FindBy(xpath = "//*[@id='dw_bcp_busca_detail_0']//option[@value='SMG']")
    private WebElementFacade valueClinicaSemeg;

    @FindBy(xpath = "//*[@id=\"dw_bcp_detail_0\"]/span[4]/div")
    private WebElementFacade CredorSGEFornecedor;

    @FindBy(xpath = "//*[@id=\"dw_bcp_detail_0\"]/span[7]")
    private WebElementFacade valorPagamento;

    @FindBy(xpath = "//*[@id='dw_bcp_detail_0']//a[@name='t_13_0']")
    private WebElementFacade nrBaixa;


    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnBuscar;

    @FindBy(id = "bt_print")
    private WebElementFacade btnImprimirRecibo;

   // @FindBy(linkText = "Cancelar")
    @FindBy(xpath = "//a[text()='Cancelar']")
    private WebElementFacade lnkCancelarFila;


    String serieNumeroCompromissoHref = null;
    String serieNumeroBaixaCompromisso = null;

    public void selecionarEmpresa(String empresa){
        if(cboEmpresa.isPresent()&& cboEmpresa.isEnabled()
        ) {
            divAguarde();
            cboEmpresa.click();
          //  valueClinicaSemeg.click();
            cboEmpresa.selectByVisibleText(empresa);
        }
    }

    public void clicarBotaoBuscar(){
        if(btnBuscar.isPresent() && btnBuscar.isEnabled() ) {
            btnBuscar.click();
        }

    }

    public void validarPagamentosEfetuadosCompromissoGerado() {
        if(CredorSGEFornecedor.isPresent()){
            CredorSGEFornecedor.getTextValue().contains("SGE FORNECEDOR 1");
            Assert.assertTrue(valorPagamento.isPresent());
        }

       // Assert.assertTrue(waitHelper.existsElement(By.xpath("//a[contains(text(), '"+ serieNumeroCompromissoAPagar +"')]")));
    }

    public void clicarCompromissoGeradoPagamentosEfetuados() {

        if(nrBaixa.isPresent()){
            nrBaixa.waitUntilClickable().click();
        }
        //   getDriver().findElement(By.xpath("//a[contains(text(), '"+ serieNumeroCompromissoAPagar +"')]")).click();
      //  divAguarde();
    }

    public void clicarImprimirRecibo() {
        if(btnImprimirRecibo.isPresent()) {
            btnImprimirRecibo.click();
        }
    }

    public void pegarSerieNumeroDaBaixaPeloCompromissoGerado(String serieNumeroCompromisso){
        if (lnkCancelarFila.isVisible()) {
            if (waitHelper.existsElement(By.xpath("//a[contains(text(), '" + serieNumeroCompromisso + "')]"))) {
                serieNumeroCompromissoHref = getDriver().findElement(By.xpath("//a[contains(text(), '" + serieNumeroCompromisso + "')]")).getAttribute("href");
                serieNumeroBaixaCompromisso = serieNumeroCompromissoHref.substring(36, 43).replaceAll(" ", "");
            }else {
                serieNumeroBaixaCompromisso = null;
            }
        }
    }

    public void clicarBotaoCancelarDaBaixaDeCompromissoGerado(String serieNumeroCompromisso) {
       // pegarSerieNumeroDaBaixaPeloCompromissoGerado(serieNumeroCompromisso);

        if(lnkCancelarFila.isPresent()){
            lnkCancelarFila.click();
            divAguarde();
            try {
                Thread.sleep(1000);
                boolean alert = waitHelper.alertGetText().contains("Confirma o cancelamento da baixa");

                if(alert==true){
                    waitHelper.waitAlertOk();
                }
                divAguarde();

                if(lnkCancelarFila.isPresent()) {
                   lnkCancelarFila.click();
                      Thread.sleep(1000);
                        alert = waitHelper.alertGetText().contains("Confirma o cancelamento da baixa");

                        if (alert == true) {
                            waitHelper.waitAlertOk();
                        }
                }else if (lnkCancelarFila.isPresent()) {
                       lnkCancelarFila.click();
                    Thread.sleep(1000);
                    alert = waitHelper.alertGetText().contains("Confirma o cancelamento da baixa");

                    if (alert == true) {
                        waitHelper.waitAlertOk();
                    }
                }else if (lnkCancelarFila.isPresent()) {
                       lnkCancelarFila.click();
                    Thread.sleep(1000);
                    alert = waitHelper.alertGetText().contains("Confirma o cancelamento da baixa");

                    if (alert == true) {
                        waitHelper.waitAlertOk();
                    }
                }else if (lnkCancelarFila.isPresent()) {
                      lnkCancelarFila.click();
                    Thread.sleep(1000);
                    alert = waitHelper.alertGetText().contains("Confirma o cancelamento da baixa");

                    if (alert == true) {
                        waitHelper.waitAlertOk();
                    }
                }

            } catch (Exception e) {
                Assert.fail("MENSAGEM POPUP DE Confirmação do cancelamento da Baixa não exibida.");
            }
        }

     //   getDriver().findElement(By.xpath("//a[@onclick='cancelarBaixa("+ serieNumeroCompromisso +")' and contains(text(), 'Cancelar')]")).click();
     //   validarMensagemCancelamentoDaBaixa(serieNumeroBaixaCompromisso);
    }

    public void validarMensagemCancelamentoDaBaixa(String numeroBaixaCompromisso) {
        String numeroBaixaCompromissoMensagem = numeroBaixaCompromisso;
        numeroBaixaCompromissoMensagem = numeroBaixaCompromissoMensagem.replaceAll(",", ".");
        validarMensagemAlertaPopUp("Confirma o cancelamento da baixa "+ numeroBaixaCompromissoMensagem +"?");
        waitHelper.alertOk();
        divAguarde();
    }

    public boolean visibilidadeBaixaCompromissoGerado(String serieNumeroCompromisso){
      //  pegarSerieNumeroDaBaixaPeloCompromissoGerado(serieNumeroCompromisso);
       // return waitHelper.existsElement(By.xpath("//a[@onclick='cancelarBaixa("+ serieNumeroBaixaCompromisso +")' and contains(text(), 'Cancelar')]"));

        divAguarde();
        divAguarde();
        divAguarde();
       // System.out.println("passou aqui");
      //  return waitHelper.existsElement(By.xpath("//a[text()='Cancelar']"));


        return (lnkCancelarFila.isPresent());


    }

}