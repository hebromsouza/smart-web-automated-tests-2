package com.pixeon.pages;

import com.pixeon.util.SeleniumUtil;
import com.pixeon.util.SeleniumWaitHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;

import java.util.Set;

public class BasePage extends PageObject {

    @FindBy(id = "msgBoxDiv")
    protected WebElementFacade divAguarde;

    @FindBy(className = "container-close")
    private WebElementFacade btnClose;

    @FindBy(id = "msg_alerta")
    private WebElementFacade divMsgAlerta;

    @FindBy(xpath = "//a[contains(text(), 'Close')]")
    private WebElementFacade btnFecharTela;

    @FindBy(xpath = "//*[@id=\"pepTab\"]/ul/li[4]/a/em[contains(text(),'Exames')]")
    private WebElementFacade abaExames;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnBuscar;

    @FindBy(id = "bt_next")
    private WebElementFacade btnAvancar;

    @FindBy(name = "cb_submit_rdi")
    private WebElementFacade btnGravar;

    @FindBy(id = "pepTab")
    private WebElementFacade pepTab;

    @FindBy(xpath = "//*[@id='pepTab']/ul/li[2]/a/em")
    private WebElementFacade abaRegistrosClinicos;

    @FindBy(xpath = "//em[contains(text(),'Diligenciamento')]")
    private WebElementFacade abaDiligenciamento;


    public static SeleniumWaitHelper waitHelper;

    private String xpathAbas;

    public BasePage (){
        waitHelper = new SeleniumWaitHelper(ThucydidesWebDriverSupport.getDriver());
    }

    //Resolver interacao com elementos da pagina que estao dentro de frames
    public void carregarTelaInicial() {
     //   getDriver().switchTo().defaultContent()
        try {
            Thread.sleep(1000);
            getDriver().switchTo().frame("mainFrame");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void divAguarde(){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!SeleniumUtil.isAlertPresent()) {
            if(divAguarde.isPresent()) {
                divAguarde.waitUntilNotVisible();
            }
        }
    }

    public void carregarFrameDefault(){
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }



    public void  carregarFrame(int frame){
        getDriver().switchTo().frame(frame);
    }

    public void switchWindow(String nameFrame){
        getDriver().switchTo().window(nameFrame);
    }

    public void switchWindowClose(String nameFrame){
        getDriver().switchTo().window(nameFrame).close();
    }

    public void closeSwitchWindow(String name){
        getDriver().switchTo().window(name).close();
    }

    public void closePanel(){
     //   divAguarde();
        if(btnClose.isPresent() && btnClose.isVisible() ){
            btnClose.click();
        }

    }

    public String divMsgAlerta(){
        return divMsgAlerta.waitUntilVisible().getText();
    }

    public void validarMensagemAlertaPopUp(String mensagem){

        if(mensagem.equals("Não é possível alterar a quantidade para valor igual ou inferior à 0")){
            try {
                Thread.sleep(2000);
               // boolean alert = waitHelper.alertGetText().contains("Não é possível alterar a quantidade para valor igual ou inferior à 0");
                boolean alert = waitHelper.waitAlertGetText().contains("Não é possível alterar a quantidade para valor igual ou inferior à 0");
                if(alert==true){
                    waitHelper.waitAlertOk();
                }
            } catch (Exception e) {
                Assert.fail("BUG ENCONTRADO, A MENSAGEM DE ALERTA ESPERADA É DIFERENTE DA MENSAGEM DE ALERTA OBTIDA.");
            }
        } else if(mensagem.equals("O preço não pode ser alterado.")){
            try {
                Thread.sleep(2000);
              //  boolean alert = waitHelper.alertGetText().contains("O preço não pode ser alterado.");
                boolean alert = waitHelper.waitAlertGetText().contains("O preço não pode ser alterado.");
                if(alert==true){
                    waitHelper.waitAlertOk();
                }
            } catch (Exception e) {
                Assert.fail("BUG ENCONTRADO, A MENSAGEM DE ALERTA ESPERADA É DIFERENTE DA MENSAGEM DE ALERTA OBTIDA.");
            }
        }else if(mensagem.contains("Primeiro é necessário cadastrar o paciente, para depois imprimir etiqueta!")){
            try {
                Thread.sleep(2000);
               // boolean alert = waitHelper.alertGetText().contains("Primeiro é necessário cadastrar o paciente, para depois imprimir etiqueta!");
                boolean alert =waitHelper.waitAlertGetText().contains("Primeiro é necessário cadastrar o paciente, para depois imprimir etiqueta!");
                if(alert==true){
                    waitHelper.waitAlertOk();
                }
            } catch (Exception e) {
                Assert.fail("BUG ENCONTRADO, A MENSAGEM DE ALERTA ESPERADA É DIFERENTE DA MENSAGEM DE ALERTA OBTIDA.");
            }
        }
        else if(mensagem.contains("Confirmar gerar o fechamento do caixa da recepção das OSs listadas com status a enviar?")){
            try {
                Thread.sleep(1000);
               // boolean alert = waitHelper.alertGetText().contains("Confirmar gerar o fechamento do caixa da recepção das OSs listadas com status a enviar?");
                boolean alert = waitHelper.waitAlertGetText().contains("Confirmar gerar o fechamento do caixa da recepção das OSs listadas com status a enviar?");
                if(alert==true){
                    waitHelper.waitAlertOk();

                    Thread.sleep(1000);

                    boolean alert2 = waitHelper.alertGetText().contains("Existem atendimentos não pagos, logo o fechamento não poderá ser realizado.");
                    if(alert2==true){
                        waitHelper.waitAlertOk();
                    }
                }

            } catch (Exception e) {
               //Não faz NADA.
            }
        }
        else if(mensagem.contains("ERRO: CPF inválido")){
            try {
                Thread.sleep(3000);
               // boolean alert = waitHelper.alertGetText().contains("ERRO: CPF inválido");
                boolean alert = waitHelper.waitAlertGetText().contains("ERRO: CPF inválido");
                if(alert==true){
                    waitHelper.waitAlertOk();
                }
            } catch (Exception e) {
                Assert.fail("BUG ENCONTRADO, CPF não VALIDADO ou MENSAGEM ESPERADA NÃO EXIBIDA. FAVOR ANALISE");
            }
        }
//        else if(mensagem.equals("Existem atendimentos não pagos, logo o fechamento não poderá ser realizado.")){
//            try {
//                Thread.sleep(1000);
//                boolean alert2 = waitHelper.alertGetText().contains("Existem atendimentos não pagos");
//                if(alert2==true){
//                    waitHelper.waitAlertOk();
//                }
//            } catch (Exception e) {
//                Assert.fail("BUG ENCONTRADO, CPF não VALIDADO ou MENSAGEM ESPERADA NÃO EXIBIDA. FAVOR ANALISE");
//            }
//        }
        else if(mensagem.equals("Atenção. A faixa de desconto informada está fora do intervalo permitido ( 5% à 100%  ).")){
            try {
                Thread.sleep(5000);
              //  boolean alert2 = waitHelper.alertGetText().contains("Atenção. A faixa de desconto informada está fora do intervalo permitido ( 5% à 100%  ).");
                boolean alert2 = waitHelper.waitAlertGetText().contains("Atenção. A faixa de desconto informada está fora do intervalo permitido ( 5% à 100%  ).");

                if(alert2==true){
                    waitHelper.waitAlertOk();
                }
            } catch (Exception e) {
                Assert.fail("BUG ENCONTRADO, MENSAGEM ESPERADA NÃO EXIBIDA. FAVOR ANALISE");
            }
        }
        else if(mensagem.equals("Nenhuma OS encontrada.")){
            try {
                Thread.sleep(5000);
               // boolean alert2 = waitHelper.alertGetText().contains("Nenhuma OS encontrada.");
                boolean alert2 = waitHelper.waitAlertGetText().contains("Nenhuma OS encontrada.");
                if(alert2==true){
                    waitHelper.waitAlertOk();
                }
            } catch (Exception e) {
                Assert.fail("BUG ENCONTRADO, MENSAGEM ESPERADA NÃO EXIBIDA. FAVOR ANALISE");
            }
        }
    divAguarde();
    }


    public String retornaMensagemAlertaPopUp(){
        return waitHelper.waitAlertGetText();
    }

    public void clicarFecharPainel(){
        carregarTelaInicial();
        if(btnFecharTela.isPresent()){
            btnFecharTela.waitUntilClickable().click();
        }
    }

    public void clicarFecharPainel2(){
        if(btnFecharTela.isPresent()){
            btnFecharTela.click();
        }
    }



    public void clicarNaAba(String aba) {
        divAguarde();
        if(pepTab.isPresent() && pepTab.isVisible()){
            pepTab.click();
        }


        if(aba.equals("Exames")){
            if(abaExames.isDisplayed()){
                abaExames.click();
            }
        } else if(aba.equals("Registros Clínicos")){
            if(abaRegistrosClinicos.isDisplayed()){
                abaRegistrosClinicos.click();
            }

        }else if(aba.equals("Prescrições")){


        }else if(aba.equals("Diligenciamento")){
            if(abaDiligenciamento.isPresent()){
                abaDiligenciamento.click();
            }
        }

        //Criando elemento dinâmico da aba que será clicada
    //    xpathAbas = "//*[@class='yui-nav']//em[text()='"+ aba +"']";
   //     getDriver().findElement(By.xpath(xpathAbas)).click();
    }

    public void scroolDown() {
        //executeScript("window.scrollBy(x-pixels,y-pixels)");
        //x-pixels é o número no eixo x, ele se move para a esquerda se o número for positivo e para a direita se o número for negativo .
        //y-pixels é o número no eixo y, se move para baixo se o número for positivo e passa para cima se o número estiver negativo.


        //Clicar na Seta para baixo
//        getDriver().switchTo().defaultContent();
//        getDriver().switchTo().frame("mainFrame");
//        getDriver().switchTo().frame(0);
//        divAguarde();
        //divAguarde();

        //Outra opçao é Clicar na tela que desejao o scrool (Mantem o FOCO nela) e realiza o scroool
        ((JavascriptExecutor)getDriver()).executeScript("window.scrollBy (0,1000)");

    }

    public void scroolUp(){
        //executeScript("window.scrollBy(x-pixels,y-pixels)");
        //x-pixels é o número no eixo x, ele se move para a esquerda se o número for positivo e para a direita se o número for negativo .
        //y-pixels é o número no eixo y, se move para baixo se o número for positivo e passa para cima se o número estiver negativo.

        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
        divAguarde();
        ((JavascriptExecutor)getDriver()).executeScript("window.scrollBy (0,-1000)");
        //ROLAGEM PARA CIMA. Foi necessário chamar o switchTo para a rolagem ser realizada na barra de navegaçao mais externa.
        //Comentando as duas linhas do switch acima, a rolagem ficará dentro (interna) da tela de Pagamento(tela em que está o FOCO)

    }

    public void clicarNoBotao(String botao) {
        //divAguarde();

        if(botao.equals("Buscar")){
          try{
              if(btnBuscar.isEnabled() && btnBuscar.isPresent()) {
                  btnBuscar.waitUntilVisible().click();
                  Thread.sleep(1000);
                  if (btnBuscar.isEnabled() && btnBuscar.isPresent()) {
                      btnBuscar.waitUntilVisible().click();
                  }
                  Thread.sleep(1000);
                  if (btnBuscar.isEnabled() && btnBuscar.isPresent()) {
                      btnBuscar.waitUntilVisible().click();
                  }
              }
          }catch (Exception e){
               Assert.assertFalse("Falhaao tenta clicar no objeto",true);
          }


            if(btnBuscar.isEnabled() && btnBuscar.isPresent()) {
                btnBuscar.waitUntilVisible().click();
                if (btnBuscar.isEnabled() && btnBuscar.isPresent()) {
                    btnBuscar.waitUntilVisible().click();
                }
            }
        }
        else if(botao.equals("Avançar") || botao.equals("Avancar")){
            if(btnAvancar.isEnabled()& btnAvancar.isPresent()){
                btnAvancar.click();
                divAguarde();
            }
        }
        else if(botao.equals("Gravar")){
            if(btnGravar.isEnabled() & btnGravar.isPresent()){
                btnGravar.click();
            }
        }
    }


}
