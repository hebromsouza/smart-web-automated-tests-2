package com.pixeon.pages.etiqueta;

import com.pixeon.pages.BasePage;
import com.pixeon.util.UtilSmartWeb;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

import static org.junit.Assert.assertEquals;

public class PageImpressaoEtiquetas extends BasePage {

    @FindBy(name = "e_nqtde_0")
    private WebElementFacade textQuantidade;

    @FindBy(id = "bt_print")
    private WebElementFacade btnImprimir;

    @FindBy(id = "servimp-list")
    private WebElementFacade cboServidorImpressao;

    @FindBy(id = "msgBoxDiv")
    private WebElementFacade divAguarde;

    public void validarQuantidade(String quantidade){
        assertEquals( textQuantidade.waitUntilVisible().getText(), quantidade );
    }

    public void inserirQuantidadeEtiqueta(String quantidadeEtiqueta){
       if(textQuantidade.isPresent()) {
           textQuantidade.waitUntilClickable().click();
           textQuantidade.type(quantidadeEtiqueta);
       }
    }

    public void selecionarServidorImpressao(String servImp){
        cboServidorImpressao.waitUntilClickable().waitUntilVisible().waitUntilPresent().waitUntilEnabled().click();
        cboServidorImpressao.selectByVisibleText(servImp);
    }

    public void clicarImprimir(){
        btnImprimir.waitUntilClickable().click();
        //divAguarde.waitUntilNotVisible();
    }

    public Boolean validarConteudoArquivoEtiqueta(String status, String caminhoEtiqueta){
        if(UtilSmartWeb.lerArquivo(caminhoEtiqueta).contains(status)){
            return true;
        }else{
            return false;
        }
    }

    public Boolean validarConteudoEtiqueta(String caminhoEtiqueta){
        if(UtilSmartWeb.lerArquivo(caminhoEtiqueta).contains("ÍVARR BEINLAUSI")){
            return true;
        }
        return false;
    }

    public Boolean validarStatusEtiqueta(String status, String caminhoEtiqueta){
        if(status.equals("(PENDENTE)")){
            validarConteudoArquivoEtiqueta(status, caminhoEtiqueta);
        }
        return false;
    }

}
