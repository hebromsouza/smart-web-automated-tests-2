package com.pixeon.pages.pesquisaDeAvaliacao;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

import static org.junit.Assert.assertTrue;

public class PageQuestionario extends BasePage {

    @FindBy(xpath = "//input[@type='text' and @name='rpa_resposta_texto_0']")
    private WebElementFacade lblPergunta1;

    @FindBy(xpath = "//*[@id='_panel71']/a")
    private WebElementFacade XFecharQuestionario;

    @FindBy(id = "_panel71_c")
    private WebElementFacade panelQuestionario;

    @FindBy(xpath = "//*[@id='dwp_qst_all_detail_3']/span/input")
    private WebElementFacade radioPergunta2;

    @FindBy(xpath = "//*[@id='dwp_qst_all_detail_7']/span/input")
    private WebElementFacade radioPergunta3;

    @FindBy(xpath = "//*[@id='dwp_qst_all_detail_10']/span/input")
    private WebElementFacade radioPergunta4;

//    @FindBy(xpath = "//input[@type='radio' and @value='1' and @name='3_5']")
    @FindBy(xpath = "//*[@id='dwp_qst_all_detail_12']/span/input")
    private WebElementFacade radioPergunta5;

//    @FindBy(xpath = "//input[@type='radio' and @value='1' and @name='3_6']")
    @FindBy(xpath = "//*[@id='dwp_qst_all_detail_14']/span/input")
    private WebElementFacade radioPergunta6;

//    @FindBy(xpath = "//input[@type='radio' and @value='1' and @name='3_8']")
    @FindBy(xpath = "//*[@id='dwp_qst_all_detail_17']/span/input")
    private WebElementFacade radioPergunta8;

//    @FindBy(xpath = "//input[@type='radio' and @value='1' and @name='3_9']")
    @FindBy(xpath = "//*[@id='dwp_qst_all_detail_19']/span/input")
    private WebElementFacade radioPergunta9;

//    @FindBy(xpath = "//input[@type='checkbox' and @value='0' and @class='3_12']")
    @FindBy(xpath = "//*[@id='dwp_qst_all_detail_25']/span/input")
    private WebElementFacade checkPergunta11_2;

//    @FindBy(xpath = "//input[@type='checkbox' and @value='0' and @class='3_13']")
    @FindBy(xpath = "//*[@id='dwp_qst_all_detail_26']/span/input")
    private WebElementFacade checkPergunta11_3;

    @FindBy(id = "bt_gravar")
    private WebElementFacade botaoGravar;

    @FindBy(xpath = "//*[@id='bt_excluir']")
    private WebElementFacade botaoExcluir;

    @FindBy(xpath = "//*[@id='bt_novo']/div")
    private WebElementFacade novoQuestionario;

    @FindBy(name = "apq_qst_cod_0")
    private WebElementFacade selecionarQuestionario;

    @FindBy(name = "apq_questionado_nome_0")
    private WebElementFacade respondidoPor;


    @FindBy(linkText = "Avaliação de Atendimento")
    private WebElementFacade lnkAvaliacaoAtendimento;

    @FindBy(id = "bt_print")
    private WebElementFacade btnImprimir;

    @FindBy(xpath = "//*[contains(text(),'Teste Pesquisa de Avaliação Automação')]")
    private WebElementFacade validConteudo;

    @FindBy(xpath = "//*[contains(text(),'Close')]")
    private WebElementFacade closeJanelas;


    public void clicarBotaoNovoQuestionario(){
        if(novoQuestionario.isPresent()){
            novoQuestionario.click();
        }

    }

    public void selecionarQuestionario(String questionario){
        if(selecionarQuestionario.isPresent()) {
            selecionarQuestionario.waitUntilEnabled().click();
            selecionarQuestionario.selectByVisibleText(questionario);
        }

        if(respondidoPor.isPresent()){
            respondidoPor.clear();
            respondidoPor.type("QA Automation Test");
        }
    }

    public void responderQuestionario(){
        if(lblPergunta1.isPresent()) {
            lblPergunta1.click();
            lblPergunta1.type("Teste Pesquisa de Avaliação Automação");
            radioPergunta2.click();
            radioPergunta3.click();

            radioPergunta4.click();
            radioPergunta5.click();
            radioPergunta6.click();
            radioPergunta8.click();
            radioPergunta9.click();
            checkPergunta11_2.click();
            checkPergunta11_3.click();
        }

        gravarQuestionario();
    }

    public void gravarQuestionario(){
        divAguarde();
        if(botaoGravar.isPresent()) {
            botaoGravar.waitUntilClickable().click();
        }
    }

    public String validarMensagemDeExclusao(){
        return waitHelper.waitAlertGetText();
    }

    public void validarAlertaConceitoObtidoQuestionario(String conceito){
        String mansagemAlertaConceito = "Conceito obtido no questionário de avaliação : " + conceito;
        if (mansagemAlertaConceito.equals(waitHelper.waitAlertGetText())){
            waitHelper.alertOk();
        }
    }

    public void validarMensagemAlertaQuestionario(String mensagemAlerta){
      String text = waitHelper.waitAlertGetText();
    //  System.out.println(text);

     if(mensagemAlerta.equals(text)) {

         try {
             Thread.sleep(1000);
             boolean alert = waitHelper.alertGetText().contains("Pergunta obrigatória não respondida: Oque achou do atendimento?");

             if(alert==true){
                 waitHelper.waitAlertOk();
             }
         } catch (Exception e) {
             System.out.println("########## LOGIN REALIZADO COM SUCESSO! INICIANDO O CENÁRIO DE TESTE ##############");
         }


         //"Confirma a exclusão do formulário?"
         //Pergunta obrigatória não respondida: O que achou do atendimento?
         //Conceito obtido no questionário de avaliação : Bom
         // Não faz nada pois a validação foi realizada
         divAguarde();
//     } else if (mensagemAlerta.equals(text)) {
//         //Pergunta obrigatória não respondida: O que achou do atendimento?
//         // Não faz nada pois a validação foi realizada
//         divAguarde();
//     }else if (mensagemAlerta.equals("Conceito obtido no questionário de avaliação : Bom")) {
//         //Conceito obtido no questionário de avaliação : Bom
//         // Não faz nada pois a validação foi realizada
//         divAguarde();
//     }
     } else {
         Assert.fail("BUG ENCONTRADO. O RESULTADO ESPERADO É DIFERENTE DO RESULTADO OBTIDO. Erro na validação da mensagem de Alerta do Questionário");
     }
    }

    public void excluirRespostaQuestionario(){
        clicarBotaoExcluir();
    }

    public void clicarBotaoExcluir(){
       // if(botaoExcluir.isPresent()) { original
        if(botaoExcluir.isVisible() && botaoExcluir.isPresent()) {// ajuste feito por gilson.lima
            botaoExcluir.click();
            try {
                Thread.sleep(1000);
                boolean alert = waitHelper.alertGetText().contains("Confirma a exclusão do formulário?");

                if(alert==true){
                    waitHelper.waitAlertOk();
                }
            } catch (Exception e) {
                Assert.fail("FALHA ENCONTRADA. MENSAGEM DE ALERTA, NAO EXIBIDA");
            }
        }
    }

    public void clicarOkAlerta(){
        waitHelper.waitAlertOk();
    }

    public void clicarLinkQuestionario(){
        lnkAvaliacaoAtendimento.waitUntilClickable().click();
    }

    public void ClicarOkEnterSair(){
        try {
            Thread.sleep(1000);
            boolean alertExclusao = waitHelper.alertGetText().contains("Confirma a exclusão do formulário?");
            boolean alertPerguntaObrigatoria = waitHelper.alertGetText().contains("Pergunta obrigatória não respondida: Oque achou do atendimento?");
            boolean alertConceito = waitHelper.alertGetText().contains("Conceito obtido no questionário de avaliação : Bom");
            boolean alertExcluir = waitHelper.alertGetText().contains("Confirma a exclusão do formulário?");

            if((alertExclusao==true) || (alertPerguntaObrigatoria==true) || (alertConceito==true) || (alertExcluir==true)){
                waitHelper.waitAlertOk();
            }
        } catch (Exception e) {
            System.out.println("       ");
        }

    }



    public void clicarBotaoImprimir(){
        btnImprimir.waitUntilClickable().click();
        divAguarde();
    }

    public String excluirQuestionario(){
        excluirRespostaQuestionario();
        String mensagem = validarMensagemDeExclusao();
        waitHelper.waitAlertOk();
        return mensagem;
    }

    public void validarImpressaoQuestionario(){
        assertTrue(validConteudo.isPresent());

        if(closeJanelas.isPresent()){
            closeJanelas.click();
        }
    }
}
