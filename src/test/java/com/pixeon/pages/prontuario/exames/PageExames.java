package com.pixeon.pages.prontuario.exames;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

import static com.pixeon.util.SeleniumUtil.javaScriptExecutorKeyboard;

public class PageExames extends BasePage {

    @FindBy(id = "txtp_string")
    private WebElementFacade lblMotivo;

    @FindBy(className = "bt")
    private WebElementFacade btnOk;

    @FindBy(id = "msgDivBox")
    private WebElementFacade divAguarde;

    @FindBy(xpath = "//*[@id='yui-gen99']")
    private WebElementFacade registroCanceladoMotivo;

    @FindBy(xpath = "//*[contains(text(), \',\tcancelado\')]")
    private WebElementFacade lblCancelado;

    @FindBy(xpath = "//div[@class='pex']/div[contains(text(), \'Registro cancelado pelo\')]")
    private WebElementFacade divMensagemCancelados;

    public void clicarCancelarExames(){
        javaScriptExecutorKeyboard("xpath", "//a[@name='del' and contains(text(),'cancelar')]", "'style', 'display:inline'", "click", "");
    }

    public void cancelarExames(){

        clicarCancelarExames();

        if(waitHelper.waitAlertGetText().equals("Confirma o cancelamento da solitação de exames?")){
            waitHelper.waitAlertOk();
        }
    }

    public void informarMotivoDeCancelamento(String motivo){
        lblMotivo.type(motivo);
        btnOk.waitUntilClickable().click();
        divAguarde.waitUntilNotVisible();
    }

    public void validarCancelamentoDeExames(){

  //      String querySelector = javaScriptExecutorKeyboard("xpath", "//div[@class='pex']/div[contains(text(), 'Registro cancelado pelo')]", "'style', 'display:inline'", "view", "document.querySelectorAll('div[id='pex_list'] ul li')[0].innerHTML");

        if(registroCanceladoMotivo.isPresent()){
            registroCanceladoMotivo.getTextValue().contains("Registro cancelado");
            Assert.assertTrue(divMensagemCancelados.waitUntilVisible().isPresent());
            divAguarde();
        }
      //  Assert.assertTrue(querySelector.contains("cancelado"));

    }

}
