package com.pixeon.pages.prontuario.exames;

import com.pixeon.pages.BasePage;
import com.pixeon.util.SeleniumUtil;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageSolicitacaoDeExames extends BasePage {

    @FindBy(xpath = "//*[@id=\'pepTab\']/div/div/div/ul/li/button[@onclick='ns_pex.Novo.start();']/div[@class='ico with-label add']")
    private WebElementFacade btnNovaSolicitacaoExames;

    @FindBy(id = "btnc_pex_next")
    private WebElementFacade btnProximo;

    @FindBy(name = "pex_med_0")
    private WebElementFacade cboProfissional;

    @FindBy(name = "pex_tiss_tipo_atende_0")
    private WebElementFacade cboTipoAtendimento;

    @FindBy(name = "pex_tipo_saida_0")
    private WebElementFacade cboTipoSaida;

    @FindBy(id = "btnc_pex_concluir")
    private WebElementFacade btnConcluir;

    @FindBy(xpath = "//*[@id=\'ygtvlabelel1\']")
    private WebElementFacade lnkGrupoCardiologia;

    @FindBy(id = "ygtvlabelel14")
    private WebElementFacade lnkAssisteVentMecani;

    @FindBy(id = "ygtvlabelel16")
    private WebElementFacade lnkAtendDisfunSexual;

    @FindBy(id = "ygtvlabelel21")
    private WebElementFacade lnkAvaliaNutricional;

    @FindBy(id = "ygtvlabelel2")
    private WebElementFacade lnkGrupoLaboratorio;

    @FindBy(id = "ygtvlabelel3")
    private WebElementFacade lnkGrupoBacteriologia;

    @FindBy(xpath = "//span[@class='ygtvlabel' and contains(text(),'(E) BACILO DIFTERICO - P')]")
    private WebElementFacade lnkBaciloDiftericoP;

    @FindBy(id = "ygtvlabelel128")
    private WebElementFacade lnkIncluirTodosOsItens;

    @FindBy(xpath = "//div[@class=\'yui-dt-liner\' and text()=\'ASSISTE VENT MECANI\']")
    private WebElementFacade divAssisteVentMecani;

    @FindBy(xpath = "//div[@class=\'yui-dt-liner\' and text()=\'ATEND DISFUN SEXUAL\']")
    private WebElementFacade divAtendDisfunSexual;

    @FindBy(xpath = "//div[@class=\'yui-dt-liner\' and text()=\'AVALIA NUTRICIONAL\']")
    private WebElementFacade divAvaliaNutriconal;

    @FindBy(xpath = "//div[@class=\'yui-dt-liner\' and text()=\'BACILO DIFTERICO - P\']")
    private WebElementFacade divBaciloDiftericoP;

    @FindBy(xpath = "//*[@class=\'pex\']")
    private WebElementFacade divPedidoExame;

    @FindBy(xpath = "//div[@class='pex']")
    private WebElementFacade divExames;

    private String dataSolicitacaoExame;

    public void clicarBotaoNovaSolicitacaoDeExames(){
        btnNovaSolicitacaoExames.waitUntilClickable().click();
    }

    public void clicarLinkGrupoExames(String grupo){
        getDriver().findElement(By.linkText(grupo)).click();
    }

    public void clicarSubGruposExames(String subGrupos){
        String arraySubGrupos[] = subGrupos.split(";");

        for(int i=0; i<=arraySubGrupos.length; i++){
            getDriver().findElement(By.linkText(arraySubGrupos[i])).click();
        }
    }

    public void clicarExames(String exames){
        String arrayExames[] = exames.split(";");

        for(int i=0; i<=arrayExames.length; i++){
            getDriver().findElement(By.linkText(arrayExames[i])).click();
        }
    }

    public void clicarGrupoCardiologia(){
        lnkGrupoCardiologia.waitUntilClickable().click();
    }

    public void inserirAssisteVentMecani(){
        lnkAssisteVentMecani.waitUntilClickable().click();
        Assert.assertTrue(divAssisteVentMecani.waitUntilPresent().isPresent());
    }

    public void inserirAtendDisfunSexual(){
        lnkAtendDisfunSexual.waitUntilClickable().click();
        Assert.assertTrue(divAtendDisfunSexual.waitUntilPresent().isPresent());
    }

    public void inserirAvaliaNutricional(){
        lnkAvaliaNutricional.waitUntilClickable().click();
        Assert.assertTrue(divAvaliaNutriconal.waitUntilPresent().isPresent());
    }

    public void clicarGrupoLaboratorio(){
        lnkGrupoLaboratorio.waitUntilClickable().click();
    }

    public void clicarSubGrupoBacteriologia(){
        lnkGrupoBacteriologia.waitUntilClickable().click();
    }

    public void clicarExameLaboratorioBaciloDiftericoP(){
        lnkBaciloDiftericoP.waitUntilClickable().click();
        Assert.assertTrue(divBaciloDiftericoP.waitUntilPresent().isPresent());
    }

    public void clicarBotaoProximo(){
        btnProximo.waitUntilClickable().click();
    }

    public void selecionarProfissional(String profissional){
        cboProfissional.waitUntilClickable().click();
        cboProfissional.selectByVisibleText(profissional);
    }

    public void selecionarTipoAtendimento(String tipoAtendimento){
        cboTipoAtendimento.waitUntilClickable().click();
        cboTipoAtendimento.selectByVisibleText(tipoAtendimento);
    }

    public void selecionarTipoSaida(String tipoSaida){
        cboTipoSaida.waitUntilClickable().click();
        cboTipoSaida.selectByVisibleText(tipoSaida);
    }

    public void clicarBotaoConcluir(){
        btnConcluir.click();
        divAguarde();
    }

    public void validarMensagemDeImpressaoECancelar(){
        Assert.assertEquals(waitHelper.waitAlertGetText(), "Deseja imprimir a solicitação de exames?");
        waitHelper.alertCancel();
        divAguarde();
        dataSolicitacaoExame = pegarDataSolicitacaoExame();
    }

    public String pegarDataSolicitacaoExame(){
        String dataExame = divPedidoExame.waitUntilVisible().getAttribute("dthr");
        return dataExame;
    }

    public void validarDataPedidoExame(){
        Assert.assertTrue(SeleniumUtil.dataAtualFormated().contains(dataSolicitacaoExame.substring(0, 13)));
    }

    public void validarPedidoExameCardiologiaLaboratorio(){
        if(divExames.getAttribute("dthr").contains(dataSolicitacaoExame.substring(0, 13))){
            Assert.assertTrue(divExames.getText().contains("ASSISTENCIA VENTILACAO MECANICA"));
            Assert.assertTrue(divExames.getText().contains("ATENDIMENTO DISFUNCAO SEXUAL DO DEFICIEN"));
            Assert.assertTrue(divExames.getText().contains("AVALIACAO NUTRICIONAL (COM CONSULTA)"));
            Assert.assertTrue(divExames.getText().contains("BACILO DIFTERICO - PESQUISA"));
        }
    }

}
