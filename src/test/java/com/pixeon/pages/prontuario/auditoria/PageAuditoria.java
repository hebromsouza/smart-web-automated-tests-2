package com.pixeon.pages.prontuario.auditoria;

import com.pixeon.datamodel.busca_auditoria.DataBuscaAuditoria;
import com.pixeon.pages.BasePage;
import com.pixeon.util.DataHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.util.List;

public class PageAuditoria extends BasePage {

    //cboTipoDeEvento
    @FindBy(xpath = "//*[@name='evt_processo_0']")
    private WebElementFacade cboTipoDeEvento;
    //txtDescricao
    @FindBy(xpath = "//*[@name='evt_registro_0']")
    private WebElementFacade txtDescricao;
    //txtPeriodoInicio
    @FindBy(xpath = "//*[@name='evt_dthr_ini_0']")
    private WebElementFacade txtPeriodoInicio;
    //txtPeriodoFim
    @FindBy(xpath = "//*[@name='evt_dthr_fim_0']")
    private WebElementFacade txtPeriodoFim;
    //Usuario
    @FindBy(xpath = "//*[@name='evt_usr_login_0']")
    private WebElementFacade txtUsuario;
    //Txt Registro Afetado
    @FindBy(xpath = "//*[@name='evt_reg_afetado_0']")
    private WebElementFacade txtRegistroAfetado;
    //txt identificador
    @FindBy(xpath = "//*[@name='evt_contador_0']")
    private WebElementFacade txtIdentificador;
    //txt paciente
    @FindBy(xpath = "//*[@name='evt_pac_reg_0']")
    private WebElementFacade txtPaciente;

    //btn buscar
    @FindBy(xpath = "//*[@id='bt_search']")
    private WebElementFacade btnBuscar;

    //tabela filtro
    @FindBy(xpath = "//*[@id='dw_psv_master_datawindow']")
    private WebElementFacade TabelaFiltro;

    //Resolver interacao com elementos da pagina que estao dentro de frames
    public void carregarTelaInicial() {
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }

    public void preencheCamposDeBuscaAuditoria(List<DataBuscaAuditoria> dataBuscaAuditoria) {
        carregarTelaInicial();
        DataBuscaAuditoria data = DataHelper.getData(dataBuscaAuditoria);
        //get Tipo De Evento
        if (data.getTipoDeEventos() != null) {
            cboTipoDeEvento.waitUntilEnabled();
            cboTipoDeEvento.click();
            cboTipoDeEvento.selectByVisibleText(data.getTipoDeEventos());
        }
        //get Descricao
        if (data.getDescricao() != null) {
            txtDescricao.waitUntilPresent().type(data.getDescricao());
        }

        //get periodo inicio
        if (data.getPeriodoInicio() != null) {
            txtPeriodoInicio.waitUntilPresent().click();
            txtPeriodoInicio.sendKeys(data.getPeriodoInicio());
        }
        //get periodo fim
        if (data.getPeriodoFim() != null) {
            txtPeriodoFim.waitUntilPresent().click();
            txtPeriodoFim.sendKeys(data.getPeriodoFim());
        }

        //get usuario
        if (data.getUsuario() != null) {
            txtUsuario.waitUntilPresent().type(data.getUsuario());
        }

        //get Registro Afetado
        if (data.getRegistroAfetado() != null) {
            txtRegistroAfetado.waitUntilPresent().type(data.getRegistroAfetado());
        }

        //get identificador
        if (data.getIdentificador() != null) {
            txtIdentificador.waitUntilPresent().type(data.getIdentificador());
        }

        //get Paciente
        if (data.getPaceinte() != null) {
            txtPaciente.waitUntilPresent().type(data.getPaceinte());
        }
    }

    //clicar no botão Buscar
    public void clicaBtnBuscar() {
        btnBuscar.click();
    }

    //clicar no nome do profissional
    public void validaPresencaDointemnaTela( String item) {
        try {
            if (!item.contains("")||item != null) {
                btnBuscar.waitUntilPresent();
                btnBuscar.waitUntilVisible();

                String xp = "//*[@id='dw_evt_container']//*[text()='" + item + "']";
                boolean existe = getDriver().findElement(By.xpath(xp)).isEnabled();
                Assert.assertTrue(existe);
            }

        } catch (Exception e) {
            Logger.getLogger("StaleElementReferenceException - item "+item +"não encontrado na tela: " + e.getMessage());
            Assert.assertTrue(false);
        }
    }


}
