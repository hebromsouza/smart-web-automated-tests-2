package com.pixeon.pages.prontuario.registro_clinico;

import com.pixeon.pages.BasePage;
import com.pixeon.pages.atendimento.ordemDeServico.PageLancamentoDeOrdemDeServico;
import org.junit.Assert;
import org.openqa.selenium.By;

public class PageRegistrosClinicos extends BasePage {

    public void validarExistenciaRegistroClinicoInternacao() {
        Assert.assertTrue(waitHelper.existsElement(By.linkText("INTERNAÇÃO")));
    }

    public void validarExistenciaMedicoRegistroClinico(String medico){
        Assert.assertTrue(waitHelper.existsElement(By.xpath("//*[@id='tb1']/tbody/tr/td[contains(text(), '" + medico + "')]")));
    }

    public void validarRegistroClinicoPorOrdemServico() {

        String numeroOS = PageLancamentoDeOrdemDeServico.numeroOS;
        boolean existsElement = waitHelper.existsElement(By.xpath("//*[@id='tb1']/tbody/tr/td[contains(text(), '"+ numeroOS +"')]"));
        int id=1, tr=1;

        while (existsElement){
            while(existsElement){
                Assert.assertTrue(waitHelper.existsElement(By.xpath("//*[@id='tb"+ id +"']/tbody/tr["+ tr +"]/td[contains(text(), '"+ numeroOS +"')]")));
                tr++;
                existsElement = waitHelper.existsElement(By.xpath("//*[@id='tb"+ id +"']/tbody/tr["+ tr +"]/td[contains(text(), '"+ numeroOS +"')]"));
            }
            id++;
            tr=1;
            existsElement = waitHelper.existsElement(By.xpath("//*[@id='tb"+ id +"']/tbody/tr["+ tr +"]/td[contains(text(), '"+ numeroOS +"')]"));
        }

    }

}
