package com.pixeon.pages.prontuario.registro_clinico.formularios;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;


public class PageFormularioDeExameFisico extends BasePage {

    //Btn btnAnexarArquivos
    @FindBy(xpath = "//*[@id='btp_files']//*[text()='Arquivos (0)']")
    private WebElementFacade btnAnexarArquivos;

       //Btn btnFinalizar
    @FindBy(xpath = "//*[@id='btp_gravar']//*[text()='Finalizar']")
    private WebElementFacade btnFinalizar;

    @FindBy(xpath = "//*[@class='container-close'][text()='Close']")
    private WebElementFacade btnFechar;

    //txr  exame fisico
    @FindBy(xpath = "//*[@name='c00492_06q_0']")
    private WebElementFacade txtExameFisico;

    //Resolver interacao com elementos da pagina que estao dentro de frames
    public void carregarTelaInicial() {
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }

    public void clickAnexarArquivos() {
        btnAnexarArquivos.waitUntilPresent().click();
    }
    public void clickFinalizar() {
        btnFinalizar.waitUntilPresent().click();
    }
    public void clickFechar() {
        btnFechar.waitUntilPresent().click();
    }

    public void informarExameFisico(String texto) {
        txtExameFisico.waitUntilPresent().type(texto);
    }


}