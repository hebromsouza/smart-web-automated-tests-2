package com.pixeon.pages.prontuario.registro_clinico;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.log4j.Logger;
import org.junit.Assert;


public class PageNovoRegistroClinico extends BasePage {
    public static  String data;
    //link formulario
    @FindBy(xpath = "//*//a[text()='FORMULÁRIO DE ANAMNESE']")
    private WebElementFacade linkFormulario;


    //Btn Grava
    @FindBy(xpath = "//*[ @type='submit' and @value='Gravar']")
    private WebElementFacade blnGravar;


    //txt Registro
    @FindBy(xpath = "//*[@name='rcl_cod_0']")
    private WebElementFacade btnRegistro;

    //Btn data
    @FindBy(xpath = "//*[@name='rcl_dthr_0']")
    private WebElementFacade txtData;

    //*[@id="tb1"]/tbody/tr[1]/td[1]/ul/li/div

    @FindBy(xpath = "//*[@id='tb1']/tbody/tr[1]/td[1]/ul/li/div")
    private WebElementFacade iconAnexo;

    @FindBy(xpath = "//*[@id=\"tb1\"]/tbody/tr[1]/td[5]")
    private WebElementFacade stausDoFormulario;


    //tablela 1 linnha coluna 4
  //  @FindBy(xpath = "//*[@id='tb1']//*/tr[1]//a[text()='FORMULÁRIO ANAMNESE - Aguardando aprovação ']")


    @FindBy(xpath = "//*[@id='tb1']//*/tr[1]//td[4]/a")
    private WebElementFacade nomeDoFormulario;

    //tablela 1 linnha coluna 2 hora
    @FindBy(xpath = "//*[@id='tb1']/tbody/tr[1]/td[2]")
    private WebElementFacade horaNaTabela;

    //Resolver interacao com elementos da pagina que estao dentro de frames
    public void carregarTelaInicial() {
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }

    //clicar no Formulario de anaminese
     public void clicarNoFormularioDeAnaminese() {
         linkFormulario.waitUntilPresent().click();
    }

    //clicar no Formulario de anaminese
    public void clicarBnGravar() {
        blnGravar.waitUntilPresent().click();
    }

    //txtRegistro
    public void txtRegistro(String registro) {
      //  carregarTelaInicial();
        btnRegistro.waitUntilPresent().click();
        btnRegistro.selectByVisibleText(registro);
    }

    public String DotaRegistrodata() {
        data=txtData.getValue();
        return data;
    }

    //validar status do Formulario
    public void validarStatusdoFormulario(String StatusdoFormulario) {
      try {
            nomeDoFormulario.waitUntilPresent();
            String statusAtual=nomeDoFormulario.getTextValue();

            Boolean mensagem =statusAtual.contains(StatusdoFormulario);
            Assert.assertTrue(mensagem);
            String dataDoRegistro=data.substring(11,16);
            horaNaTabela.waitUntilVisible();
            horaNaTabela.waitUntilPresent();
            horaNaTabela.waitUntilEnabled();
            horaNaTabela.wait(5000,10);
            String dataDoRegistronaTela=horaNaTabela.getTextValue();

            Assert.assertEquals(dataDoRegistro,dataDoRegistronaTela);
        } catch (Exception e) {
            Logger.getLogger("StaleElementReferenceException -elemento não encontrado: " + e.getMessage());
          Assert.assertTrue(false);
        }

    }

    //txtRegistro
    public void selecioanrAprimeiraLinhaDataTabelaNomeDoformulario() {
        nomeDoFormulario.waitUntilPresent().click();
    }

    public void validarNomeStatusIconDoformulario(String nome,String status ) {
        try {

            nomeDoFormulario.waitUntilPresent();
            String nomeDoFormularioNaPagina=nomeDoFormulario.getTextValue();
            Assert.assertEquals(nome,nomeDoFormularioNaPagina);//validação 1

            Boolean iconExiste= iconAnexo.isPresent();
            Assert.assertTrue(iconExiste);

            Boolean statusExiste= stausDoFormulario.containsText(status);
            Assert.assertTrue(statusExiste);


        } catch (Exception e) {
            Logger.getLogger("StaleElementReferenceException -elemento não encontrado: " + e.getMessage());
            Assert.assertTrue(false);
        }

    }


}
