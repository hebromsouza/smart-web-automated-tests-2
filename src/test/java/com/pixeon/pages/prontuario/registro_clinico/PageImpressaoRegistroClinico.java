package com.pixeon.pages.prontuario.registro_clinico;

import com.pixeon.pages.BasePage;
import com.pixeon.validation.ValidationHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class PageImpressaoRegistroClinico extends BasePage {
    //btnAdicionar
    @FindBy(xpath = "//*[@name='bt_rcl_add']//div[text()='Adicionar...']")
    private WebElementFacade btnAdicionar;
   //listcboAdicionarElement
    @FindBy(xpath = "//*[@class='bd']/ul/li")
    private List<WebElement> listcboAdicionarElement;

    //btn Imprimir
    @FindBy(xpath = "//*[@class=\"print\"][text()='Imprimir']")
    private WebElementFacade btnImprimir;


    
    //pagImpimir
    @FindBy(xpath = "  //*[@class='yui-module yui-overlay yui-panel']")
    private WebElementFacade pagImpimir;
    
    
    //Resolver interacao com elementos da pagina que estao dentro de frames
    public void carregarTelaInicial() {
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }

    public void clicarNoBtnAdiconarESelecionar0itemDDaPage(String campo ) {
        carregarTelaInicial();

        try {

            btnAdicionar.waitUntilPresent().click();
            String xp = "//*[//*[@class='bd']]//*//*[text()='" + campo + "']";
            getDriver().findElement(By.xpath(xp)).click();

           /*
           for (int i = 0; i <= listcboAdicionarElement.size(); i++) {
                if (listcboAdicionarElement.get(i).getText().equals(campo)) {
                    listcboAdicionarElement.get(i).click();
                    break;
                }
            }


            */
        } catch (StaleElementReferenceException e) {
            Logger.getLogger("StaleElementReferenceException -Combo Adicionar: " + e.getMessage());
            Assert.assertTrue(false);
        } catch (Exception e) {
            Logger.getLogger("StaleElementReferenceException -Combo Adicionar: " + e.getMessage());
            Assert.assertTrue(false);
        }

    }

    //clicar no Botão clicarFechar
    public void clicarImprimir() {
            btnImprimir.waitUntilPresent().click();
    }
    
    
  //validarPDF
    public void validarPDF(String txtParavalidar) {
      
    	try {
         	carregarTelaInicial();
           pagImpimir.waitUntilVisible();
           assertThat("Validando dados de versão do Software no PDF", ValidationHelper.verifyPDFContent(ThucydidesWebDriverSupport.getDriver().getCurrentUrl(), txtParavalidar));
            ThucydidesWebDriverSupport.getDriver().close();
	   } catch (Exception e) {
		   Logger.getLogger("StaleElementReferenceException -: " + e.getMessage());
		  
	   }
    }


}
