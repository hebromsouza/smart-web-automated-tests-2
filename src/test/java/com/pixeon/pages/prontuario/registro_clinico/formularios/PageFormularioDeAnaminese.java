package com.pixeon.pages.prontuario.registro_clinico.formularios;

import com.pixeon.pages.BasePage;
import com.pixeon.validation.ValidationHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;

import static org.hamcrest.MatcherAssert.assertThat;

public class PageFormularioDeAnaminese extends BasePage {
    @FindBy(id = "msgBoxDiv")
    private WebElementFacade divAguarde;

    //Botão Gravar
    @FindBy(xpath = "//*[@id='btp_gravar']//div[text()='Gravar']")
    private WebElementFacade btnGravar;

    //Botão Finalizar
    @FindBy(xpath = "//*[@id='btp_gravar']//div[text()='Finalizar']")
    private WebElementFacade btnFinalizar;




    //Botão Assinar
    @FindBy(xpath = "//*[@id='btpc_sign']/*//div[text()='Assinar...']")
    private WebElementFacade btnAssinar;

    //Txt Queixa principa
    @FindBy(xpath = "//*[@name='c00281_01q_0']")
    private WebElementFacade txtQueixaPrincipal;

    //Txt Queixa principa
    @FindBy(xpath = "//*[@id='yui-gen62']")
    private WebElementFacade txtUrl;

    //btnFechar
    @FindBy(xpath = "//*[@class='container-close']")
    private WebElementFacade btnFechar;

    //btnAprovar
    @FindBy(xpath = "//*[@id='btp_confirmar']")
    private WebElementFacade btnAprovar;




    //Resolver interacao com elementos da pagina que estao dentro de frames
    public void carregarTelaInicial() {
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }

    //clicar no Botão Gravar
    public void clicarBtnGravar() {
     //   carregarTelaInicial();
        try {
            divAguarde.waitUntilNotVisible();
            Thread.sleep( 1000);
            btnGravar.waitUntilEnabled();
            btnGravar.waitUntilPresent().click();
            Thread.sleep( 1000);
            divAguarde.waitUntilNotVisible();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //clicar no Botão clicarFechar
    public void clicarFechar() {
        try {
            divAguarde.waitUntilNotVisible();
            Thread.sleep( 2000);
            btnFechar.waitUntilPresent().click();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //clicar no Botão Aprovar
    public void clicarAprovar() {
        try {
            divAguarde.waitUntilNotVisible();
            Thread.sleep( 2000);
            btnAprovar.waitUntilPresent().click();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    //clicar no Botão Assinar
    public void clicarbtnAssinar() {
     //   carregarTelaInicial();
        divAguarde.waitUntilNotVisible();
        btnAssinar.waitUntilPresent().click();

        try {
            divAguarde.waitUntilNotVisible();
            Thread.sleep( 15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //clicar no Botão Finalizar
    public void clicarbtnFinalizar() {
        //   carregarTelaInicial();
        divAguarde.waitUntilNotVisible();
        btnFinalizar.waitUntilPresent().click();

        try {
            divAguarde.waitUntilNotVisible();
            Thread.sleep( 15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    //inserir Dado Queixa Principal
    public void inserirDadoQueixaPrincipal(String queixaPrincipal) {
       // carregarTelaInicial();
        txtQueixaPrincipal.waitUntilPresent().type(queixaPrincipal);
    }


    //validarPDF
    public void validarPDF(String txtParavalidar) {
        carregarTelaInicial();
      //  divAguarde.waitUntilNotVisible();
        assertThat("Validando dados de versão do Software no PDF", ValidationHelper.verifyPDFContent(ThucydidesWebDriverSupport.getDriver().getCurrentUrl(), txtParavalidar));
         ThucydidesWebDriverSupport.getDriver().close();


    }

}
