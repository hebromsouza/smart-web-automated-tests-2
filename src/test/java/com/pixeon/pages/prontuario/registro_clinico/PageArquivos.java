package com.pixeon.pages.prontuario.registro_clinico;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

import java.awt.*;
import java.awt.event.InputEvent;

public class PageArquivos extends BasePage {

    //Btn Adicionar Arquivos
    @FindBy(xpath = "//*[@class='bd']//*[text()='Adicionar Arquivos...']")
    private WebElementFacade btnAdicionarArquivos;

    @FindBy(xpath = "//*[@id='ged']/*[@class='arq']")
    private WebElementFacade ArquivoAdicionado;

    @FindBy(xpath = "//*[@class='container-close'][text()='Close']")
    private WebElementFacade btnFechar;

    @FindBy(xpath = "//a[text()='Clique aqui para habilitar o Flash']")
    private WebElementFacade LinkhabilitarFlash;


    //Resolver interacao com elementos da pagina que estao dentro de frames
    public void carregarTelaInicial() {

        try {
         getDriver().switchTo().defaultContent();
         getDriver().switchTo().frame("mainFrame");
         getDriver().switchTo().frame(0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void clickAdicionarArquivos() {
        carregarTelaInicial();
        try {
           getDriver().switchTo().frame(0);
           btnAdicionarArquivos.waitUntilEnabled();	
           btnAdicionarArquivos.waitUntilPresent().click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clickFechar() {
        carregarTelaInicial();
        getDriver().switchTo().parentFrame();
        btnFechar.waitUntilPresent().click();
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }
    public void validarExistenciaDoArquivo() {
        getDriver().switchTo().frame(0);
        btnAdicionarArquivos.waitUntilPresent();
        Boolean existe=ArquivoAdicionado.isPresent();
        Assert.assertTrue(existe );
    }

    public void clickLinkhabilitarFlash() {
    try {
    	 carregarTelaInicial();
         LinkhabilitarFlash.waitUntilPresent().click();
            Thread.sleep(1200);
            Robot bot=new Robot();
            bot.mouseMove(295, 165);
            bot.mouseMove(295, 165);
            bot.mouseMove(295, 165);
            bot.mouseMove(295, 165);
            bot.mousePress(InputEvent.BUTTON1_MASK);
            bot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
