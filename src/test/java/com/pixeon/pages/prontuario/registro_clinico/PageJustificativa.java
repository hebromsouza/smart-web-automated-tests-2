package com.pixeon.pages.prontuario.registro_clinico;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageJustificativa extends BasePage {

    //txtJustificativa
    @FindBy(xpath = "//*[@class='bd']//*//textarea")
    private WebElementFacade txtJustificativa;


    //btnOK
    @FindBy(xpath = "//*[@type='Submit'  and @value='OK']")
    private WebElementFacade btnOK;


    //Resolver interacao com elementos da pagina que estao dentro de frames
    public void carregarTelaInicial() {
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }

    public void inseirJustificativaEConfirmar(String justificativa) {
        carregarTelaInicial();
        txtJustificativa.waitUntilPresent().type(justificativa);
        btnOK.waitUntilPresent().click();
    }

    public void clickOK() {
        btnOK.waitUntilPresent().click();
    }
}
