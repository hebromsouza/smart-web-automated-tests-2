package com.pixeon.pages.prontuario.registro_clinico;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PagePeriodoRegistroClinico extends BasePage {

    //txtDatInicio
    @FindBy(xpath = "//*[@name='dthr_ini_0']")
    private WebElementFacade txtDatInicio;

    //txtDataFim
    @FindBy(xpath = "//*[@name='dthr_fim_0']")
    private WebElementFacade txtDataFim;

    //btnBusca
    @FindBy(xpath = "//*[@name='cb_submit_0' and @value='Buscar']")
    private WebElementFacade btnBusca;

    //btnFechar
    @FindBy(xpath = "//*[text()='Close'][@class='container-close']")
    private WebElementFacade btnFechar;


    //Resolver interacao com elementos da pagina que estao dentro de frames
    public void carregarTelaInicial() {
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }

    public void inseirDataEConfirmar(String datainicio, String dataFim) {
        carregarTelaInicial();       
    	txtDatInicio.waitUntilPresent().click(); 
    	txtDatInicio.sendKeys(datainicio);
    	txtDataFim.waitUntilPresent().click(); 
        txtDataFim.sendKeys(dataFim);
        btnBusca.click();

    }

    public void clickFechar() {
        btnFechar.waitUntilPresent().click();
    }


}
