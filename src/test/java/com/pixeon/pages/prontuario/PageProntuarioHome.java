package com.pixeon.pages.prontuario;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class PageProntuarioHome extends BasePage {

    @FindBy(xpath = "//*[@class='yui-nav']//em[text()='Início']")
    private WebElementFacade abaRegistroClinico;

    @FindBy(linkText = "Sair da internação/tratamento")
    private WebElementFacade lnkSairInternacaoTratamento;

    private String xpathAbas;

    //Resolver interacao com elementos da pagina que estao dentro de frames
    public void carregarTelaInicial() {
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }

    //Clicar na Aba Selecionada pelo Exemplo da Feature
    public void clicarAbaRegistroClinico(String abaProntuario) {
        abaRegistroClinico.waitUntilPresent();

        //Criando elemento dinâmico da aba que será clicada
        xpathAbas = "//*[@class='yui-nav']//em[text()='"+ abaProntuario +"']";
        getDriver().findElement(By.xpath(xpathAbas)).click();

    }

    public void clicarSairDaInternacaoTratamento() {
        lnkSairInternacaoTratamento.waitUntilClickable().click();
        divAguarde();
    }
}
