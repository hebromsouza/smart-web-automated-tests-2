package com.pixeon.pages.prontuario.acessoNegado;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageAcessoNegado extends BasePage {

    //txmessagem
    @FindBy(xpath = "//*[@id='msg_atencao']")
    private WebElementFacade txmessagem;

    //validar mensagem De Acesso Negado
    public void validarMensagemDeAcessoNegado(String AcessoNegado) {
        Boolean mensagem =(txmessagem.getText()).contains(AcessoNegado);
        Assert.assertTrue(mensagem);
    }

}
