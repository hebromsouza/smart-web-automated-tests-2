package com.pixeon.pages.totem;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class PageRetiradaSenha extends BasePage {

    @FindBy(className = "toque_aqui")
    private WebElementFacade btnToqueAqui;

    @FindBy(id = "msgBox")
    private WebElementFacade divMsgBox;

    public static String retiradaSenha = null;

    public void clicarBotaoToqueAqui(){
        btnToqueAqui.waitUntilVisible().click();
        divAguarde.waitUntilNotVisible();
        retiradaSenha = divMsgBox.waitUntilVisible().getText();
        closeSwitchWindow("fs");
    }

    public void clicarBotaoToqueAquiTipoAtendimento(String tipoAtendimento){
        divAguarde();
        getDriver().findElement(By.xpath("//div[@class='psv_nome' and contains(text(),'" + tipoAtendimento + "')]")).click();
        divAguarde();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
