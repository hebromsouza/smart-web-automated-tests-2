package com.pixeon.pages.totem;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageDetalhesPacienteFilaEspera extends BasePage {

    @FindBy(name = "fle_fle_status_0")
    private WebElementFacade cboStatusFila;

    @FindBy(className = "save")
    private WebElementFacade btnGravar;

    public void selecionarStatusPacienteFilaDeEspera(String status){
        cboStatusFila.click();
        cboStatusFila.selectByVisibleText(status);
    }

    public void clicarBotaoGravar(){
        btnGravar.click();
        divAguarde();
    }

}
