package com.pixeon.pages.totem;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;

//@DefaultUrl("http://localhost/smartweb/totem/start.asp")
public class PageTotem extends BasePage {

    @FindBy(linkText = "Buscar setor...")
    private WebElementFacade lnkBuscarSetor;

    @FindBy(name = "tab_nome_0")
    private WebElementFacade lblNomeSetor;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnBuscar;

    @FindBy(linkText = "Selecionar Médico")
    private WebElementFacade lnkSelecionarMedico;

    @FindBy(name = "cb_retirada")
    private WebElementFacade btnRetiradaSenha;

    @FindBy(id = "dw_tot_fila_summary")
    private WebElementFacade btnTotFilaSumary;

    @FindBy(xpath = "//*[@id='dw_tot_fila_summary']/input[@value='Abrir Painel de CHAMADA de Senha']")
    private WebElementFacade btnChamadaSenha;

    @FindBy(xpath = "//*[@id='dw_tot_fila_detail_1']/input[@value='ADRIANO EXECUTANTE']")
    private WebElementFacade adrianoExecutante;


    @FindBy(name = "t_2_0")
    private WebElementFacade btnTipoAtendimento;

    @FindBy(name = "compute_2_1")
    private WebElementFacade btnSelecionarMedico2;

    @FindBy(name = "compute_2_2")
    private WebElementFacade btnSelecionarMedico3;

    @FindBy(name = "compute_2_3")
    private WebElementFacade btnSelecionarMedico4;

    @FindBy(name = "t_2_1")
    private WebElementFacade btnTipoAtendimento2;

    @FindBy(name = "t_2_2")
    private WebElementFacade btnTipoAtendimento3;

    @FindBy(name = "t_2_3")
    private WebElementFacade btnTipoAtendimento4;

    public void clicarLupaBuscarSetor(){
        lnkBuscarSetor.click();
        lblNomeSetor.waitUntilVisible();
    }

    public void escreverSetor(String setor) {
        lblNomeSetor.type(setor);
    }

    public void clicarBotaoBuscarSetores(){
        btnBuscar.click();
        divAguarde();
    }

    public void clicarLinkSetor(String setor){
        getDriver().findElement(By.linkText(setor)).click();
    }

    public void clicarLupaSelecionarMedico(){
        lnkSelecionarMedico.click();
    }

    public void clicarLupaMedico2() {
        btnSelecionarMedico2.click();
    }

    public void clicarLupaMedico3() {
        btnSelecionarMedico3.click();
    }

    public void clicarLupaMedico4() {
        btnSelecionarMedico4.click();
    }

    public void clicarBotaoRetiradaSenha(){
        btnRetiradaSenha.isPresent();{
            btnRetiradaSenha.click();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            switchWindow("fs");
        }

    }

    public void validarTelaTotemAberta(){

        String totem = getDriver().getTitle();

        if(totem.equals("Totem")){
            totem.contains("Totem");
            //Nao faz nada, pois já está na tela de TOTEM
        } else{
            Assert.fail("BUG ENCONTRADO, TOTEM NÃO EXISTE, OU NÃO ENCONTRADO");
        }


    }

    public void acessarSmartWeb() {
        getDriver().switchTo().window(getDriver().getWindowHandle());
    //    getDriver().get("http://localhost/smartweb/");
        carregarFrameDefault();
    }

    public void clicarBotaoChamadaSenha(){
        if(btnChamadaSenha.isPresent()) {
            btnChamadaSenha.click();
         //   getDriver().switchTo().window("fs");
        }
    }

    public void ValidacaoSenhasRetiradasSemErros(){


       //Complicado Validar senhas retiradas sem erros e uma interface dinamica..>Validando se voltou a tela do totem apos retirar senha. Se voltou, deduz-se que tudo ocorreu bem.
        if(adrianoExecutante.isPresent()){
            adrianoExecutante.getTextValue().equals("ADRIANO EXECUTANTE");

        } else {
            Assert.fail("BUG ENCONTRADO: SENHAS NÃO FORAM GERADAS, ou HOUVE ALGUM PROBLEMA. POR FAVOR, ANALISE E IDENTIFIQUE");
        }
    }


    public void validarChamadaSenhaPainel(String retiradaSenha) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(waitHelper.existsElement(By.xpath("//div[@id='cur_nome' and contains(text(), '" + retiradaSenha + "')]")));
    }

    public void validarChamadaSenhaPainelMedico(String medico){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(waitHelper.existsElement(By.xpath("//div[@id='cur_medico' and contains(text(), '" + medico + "')]")));
    }

    public void clicarLupaTipoAtendimento() {
        btnTipoAtendimento.click();
    }

    public void clicarLupaTipoAtendimento2() {
        btnTipoAtendimento2.click();
    }

    public void clicarLupaTipoAtendimento3() {
        btnTipoAtendimento3.click();
    }

    public void clicarLupaTipoAtendimento4() {
        btnTipoAtendimento4.click();
    }

    public void clicarTipoAtendimento(String tipoAtendimento) {
        getDriver().findElement(By.linkText(tipoAtendimento)).click();
    }

}