package com.pixeon.pages.atendimento.entregaDeLaudos;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageEntregaLaudos extends BasePage {

    @FindBy(xpath = "//span[contains(text(),'Liberado')]")
    private WebElementFacade spanLiberado;

    @FindBy(xpath = "//span[contains(text(),'Entregue')]")
    private WebElementFacade spanEntregue;

    @FindBy(id = "bt_laudos")
    private WebElementFacade btnImprimirLaudos;

    @FindBy(id = "msgBoxDiv")
    private WebElementFacade divAguarde;

    @FindBy(id = "bt_print")
    private WebElementFacade btnImprimir;

    @FindBy(id = "txtp_string")
    private WebElementFacade lblNomePessoaEntrega;

    @FindBy(className = "bt")
    private WebElementFacade btnOk;

    @FindBy(linkText = "Entrega de Resultado")
    private WebElementFacade lnkEntregaDeResultado;

    @FindBy(id = "bt_entrega")
    private WebElementFacade btnRegistrarEntrega;

    @FindBy(xpath = "//*[contains(text(),'LUANA OLIVEIRA DE SOUZA')]")
    private WebElementFacade EntregaALuanaOliveira;



    @FindBy(xpath = "//a[contains(text(),'Close')]")
    private WebElementFacade lnkClose;

    @FindBy(name = "smm_smm_cml_qt_0")
    private WebElementFacade chkItem;

    private StringBuilder verificationErros = new StringBuilder();

    static String nomePessoaEntregaExame = null;

    public void verificarStatusExame(String status){
        try{
            if(status.equals("Liberado")) {
                Assert.assertEquals(spanLiberado.getText(), status);
            }else if(status.equals("Entregue")){
                Assert.assertEquals(spanEntregue.getText(), status);
            }
        }catch (Error e){
            verificationErros.append(e.toString());
        }
    }

    public void clicarBotaoImprimirLaudos() {
        if(btnImprimirLaudos.isPresent()) {
            btnImprimirLaudos.click();
            divAguarde();
            divAguarde.waitUntilNotVisible();
        }
    }

    public void clicarBotaoImprimir() {
        if(btnImprimir.isPresent()) {
            btnImprimir.click();
            divAguarde(); divAguarde();
        }
    }

    public void digitarNomePessoaEntrega(String nomePessoaEntrega){
        if(lblNomePessoaEntrega.isPresent()){
            lblNomePessoaEntrega.getTextValue().equals(nomePessoaEntrega);
            lblNomePessoaEntrega.type(nomePessoaEntrega);
        }else{
            nomePessoaEntregaExame = nomePessoaEntrega;
        }
    }

    public void clicarBotaoOk(){
        if(btnOk.isPresent()) {
            btnOk.click();
            divAguarde();
        }

        verificarAlerta();

    }

    public void verificarAlerta(){
        try {
            Thread.sleep(1000);
            boolean alert = waitHelper.alertGetText().contains("Tempo limite estourado para execução do processo");

            if(alert==true){
                waitHelper.waitAlertOk();

                //Repete operacao
                clicarBotaoImprimir();
                clicarBotaoOk();
                Thread.sleep(1000);
                fecharTelaImpressao();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void fecharTelaImpressao(){

        if(lnkClose.isPresent()) {
            lnkClose.click();
            if(lnkClose.isPresent()) {
                lnkClose.click();
            }

        }
    }

    public void clicarLinkEntregaDeResultado(){
        fecharTelaImpressao();
        if(lnkEntregaDeResultado.isPresent()) {
            lnkEntregaDeResultado.click();
        }
    }

    public void clicarBotaoRegistrarEntrega(){
        if(chkItem.isPresent()) {
            chkItem.click();
            btnRegistrarEntrega.click();
        }
    }

    public boolean validarNomePessoaEntrega(){

        if(EntregaALuanaOliveira.getTextValue().equals("LUANA OLIVEIRA DE SOUZA")){
            return true;
        } else {
            return false;
        }

    }

}
