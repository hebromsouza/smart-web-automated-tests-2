package com.pixeon.pages.atendimento.orcamentoEPreAtendimento;

import com.pixeon.pages.BasePage;
import com.pixeon.pages.paciente.PageBuscarPaciente;
import com.pixeon.util.SeleniumUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.awt.*;
import java.text.DecimalFormat;

public class PageOrcamentoEPreAtendimento extends BasePage {

    @FindBy(id = "bt_gravar")
    private WebElementFacade btnGravar;

    @FindBy(xpath = "//*[@id=\"sle_smkcod\"]")
    private WebElementFacade txtItem;

    @FindBy(xpath = "//*[@id='formsmk']/table/tbody/tr[1]/td[4]/button/div")
    private WebElementFacade btnBuscar;

    @FindBy(xpath = "//*[@id='formsmk']/table/tbody/tr[1]/td[4]/button/div")
    private WebElementFacade btnBuscarServico;


    @FindBy(xpath = "//*[@id=\"formsmk\"]/table/tbody/tr[1]/td[4]/button/div")
    private WebElementFacade btnBuscar2;

    @FindBy(name = "smk_cod_filtro_0")
    private WebElementFacade campodeBusca;

    @FindBy(xpath = "//*[@id='dw_filtro_detail_0']/input[1]")
    private WebElementFacade botaoBuscarServico;



    @FindBy(xpath = "//*[contains(text(),'GLICOSE (URINA) 24 H (GLIU)')]")
    private WebElementFacade glicoseUrina;

    @FindBy(xpath = "//*[@id='bt_incluir']")
    private WebElementFacade btnIncluir;


    @FindBy(id = "ck_iop_all")
    private WebElementFacade allItensCheck;

    @FindBy(id = "btnIncluirSmk")
    private WebElementFacade btnIncluirItem;

    @FindBy(id = "bt_excluir")
    private WebElementFacade btnExcluir;

    @FindBy(xpath = "(//div[@id='bd-main']/h2)[2]")
    private WebElementFacade lblMenu;

    @FindBy(name = "orp_orp_cnv_cod_0")
    private WebElementFacade txtConvenio;

    @FindBy(xpath = "//*[@id=\"dw_orp_detail_0\"]/select[1]/option[5]")
    private WebElementFacade convenioParticular;

    @FindBy(xpath = "//*[@id=\"dw_orp_detail_0\"]/select[1]/option[2]")
    private WebElementFacade bradescoSaude;

    @FindBy(xpath = "//*[@id=\"dw_orp_detail_0\"]/select[1]/option[9]")
    private WebElementFacade sulAmericaSaude;

    @FindBy(xpath = "//*[@id=\"dw_orp_detail_0\"]/select[1]/option[10]")
    private WebElementFacade ConvenioSus;

    @FindBy(id = "msgErro")
    private WebElementFacade lblErro;

    @FindBy(name = "orp_orp_cnv_cod_0")
    private WebElementFacade lblConvenio;

    @FindBy(name = "iop_iop_valor_0")
    private WebElementFacade txtValor;

    @FindBy(name = "marcado_0")
    private WebElementFacade lblItemMarcado;

    @FindBy(name = "iop_iop_smk_cod_1")
    private WebElementFacade lblCodigoItem;

    @FindBy(name = "iop_iop_valor_1")
    private WebElementFacade lblValorCoper;

    @FindBy(id = "bt_gerar_os")
    private WebElementFacade btnGerarOS;

    @FindBy(name = "orp_orp_tdc_cod_0")
    private WebElementFacade cboDesconto;

    @FindBy(name = "orp_orp_desconto_0")
    private WebElementFacade lblDesconto;

    @FindBy(name = "orp_orp_desconto_0")
    private WebElementFacade txtDesconto;


    @FindBy(xpath = "//*[@id='dw_iop_summary']/span[9]")
    private WebElementFacade txtValorComDesconto;

    @FindBy(xpath = "//*[@id='dw_iop_summary']/span[7]")
    private WebElementFacade txtValorComDescontoPreAtendimento;

    @FindBy(id = "bt_gerar_pre")
    private WebElementFacade btnGerarPreAtendimento;

    @FindBy(xpath = "//h2[contains(text(), 'Pré-Atendimento')]")
    private WebElementFacade txtTituloPreAtendimento;

    @FindBy(xpath = "//*[@id='a_novo']/div")
    private WebElementFacade iconAdicionarOS;


    @FindBy(xpath = "//*[@id='secDadosSmm']/div[2]/span/button[1]/div")
    private WebElementFacade btnConcluir;


    @FindBy(xpath = "//*[contains(text(),'Close')]")
    private WebElementFacade closeJanelas;

    @FindBy(xpath = "//*[@id=\"_panel0_h\"]")
    private WebElementFacade telaQuestionario;


    @FindBy(id = "cheq1")
    private WebElementFacade check1;

    @FindBy(xpath = "//*[@id='ck_smm_all']")
    private WebElementFacade checkAll;

    @FindBy(id = "cheqpaipend")
    private WebElementFacade checkAllItens2;

    @FindBy(id = "btnConfirmar")
    private WebElementFacade btnConfirmar;


    @FindBy(xpath = "//a[@name='t_8_0']")
    private WebElementFacade OrdemServicoLk;

    private WebElementFacade lnkOrdemServico;

    public static String ordemServicoGeradoOrcamento;
    public static String valorComDesconto;

    public void clicarBtnGravar (){
        if(btnGravar.isPresent()) {
            btnGravar.waitUntilClickable().click();
            divAguarde();
        }
    }

    public void escreverItem (String campoItem){
        divAguarde();
        if(txtItem.isVisible()) {
            txtItem.type(campoItem);
            divAguarde();
        }
 //       txtItem.type(String.valueOf(Keys.TAB));
    }

    public void fecharJanelasAbertas(){

        if (closeJanelas.isVisible() && closeJanelas.isPresent()) {
            closeJanelas.click();
        }
    }

    public void AdicionarNovaOS(){
       if(iconAdicionarOS.isPresent()){
           iconAdicionarOS.click();
       }
        fecharJanelasAbertas();

    }

    public void buscarItemServico(String item){
        campodeBusca.type(item);
        divAguarde();divAguarde();
        if(glicoseUrina.isPresent()) {
            glicoseUrina.click();
        }
      //  campodeBusca.type(String.valueOf(Keys.TAB));

        if(btnBuscar.isPresent()) {
            btnBuscar.click();
            divAguarde();
        }
    }

    public void IncluirItem(){
        btnIncluirItem.click();
        divAguarde();
    }

    public void AdicionarItemNaOS1(){
        clicarBtnGravar();

       // buscarItemServico();

        IncluirItem();

        AlertMensagemGlicoOk();

        ConfirmarInlusaoItem();

        ConcluirLancamentoItemOS();

        ExcluirQuestionario();
    }

    public void BuscarServico(String item){
      if(campodeBusca.isPresent()){
          campodeBusca.type(item);
        //  divAguarde();
          botaoBuscarServico.click();
      }

    }

    public void AdicionarItemNaOS(String item){
        clicarBtnGravar();

        buscarItemServico(item);

        IncluirItem();

        AlertMensagemGlicoOk();

        ConfirmarInlusaoItem();

        ConcluirLancamentoItemOS();

        ExcluirQuestionario();
    }

    public void ExcluirQuestionario(){
        divAguarde();
        if(telaQuestionario.isPresent()){
            telaQuestionario.click();

            if(btnExcluir.isPresent()) {
                btnExcluir.click();
                confirmarAlerta();
            }
        }
        divAguarde();
    }

    public void ConfirmarInlusaoItem() {
        divAguarde();
            if(check1.isPresent()){
                check1.click();
            } else if (checkAll.isPresent()){
                checkAll.click();
            }
            else {
                checkAllItens2.click();
            }

        divAguarde();
        clicarBtnConfirmar();

    }

    public void ConcluirLancamentoItemOS(){
        divAguarde();
        if(btnConcluir.isPresent()) {
            btnConcluir.waitUntilClickable().click();
        }
    }

    public void AlertMensagemGlicoOk(){

        try {

            boolean alert = waitHelper.alertGetText().contains("GLICO");

            if(alert==true){
                 waitHelper.waitAlertOk();
            }
        } catch (Exception e) {
            System.out.println("           ");
        }
    }

    public void clicarBtnConfirmar(){
        divAguarde();
        if(btnConfirmar.isPresent()) {
            btnConfirmar.waitUntilClickable().click();
        } else {
            btnConcluir.click();
        }
    }

    public void clicarBtnBuscar(){
        if(btnBuscar.isPresent()) {
            btnBuscar.waitUntilClickable().click();
        }
    }


    public void clicarBtnBuscarServico(){
        if(btnBuscarServico.isPresent()) {
            btnBuscarServico.waitUntilClickable().click();
        }
    }
    public void clicarBtnIncluir(){

        try {
            if(btnIncluir.isVisible()) {
                btnIncluir.click();
                divAguarde();
            }

            if(allItensCheck.isPresent()){
                allItensCheck.click();
            }


        } catch (Exception e) {
          System.out.println(e);
        }


    }

    public void validaSubMenu(String subMenu){
        lblMenu.shouldContainOnlyText(subMenu);
    }

    public void confirmarAlerta(){
        waitHelper.alertOk();
    }


    public void AlterarConvenio(String convenio){
        if(convenio.equals("PARTICULAR")){
            if(txtConvenio.isPresent()){
                txtConvenio.click();
                convenioParticular.click();
            }
        } else if (convenio.equals("BRADESCO SAUDE")){
            if(txtConvenio.isPresent()){
                txtConvenio.click();
                bradescoSaude.click();
            }
        }else if (convenio.equals("SULAMERICA SAUDE")){


            if(txtConvenio.isPresent()){
                txtConvenio.selectByVisibleText("SULAMERICA SAUDE");
             //   txtConvenio.click();
             //   sulAmericaSaude.click();
            }
        }else if (convenio.equals("SUS")){
            if(txtConvenio.isPresent()){

              //  txtConvenio.click();
                txtConvenio.selectByVisibleText(convenio);
               // ConvenioSus.click();

            }
        }

        clicarBtnGravar();

    }

    public void
    ConfirmarConvenio(String convenio){
        if(convenioParticular.getText().equals(convenio)){
            SystemColor.GREEN.brighter();
        //    System.out.println("Convenio Confirmado");
            Assert.assertEquals("PARTICULAR", convenioParticular.getText());

        } else if(bradescoSaude.getText().equals("BRADESCO SAUDE")){
            SystemColor.GREEN.brighter();
         //   System.out.println("Convenio Confirmado");
            Assert.assertEquals("BRADESCO SAUDE", bradescoSaude.getText());

       // } else if(sulAmericaSaude.getText().equals("SULAMERICA SAUDE")){
        } else if(sulAmericaSaude.getText().equals("SULAMERICA SAUDE")){
            SystemColor.GREEN.brighter();
          //  System.out.println("Convenio Confirmado");
            Assert.assertEquals("SULAMERICA SAUDE", sulAmericaSaude.getText());

        }else if(ConvenioSus.getText().equals("SUS")){
            SystemColor.GREEN.brighter();
          //ystem.out.println("Convenio Confirmado");
            Assert.assertEquals("SUS", ConvenioSus.getText());

        }
        else {
            SystemColor.RED.brighter().getRed();
            System.out.println("BUG ENCONTRADO. Convenio do Orçamento não é diferente do convenio do Paciente.");
        }



    }

    public String obterTxtConvenio(){
        return txtConvenio.getText();
    }

    public boolean validarSeTxtConvenioEstaPresente(){ return txtConvenio.isPresent(); }

    public String obterTxtErro(){
        return lblErro.getText();
    }

    public boolean validarTxtDoLocatorVazio(){ return txtConvenio.getText().isEmpty(); }

    public void selecionarConvenio(String convenio){
        divAguarde();
        if(lblConvenio.isVisible()) {
            lblConvenio.waitUntilClickable().selectByVisibleText(convenio);
            lblConvenio.selectByVisibleText(convenio);

        }
    }

    public String obterValueTxtValor(){
        return txtValor.getAttribute("value");
    }

    public void clicarBotaoGerarOrdemDeServico(){
        if(btnGerarOS.isPresent()) {
            btnGerarOS.waitUntilClickable().click();
            divAguarde();
        }
    }

    public boolean validarSeLblItemMarcadoEstaPresente(){
        return lblItemMarcado. isCurrentlyVisible();
    }

    public void validarInclusaoItemCustoOperacional(String coper){
        Assert.assertEquals(lblCodigoItem.waitUntilVisible().getTextValue(), coper);
    }

    public void validarCustoOperacionalComCotacaoConvenio(Float custoOperacional, Float cotacao, String convenio){
        DecimalFormat formatter = new DecimalFormat("#.00");
        Assert.assertEquals(String.valueOf(formatter.format(custoOperacional*cotacao)), lblValorCoper.waitUntilVisible().getValue());
    }

    public void validarValorItemComCustoOperacionalAgregado(Float valorItem, Float custoOperacional, Float cotacao){
        DecimalFormat formatter = new DecimalFormat("#.00");
        Assert.assertEquals(String.valueOf(formatter.format(valorItem+custoOperacional*cotacao)),txtValor.waitUntilVisible().getValue());
    }

    public void validarInexistenciaServicoCoper(String coper){
        Assert.assertFalse(lblCodigoItem.isVisible());
    }

    public void selecionarDesconto(String desconto) {
        cboDesconto.click();
        cboDesconto.selectByVisibleText(desconto);
        divAguarde();
    }

    public void validarDescontoAplicado() {
        DecimalFormat formatter = new DecimalFormat("#.00");
        float desconto = Float.parseFloat(txtDesconto.getValue().replaceAll(",", "."));

        if(txtDesconto.getValue().equals("5,00")){
            System.out.println("Desconto de "+ txtDesconto +" aplicado");
        } else if(txtDesconto.getValue().equals("20,00")){
            System.out.println("Desconto de "+ txtDesconto +" aplicado");
        }
        else{
            System.out.println("BUG ENCONTRADO, Desconto não aplicado. O Resultado Obtido é diferente do resultado esperado.");
        }
    }

    public void clicarBotaoGerarPreAtendimento() {
        btnGerarPreAtendimento.click();
        divAguarde();
    }

    public void validarTelaPreAtendimento(){
        Assert.assertTrue(txtTituloPreAtendimento.isVisible());
    }

    public void inserirPercentualDesconto(String percentual) {
        lblDesconto.typeAndTab(percentual);
        if (!SeleniumUtil.isAlertPresent()) divAguarde();
    }

    public void validarOrcamentoPreAtendimento() {
        Assert.assertTrue(waitHelper.existsElement(By.xpath("//span[contains(text(), '"+ PageBuscarPaciente.numeroOrp +"')]")));
    }

    public void pegarNumeroOrdemServicoGeradaOrcamento() {
        ordemServicoGeradoOrcamento = lnkOrdemServico.getText().substring(4);
    }

    public void clicarLinkOrdemDeServiço(){
        if(OrdemServicoLk.isPresent()){
            OrdemServicoLk.click();
        }
     //   pegarNumeroOrdemServicoGeradaOrcamento();
      //  lnkOrdemServico.click();
     //   divAguarde();
    }

}
