package com.pixeon.pages.atendimento.filaDeEspera;

import com.pixeon.pages.BasePage;
import com.pixeon.util.SeleniumUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;

public class PageFilaDeEspera extends BasePage {

    @FindBy(linkText = "Fila de Espera")
    private WebElementFacade lnkFilaDeEspera;

    @FindBy(xpath = "//*[@id='yui-gen16']/a")
    private WebElementFacade menuFilaEspera;

    @FindBy(name = "e_dtini_0")
    private WebElementFacade lblDataInicial;

    @FindBy(name = "e_dtfim_0")
    private WebElementFacade lblDataFinal;

    @FindBy(name = "e_npsvcod_0")
    private WebElementFacade cboFila;

    @FindBy(id = "bt_search")
    private WebElementFacade btnBuscar;

    @FindBy(name = "msgDivBox")
    private WebElementFacade divAguarde;

    @FindBy(name = "compute_7_0")
    private WebElementFacade lnkNomePaciente;

    @FindBy(name = "compute_7_1")
    private WebElementFacade lnkNomePacienteFinalFila;

    @FindBy(id = "bt_add_pac")
    private WebElementFacade btnAdicionarNaFila;

    @FindBy(name = "fle_fle_psv_cod_0")
    private WebElementFacade cboFilaMedico;

    @FindBy(id = "bt_gravar_fila")
    private WebElementFacade btnGravar;

    @FindBy(name = "fle_fle_str_cod_0")
    private WebElementFacade cboRecepcao;

    @FindBy(xpath = "//*[@id='dwp_fle_detail_0']/span/input[@value='A']")
    private WebElementFacade radAntesDe;

    @FindBy(name = "fle_fle_ordem_0")
    private WebElementFacade cboOrdemFila;

    @FindBy(xpath = "//*[@id='dw_fila_espera_groupheader1_0']/span")
    private WebElementFacade txtQuantidadePacientesFila;

    @FindBy(linkText = "Enviar Mensagem")
    private WebElementFacade lnkEnviarMensagem;

    @FindBy(linkText = "Detalhes")
    private WebElementFacade lnkDetalhes;

    @FindBy(id = "bt_chamar_proximoTop")
    private WebElementFacade btnChamarProximo;

    @FindBy(name = "fle_fle_bip_0")
    private WebElementFacade txtSenhaBip;

    @FindBy(name = "pac_nome_0")
    private WebElementFacade lblNomePaciente;

    public static String proximaSenha = null;

    public void clicarLinkFilaDeEspera(){
        if (lnkFilaDeEspera.isVisible())lnkFilaDeEspera.waitUntilClickable().click();
    }

    public void aceitarAlertaInicialDaFila(){
        if (SeleniumUtil.isAlertPresent()){
            if(waitHelper.waitAlertGetText().equals("É necessário informar a fila.")) waitHelper.alertOk();
        }
    }


    public void SelecionarMenuFilaEspera(String menuFilaEspera){
        if(menuFilaEspera.equals("Fila de Espera")) {
            if (lnkFilaDeEspera.isPresent()) {
                lnkFilaDeEspera.click();
            }
        }
    }

    public void digitarDataInicial(String dtInicial){
        lblDataInicial.waitUntilClickable().click();
        lblDataInicial.clear();
        lblDataInicial.type(dtInicial);
    }

    public void digitarDataFinal(String dtFinal){
        lblDataFinal.waitUntilClickable().click();
        lblDataFinal.clear();
        lblDataFinal.type(dtFinal);
    }

    public void selecionarFila(String fila){
        cboFila.click();
        cboFila.selectByVisibleText(fila);
        divAguarde.waitUntilNotVisible();
    }

    public void clicarBotaoBuscar(){
        btnBuscar.click();
    }

    public void clicarBotaoAdicionarNaFila(){
        btnAdicionarNaFila.waitUntilClickable().click();
        divAguarde.waitUntilNotVisible();
    }

    public void selecionarFilaMedico(String filaMedico){
        cboFilaMedico.waitUntilClickable().click();
        cboFilaMedico.selectByVisibleText(filaMedico);
    }

    public void selecionarRecepcao(String recepcao){
        if(cboRecepcao.isVisible()) {
            cboRecepcao.click();
            cboRecepcao.selectByVisibleText(recepcao);
        }else{
            Assert.assertEquals(getDriver().findElement(By.xpath("//*[@id=\'dwp_fle_detail_0\']/span[contains(text(), 'CONSULTORIOS')]")).getText(), recepcao);
        }
    }

    public void clicarRadioNaFrenteDe(){
        radAntesDe.click();
    }

    public void selecionarOrdemDoPacienteDaFila(String paciente){
        cboOrdemFila.click();
        cboOrdemFila.selectByVisibleText(paciente);
    }

    public void clicarBotaoGravar(){
        btnGravar.click();
        divAguarde.waitUntilNotVisible();
    }

    public void validaPacienteFilaEspera(String paciente){
        //Assert.assertEquals(lnkNomePaciente.getText(), paciente);
        Assert.assertTrue(waitHelper.existsElement(By.linkText(paciente)));
    }

    public void validarPacienteFinalFilaEspera(String paciente){
        Assert.assertEquals(lnkNomePacienteFinalFila.getText(), paciente);
    }

    public boolean retornarVisibilidadeDePacienteNaFilaDoMedico(){
        return lnkNomePaciente.isVisible();
    }

    public String retornaSenhaGeradaNaRetiradaDeSenhaDoTotem(String retiradaSenha){
        return retiradaSenha.substring(retiradaSenha.indexOf("V"));
    }

    public void validarInclusaoSenhaFilaEspera(String retiradaSenha, String medico) {
        retiradaSenha = retornaSenhaGeradaNaRetiradaDeSenhaDoTotem(retiradaSenha);
        Assert.assertTrue(waitHelper.existsElement(By.xpath("//div[contains(text(), '"+ retiradaSenha +" ("+ medico +")')]")));
    }

    public boolean retornaVisibilidadeSenhaGeradaFilaEspera(String retiradaSenha){
        retiradaSenha = retornaSenhaGeradaNaRetiradaDeSenhaDoTotem(retiradaSenha);
        return waitHelper.existsElement(By.xpath("//div[contains(text(), '"+ retiradaSenha +"')]"));
    }

    public void validarQuantidadePacientesFilaDeEspera() {
        Number qtdPaciente = getDriver().findElements(By.xpath("//*[@name='dw_fila_espera_dataForm']/span")).size() - 3;
        Assert.assertEquals("Esperando... "+ qtdPaciente +" Paciente(s)...", txtQuantidadePacientesFila.getText());
    }

    public void clicarSenhaGeradaNoTotem(String retiradaSenha) {
        retiradaSenha = retornaSenhaGeradaNaRetiradaDeSenhaDoTotem(retiradaSenha);
        getDriver().findElement(By.xpath("//div[contains(text(), '"+ retiradaSenha +"')]")).click();
    }

    public void clicarPacienteFilaDeEspera(String paciente){
        getDriver().findElement(By.xpath("//div[contains(text(), '"+ paciente +"')]")).click();
    }

    public void clicarEnviarMensagem() {
        lnkEnviarMensagem.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clicarLinkDetalhes() {
        lnkDetalhes.click();
    }

    public void validarQuantidadeSenhasFilasDeEspera() {
        Assert.assertEquals("Esperando... 11 Paciente(s)...", txtQuantidadePacientesFila.getText());
    }

    public void clicarLinkNomePaciente(){
        lnkNomePaciente.click();
    }

    public void clicarBotaoChamarProximo(){
        proximaSenha = txtSenhaBip.getText();
        btnChamarProximo.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
