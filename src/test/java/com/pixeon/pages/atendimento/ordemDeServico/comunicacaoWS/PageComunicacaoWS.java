package com.pixeon.pages.atendimento.ordemDeServico.comunicacaoWS;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PageComunicacaoWS extends BasePage {

    @FindBy(name = "mdl_file_id_to_0")
    private WebElementFacade linkXmlEnviado;

    @FindBy(name = "mdl_file_id_from_0")
    private WebElementFacade linkXmlRetornado;

    @FindBy(id = "msgBoxDiv")
    private WebElementFacade divAguarde;

    @FindBy(id = "msgBox")
    private WebElementFacade divConectando;

    @FindBy(id = "dwp_mdl_detail_0")
    private WebElementFacade spanConteudoXml;

    public void clicarLinkXmlEnviado(){
        linkXmlEnviado.waitUntilClickable().click();
    }

    public void clicarLinkXmlRetornado(){
        linkXmlRetornado.waitUntilClickable().click();
    }

    public void validarConexaoWebServiceLaboratorio(String wsLaboratorio){
        assertEquals(divConectando.waitUntilVisible().getText(), wsLaboratorio);
//        divConectando.waitUntilVisible().getText().contains(wsLaboratorio);
    }

    public void validarXmlEnvio(){
        assertTrue(spanConteudoXml.waitUntilVisible().containsText("</amostra>"));
    }

    public void validarXmlRetorno(){
        assertTrue(spanConteudoXml.waitUntilVisible().containsText("</layoutetiqueta>"));
    }

}
