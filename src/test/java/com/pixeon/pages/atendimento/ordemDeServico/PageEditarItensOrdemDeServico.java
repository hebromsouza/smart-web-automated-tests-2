package com.pixeon.pages.atendimento.ordemDeServico;

import com.pixeon.pages.BasePage;
import com.pixeon.util.SeleniumUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.Alert;

import java.text.DecimalFormat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class PageEditarItensOrdemDeServico extends BasePage {

    @FindBy(name = "marcado_0")
    private WebElementFacade checkItemMarcado;

    @FindBy(xpath = "//input[@value='A']")
    private WebElementFacade checkItemDesmarcado;

    @FindBy(id = "acaoItem")
    private WebElementFacade selecionarAcao;

    @FindBy(name = "mot_cod_0")
    private WebElementFacade selecionarMotivo;;

    @FindBy(name = "obs_0")
    private WebElementFacade descreverMotivo;

    @FindBy(id = "bt_mot_ok")
    private WebElementFacade botaoOk;

    @FindBy(xpath = "//*[@id='acaoItem']/option[@value='CITEM']")
    private WebElementFacade itemCancelarItem;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[11]/a")
    private WebElementFacade valorTotalComDesconto;

    @FindBy(xpath = "//*[@id='acaoItem']/option[@id='o_cguia']")
    private WebElementFacade itemCancelarGuia;

    @FindBy(xpath = "//*[@id='acaoItem']/option[@value='INTERNO']")
    private WebElementFacade itemInternoInverter;

    @FindBy(xpath = "//*[@id='acaoItem']/option[@value='PEND']")
    private WebElementFacade itemPendenciaInverter;

    @FindBy(xpath = "//*[@id='acaoItem']/option[@value='URG']")
    private WebElementFacade itemUrgenteInverter;

    @FindBy(xpath = "//*[@id='acaoItem']/option[@value='RP']")
    private WebElementFacade itemResultadoParcial;

    @FindBy(xpath = "//*[@id='acaoItem']/option[@value='DTCOL']")
    private WebElementFacade itemDtColeta;

    @FindBy(xpath = "//*[@id='acaoItem']/option[@value='DTRES']")
    private WebElementFacade itemDtResultado;

    @FindBy(xpath = "//*[@id='ck_smm_all']")
    private WebElementFacade checkAllItensSelecionados;

    @FindBy(xpath = "//*[@id='acaoItem']/option[@value='STREXEC']")
    private WebElementFacade alterarSetorExecutante;

    @FindBy(linkText = "Concluir")
    private WebElementFacade concluirItem;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[1]")
    private WebElementFacade statusItem;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[3]")
    private WebElementFacade internoResultadoInverter;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[4]")
    private WebElementFacade amostraPendente;

    @FindBy(linkText = "GLICOSE (GLICEMIA)")
    private WebElementFacade textGlicose;

    @FindBy(name = "dt_entrada_0")
    private WebElementFacade dataEntrada;

    @FindBy(xpath = "//*[@id='dwp_dthr_detail_0']/input[2]")
    private WebElementFacade btOkDataColeta;


    @FindBy(name = "cb_submit_0")
    private WebElementFacade botaoOkBuscar;

    @FindBy(name = "smm_smm_dt_result_0")
    private WebElementFacade campoResultado;

    @FindBy(name = "tab_nome_0")
    private WebElementFacade fieldNomeSetor;

    @FindBy(name = "BIO")
    private WebElementFacade setorNomeBIO;

    @FindBy(name = "ALM")
    private WebElementFacade setorNomeALM;

    @FindBy(name = "str_nome_1")
    private WebElementFacade setorBioquimica;

    @FindBy(name = "str_nome_0")
    private WebElementFacade setorAlmoxarifado;

    @FindBy(xpath = "//*[@id='a_edit-itens']")
    private WebElementFacade EditarItens;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[5]/a")
    private WebElementFacade detalhesItem;

    @FindBy(name = "smm_smm_str_0")
    private WebElementFacade setorExecutanteTelaDestalhesItem;

    @FindBy(linkText = "Concluir")
    private WebElementFacade btnConcluir;

    @FindBy(id = "bt_lanc-itens")
    private WebElementFacade novoLancamentoDeItem;

    @FindBy(id = "bt_button_ok")
    private WebElementFacade btnOkAutorizacao;

    @FindBy(id = "bt_excluir")
    private WebElementFacade btnExcluirQuestionario;

    @FindBy(id = "bt_lanc-itens")
    private WebElementFacade btnLancamentoItens;

    @FindBy(xpath = "//*[@id='bt_button_ok']")
    private WebElementFacade btnOkAutorizacaoXpath;


    @FindBy(xpath = "//*[@id='bd-main']/p/a[4]")
    private WebElementFacade linkLancamentoDeOs;

    @FindBy(name = "t_2_0")
    private WebElementFacade buscarMedico;

    @FindBy(id = "sle_psvnome")
    private WebElementFacade nomeMedico;

    @FindBy(className = "bt")
    private WebElementFacade buscarProfissional;

    @FindBy(name = "psv_nome_0")
    private WebElementFacade nomeProfissional;

    @FindBy(xpath = "//*[@id='a_edit-itens']")
    private WebElementFacade btnEditarItens;

    @FindBy(id = "bt_gravar")
    private WebElementFacade botaoGravarOs;

    @FindBy(name = "mot_cod_0")
    private WebElementFacade motivoAlteracaoOs;

    @FindBy(id = "bt_mot_ok")
    private WebElementFacade botaoOkMotivoAlteracao;

    @FindBy(linkText = "5,00")
    private WebElementFacade lnkQuantidadeItem;

    @FindBy(id = "txtp_string")
    private WebElementFacade lblQuantidadeItem;

    @FindBy(className = "bt")
    private WebElementFacade btnOk;

    @FindBy(xpath = "//span[contains(text(),'0,00')]  ")
    private WebElementFacade valorZeradoItem;

    @FindBy(xpath = "//span[contains(text(),'5,00')]")
    private WebElementFacade valorCinco;

    @FindBy(linkText = "0,00")
    private WebElementFacade lnkValorZero;

    @FindBy(linkText = "5,00")
    private WebElementFacade lnkValor5;

    @FindBy(linkText = "20,20")
    private WebElementFacade lnkValor2020;

    @FindBy(linkText = "20,21")
    private WebElementFacade lnkValor2021;

    private WebElementFacade nomeCampo;
    public static String valorItem = null;

    public void pegarValorItem() {
       // clicarFecharPainel();

        if(lnkValor2020.isPresent()){
            lnkValor2020.click();
        } else{
            if(lnkValor2021.isPresent()) {
                lnkValor2021.click();
            }
        }
    }

    public String retornarValorItem(){
        return lnkValor5.waitUntilVisible().getText();
    }

    public void selecionarItem(){

        if(checkItemDesmarcado.isPresent()){
            //Tendo item desmarcado -> Clica para Marcar
            checkItemDesmarcado.click();
        }
    }

    public void validarValorTotal(){
       if(valorTotalComDesconto.getTextValue().equals("65,00")){
           valorTotalComDesconto.getTextValue().equals("65,00");
       } else{
           Assert.fail("BUG ENCONTRADO. VALOR ESPERADO É DIFERENTE DO VALOR ENCONTRADO");
       }
    }

    public void escolherAcaoItem(String acaoItem){

        if(selecionarAcao.isPresent()) {
            selecionarAcao.click();

            if(acaoItem.equals("Cancelar Item")){

                if(itemCancelarItem.isPresent()){
                    itemCancelarItem.click();
                }

            } else if(acaoItem.equals("Interno (Inverter)")){
                if(itemInternoInverter.isPresent()){
                    itemInternoInverter.click();
                    divAguarde();
                }

            }else if(acaoItem.equals("Pendência (Inverter)")){
                    if(itemPendenciaInverter.isPresent()){
                        itemPendenciaInverter.click();
                    }

            }else if(acaoItem.equals("Urgente (Inverter)")){
                if(itemUrgenteInverter.isPresent()){
                    itemUrgenteInverter.click();
                    divAguarde();
                }

            }else if(acaoItem.equals("Resultado Parcial (Inverter)")){
                if(itemResultadoParcial.isPresent()){
                    itemResultadoParcial.click();
                    divAguarde();
                }
            }else if(acaoItem.contains("Data da Coleta")){

                if(itemDtColeta.isPresent()){
                    itemDtColeta.click();
                }
            }else if(acaoItem.contains("Data do Resultado")){

                if(itemDtResultado.isPresent()){
                    itemDtResultado.click();
                }
            }else if(acaoItem.equals("Alterar o Setor Executante")){
                if(alterarSetorExecutante.isPresent()){
                    alterarSetorExecutante.click();
                }
            }
            else if(acaoItem.equals("Cancelar Guia(s)")){
                    if(itemCancelarGuia.isPresent()){
                        itemCancelarGuia.click();
                    }
                }
        }
    }



    public void motivoCancelamento(String tipoCancelamento){
        if(selecionarMotivo.isPresent()) {
            selecionarMotivo.click();
            selecionarMotivo.selectByVisibleText(tipoCancelamento);
        }
    }

    public void observacaoCancelamento(String motivoCancelamento){
        if(descreverMotivo.isPresent()) {
            descreverMotivo.waitUntilClickable().click();
            descreverMotivo.type(motivoCancelamento);
        }
    }

    public void clicarBotaoOk(){
        if(botaoOk.isPresent()) {
            botaoOk.waitUntilClickable().click();
        }
    }

    public void digitarDataDeColetaResultadoAnterior(){
        if(dataEntrada.isPresent()) {
            String dataHoje = SeleniumUtil.getDataAtual();
            dataEntrada.click();
            //Resultado com Alerta = A data da coleta não pode ser menor do que a data da OS
            dataEntrada.sendKeys(SeleniumUtil.getDataAtualFormato(dataHoje));

            if(btOkDataColeta.isPresent()){
                btOkDataColeta.click();
            }
        }
    }

    public void alertaSelecioneItens(){
        try {
            Thread.sleep(1000);
            boolean alert = waitHelper.alertGetText().contains("Selecione os itens nos quais a ação será aplicada!");

            if(alert==true){
                waitHelper.waitAlertOk();
            }
        } catch (Exception e) {
            System.out.println("########## ALERTA - Selecione os itens nos quais a ação será aplicada! Não exibido ##############");
        }
    }
    public void digitarDataColetaResultado(){
        String dataHoje = SeleniumUtil.getDataAtual();
        if(dataEntrada.isPresent()) {
            dataEntrada.click();
            dataEntrada.sendKeys(SeleniumUtil.getDataAtualFormato(dataHoje) + " 23:59:59");

            if (btOkDataColeta.isPresent()) {
                btOkDataColeta.click();
            }
        }
    }

    public void clicarBotaoOkBuscar(){
        if(botaoOkBuscar.isPresent()) {
            botaoOkBuscar.click();
        }
    }

    public void digitarNomeSetor(String setorExecutante){
          try{
              if(fieldNomeSetor.waitUntilPresent().isPresent()){
                  fieldNomeSetor.waitUntilPresent().click();
                  Thread.sleep(4000);

                  fieldNomeSetor.type(setorExecutante);

              }
          }catch (Exception e){
              Assert.fail("Falha "+e.getMessage());
          }

    }

    public void selecionarSetor(){
        if(setorBioquimica.isPresent()) {
            setorBioquimica.waitUntilClickable().click();
        } else {
            setorAlmoxarifado.waitUntilPresent().click();
        }
    }

    public void aceitarAlerta(){
        waitHelper.alertOk();
    }

    public void clicarDetalhesItem(){

        detalhesItem.waitUntilClickable().click();
    }

    public void clicarDetalhesItem(String itemDesc){
        if(EditarItens.isPresent() || detalhesItem.isPresent()) {

            if(EditarItens.isPresent()){
                EditarItens.click();
            }

            if (detalhesItem.isPresent()) {
                detalhesItem.getTextValue().equals(itemDesc);
                detalhesItem.click();
                divAguarde();
            }
        }

    }

    public void concluirLancamentoDeItem() throws InterruptedException {
        divAguarde();
        if(btnConcluir.isPresent()) {
            btnConcluir.waitUntilClickable().click();
            divAguarde();
            clicarBotaoOkAutorizacao();
        }
    }

    public void novoLancamentoDeItem(){
        if(novoLancamentoDeItem.isPresent()) {
            divAguarde();
            novoLancamentoDeItem.click();
        }
    }

    public void clicarBotaoOkAutorizacao() {
        if (btnOkAutorizacao.isPresent() || btnOkAutorizacaoXpath.isPresent() ) {

            if(btnOkAutorizacao.isPresent()){
                btnOkAutorizacao.click();
            } else {
                btnOkAutorizacaoXpath.click();
            }
            divAguarde();
        }
    }

    public void excluirRespostaQuestionario() {
        if (btnExcluirQuestionario.isPresent() ) {
            btnExcluirQuestionario.click();

            try {
                Thread.sleep(1000);
                boolean alert = waitHelper.alertGetText().contains("Confirma a exclusão do formulário?");

                if(alert==true){
                    waitHelper.waitAlertOk();
                }
            } catch (Exception e) {
                System.out.println("Exclusão do formulário Não Realizada.");
            }

        }
    }

    public void clicarLancamentoItens() {
        if (btnLancamentoItens.isPresent() ) {
            btnLancamentoItens.click();
            divAguarde();
        }
    }


    public void confirmarCancelamentoNoWebService(){
        if(waitHelper.waitAlertGetText().equals("Este recurso irá cancelar a OS no WebService.\nDeseja continuar?")){
            waitHelper.waitAlertOk();
        }
    }

    public void clcarLinkLancamentoDeOs(){
        linkLancamentoDeOs.waitUntilClickable().click();
    }

    public void buscarProfissional(){
        buscarMedico.waitUntilClickable().click();
        nomeMedico.waitUntilClickable().click();
        nomeMedico.type("ADRIANO EXECUTANTE");
        buscarProfissional.waitUntilClickable().click();
  //      divAguarde();
        nomeProfissional.waitUntilClickable().click();
        divAguarde();
    }

    public void preencherMotivoAlteracao(){
        motivoAlteracaoOs.waitUntilClickable().click();
        motivoAlteracaoOs.selectByVisibleText("ALTERAÇÕES GERAIS");
        botaoOkMotivoAlteracao.waitUntilClickable().click();
        divAguarde();
    }

    public void inserirNovoProfissional(){
        if(waitHelper.waitAlertGetText().equals("Não é possível criar um profissional a partir do cadastro '[Não-especificado]'")) {
            waitHelper.waitAlertOk();
            clcarLinkLancamentoDeOs();
            buscarProfissional();
        }
    }

    public void gravarOrdemDeServico(){
        divAguarde();
        botaoGravarOs.waitUntilClickable().click();
    }

    public void clicarEditarItens(){
        if(btnEditarItens.isPresent()){
            btnEditarItens.click();
        }
    }

    public void validarCancelamentoDeItem(){
        try {
            Thread.sleep(1000);
            boolean alert = waitHelper.alertGetText().contains("GLICO");

            if(alert==true){
                waitHelper.waitAlertOk();
            }
        } catch (Exception e) {
            //Nada Faz
        }
    }

    public boolean validarAcaoItem(String acaoItem) {

        if (acaoItem.equals("Cancelar Guia(s)")) {
            boolean alertext = waitHelper.waitAlertGetText().contains("Não há guia a ser cancelada.");
            if (alertext == true) {
                waitHelper.alertOk();
                return true;
            }
        } else if (acaoItem.equals("Interno (Inverter)")) {
            divAguarde();
            if (internoResultadoInverter.getTextValue().equals("i")) {
                return true;
            }
        } else if (acaoItem.equals("Pendência (Inverter)")) {
            if(textGlicose.isPresent()){
                textGlicose.getTextValue().equals("GLICOSE (GLICEMIA)");
                return true;
            }
        } else if (acaoItem.equals("Urgente (Inverter)") || acaoItem.equals("Resultado Parcial (Inverter)")) {
            if (internoResultadoInverter.isPresent()) {
                internoResultadoInverter.getText().contains("!");
                return true;
            }
        } else if (acaoItem.equals("Data da Coleta")) {
            try {
                Thread.sleep(1000);
                boolean alert = waitHelper.alertGetText().contains("A data da coleta não pode ser menor do que a data da OS.");

                if (alert == true) {
                    waitHelper.waitAlertOk();
                    return true;
                }
            } catch (Exception e) {
                Assert.fail("BUG ENCONTRADO. O RESULTADO OBTIDO É DIFERENTE DO RESULTADO ENCONTRADO. ABRE UM BUG E FAÇA A ANALISE DO PROBLEMA");
            }
        } else if (acaoItem.equals("Data da Coleta 2")) {
            try {
                Thread.sleep(1000);
                boolean alert2 = waitHelper.alertGetText().contains("Só é possível alterar a data da coleta para procedimentos em aberto.");

                if (alert2 == true) {
                    waitHelper.waitAlertOk();
                    excluirRespostaQuestionario();
                    return true;
                }

            } catch (Exception e) {
                Assert.fail("BUG ENCONTRADO. O RESULTADO OBTIDO É DIFERENTE DO RESULTADO ENCONTRADO. ABRE UM BUG E FAÇA A ANALISE DO PROBLEMA");
            }
        }
        else if (acaoItem.equals("Data do Resultado")) {
            try {
                Thread.sleep(1000);
                boolean alert = waitHelper.alertGetText().contains("A data de resultado não pode ser anterior a data da coleta.");

                if (alert == true) {
                    waitHelper.waitAlertOk();
                    return true;
                }
            } catch (Exception e) {
                Assert.fail("BUG ENCONTRADO. O RESULTADO OBTIDO É DIFERENTE DO RESULTADO ENCONTRADO. ABRE UM BUG E FAÇA A ANALISE DO PROBLEMA");
            }
        } else if (acaoItem.equals("Data do Resultado 2")) {
            try {
                Thread.sleep(1000);
                boolean alert2 = waitHelper.alertGetText().contains("Só é possível alterar a data do resultado para procedimentos em aberto.");

                if (alert2 == true) {
                    waitHelper.waitAlertOk();
                    excluirRespostaQuestionario();
                    return true;
                }
            } catch (Exception e) {
                Assert.fail("BUG ENCONTRADO. O RESULTADO OBTIDO É DIFERENTE DO RESULTADO ENCONTRADO. ABRE UM BUG E FAÇA A ANALISE DO PROBLEMA");
            }
        }
        else {
            Assert.fail("BUG ENCONTRADO. O RESULTADO OBTIDO É DIFERENTE DO RESULTADO ENCONTRADO. ABRE UM BUG E FAÇA A ANALISE DO PROBLEMA");
        }

        return false;
    }

    public void validarAlertaDataAnterior(String acaoItem){
        if(acaoItem.equals("Data da Coleta")) {
           try {
                Thread.sleep(1000);
                boolean alert = waitHelper.alertGetText().contains("A data da coleta não pode ser menor do que a data da OS.");

                if(alert==true){
                    assertEquals("A data da coleta não pode ser menor do que a data da OS.", waitHelper.alertGetText());
                    waitHelper.waitAlertOk();
                }
            } catch (Exception e) {
                Assert.fail("BUG ENCONTRADO. O RESULTADO OBTIDO É DIFERENTE DO RESULTADO ENCONTRADO. ABRE UM BUG E FAÇA A ANALISE DO PROBLEMA");
            }

        }else if(acaoItem.equals("Data do Resultado")){
            try {
                Thread.sleep(1000);
                boolean alert = waitHelper.alertGetText().contains("A data de resultado não pode ser anterior a data da coleta.");

                if(alert==true){
                    assertEquals("A data de resultado não pode ser anterior a data da coleta.", waitHelper.alertGetText());
                    waitHelper.waitAlertOk();
                }
            } catch (Exception e) {
                Assert.fail("BUG ENCONTRADO. O RESULTADO OBTIDO É DIFERENTE DO RESULTADO ENCONTRADO. ABRE UM BUG E FAÇA A ANALISE DO PROBLEMA");
            }
        }
    }

    public void validarAlteracaoSetorExecutante(String setorExecutante){

        if(setorExecutante != null){
            //Validação 1 - Verifica se está presente
            Assert.assertTrue(setorExecutanteTelaDestalhesItem.isPresent());

            //Validação 2 -
           // if(setorExecutante.equals("BIO")) {
           //     setorExecutanteTelaDestalhesItem.getSelectedVisibleTextValue().contains("BIO");
           // }

                if(setorExecutante.equals(setorExecutante)) {
                // if(setorExecutante.equals("ALM")) {
                ///setorExecutanteTelaDestalhesItem.getSelectedVisibleTextValue().contains("ALM"); BIOLOGIA MOLECULAR
                Assert.assertTrue( setorExecutanteTelaDestalhesItem.getSelectedVisibleTextValue().contains(setorExecutante));
            }

        } else {
            Assert.fail("BUG ENCONTRADO. O RESULTADO ENCONTRAADO É DIFERENTE DO RESULTAOD ESPERADOP");
            fail("BUG ENCONTRADO");
        }


    }

    public void validarCancelamentoGuia(){
        assertEquals(waitHelper.waitAlertGetText(), "Cancelamento da guia realizado com sucesso!");
    }

    public void validarMensagemNaoTemGuia(){
        boolean alertext =  waitHelper.waitAlertGetText().contains("Não há guia a ser cancelada.");
        if(alertext==true){
            waitHelper.alertOk();
        } else {
            Assert.fail("BUG ENCONTRADO. O RESULTADO ESPERADO é DIFERENTE DO RESULTADO OBTIDO");
        }
    }

    public void confirmarResultadoSabado() {
        if (waitHelper.waitAlertGetText().equals("A data selecionada corresponde a um Sábado. Confirma sua seleção?")) waitHelper.alertOk();
        divAguarde();
    }

    public void clicarLinkQuantidadeItem(){
        if(lnkQuantidadeItem.isPresent()) {
            lnkQuantidadeItem.click();
        }
    }

    public void escreverQuantidadeDoItem(String quantidadeItem) {
        if(lblQuantidadeItem.isPresent()) {
            lblQuantidadeItem.type(quantidadeItem);
        }
    }
    public void clicarBotaoOkQuantidadeItem(){
        if(btnOk.isPresent()) {
            btnOk.click();
            divAguarde();
        }
    }

    public void validarValor() {
        DecimalFormat formatter = new DecimalFormat("#.00");
        if(lnkValorZero.isPresent()) {
            valorItem = lnkValorZero.getText();
            Assert.assertTrue("Contém o Item", valorItem.contains("0,00"));
        } else if(lnkValor5.waitUntilPresent().isPresent()){
            valorItem = lnkValor5.getText();
            Assert.assertTrue("Contém o Item", valorItem.contains("5,00"));
        } else{
            Assert.fail("BUG ENCONTRADO. O RESULTADO ENCONTRADO É DIFERENTE DO RESULTADO ESPERADO.");
        }
       // Assert.assertEquals(formatter.format(Float.parseFloat(lnkQuantidadeItem.getText().replaceAll(",", ".")) * Float.parseFloat(valorItem.replaceAll(",", "."))), lnkValorZero.getText());
    }

    public void LinkNaoDisponivelParaAlterarPreco() {
        //Não é validar mensagem e sim, não ser possível alterar o valor>> Link de valor desabilitado
        //Falso se o Link não estiver PRESENTE
        if(lnkValor5.isPresent()){
            //Se estiver presente, uma falha deve ocorrer, pois o esperado é o link não estar presente.
            Assert.fail("POSSIVEL BUG ENCONTRADO. Link Está Presente, mas o  resultado esperado é o Link não estar presente");

        } else {
            Assert.assertFalse(lnkValor5.isPresent());
        }

    }


    public boolean verificarAlteracaoQuantidade() {
        return lnkQuantidadeItem.isVisible();
    }

    public boolean verificarAlteracaoValor() {
        return lnkValor5.isVisible();
    }

}
