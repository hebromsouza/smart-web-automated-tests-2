package com.pixeon.pages.atendimento.ordemDeServico;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class PageItensDaOrdemDeServico extends BasePage {

    @FindBy(linkText = "Pagamento")
    private WebElementFacade linkPagamento;

    @FindBy(id = "bt_smm_print")
    private WebElementFacade dwpImprimir;

    @FindBy(id = "msgBox")
    private WebElementFacade divConectando;

    @FindBy(linkText = "Etiqueta de Amostra/Exame")
    private WebElementFacade linkImpressaoEtiqueta;

    @FindBy(linkText = "Ordem de Serviço")
    private WebElementFacade lnkImpressaoOrdemServico;

    @FindBy(linkText = "Close")
    private WebElementFacade btnFechar;

    @FindBy(xpath = "//*[@id='dw_pend_footer']/input[@value='Pagar']")
    private WebElementFacade btnPagar;




    public void clicarLinkPagamento(){
        if(linkPagamento.isPresent()) {
            linkPagamento.waitUntilClickable().click();
            divAguarde();
        }
    }

    public void clicarLinkPagamentoSemFormaPagamento(){
        if(linkPagamento.isPresent()) {
            linkPagamento.click();
            divAguarde();
        }

        //Clicar no botão Pagar
        clicarBotaoPagar();
    }

    public void carregarTelaPagamento(){
        getDriver().switchTo().frame(0);
        divAguarde();
    }

    public void clicarBotaoPagar(){
        carregarTelaPagamento();
        if(btnPagar.isPresent()){
            btnPagar.click();
            divAguarde();
        }
        scroolDown();
    }

    public void selecionarImpressao(String tipoImpressao){
        if(dwpImprimir.isPresent()) {
            dwpImprimir.waitUntilClickable().click();
            getDriver().findElement(By.linkText(tipoImpressao)).click();
            divConectando.waitUntilNotVisible();
        }
    }

    public void imprimirOrdemDeServico(){
        if(dwpImprimir.isPresent()) {
            dwpImprimir.waitUntilClickable().click();
            lnkImpressaoOrdemServico.click();
            divAguarde();
        }
    }

    public void fecharTelaImpressao(){
        if(btnFechar.isPresent()) {
            btnFechar.click();
            divAguarde();
        }
    }

}
