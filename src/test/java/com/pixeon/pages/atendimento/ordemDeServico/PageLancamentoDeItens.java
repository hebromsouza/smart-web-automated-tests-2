package com.pixeon.pages.atendimento.ordemDeServico;

import com.pixeon.pages.BasePage;
import com.pixeon.util.SeleniumUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class PageLancamentoDeItens extends BasePage {

    @FindBy(xpath = "//*[@id='dw_filtro_detail_0']/input[2]")
    private WebElementFacade lblBuscaItem;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnBuscar;

    @FindBy(id = "btnIncluirSmk")
    private WebElementFacade btnIncluirItem;

    @FindBy(name = "marcado_0")
    private WebElementFacade checkItem;

    @FindBy(css = "span > button")
    private WebElementFacade botaoConcluir;

    @FindBy(name = "smk_tipo_filtro_0")
    private WebElementFacade cboTipoFiltroItem;

    @FindBy(linkText = "Itens da OS")
    private WebElementFacade lnkItensDaOS;

    @FindBy(xpath = "//*[@id='secDwErroInclusao']/table/tbody/tr/td[contains(text(), 'Exame já lançado em outra OS para essa mesma data, deseja lançar novamente?')]")
    private WebElementFacade divConfirmaInclusaoItemNovamente;

    @FindBy(id = "btnConfirmar")
    private WebElementFacade btnConfirmar;

    @FindBy(xpath = "//*[@id='cheqpaipend']")
    private WebElementFacade allItensConfirmar;

    @FindBy(xpath = "//*[@id=\"cheq1\"]")
    private WebElementFacade checkItem1;

    @FindBy(id = "cheq1")
    private WebElementFacade chkItem;

    @FindBy(id = "cheqpaipend")
    private WebElementFacade chkAllItens;

    @FindBy(id = "ck_smm_all")
    private WebElementFacade chkItensAll;

    @FindBy(name = "smk_smk_ind_usual_0")
    private WebElementFacade chkItemInclusao;

    @FindBy(xpath = "//*[@id='dw_smk_busca_detail_0']//input[@value='Y']")
    private WebElementFacade CheckedItemMarcado;

    @FindBy(xpath = "//*[@id='dw_smk_busca_detail_0']//input[@value='N']")
    private WebElementFacade CheckedItemDesmarcado;


    @FindBy(id = "_panel78_c")
    private WebElementFacade painelBuscaItens;

    @FindBy(name = "smk_smk_ind_usual_1")
    private WebElementFacade chkItemInclusao1;

    @FindBy(xpath = "//*[@id='btnIncluirSmk']/div")
    private WebElementFacade BtIncluir;

    @FindBy(name = "bt_button_ok")
    private WebElementFacade btOkBuscaItens;


    @FindBy(xpath = "//*[@id=\"_panel75\"]/a")
    private WebElementFacade XFecharBuscaItens;

    @FindBy(linkText = "Close")
    private WebElementFacade fecharDiv;

    @FindBy(xpath = "//*[@id='secItensSelecionados']/ul/li[1]/a")
    private WebElementFacade btnConcluir;

    @FindBy(xpath = "//*[@id='ck_smm_all']")
    private WebElementFacade AllItensSelecionados;

    public void escreverItemServico(String itemServico){
        if(lblBuscaItem.isPresent()) {
            lblBuscaItem.waitUntilClickable().click();
            lblBuscaItem.type(itemServico);
        }
    }

    public void selecionarTipoFiltroItem(String tipoItem){
        if(cboTipoFiltroItem.isPresent()) {
            cboTipoFiltroItem.waitUntilClickable().click();
            cboTipoFiltroItem.selectByVisibleText(tipoItem);
        }
    }

    public void buscarItemServico(){
        if(btnBuscar.isPresent()) {
            btnBuscar.click();
            divAguarde();
        }
    }

    public void clicarNoBotaoFechar(){
        if(fecharDiv.isPresent()) {
            fecharDiv.waitUntilClickable().click();
        }
    }

    public void concluirItemServico(){
        if(AllItensSelecionados.isPresent()){
            AllItensSelecionados.click();

            if(btnConcluir.isPresent()) {
                btnConcluir.click();
            }
        }


    }


    public void incluirItemServico(){
        clicarNoBotaoFechar();


        if(CheckedItemMarcado.isPresent()){
            //Se CheckedItemMarcado estiver presente Significa que estará presente e marcado -> Nada faz

        } else {
            // Se CheckedItemMarcado não estiver presente. Então CheckedItemDesmarcado estará presente.
            // Se CheckedItemDesmarcado estiver presente -> Então clico nele para marcar.
            if(CheckedItemDesmarcado.isPresent()) {
                CheckedItemDesmarcado.click();
            }
        }


        if(BtIncluir.isVisible()){
            BtIncluir.click();
            divAguarde();
        }


    }

    public void selectAllItens(){
        if (chkAllItens.isVisible()) {
            chkAllItens.click();
        }else {
            if(chkItensAll.isVisible()) {
                chkItensAll.click();
            }
        }
    }

    public void clicarBotaoConfirmar(){

        if(chkItem.isPresent()) {
          //  allItensConfirmar.click();
            chkItem.waitUntilClickable().click();
            divAguarde();

            if (btnConfirmar.isVisible()) {
                btnConfirmar.click();
                divAguarde();
            }
        }
    }

    public void confirmaInclusaoItemNovamente(){
       if(divConfirmaInclusaoItemNovamente.isVisible()){
            if (chkAllItens.isVisible()){
                chkAllItens.click();
            }else {
                chkItem.waitUntilClickable().click();
            }
            btnConfirmar.click();
        }
    }

    public void clicarLinkProfissional(String profissional){
        getDriver().findElement(By.linkText(profissional)).click();
    }

    public void clicarLinkItensOrdemDeServico(){
        if(lnkItensDaOS.isPresent()) {
            lnkItensDaOS.waitUntilClickable().click();
        }
    }

    public String validarInstrucaoDoExame(){
        return waitHelper.waitAlertGetText();
    }

    public void lancarItemServico(String itemServico){
        escreverItemServico(itemServico);
        buscarItemServico();
        incluirItemServico();
        confirmaInclusaoItemNovamente();
    }

    public void aceitarAlertaInstrucaoDoitem() {
        if (SeleniumUtil.isAlertPresent()){
            if (waitHelper.waitAlertGetText().equals("GLICO\n- Jejum não obrigatório;\n- Enviar cópia do documento do paciente;\n- Obrigatório Pedido Médico e Relatório Clínico;\n- Preencher questionário Formulário Requisição de Exame Translocação BCR-ABL;\n- Coleta somente de segunda a quarta nas unidades Pituba e Rio Vermelho;")) waitHelper.alertOk();
        }
    }

    public void lancarItemServicoSemInstrucao(String itemServico){
        lancarItemServico(itemServico);
        confirmaInclusaoItemNovamente();
        waitHelper.alertOk();
    }

    public String lancarItemServicoComInstrucaoSemConcluir(String itemServico){
        lancarItemServico(itemServico);
        String instrucao = validarInstrucaoDoExame();
        waitHelper.waitAlertOk();
        if(checkItem.isPresent()) {
            checkItem.click();
        }

        return instrucao;
    }

    public void buscarItem(String item){
        escreverItemServico(item);
        buscarItemServico();
    }

    public void IncluirItemNaOS(String item){
        escreverItemServico(item);
        buscarItemServico();
        incluirItemServico();
        concluirItemServico();
     //   clicarBotaoConfirmar();

  //      ConfirmarInlusaoItem();

 //       ConcluirLancamentoItemOS();

 //       ExcluirQuestionario();
///
    }

    public void aceitarPopUpInstrucaoItem(){
        String instrucao = "GLICO\n- Jejum não obrigatório;\n- Enviar cópia do documento do paciente;\n- Obrigatório Pedido Médico e Relatório Clínico;\n- Preencher questionário Formulário Requisição de Exame Translocação BCR-ABL;\n- Coleta somente de segunda a quarta nas unidades Pituba e Rio Vermelho;";
        if (waitHelper.waitAlertGetText().equals(instrucao)){
            waitHelper.alertOk();
        }
    }

}
