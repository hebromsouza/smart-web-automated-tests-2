package com.pixeon.pages.atendimento.ordemDeServico;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class PageBuscaDeProfissional extends BasePage {

    @FindBy(id = "sle_psvnome")
    private WebElementFacade lblNomeMedico;

    @FindBy(className = "bt")
    private WebElementFacade btnBuscar;

    @FindBy(id = "msgDivBox")
    private WebElementFacade divAguarde;

    @FindBy(name = "psv_nome_0")
    private WebElementFacade lnkNomeMedico;

    @FindBy(linkText = "ADRIANO EXECUTANTE")
    private WebElementFacade linlMedico;

    @FindBy(name = "psv_psv_crm_0")
    private WebElementFacade lblCrmMedico;

    public void digitarNomeMedico(String nomeMedico){
        lblNomeMedico.waitUntilClickable().click();
        lblNomeMedico.type(nomeMedico);
    }

    public void clicarBotaoBuscar(){
        btnBuscar.waitUntilClickable().click();
        divAguarde.waitUntilNotVisible();
        lnkNomeMedico.waitUntilVisible().waitUntilPresent().waitUntilEnabled();
    }

    public void clicarLinkNomeMedico(String nomeMedico){
        getDriver().findElement(By.linkText(nomeMedico)).click();
    }

    public void clicarLinkNomeMedico(){
        linlMedico.waitUntilClickable().click();
        divAguarde();
    }

}
