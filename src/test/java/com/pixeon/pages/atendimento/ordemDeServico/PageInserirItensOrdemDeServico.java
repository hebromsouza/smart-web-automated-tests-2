package com.pixeon.pages.atendimento.ordemDeServico;

import com.pixeon.pages.BasePage;
import com.pixeon.util.SeleniumUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

import java.text.ParseException;

public class PageInserirItensOrdemDeServico extends BasePage {

    PageLancamentoDeItens pageLancamentoDeItens;

    @FindBy(name = "smk_cod_filtro_0")
    private WebElementFacade buscaItem;

    @FindBy(name = "smk_tipo_filtro_0")
    private WebElementFacade tipoFiltro;

    @FindBy(name = "tipo_busca_filtro_0")
    private WebElementFacade tipoBusca;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade botaoBuscar;

    @FindBy(name = "material_filtro_0")
    private WebElementFacade filtroAmostra;

    @FindBy(name = "t_3_0")
    private WebElementFacade pesquisarAmostra;

    @FindBy(name = "amo_amo_qlf_rot_0")
    private WebElementFacade escolherAmostra;

    @FindBy(xpath = "//*[@id='btnIncluirSmk']/div")
    private WebElementFacade botaoIncluir;

    @FindBy(xpath = "//*[@id='dw_smk_busca_detail_0']/span[2]/input[@value='N']")
    private WebElementFacade btSelecionarPrimeiroItemDesmarcado;

    @FindBy(name = "pac_nome_0")
    private WebElementFacade nomePaciente;

    @FindBy(linkText = "Concluir")
    private WebElementFacade btnConcluir;

    @FindBy(xpath = "//*[@id='ck_smm_all']")
    private WebElementFacade AllItensSelecionados;

    @FindBy(xpath = "//*[contains(text(),'Close')]")
    private WebElementFacade closeJanelas;

    @FindBy(xpath = "//*[@id='bt_button_ok']/div")
    private WebElementFacade btnOkDesejoContinuar;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[5]/a")
    private WebElementFacade descricaoItem;

    @FindBy(name = "smm_smm_pre_ccv_0")
    private WebElementFacade codigoConvenio;

    @FindBy(name = "smk_smk_amb_0")
    private WebElementFacade codigoAmb;

    @FindBy(xpath = "//*[@id=\"dw_smm_detail_0\"]/span[3]")
    private WebElementFacade codigoItem;

    @FindBy(name = "smk_rot_0")
    private WebElementFacade nomeItem;

    @FindBy(id = "sle_psvnome")
    private WebElementFacade nomeProfissional;

    @FindBy(className = "bt")
    private WebElementFacade botaoGravarProfissional;

    @FindBy(name = "psv_nome_0")
    private WebElementFacade escolherProfissional;

    @FindBy(name = "compute_7_0")
    private WebElementFacade statusItem;

    @FindBy(name = "smk_smk_ind_usual_0")
    private WebElementFacade checkItem;

    @FindBy(name = "t_3_0")
    private WebElementFacade searchAmostra;

    @FindBy(linkText = "Urina")
    private WebElementFacade getAmostra;

    @FindBy(xpath = "//*[@id=\"dw_smm_detail_0\"]/span[4]/div")
    private WebElementFacade valorAmostra;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[4]")
    private WebElementFacade ItemUrina;

    @FindBy(name = "smm_smm_cod_amostra_0")
    private WebElementFacade lblcodigoAmostra;

  //  @FindBy(xpath = "//*[@id='dw_smm_detail_0']/input[@name='smm_smm_cod_0']")  //original
    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[3]")
    private WebElementFacade ItemGlicoLancado;

    @FindBy(xpath = " //*[@id='dw_smm_detail_0']/span[3]")
    private WebElementFacade codigoItemLinha1;


    @FindBy(xpath = "//*[@id=\"dw_smm_detail_0\"]/input[@value=\"LGLICO\"]")
    private WebElementFacade ItemLGLICO;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[4]")
    private WebElementFacade CogidoRadioterapia;

    @FindBy(name = "smm_smm_cod_0")
    private WebElementFacade ItemN73;

    @FindBy(xpath = "//a[contains(text(),'SGE MATERIAL 1')]")
    private WebElementFacade ItemSGEMaterial;

    @FindBy(xpath = "//span[contains(text(),'K114')]")
    private WebElementFacade ItemK114;

    @FindBy(xpath = "//*[@id='dw_smk_busca_detail_0']/span[2]/input")
    private WebElementFacade CheckItemL80;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/input[@name='smm_smm_cod_0']")
    private WebElementFacade ItemL80;

    private String nomeCampo = null;
    private String nomeCampoValor = null;
    public static String numeroAmostraItem = null;

    public void buscarItens(String item){
        if(buscaItem.isPresent()) {
            buscaItem.waitUntilClickable().click();
            buscaItem.type(item);
        }

    }

    public void escolherTipoDeFiltro(String tipoItem){
        tipoFiltro.waitUntilClickable().click();
        tipoFiltro.selectByVisibleText(tipoItem);
    }

    public void escolherTipoDeBusca(String filtroTipoBusca){
        tipoBusca.waitUntilClickable().click();
        tipoBusca.selectByVisibleText(filtroTipoBusca);
    }

    public void clicarBotaoBuscar(){
        if(botaoBuscar.isPresent()) {
            botaoBuscar.waitUntilClickable().click();
            divAguarde();

          if(btnOkDesejoContinuar.isPresent()){
              btnOkDesejoContinuar.click();
          }
        }
    }

    public void selecionarItem(){
        if(checkItem.isPresent()) {
            checkItem.waitUntilClickable().click();
        }
    }

    public void clicarBotaoIncluir(){
        if(botaoIncluir.isPresent()) {
            botaoIncluir.waitUntilClickable().click();
        }
    }

    public void incluirItensSelecionados(){
        if(btSelecionarPrimeiroItemDesmarcado.isPresent()){
            btSelecionarPrimeiroItemDesmarcado.click();
            divAguarde();

        }

        if(CheckItemL80.isPresent() && ItemL80.isPresent()){
            CheckItemL80.click();
        }

        if(botaoIncluir.isPresent()) {
            botaoIncluir.waitUntilClickable().click();

            pageLancamentoDeItens.confirmaInclusaoItemNovamente();
            waitHelper.alertOk();
            divAguarde();
        //    waitHelper.alertOk();
        }
    }

    public void incluirItens() {
        botaoIncluir.waitUntilClickable().click();
        divAguarde();
        pageLancamentoDeItens.confirmaInclusaoItemNovamente();
        divAguarde();
    }

    public void CadastrarPacienteFaker() {

        if(nomePaciente.isPresent()){

        }

    }


    public void inserirProfissional(){
        if(nomeProfissional.isPresent()) {
            nomeProfissional.waitUntilClickable().click();
            nomeProfissional.type("ADRIANO EXECUTANTE");
            botaoGravarProfissional.waitUntilClickable().click();
            divAguarde();
            escolherProfissional.waitUntilClickable().click();
            inserirProfissional();
        }
    }

    public void concluirLancamentoDeItens(){
        waitHelper.alertOk();

        if(AllItensSelecionados.isPresent() && AllItensSelecionados.isVisible()) {
            AllItensSelecionados.click();

            if(btnConcluir.isPresent()) {
                btnConcluir.click();
            }
        }

    //    divAguarde();

        if (btnOkDesejoContinuar.isPresent()) {
     //       divAguarde();
            btnOkDesejoContinuar.click();
        }

    }

    public void abrirTelaDeDescricaoDeItens(){
        if(descricaoItem.waitUntilPresent().isPresent()) {
            descricaoItem.click();
        }
    }

    public void buscarAmostra(String amostra){
        if(searchAmostra.isPresent()) {
            searchAmostra.click();
            getAmostra.click();
        }

    }

    public String escolherCampoParaValidacao(String item, String tipoItem, String filtroTipoBusca){
        if(filtroTipoBusca.equals("Cód. Convênio")){
            nomeCampo = codigoConvenio.getValue();
        }else if(filtroTipoBusca.equals("Cód. AMB")){
            nomeCampo = codigoAmb.getValue();
        }else if((filtroTipoBusca.equals("Smart") && item.equals("SGE MATERIAL 1")) || filtroTipoBusca.equals("Nome")){
            nomeCampo = nomeItem.getText();
        }else if(tipoItem.equals("Kit") && filtroTipoBusca.equals("Smart") || filtroTipoBusca.equals("Cód. SUS")){
            nomeCampo = statusItem.getText();
        }else{
            nomeCampo = codigoItem.getText();
        }

        return nomeCampo;
    }

    public void alertaAutorizacaoOnline(){
        //if(closeJanelas.isPresent()){ //original

        if(closeJanelas.isPresent() && closeJanelas.isVisible()&& closeJanelas.isEnabled()){// ajuste por gilson.lima
            closeJanelas.click();
        }
    }
    public void validaInsercaoItem2(String item){
        alertaAutorizacaoOnline();

        if(item.equals("GLICO") || item.equals("28010974")){
        //    ItemGlicoLancado.getTextValue().equals("GLICO"); //original
        //    Assert.assertTrue("Item GLICO Está presente!", ItemGlicoLancado.isPresent());//original
            if(ItemGlicoLancado.isPresent()&& ItemGlicoLancado.isVisible()) {
                Boolean status= ItemGlicoLancado.waitUntilPresent().getTextValue().equals("GLICO"); // ajuste por gilson.lima
                Assert.assertTrue("Item GLICO Está presente!", status);// ajuste por gilson.lima
            }else{
                Assert.fail("BUG ENCONTRADO item não contrado "+item );
            }

        } else if(item.equals("SGE MATERIAL 1")){
            ItemSGEMaterial.waitUntilPresent().getTextValue().equals("SGE MATERIAL 1");
            Assert.assertTrue("Item SGE MATERIAL 1 Está presente!", ItemSGEMaterial.isPresent());
        }  else if(item.equals("K114")){
            if(ItemK114.isPresent() && ItemK114.isVisible() ) {
                ItemK114.getTextValue().equals("K114");
                Assert.assertTrue("Item K114 Está presente!", ItemK114.isPresent());
            }else{
            Assert.fail("BUG ENCONTRADO item não contrado "+item );
            }
        }else if(item.equals("1") || item.equals("L80")){
            alertaAutorizacaoOnline();
           // if(ItemL80.isPresent()){
           // //    ItemL80.click();
           //     ItemL80.getTextValue().equals("L80");
           //     Assert.assertTrue("Item L80 Está presente!", ItemL80.isPresent());
           // }

            if(codigoItemLinha1.isPresent() && codigoItemLinha1.isVisible()) {
                Boolean status=  codigoItemLinha1.waitUntilPresent().getTextValue().equals("L80"); // ajuste por gilson.lima
                Assert.assertTrue("Item GLICO Está presente!", status);// ajuste por gilson.lima
            }else{
                Assert.fail("BUG ENCONTRADO item não contrado "+item );
            }

        }else if(item.equals("6574589652")){
            alertaAutorizacaoOnline();
           // if(ItemLGLICO.isPresent()){ //original
           //     ItemLGLICO.getTextValue().contains("LGLICO");
           //     Assert.assertTrue(ItemLGLICO.isPresent());
            if(codigoItemLinha1.isPresent() && codigoItemLinha1.isVisible()){ //original
                    codigoItemLinha1.getTextValue().contains("LGLICO");
                 Assert.assertTrue(codigoItemLinha1.isPresent());
            }else{
                Assert.fail("BUG ENCONTRADO"+item);
            }
        }else if(item.equals("N73")){
            alertaAutorizacaoOnline();
//            if(ItemN73.isPresent()){ //original
//                ItemN73.getTextValue().contains("N73"); //original
//                Assert.assertTrue(ItemN73.isPresent()); //original
//            }else{
//                Assert.fail("BUG ENCONTRADO"+item); //original
//            }

            if(codigoItemLinha1.isPresent()){ //original
                Boolean status= codigoItemLinha1.waitUntilPresent().getTextValue().equals("N73"); // ajuste por gilson.lima
                Assert.assertTrue( item+" Está presente!", status);// ajuste por gilson.lima
            }else{
                Assert.fail("BUG ENCONTRADO"+item);
            }
        }else if(item.equals("RADIOTERAPIA")){
            if(codigoItemLinha1.isPresent()&&codigoItemLinha1.isVisible()) {
               // CogidoRadioterapia.waitUntilPresent().getTextValue().equals("35000007");
                Assert.assertTrue(codigoItemLinha1.waitUntilPresent().getTextValue().equals("35000007"));
            }
        }
        else {

            if(codigoItemLinha1.isPresent()) {
                // CogidoRadioterapia.waitUntilPresent().getTextValue().equals("35000007");
                Assert.assertTrue("item   "+item , codigoItemLinha1.waitUntilPresent().getTextValue().equals(item));
            }
            Assert.fail("BUG ENCONTRADO. O RESULTADO ESPERADO É DIFERENTE DO RESULTADO OBTIDO.");
        }
    }

    public boolean validaInsercaoItem(String item, String tipoItem, String filtroTipoBusca){
        nomeCampoValor = escolherCampoParaValidacao(item, tipoItem, filtroTipoBusca);

        if(nomeCampoValor.equals(item) || nomeCampo.equals("Aberto")) {
            return true;
        }else{
            return false;
        }
    }

    public boolean validaInclusaoItemComAmostra(String amostra){
        alertaAutorizacaoOnline();
        if(amostra.equals("Urina")) {
            if(ItemUrina.isPresent()) {
                ItemUrina.getTextValue().equals("Urina");
                ItemUrina.getTextValue().equals(amostra);
            }
        } else {
            Assert.fail("BUG ENCONTRADO. O RESULTADO ESPERADO É DIFERENTE DO RESULTADO OBTIDO.");
        }


        if(ItemUrina.getTextValue().equals(amostra)){
            return true;
        }else{
            return false;
        }
    }

    public void validarBloqueioCotacao(String mensagem){
        Assert.assertTrue(waitHelper.waitAlertGetText().contains(mensagem));
    }

    public void pegarNumeroAmostraItem() {
        if(lblcodigoAmostra.isPresent()) {
            numeroAmostraItem = lblcodigoAmostra.getValue();
        }
    }

    public void validarResultadoItemSabado(int dias){
        try {
            int day = dias - SeleniumUtil.weekDay();
            if (day == 0) day = dias;
            String dataResultado = SeleniumUtil.adicionarDiasDataAtual("dd/MM", day);
          //  Assert.assertTrue(waitHelper.existsElement(By.xpath("//span[contains(text(), '"+ dataResultado +"')]")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void validarAlertaCotacaoPainel(String mensagem) {
        Assert.assertTrue(mensagem.contains(divMsgAlerta()));
    }

}
