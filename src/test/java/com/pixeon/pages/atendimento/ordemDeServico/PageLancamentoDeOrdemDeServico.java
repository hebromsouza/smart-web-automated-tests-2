package com.pixeon.pages.atendimento.ordemDeServico;

import com.pixeon.datamodel.ordemDeServico.DataOrdemDeServico;
import com.pixeon.pages.BasePage;
import com.pixeon.util.DataHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class PageLancamentoDeOrdemDeServico extends BasePage {

    @FindBy(xpath = "//div[@class='hd' and contains(text(),'Instruções')]")
    private WebElementFacade divInstrucoes;

    @FindBy(linkText = "Close")
    private WebElementFacade fecharDiv;

    @FindBy(xpath = "//*[@id='bd-main']/h2[2]")
    private WebElementFacade tituLoOrdemServico;

    @FindBy(xpath = "//*[@id='a_novo']/div")
    private WebElementFacade adicionarOs;


    @FindBy(id = "a_edit-itens")
    private WebElementFacade EditarItens;

    @FindBy(css = "ico save")
    private WebElementFacade botaoGravarOs;

    @FindBy(xpath = "//*[contains(text(),'Close')]")
    private WebElementFacade CloseMotivo;

    @FindBy(xpath = "//*[@id='a_novo']/div")
    private WebElementFacade adicionarOrdemServico;


    @FindBy(xpath = "//*[@id=" +
            "'bt_gravar']/div")
    private WebElementFacade btnGravar;

    @FindBy(xpath = "//*[@id='bt_next']")
    private WebElementFacade botaoAvancar;

    @FindBy(name = "osm_cnv_0")
    private WebElementFacade ComboSelecionaConvenio;

    @FindBy(xpath = "//*[@id='dw_osm_detail_0']/select[1]/option[@value='1']")
    private WebElementFacade convenioParticularCod1;


    //@FindBy(xpath = "//*[@id=\"dw_dados_os_pac_header\"]/span[8]")
    @FindBy(css = "#dw_dados_os_pac_header > span:nth-child(8)")
    private WebElementFacade campoConvenioLancamentoDeItens;

    @FindBy(name = "osm_osm_ctle_cnv_0")
    private WebElementFacade campoGuia;

    @FindBy(name = "osm_str_0")
    private WebElementFacade Setor;

    @FindBy(xpath = "//*[@name='osm_str_0']/option[@value='CON']")
    private WebElementFacade cboSetorSolicitante;

    @FindBy(name = "t_2_0")
    private WebElementFacade divSearch;

    @FindBy(name = "osm_osm_cid_cod_0")
    private WebElementFacade lblCid;

    @FindBy(xpath = "//*[@id=\'dw_dados_os_pac_header\']/span/a")
    private WebElementFacade lnkOrdemServico;

    @FindBy(name = "numero_da_os_0")
    private WebElementFacade lnkNumeroOrdemServico;

    @FindBy(id = "sle_psvnome")
    private WebElementFacade lblNomeMedico;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnBuscar;

    @FindBy(id = "btnIncluirSmk")
    private WebElementFacade btnIncluir;

    @FindBy(name = "smk_cod_filtro_0")
    private WebElementFacade campoBuscar;


    @FindBy(name = "psv_psv_nome_0")
    private WebElementFacade lblNomePrestador;

    @FindBy(name = "t_1_0")
    private WebElementFacade iconBuscaEmpresa;

    @FindBy(name = "cnv_nome_0")
    private WebElementFacade lblNomeFantasia;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnBuscarNomeEmpresa;

    @FindBy(name = "osm_osm_empnome_solic_0")
    private WebElementFacade lblNomeEmpresa;

    @FindBy(linkText = "Lançamento de OS")
    private WebElementFacade lnkLancamentoDeOs;

    @FindBy(id = "a_pagamento")
    private WebElementFacade lnkPagamento;

    @FindBy(id = "a_questionario")
    private WebElementFacade lnkQuestionario;

    @FindBy(name = "mot_cod_0")
    private WebElementFacade comboMotivo;

    @FindBy(xpath = "//*[@id='dwp_mot_detail_0']/select/option[2]")
    private WebElementFacade alteracoesGerais;

    @FindBy(xpath = "//*[@id='bt_mot_ok']/div")
    private WebElementFacade btnOKMotivoAlteracao;

    @FindBy(xpath = "//*[@id='dw_osm_detail_0']/input[11]")
    private WebElementFacade lblCpfCnpj;

    public static String numeroOS;
    private WebDriver driver;

    public WebDriver getDriver() {
        return driver;
    }

    public void clicarNoBotaoFechar(){
        if(fecharDiv.isPresent()) {
            fecharDiv.waitUntilClickable().click();
        }
    }

    public void clicarNoBotaoNovaOs(){
        try {
            if(botaoGravarOs.isPresent()) {
                botaoGravarOs.click();
                adicionarOs.click();
                divAguarde();
            } else if(btnGravar.isPresent()){
                btnGravar.click();
                divAguarde();
            }
            else {
                EditarItens.click();
            }

        } catch (Exception e){
            System.out.println(e.getMessage());
        }

    }

    public void AdicionarNovaOrdemServico(){
    //    getDriver().findElement(By.xpath("//*[@id=\'a_novo\']/div")).click();

   //    driver.findElement(By.cssSelector(".ico new")).click();
        adicionarOrdemServico.click();
  //      tituLoOrdemServico.click();
  //      adicionarOs.click();
    }

    public void preencherCampoGuia(){
        divAguarde();
        if(campoGuia.isDisplayed()){
            campoGuia.waitUntilClickable().click();
            campoGuia.type("11141516");
        }

    }

    public void incluirMedicoSolicitante(String nomePrestador){
        divSearch.waitUntilClickable().click();
        lblNomeMedico.type(nomePrestador);
        btnBuscar.click();
        getDriver().findElement(By.linkText(nomePrestador)).click();
        Assert.assertEquals(lblNomePrestador.getText(), nomePrestador);
    }

    public void incluirConvenio(String empresa){
        iconBuscaEmpresa.waitUntilClickable().click();
        lblNomeFantasia.waitUntilClickable().type(empresa);
        btnBuscarNomeEmpresa.click();
        getDriver().findElement(By.linkText(empresa)).click();
        divAguarde();
        Assert.assertEquals(lblNomeEmpresa.getText(), empresa);
    }

    public void preencherDadosOrdemDeServico(List<DataOrdemDeServico> dataOrdemDeServico){
        DataOrdemDeServico data = DataHelper.getData(dataOrdemDeServico);

        if(data.getNomePrestador() != "" && data.getNomePrestador() != null)
            incluirMedicoSolicitante(data.getNomePrestador());

        if(data.getNomeEmpresa() != "" && data.getNomeEmpresa() != null)
            incluirConvenio(data.getNomeEmpresa());

        if (data.getSetorSolicitante() != "" && data.getSetorSolicitante() != null){
            cboSetorSolicitante.click();
            cboSetorSolicitante.selectByVisibleText(data.getSetorSolicitante());
        }

    }

    public void selecionarSetorSolicitante(String setorSolicitante){
        if(cboSetorSolicitante.isVisible()) {
      //      getDriver().switchTo().defaultContent();
     //       getDriver().switchTo().frame(1);

            Setor.click();
            cboSetorSolicitante.click();
        }
     //   cboSetorSolicitante.selectByVisibleText(setorSolicitante);
    }

    public void digitarCid(String cid){
        lblCid.waitUntilClickable().click();
        lblCid.typeAndTab(cid);
    }

    public void clicarNoBotaoGravar(){
        if(btnGravar.isDisplayed()){
            btnGravar.waitUntilClickable().click();
            divAguarde();
            divAguarde();
        }

    }

    public void clicarBotaoGravar(){
        if(btnGravar.isDisplayed()){
            btnGravar.click();
            divAguarde();
        }
    }

    public void clicarNoBotaoAvancar(){
        if(botaoAvancar.isPresent()) {
            botaoAvancar.click();
        }
    }

    public void esperarMensagemAguarde(){
      //  divAguarde();
    }

    public void alterarConvenioParaParticular(String codigoConvenio){
        if(ComboSelecionaConvenio.isPresent()) {
            ComboSelecionaConvenio.click();
      //      convenioParticularCod1.click();
            ComboSelecionaConvenio.selectByValue(codigoConvenio);
        }
    }

    public void alterarParticularParaConvenio(String codigoConvenio){
        if(ComboSelecionaConvenio.isPresent()) {
            ComboSelecionaConvenio.waitUntilClickable().click();
            ComboSelecionaConvenio.selectByValue(codigoConvenio);
        }
    }

    public void numeroOrdemServico(){
        numeroOS = lnkOrdemServico.waitUntilVisible().getText();
    }

    public boolean validarExibicaoDeJanelaInstrucoes(){
        return divInstrucoes.isPresent();
    }

    public void fecharJanelaInstrucoes(){
        if(validarExibicaoDeJanelaInstrucoes()==true){
            clicarNoBotaoFechar();
        }
    }
    public void clicarNoBotaoBuscar(String item){
        if(btnBuscar.isPresent()){
            campoBuscar.clear();
            campoBuscar.type(item);
            btnBuscar.click();
        }

        if(btnIncluir.isPresent()) {
            btnIncluir.click();
        }
    }

    public boolean validarBotaoAvancarCadastroPaciente(){
        return botaoAvancar.isPresent();
    }

    public boolean validarNovaOrdemDeServico(String plano){


        if(waitHelper.existsElement(By.xpath("//*[@id='dw_dados_os_pac_header']/span[contains(text(), '" + plano + "')]"))){
            return true;
        }else{
            return false;
        }

    }

    public void validarHerancaSetorSolicitante(String setor){
        Assert.assertEquals(cboSetorSolicitante.getValue(), setor);
    }

    public void validarBuscaOrdemServico(){
        Assert.assertEquals(numeroOS, lnkNumeroOrdemServico.getText());
    }

    public void validarAusenciaOrdemServico(){
        Assert.assertNotEquals(numeroOS, lnkNumeroOrdemServico.getText());
    }

    public void clicarLinkLancamentoDeOs() {
        divAguarde();
        lnkLancamentoDeOs.waitUntilClickable().click();

    }

    public void clicarLinkQuestionario() {
        lnkQuestionario.waitUntilClickable().click();
    }

    public void escreverCpfCnpjPrestador(String cpfCnpj) {
        if(lblCpfCnpj.isPresent()){
            lblCpfCnpj.clear();
            lblCpfCnpj.click();
            lblCpfCnpj.type("123456");
        }


    }

    public void InformarMotivoAlteracao() {
     //   getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame(2);
        if(comboMotivo.isPresent()){
            comboMotivo.click();
            alteracoesGerais.click();
            btnOKMotivoAlteracao.click();
        }
        divAguarde();
    }


    public String retornarValorCpfCnpjPrestador(){
        return lblCpfCnpj.waitUntilVisible().getValue();
    }

    public void validarMensagemAlert(String mensagem){

        try {
            Thread.sleep(1000);
            boolean alert = waitHelper.alertGetText().contains("Não foi possível completar a operação.");

            if(alert==true){
                waitHelper.waitAlertOk();

             //   InformarMotivoAlteracao();
                if(CloseMotivo.isPresent()) {
                    CloseMotivo.click();
                    CloseMotivo.click();
                }
//                Thread.sleep(1000);
//                boolean alert2 = waitHelper.alertGetText().contains("É necessário informar o motivo.");
//                if(alert2==true){
//                    waitHelper.waitAlertOk();
//                }
            }
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("BUG ENCONTRADO");
        }

        if(CloseMotivo.isPresent()) {
            CloseMotivo.click();
            CloseMotivo.click();
        }

        waitHelper.waitAlertOk();

/*        //Mudar para a caixa de alerta antes de interagir
        Alert alert = getDriver().switchTo().alert();
        System.out.println(alert.getText());

        String text = driver.switchTo().alert().getText();
        System.out.println(text);

        if(text.contains("Não foi possivel completar a operação. Cpf inválido")){
            System.out.println("OKKKKKKKKKKKKKKKKKKKKKKKKKKK");
        } else {
            System.out.println("BUG ENCONTRADO");
        }
        // Confirmar texto pop-up do alerta e fechar o alerta
        //   assertEquals("Não foi possível completar a operação. Cpf inválido",  closeAlertAndGetItsText);

        System.out.println(alert.getText()); //Print Alert popup
        alert.accept(); //Close Alert popup*/
    }


}
