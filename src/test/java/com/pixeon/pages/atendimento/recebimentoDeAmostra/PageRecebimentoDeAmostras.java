package com.pixeon.pages.atendimento.recebimentoDeAmostra;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageRecebimentoDeAmostras extends BasePage {

    @FindBy(name = "compute_os_0")
    private WebElementFacade linkOrdemServico;

    public void clicarOrdemServico(){
        linkOrdemServico.waitUntilClickable().click();
    }

    public void validarImpressaoOrdemServicoPeloNome(String nomePaciente){
        getDriver().switchTo (). frame (1);
        System.out.println("");
    }

}
