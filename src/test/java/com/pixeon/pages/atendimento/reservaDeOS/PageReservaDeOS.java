package com.pixeon.pages.atendimento.reservaDeOS;

import com.pixeon.datamodel.reservaDeOS.DataReservaOS;
import com.pixeon.pages.BasePage;
import com.pixeon.util.DataHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

import java.util.List;

public class PageReservaDeOS extends BasePage {

    @FindBy(name = "ros_osm_serie_0")
    private WebElementFacade lblSerieOS;

    @FindBy(name = "ros_osm_quantidade_0")
    private WebElementFacade lblQuantidadeOS;

    @FindBy(name = "ros_str_solic_0")
    private WebElementFacade cboSetorSolicitante;

    @FindBy(name = "ros_obs_0")
    private WebElementFacade lblObservacao;

    @FindBy(id = "bt_gerar")
    private WebElementFacade btnGerarReserva;

    @FindBy(xpath = "//div[contains(text(), 'Reserva gerada com sucesso!')]")
    private WebElementFacade txtMsgSucess;

    @FindBy(id = "btp_print_form")
    private WebElementFacade btnImprimirFichaAtendimento;

    public void preencherCamposReservaOS(List<DataReservaOS> dataReservaOS) {
        DataReservaOS data = DataHelper.getData(dataReservaOS);

        if (data.getSerieOs() != null) lblSerieOS.type(data.getSerieOs());
        if (data.getQuantidade() != null) lblQuantidadeOS.type(data.getQuantidade());
        if (data.getObservacao() != null) lblObservacao.type(data.getObservacao());

        if (data.getSetorSolic() != null){
            cboSetorSolicitante.click();
            cboSetorSolicitante.selectByVisibleText(data.getSetorSolic());
            divAguarde();
        }

    }

    public void clicarBotaoGerarReserva(){
        btnGerarReserva.click();
    }

    public void confirmarPopUpGeracaoReserva(String quantidade, String setorSolicitante){
        if (waitHelper.waitAlertGetText().equals("Confirma gerar uma reserva de "+ quantidade +" ordens de serviço para o sertor "+ setorSolicitante +"?")) waitHelper.alertOk();
    }

    public void validarPainelReservaGerada() {
        btnImprimirFichaAtendimento.waitUntilClickable();
        Assert.assertTrue(txtMsgSucess.isVisible());
    }

    public void clicarBotaoImprimirFichasAtendimento() {
        btnImprimirFichaAtendimento.click();
    }

}
