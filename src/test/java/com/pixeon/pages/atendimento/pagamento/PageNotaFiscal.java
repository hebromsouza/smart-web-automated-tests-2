package com.pixeon.pages.atendimento.pagamento;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageNotaFiscal extends BasePage {

    @FindBy(name = "nfl_valor_0")
    private WebElementFacade lblValorBruto;

    @FindBy(name = "btn_consultarrps_0")
    private WebElementFacade btnConsultarRps;

    @FindBy(name = "btn_ok_0")
    private WebElementFacade btnGravar;

    @FindBy(name = "btn_imprimir_0")
    private WebElementFacade btnImprimirNfse;

    @FindBy(name = "btn_excluir_0")
    private WebElementFacade btnExcluirNota;

    @FindBy(name = "motivo_0")
    private WebElementFacade lblMotivoCancelamento;

    @FindBy(id = "bt_gravar")
    private WebElementFacade btnGravarCancelamento;

    @FindBy(name = "nfl_num_0")
    private WebElementFacade lblNumeroNotaFiscal;

    public void validarValorBruto(){
        Assert.assertEquals(lblValorBruto.waitUntilVisible().getValue(), PagePagamento.valorTotalPagamento);
    }

    public void clicarBotaoGravar(){
        btnGravar.click();
        divAguarde();
    }

    public void clicarBotaoConsultarRps(){
        btnConsultarRps.click();
    }

    public void clicarBotaoImprimirNotaFiscalEletronica(){
        btnImprimirNfse.click();
        divAguarde();
    }

    public void clicarBotaoExcluirNotaFiscal(){
        if(btnExcluirNota.isPresent()) {
            btnExcluirNota.click();
            divAguarde();
        }
    }

    public void escreverMotivoDeCancelamento(){
        lblMotivoCancelamento.type("Cancelamento Nota Fiscal");
    }

    public void clicarBotaoGravarCancelamento(){
        btnGravarCancelamento.click();
    }

    public void aceitarPopUpCancelamento(){
        if (waitHelper.waitAlertGetText().equals("Confirma realizar o cancelamento dessa nota fiscal?"))waitHelper.alertOk();
        divAguarde();
    }

    public String pegarNumNotaFiscal(){
        return lblNumeroNotaFiscal.getValue();
    }

}
