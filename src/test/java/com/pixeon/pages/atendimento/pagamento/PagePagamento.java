package com.pixeon.pages.atendimento.pagamento;

import com.pixeon.pages.BasePage;
import com.pixeon.pages.home.PageHome;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

public class PagePagamento extends BasePage {

    String total = null;
    String desconto = null;
    String valorExame = null;
    String valorExameParcial = null;
    String primeiroValor = null;
    String aPagarTotal = null;
    Double valorComDesconto;
    Double valorComDescontoParcial;
    Boolean campoValor1Parcial;
    Boolean descontoAplicado;

    PageHome pageHome;

    @FindBy(name = "cb_pagar")
    private WebElementFacade btnPagar;

    @FindBy(name = "cb_submit")
    private WebElementFacade btnGravarPagamento;


    @FindBy(xpath = "//*[@id='dw_rdi_summary']/input")
    private WebElementFacade btnGravarFormaDePagamento;

    @FindBy(xpath = "//a[1][@class='container-close']")
    private WebElementFacade btnFechar;

    @FindBy(xpath = "//*[contains(text(),'Close')]")
    private WebElementFacade closeJanelas;

    @FindBy(xpath = "//*[@id='dw_rdi_summary']/span")
    private WebElementFacade spanValor;

    @FindBy(xpath = "//*[@id='a_nota_fiscal']")
    private WebElementFacade lnkNotaFiscal;

    @FindBy(xpath = "//*[@id='mtePrint']/ul/li[1]/a[contains(text(),'Imprimir Recibo')]")
    private WebElementFacade lnkImprimirRecibo;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[11]/div")
    private WebElementFacade checkFaturado;

    @FindBy(xpath = "//*[@id='dw_rdi_detail_0']/input[2]")
    private WebElementFacade lblValor0;

    @FindBy(name = "rdi_valor_1")
    private WebElementFacade lblValor1;

    @FindBy(name = "a_pagar_0")
    private WebElementFacade aPagar;

    @FindBy(css = "#secRdi > ul.ctrl.h2 > li > a > div.ico.new")
    private WebElementFacade novoPagamento;

    @FindBy(name = "rdi_forma_pag_0")
    private WebElementFacade cboFormaPagamento;

    @FindBy(name = "rdi_forma_pag_1")
    private WebElementFacade cboFormaPagamento1;

    @FindBy(name = "rdi_cre_cod_0")
    private WebElementFacade cboCartao0;

    @FindBy(name = "rdi_cre_cod_1")
    private WebElementFacade cboCartao1;

    @FindBy(name = "rdi_vcto_0")
    private WebElementFacade lblVencimento0;

    @FindBy(name = "rdi_vcto_1")
    private WebElementFacade lblVencimento1;

    @FindBy(name = "rdi_validade_cartao_0")
    private WebElementFacade lblValidade0;

    @FindBy(name = "rdi_validade_cartao_1")
    private WebElementFacade lblValidade1;

    @FindBy(name = "mte_mte_tdc_cod_0")
    private WebElementFacade descontoLivre;

    @FindBy(name = "desc_perc_0")
    private WebElementFacade descontoLivreValor;

    @FindBy(name = "mte_desconto_0")
    private WebElementFacade valorDesconto;

    @FindBy(name = "mte_valor_0")
    private WebElementFacade lblValorTotal;

    @FindBy(name = "mte_obs_0")
    private WebElementFacade campoObservacao;

    @FindBy(name = "mte_ind_pac_resp_0")
    private WebElementFacade responsavelFinanceiro;

    @FindBy(name = "mte_mte_obs_desconto_0")
    private WebElementFacade observacaoDescricao;

    @FindBy(name = "mte_mte_fis_jur_0")
    private WebElementFacade selecionarTipoPessoa;

    @FindBy(name = "mte_num_cpf_0")
    private WebElementFacade numeroCpf;

    @FindBy(name = "mte_resp_fone_0")
    private WebElementFacade foneResponsavel;

    @FindBy(name = "mte_resp_nome_0")
    private WebElementFacade responsavel;

    @FindBy(name = "mte_mte_resp_cep_0")
    private WebElementFacade cepResponsavel;

    @FindBy(name = "mte_resp_rg_0")
    private WebElementFacade rgResponsavel;

    @FindBy(name = "mte_resp_dt_nasc_0")
    private WebElementFacade nascimentoResponsavel;

    @FindBy(name = "mte_resp_sexo_0")
    private WebElementFacade sexoResponsavel;

    @FindBy(name = "compute_7_0")
    private WebElementFacade buscaCidade;

    @FindBy(id = "sle_cdenome")
    private WebElementFacade nomeCidade;

    @FindBy(linkText = "SALVADOR")
    private WebElementFacade linkCidade;

    @FindBy(name = "mte_resp_ender_0")
    private WebElementFacade enderecoResponsavel;

    @FindBy(name = "mte_mte_resp_end_num_0")
    private WebElementFacade enderecoNumero;

    @FindBy(name = "mte_mte_resp_email_0")
    private WebElementFacade emailResponsavel;

    @FindBy(name = "mte_mte_resp_bairro_0")
    private WebElementFacade bairroResponsavel;

    @FindBy(name = "busca_log_0")
    private WebElementFacade buscaLogradouro;

    @FindBy(id = "sle_log-desc")
    private WebElementFacade descricaoLogradouro;

    @FindBy(linkText = "ACESSO")
    private WebElementFacade linkLogradouro;

    @FindBy(className = "search")
    private WebElementFacade btnBuscarComClassName;

    @FindBy(css = "button[type=submit]")
    private WebElementFacade btnBuscarComCssSubmit;

    @FindBy(linkText = "ACESSO")
    private WebElementFacade linkAcesso;

    @FindBy(name = "mte_num_cgc_0")
    private WebElementFacade numCnpj;

    @FindBy(name = "mte_insc_est_0")
    private WebElementFacade inscricaoEstadual;

    @FindBy(name = "mte_insc_mun_0")
    private WebElementFacade inscricaoMunicipal;

    @FindBy(name = "cb_parc")
    private WebElementFacade btnParcelar;

    @FindBy(id = "txtp_string")
    private WebElementFacade lblQuantidadeParcelas;

    @FindBy(className = "bt")
    private WebElementFacade btnOk;

    @FindBy(linkText = "Pagamento")
    private WebElementFacade opcaoPagamento;

    @FindBy(id = "a_pagamento")
    private WebElementFacade btnPagamento;

    @FindBy(name = "rdi_obs_0")
    private WebElementFacade lblObservacaoFormaPagamento;

    @FindBy(id = "a_receber")
    private WebElementFacade lnkPagamentoAReceber;

    @FindBy(name = "rdi_num_banco_0")
    private WebElementFacade lblNumeroBanco;

    @FindBy(name = "rdi_num_agencia_0")
    private WebElementFacade lblNumeroAgencia;

    @FindBy(name = "rdi_num_conta_0")
    private WebElementFacade lblNumeroConta;

    @FindBy(name = "rdi_num_cheque_0")
    private WebElementFacade lblNumeroCheque;

    @FindBy(name = "rdi_praca_0")
    private WebElementFacade cboPraca;

    @FindBy(id = "bt_gravar")
    private WebElementFacade btnGravar;

    @FindBy(id = "a_deposito")
    private WebElementFacade lnkDeposito;

    @FindBy(name = "mte_mte_str_recep_0")
    private WebElementFacade cboRecepcao;

    @FindBy(name = "mte_mte_tca_cod_0")
    private WebElementFacade cboTipoCaucao;

    @FindBy(xpath = "//*[@id='dw_rdi_summary']/span")
    private WebElementFacade divValorTotal;

    @FindBy(xpath = "//div[@class='ico new' and contains(text(),'Nova Parcela')]")
    private WebElementFacade btnNovaParcela;

    @FindBy(name = "nfl_serie_aux_0")
    private WebElementFacade lblSerieNotaFiscal;

    @FindBy(className = "print")
    private WebElementFacade divImprimirRecibo;

    @FindBy(id = "bt_print")
    private WebElementFacade btnImprimirRecibo;

    @FindBy(name = "nfl_num_0")
    private WebElementFacade lblNumeroNotaFiscal;

    @FindBy(name = "btn_ok_0")
    private WebElementFacade btnGravarNotaFiscal;

    @FindBy(name = "txt_0")
    private WebElementFacade cboTipoRecibo;

    private StringBuffer verificationErrors = new StringBuffer();
    public static String numeroNotaFiscal;
    public static String valorTotalPagamento;

    public void carregarTelaPagamento(){
        divAguarde();
            getDriver().switchTo().frame(0);
        divAguarde();
    }

    public void iniciarPagamento(){
        carregarTelaPagamento();
        if (btnPagar.isPresent()) {
            btnPagar.click();
            divAguarde();
        }
    }

    public void clicarBotaoPagamento(){
        if(btnPagamento.isPresent()) {
            btnPagamento.waitUntilClickable().click();
            carregarTelaPagamento();
        }
    }

    public void clicarBotaoPagamentoAReceber(){
        lnkPagamentoAReceber.waitUntilClickable().click();
    }

    public void iniciarPagamentoComResponsavelFinanceiro(String tipoPessoa){
        iniciarPagamento();
        if(responsavelFinanceiro.isPresent()){
            divAguarde();
            responsavelFinanceiro.click();
        }

        if(selecionarTipoPessoa.isPresent()) {
            selecionarTipoPessoa.waitUntilEnabled().click();
            selecionarTipoPessoa.selectByVisibleText(tipoPessoa);
            divAguarde();
        }
    }

    public void clicarLinkDeposito(){
        if(lnkDeposito.isPresent()) {
            lnkDeposito.waitUntilClickable().click();
            divAguarde();
            carregarTelaPagamento();
        }
    }

    public void selecionarFormaPagamento(String formaPagamento){
        if(cboFormaPagamento.isPresent()) {
            cboFormaPagamento.click();
            cboFormaPagamento.selectByVisibleText(formaPagamento);
        }
    }

    public void selecionarCartao(String cartao){
        cboCartao0.waitUntilClickable().click();
        cboCartao0.selectByVisibleText(cartao);
    }

    public void digitarDataDeVencimento(String vencimento){
        lblVencimento0.waitUntilEnabled().click();
        lblVencimento0.sendKeys(vencimento);
    }

    public void digitarDataDeValidade(String validade){
        lblValidade0.waitUntilEnabled().click();
        lblValidade0.sendKeys(validade);
    }

    public void digitarDataDeVencimentoSegundaParcela(String vencimento){
        lblVencimento1.waitUntilEnabled().click();
        lblVencimento1.sendKeys(vencimento);
    }

    public void digitarDataDeValidadeSegundaParcela(String validade){
        lblValidade1.waitUntilEnabled().click();
        lblValidade1.sendKeys(validade);
    }

    public void clicarBotaoParcelar(){
        btnParcelar.click();
    }

    public void digitarQuantidadeDeParcelas(String parcelas){
        lblQuantidadeParcelas.waitUntilEnabled().type(parcelas);
    }

    public void clicarBotaoOk(){
        btnOk.click();
        divAguarde();
    }

    public void escreverValorTotal(String valorTotal){
        if(lblValorTotal.isPresent()) {
            lblValorTotal.waitUntilClickable().click();
            lblValorTotal.type(valorTotal);
        }
    }

    public String returnSubtracaoDeValorTotalComPrimeiroValorDeFormaDePagamento(){
        return format(Double.parseDouble(divValorTotal.getText().replaceAll(",", ".")) - Double.parseDouble(lblValor0.getValue().replaceAll(",", ".")));
    }

    public void clicarBotaoNovaParcela(){
        btnNovaParcela.waitUntilClickable().click();
    }

    public void selecionarRecepcao(String recepcao){
        cboRecepcao.click();
        cboRecepcao.selectByVisibleText(recepcao);
    }

    public void selecionarTipoCaucao(String tipoCaucao){
        cboTipoCaucao.click();
        cboTipoCaucao.selectByVisibleText(tipoCaucao);
    }

    public void gravarPagamento(){
        if(btnGravarPagamento.isPresent()){
            btnGravarPagamento.click();
            cboFormaPagamento.waitUntilClickable().waitUntilEnabled().waitUntilPresent();
            divAguarde();
        }

    }

    public void clicarBotaoGravarPagamento(){
        if(btnGravarFormaDePagamento.isPresent()) {
            divAguarde();
            btnGravarFormaDePagamento.click();
            divAguarde();
        }

        if(lnkImprimirRecibo.isVisible()) {
            lnkImprimirRecibo.isVisible();
            clicarLinkImprimirRecibo();
        }
    }

    public void escolherFormaDePagamento(String cartao, String vencimento, String validade, String formaPagamento){
        if(cboFormaPagamento1.isPresent()) {
            cboFormaPagamento1.waitUntilVisible().click();
            cboFormaPagamento1.selectByVisibleText(formaPagamento);
            divAguarde();
        }
        if(cboCartao1.isPresent()) {
            cboCartao1.selectByVisibleText(cartao);
            divAguarde();
        }
        if(lblVencimento1.isPresent()) {
            lblVencimento1.waitUntilEnabled().click();
            lblVencimento1.sendKeys(vencimento);
        }

        if(lblValidade1.isPresent()) {
            lblValidade1.waitUntilEnabled().click();
            lblValidade1.sendKeys(validade);
        }
    }

    public void informarValorParcialEmEspecie(String valor){
        divAguarde();
        lblValor0.waitUntilEnabled().click();
        lblValor0.typeAndTab(valor);
        divAguarde();
        campoObservacao.waitUntilClickable().click();
    }

    public void selecionarDesconto(String valor, String tpDesconto){
        if(descontoLivre.isPresent()) {
            descontoLivre.selectByVisibleText(tpDesconto);
        }

        if(tpDesconto.equals("SGE DESCONTO")){
            divAguarde();
            descontoLivreValor.waitUntilVisible().waitUntilClickable().click();
            descontoLivreValor.typeAndTab(valor);
        }
        divAguarde();
    }

    public void preencherEndereco(){
        if(cepResponsavel.isPresent()){
            cepResponsavel.click();
            cepResponsavel.type("40080166");
        }

        if(buscaCidade.isPresent()){
            buscaCidade.click();
        }

        if(nomeCidade.isPresent()){
            nomeCidade.waitUntilPresent().click();
            nomeCidade.typeAndTab("SALVADOR");
        }

        if(btnBuscarComClassName.isPresent()){
            btnBuscarComClassName.click();
        }

        if(linkCidade.isPresent()){
            linkCidade.click();
        }
        divAguarde();
        if(enderecoResponsavel.isPresent()){
            enderecoResponsavel.click();
            enderecoResponsavel.type("RUA POLITEAMA DE BAIXO");
        }

        if(enderecoNumero.isPresent()){
            enderecoNumero.click();
            enderecoNumero.type("298");
        }

        if(emailResponsavel.isPresent()){
            emailResponsavel.waitUntilEnabled().click();
            emailResponsavel.type("isaias.silva@pixeon.com");
        }

        if(bairroResponsavel.isPresent()){
            bairroResponsavel.click();
            bairroResponsavel.type("POLITEAMA");
        }
        divAguarde();
        if(buscaLogradouro.isPresent()){
            buscaLogradouro.click();

            if(descricaoLogradouro.isPresent()) {
                descricaoLogradouro.click();
                descricaoLogradouro.typeAndTab("acesso");
            }
        }

        if(btnBuscarComCssSubmit.isPresent()){
            btnBuscarComCssSubmit.click();
        }

        divAguarde();
        if(linkLogradouro.isPresent()) {
            linkLogradouro.click();
        }
    }

    public void registrarPessoaFisica() {
        if (foneResponsavel.isPresent()){
            foneResponsavel.waitUntilEnabled().click();
            foneResponsavel.type("7133325696");
         }
        if(responsavel.isPresent()) {
            responsavel.waitUntilClickable().waitUntilVisible().waitUntilEnabled().waitUntilPresent().click();
            responsavel.type("TESTE RESPONSÁVEL FINANCEIRO PF");
            rgResponsavel.waitUntilEnabled().click();
            rgResponsavel.type("143710023");
        }
    }

    public void nascimentoCpfSexo(){
        if(numeroCpf.isPresent()){
            numeroCpf.click();
            numeroCpf.typeAndTab("65881325060");
            numeroCpf.click();
        }

        if(nascimentoResponsavel.isPresent()){
            nascimentoResponsavel.waitUntilClickable().click();
            nascimentoResponsavel.sendKeys("27/01/1991");
        }

        if(sexoResponsavel.isPresent()) {
            sexoResponsavel.waitUntilEnabled().click();
            sexoResponsavel.selectByVisibleText("Feminino");
        }
    }

    public void registrarCnpj(){
        numCnpj.waitUntilClickable().click();
        numCnpj.typeAndTab("90719613000176");
    }

    public void fecharTelaPagamento(){
        carregarFrameDefault();
        closePanel();
        divAguarde();
    }

    public void registrarPessoaJuridica(){
        registrarCnpj();
        if(foneResponsavel.isPresent()) {
            foneResponsavel.waitUntilEnabled().click();
            foneResponsavel.type("71965656965");
        }
        if(responsavel.isPresent()) {
            responsavel.waitUntilEnabled().click();
            responsavel.type("TESTE RESPONSÁVEL FINANCEIRO PJ");
        }
        if(inscricaoEstadual.isPresent()) {
            inscricaoEstadual.waitUntilEnabled().click();
            inscricaoEstadual.type("5968");
            inscricaoMunicipal.waitUntilEnabled().click();
            inscricaoMunicipal.type("564");
        }
    }

    public void escreverNumeroBanco(String numBanco){
        lblNumeroBanco.type(numBanco);
    }

    public void escreverNumeroAgencia(String numAgencia){
        lblNumeroAgencia.type(numAgencia);
    }

    public void escreverNumeroConta(String numConta){
        lblNumeroConta.type(numConta);
    }

    public void escreverNumeroCheque(String numCheque){
        lblNumeroCheque.type(numCheque);
    }

    public void selecionarPraca(String praca){
        cboPraca.click();
        cboPraca.selectByValue(praca);
    }

    public void clicarBotaoGravar(){
        btnGravar.click();
    }

    public void finalizarPagamentoSemDescontoVerificandoValor(){
        iniciarPagamento();
        if(btnGravarPagamento.isPresent()) {
            btnGravarPagamento.click();
            divAguarde();
        }

        if(btnGravarFormaDePagamento.isPresent()) {
            btnGravarFormaDePagamento.click();
            divAguarde();
        }

        fecharJanelasAbertas();
    }

    public void clicarLinkNotaFiscal(){
        if (!validarLinkNotaFiscal())
            carregarFrame(0);

        if(lnkNotaFiscal.isPresent()) {
            lnkNotaFiscal.waitUntilClickable().click();
            carregarFrameDefault();
            lblSerieNotaFiscal.waitUntilVisible();
        }
    }

    public void clicarLinkImprimirRecibo(){
        //carregarTelaPagamento();
        //Seta para baixo
        if(btnGravarFormaDePagamento.isPresent()){
            btnGravarFormaDePagamento.sendKeys(Keys.PAGE_DOWN);
        }

        if(lnkImprimirRecibo.isPresent()) {
            lnkImprimirRecibo.waitUntilClickable().click();
            divAguarde();
        }
    }

    public void clicarBotaoImprimirRecibo(){
        carregarFrameDefault();
        if(btnImprimirRecibo.isPresent()) {
            btnImprimirRecibo.waitUntilClickable().click();
            divAguarde();
            divAguarde();
            divAguarde();
        }
    }

    public void gravarNotaFiscal(){
        numeroNotaFiscal = null;
        btnGravarNotaFiscal.waitUntilClickable().click();
        divAguarde();
        numeroNotaFiscal = lblNumeroNotaFiscal.getValue();
    }

    public void finalizarPagamentoSemDesconto(){
        finalizarPagamentoSemDescontoVerificandoValor();
        if(lnkNotaFiscal.isPresent()){
            lnkNotaFiscal.waitUntilVisible();
        }
    }

    public void scroolUp(){
        //executeScript("window.scrollBy(x-pixels,y-pixels)");
        //x-pixels é o número no eixo x, ele se move para a esquerda se o número for positivo e para a direita se o número for negativo .
        //y-pixels é o número no eixo y, se move para baixo se o número for positivo e passa para cima se o número estiver negativo.

        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
        divAguarde();
        ((JavascriptExecutor)getDriver()).executeScript("window.scrollBy (0,-1000)");
        //ROLAGEM PARA CIMA. Foi necessário chamar o switchTo para a rolagem ser realizada na barra de navegaçao mais externa.
        //Comentando as duas linhas do switch acima, a rolagem ficará dentro da tela de Pagamento(tela em que está o FOCO)

    }

    public void fecharJanelasAbertas() {
        divAguarde();
        //ROLAR A BARRA DE ROLAGEM PARA CIMA.
        scroolUp();

        if (closeJanelas.isVisible() && closeJanelas.isPresent()) {
            closeJanelas.click();
        }
    }

    public void finalizarPagamentoComDesconto(String valor, String tpDesconto){
        iniciarPagamento();
        selecionarDesconto(valor, tpDesconto);

        if(btnGravarPagamento.isPresent()) {
            divAguarde();
            btnGravarPagamento.click();
        }

        if(btnGravarFormaDePagamento.isPresent()){
            btnGravarFormaDePagamento.click();
        }

    }

    public void finalizarPagamentoComValorParcial(String valor, String formaPagamento, String cartao, String vencimento, String validade){
        iniciarPagamento();
        btnGravarPagamento.waitUntilPresent().click();
        divAguarde();
        informarValorParcialEmEspecie(valor);
        novoPagamento.waitUntilClickable().click();
        escolherFormaDePagamento(cartao, vencimento, validade, formaPagamento);
        if(btnGravarFormaDePagamento.isPresent()){
            btnGravarFormaDePagamento.click();
        }
        divAguarde();
    }

    public void finalizarPagamentoTipoPessoaFisicaJuridica(String tipoPessoa, String cpfCnpj){
        iniciarPagamentoComResponsavelFinanceiro(tipoPessoa);
        if(tipoPessoa.equals("P. Física")){
            finalizarPagamentoPessoaFisica(cpfCnpj);
        }else if(tipoPessoa.equals("P. Jurídica")){
            finalizarPagamentoPessoaJuridica(cpfCnpj);
        }else{
            finalizarPagamentoPessoaEstrangeira();
        }
        divAguarde();
    }

    public void finalizarPagamentoPessoaFisica(String cpfCnpj) {
        nascimentoCpfSexo();
        registrarPessoaFisica();
        preencherEndereco();
        if(campoObservacao.isPresent()){
            campoObservacao.click();
            campoObservacao.type("TESTE RESPONSAVEL FINANCEIRO PESSOA FÍSICA OBS");
        }

        if(btnGravarPagamento.isPresent()) {
            btnGravarPagamento.click();
        }
    }

    public void finalizarPagamentoPessoaJuridica(String cpfCnpj){
        registrarPessoaJuridica();
        preencherEndereco();

        if(campoObservacao.isPresent()){
            campoObservacao.click();
            campoObservacao.type("TESTE RESPONSAVEL FINANCEIRO PESSOA JURÌDICA OBS");
        }

        if(btnGravarPagamento.isPresent()) {
            btnGravarPagamento.click();
        }
    }

    public void finalizarPagamentoPessoaEstrangeira(){
        registrarPessoaFisica();
        preencherEndereco();
        if(campoObservacao.isPresent()) {
            campoObservacao.waitUntilEnabled().click();
            campoObservacao.type("TESTE RESPONSAVEL FINANCEIRO ESTRANGEIRO OBS");
        }

        if(btnGravarPagamento.isPresent()) {
            btnGravarPagamento.click();
        }
    }

    public void escreverObservacaoFormaPagamento(String observacaoFormaPagamento){
        if(lblObservacaoFormaPagamento.isPresent()) {
            lblObservacaoFormaPagamento.type(observacaoFormaPagamento);
        }
    }

    public static String format(double x) {
        return String.format("%.2f", x);
    }

    public boolean validarDesconto(){
        divAguarde();
        if(lblValorTotal.isPresent()) {
            total = lblValorTotal.waitUntilVisible().getTextValue().replaceAll(",", ".");
        }

        if(valorDesconto.isPresent()) {
            desconto = valorDesconto.getTextValue().replaceAll(",", ".");
            valorComDesconto = Double.parseDouble(total) - Double.parseDouble(desconto);
            valorExame = format(valorComDesconto);
        }

        if( lblValor0.getTextValue().equals(valorExame) ){
            descontoAplicado = true;
            System.out.println("Desconto foi Aplicado");
        }else{
            descontoAplicado = false;
            System.out.println("Desconto não foi Aplicado");
            Assert.fail("Desconto não foi Aplicado");
        }
        divAguarde();

        return descontoAplicado;

    }

    public boolean validarValorParcial(){
        lnkNotaFiscal.waitUntilVisible();
        aPagarTotal = aPagar.getValue().replaceAll(",", ".");
        primeiroValor = lblValor0.getValue().replaceAll(",", ".");
        valorComDescontoParcial = Double.parseDouble(aPagarTotal) - Double.parseDouble(primeiroValor);
        valorExameParcial = format(valorComDescontoParcial);
        if(lblValor1.getValue().equals(valorExameParcial)){
            campoValor1Parcial = true;
        }else{
            campoValor1Parcial = false;
        }
        return campoValor1Parcial;
    }

    public boolean validarCheckDeItemFaturado(){
        if(checkFaturado.isPresent()) {
            checkFaturado.getTextValue().contains("Faturada");
        } else {
            Assert.fail("BUG ENCONTRADO. O RESULTADO ENCONTRADO É DIFERENTE DO RESULTADO ESPERADO!");
        }
        return checkFaturado.isPresent();
    }

    public boolean validarLinkNotaFiscal(){
        if(btnGravarFormaDePagamento.isPresent()){
            btnGravarFormaDePagamento.click();
            divAguarde();
        }

      //  carregarTelaPagamento();
      //  divAguarde();

        if(btnPagamento.isPresent() || opcaoPagamento.isPresent()){
            btnPagamento.click();
            divAguarde();
            carregarTelaPagamento();

            if(observacaoDescricao.isPresent()){
                //Clica e insere a observacao para FOCAR na tela Interna e permitir o scrool.
                observacaoDescricao.click();
                observacaoDescricao.type("TESTE");

                //Rolar para baixo
                scroolDown();
              //  scroolDown();
            }

            if(lnkNotaFiscal.isPresent()){
                lnkNotaFiscal.click();
                lnkNotaFiscal.getTextValue().contains("Nota Fiscal");
            } else {
                Assert.fail("BUG ENCONTRADO. O RESULTADO ENCONTRADO É DIFERENTE DO RESULTADO ESPERADO!");
            }
        }

        return lnkNotaFiscal.isPresent();
    }

    public void validarValorParcelas(int quantidadeParcelas){
        if(lblValor0.isPresent() && lblValor1.isPresent()){
            lblValor0.getTextValue().equals("26,00");
            lblValor1.getTextValue().equals("26,00");
            Assert.assertTrue("Duas Parcelas de 26,00 é Exibida", lblValor0.isVisible());
            Assert.assertTrue("Duas Parcelas de 26,00 é Exibida", lblValor1.isVisible());
        } else {
            Assert.fail("BUG ENCONTRADO. O RESULTADO OBTIDO, É DIFERENTE DO RESULTADO ESPERADO");
        }
    }

    public void validarValorDeposito(String valorDeposito){
        try{
            Assert.assertEquals(valorDeposito, lblValor0.getValue());
        }catch(Error e){
            verificationErrors.append(e.toString());
        }
    }

    public void validarValorRestanteDeposito(){
        try{
            Assert.assertEquals(returnSubtracaoDeValorTotalComPrimeiroValorDeFormaDePagamento(), lblValor1.getValue());
        }catch (Error e){
            verificationErrors.append(e.toString());
        }
    }

    public void pegarValorTotalPagamento() {
        valorTotalPagamento = divValorTotal.getText();
    }

}
