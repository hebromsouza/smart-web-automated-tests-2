package com.pixeon.pages.atendimento.pagamento;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageRecebimentos extends BasePage {

    @FindBy(xpath = "//*/span/a")
    private WebElementFacade lnkNumeroRecebimento;

    @FindBy(id = "bt_print")
    private WebElementFacade btnImprimirRecibo;

    @FindBy(id = "msgDivBox")
    private WebElementFacade divAguarde;

    public void clicarLinkNumeroRecebimento(){
        lnkNumeroRecebimento.waitUntilClickable().click();
    }

    public void clicarBotaoImprimirRecibo(){
        btnImprimirRecibo.waitUntilClickable().click();
        divAguarde.waitUntilNotVisible();
    }

    public void validarRecebimentoDePagamento(){
        Assert.assertTrue(lnkNumeroRecebimento.isVisible());
    }

}
