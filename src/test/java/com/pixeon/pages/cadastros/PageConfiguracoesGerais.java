package com.pixeon.pages.cadastros;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageConfiguracoesGerais extends BasePage {


    //txtTempoDeExpiracaoDaSessao
    @FindBy(xpath = "//input[@id='SMART.AUTOLOGOUT']")
    private WebElementFacade txtTempoDeExpiracaoDaSessao;

    //txtTempoDeExpiracao da Senha
    @FindBy(xpath = "//input[@id='SMART.USR_EXP_PR']")
    private WebElementFacade txtTempoDeExpiracaoSenha;

    //txtnumeroMaximoDetentativaDeLogin
    @FindBy(xpath = "//input[@id='SMART.MAX_LOGIN']")
    private WebElementFacade txtnumeroMaximoDetentativaDeLogin;

    //txtE-mail
    @FindBy(xpath = "//input[@id='WEB.EMAIL_TI']")
    private WebElementFacade txtEmail;

    //txtDoGerente
    @FindBy(xpath = "//input[@id='WEB.EMAIL_GER']")
    private WebElementFacade txtDoGerente;

    //txtLimiteDeOcupante
    @FindBy(xpath = "//input[@id='WEB.INI_LIMIAR']")
    private WebElementFacade txtLimiteDeOcupante;

    //btnGrava
    @FindBy(xpath = "//button/*[text()='Gravar']")
    private WebElementFacade btnGravar;

    //btnFechar
    @FindBy(xpath = "// *[@class='container-close'] [text()='Close']")
    private WebElementFacade btnFechar;


    //Resolver interacao com elementos da pagina que estao dentro de frames
    public void carregarTelaInicial() {
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
        getDriver().switchTo().frame(0);
    }


    public void clicarbtnGravar() {
        carregarTelaInicial();
        btnGravar.waitUntilPresent().click();
    }


    public void clicarbtnFechar() {
         getDriver().switchTo().parentFrame();
          btnFechar.waitUntilPresent().click();
    }


    public void informarTempoDeExpiracaoDaSessao(String tempo) {
          carregarTelaInicial();
          txtTempoDeExpiracaoDaSessao.waitUntilPresent().type(tempo);
    }


    public void informatetativalogin(String tetativa) {
        carregarTelaInicial();
        txtnumeroMaximoDetentativaDeLogin.waitUntilPresent().type(tetativa);
    }


}
