package com.pixeon.pages.cadastros;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;

public class PageCadastros extends BasePage {

    //listcboAdicionarElement
    @FindBy(xpath = "//*[@class='bd_cadastros']//ul/li")
    private WebElementFacade listaCadastro;



    //Resolver interacao com elementos da pagina que estao dentro de frames
    public void carregarTelaInicial() {
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }

    public void clicarNoTipoCadastro(String cadastro ) {
       // carregarTelaInicial();
        try {
            listaCadastro.waitUntilPresent();
            String xp="//*[@class='bd_cadastros']//ul/li//*[text()='"+cadastro+"']";
            getDriver().findElement(By.xpath(xp)).click();

        } catch (StaleElementReferenceException e) {
            Logger.getLogger("StaleElementReferenceException - menu: " + e.getMessage());
        } catch (Exception e) {
            Logger.getLogger("StaleElementReferenceException - menu: " + e.getMessage());
        }
    }

    public void clicarLinkoConfiguracao(String configuracao ) {
        // carregarTelaInicial();
        try {
            listaCadastro.waitUntilPresent();
            String xp="//*[@class='panelRight']//ul/li//*[text()='"+configuracao+"']";
            getDriver().findElement(By.xpath(xp)).click();
        } catch (StaleElementReferenceException e) {
            Logger.getLogger("StaleElementReferenceException - menu: " + e.getMessage());
        } catch (Exception e) {
            Logger.getLogger("StaleElementReferenceException - menu: " + e.getMessage());
        }
    }

    public void clicarLinkoCadastrosMedicos (String cadastrosMedicos ) {
        // carregarTelaInicial();
        try {
            listaCadastro.waitUntilPresent();
            String xp="//* [@id=1cadastros_gerais1]//a[text()="+cadastrosMedicos+"']";
            ((JavascriptExecutor)getDriver()).executeScript("scroll(0,400)");
            getDriver().findElement(By.xpath(xp)).click();
        } catch (StaleElementReferenceException e) {
            Logger.getLogger("StaleElementReferenceException - menu: " + e.getMessage());
        } catch (Exception e) {
            Logger.getLogger("StaleElementReferenceException - menu: " + e.getMessage());
        }
    }
}
