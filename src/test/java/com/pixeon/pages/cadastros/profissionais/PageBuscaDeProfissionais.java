package com.pixeon.pages.cadastros.profissionais;

import com.pixeon.datamodel.cadastros.DataModelBuscaDeProfinionais;
import com.pixeon.pages.BasePage;
import com.pixeon.util.DataHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import java.util.List;

public class PageBuscaDeProfissionais extends BasePage {


    //TXTConselho
    @FindBy(xpath = "//*[@name='psv_crm_0']")
    private WebElementFacade txConselho;

    //nome
    @FindBy(xpath = "//*[@name='psv_nome_0']")
    private WebElementFacade txbtnNome;

    //Filtro
    @FindBy(xpath = "//*[@type='submit' and @value='Filtrar']")
    private WebElementFacade btnFiltrar;

    //tabela filtro
    @FindBy(xpath = "//*[@id='dw_psv_master_datawindow']")
    private WebElementFacade TabelaFiltro;

    //  //*[@id="dw_psv_master_datawindow"]//*//*[text()='ADRIANO ROCHA']
    public void preencheCamposDeBuscaDePafissionais(List<DataModelBuscaDeProfinionais> dataModelBuscaDeProfinionais) {
        DataModelBuscaDeProfinionais data = DataHelper.getData(dataModelBuscaDeProfinionais);
        //get nome
        if (data.conselho() != null) {
            txConselho.waitUntilPresent().type(data.conselho());
        }
        //get nome
        if (data.nome() != null) {
            txbtnNome.waitUntilPresent().type(data.nome());
        }
    }

    //clicar no botão Filtrar
    public void clicaFiltrar() {
        btnFiltrar.waitUntilPresent().click();
    }

    //clicar no nome do profissional
    public void clicaNoNomeDoProficonal( String nomeDoProficional) {
        try {
            TabelaFiltro.waitUntilPresent();
            String xp = "//*[@id='dw_psv_master_datawindow']//*//*[text()='" + nomeDoProficional + "']";
            getDriver().findElement(By.xpath(xp)).click();
        } catch (Exception e) {
            Logger.getLogger("StaleElementReferenceException - nome do proficonal não encontrado: " + e.getMessage());
        }
    }
}

