package com.pixeon.pages.cadastros.profissionais;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageDadosDosProfissionais extends BasePage {

    //CHK buton Supervisor
    @FindBy(xpath ="//*[@type='checkbox' and @name='psv_psv_sob_supervisao_0']")
    private WebElementFacade chkSobSupervisao;

    //piverispr imeidato
    @FindBy(xpath = "//*[@name='psv_psv_surpervisor_0']")
    private WebElementFacade cboSupervisorImediato;

    //btnGravar
    @FindBy(xpath = "//*[@id='bt_psv_save']/div")
    private WebElementFacade btnGravar;

    //btnFechar
    @FindBy(xpath = "//*[@class='container-close']")
    private WebElementFacade btnFechar;


    //selecionor chk Supervisao
    public void selecianarchkSupervisao() {
        chkSobSupervisao.waitUntilPresent();
        if(!chkSobSupervisao.isSelected()) {
            chkSobSupervisao.click();
        }
    }

    //clicar no botão gravar
    public void clicarGravar() {
        btnGravar.waitUntilPresent().click();
    }

    //clicar no botão Fechar
    public void clicarFechar() {
        btnFechar.waitUntilPresent().click();
    }

    //elecionar Superviso Imediato
    public void selecionarSupervisoImediato(String supervisor) {
        cboSupervisorImediato.waitUntilPresent();
        cboSupervisorImediato.click();
        cboSupervisorImediato.waitUntilPresent().selectByVisibleText(supervisor);

    }



}
