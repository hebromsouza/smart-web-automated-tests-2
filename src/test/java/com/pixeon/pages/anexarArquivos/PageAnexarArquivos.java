package com.pixeon.pages.anexarArquivos;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.awt.*;
import java.awt.event.KeyEvent;

public class PageAnexarArquivos extends BasePage {

    @FindBy(id = "bt_arquivos")
    private WebElementFacade btnAnexar;

    @FindBy(id = "msgBoxDiv")
    private WebElementFacade divAguarde;

    @FindBy(id = "enable_flash_link")
    private WebElementFacade lnkHabilitarFlash;

    @FindBy(xpath = "//*[@class='bd']//*[text()='Adicionar Arquivos...']")
    private WebElementFacade btnAdicionarArquivos;

    @FindBy(xpath = "//*[@id='ged']/*[@class='arq']")
    private WebElementFacade arquivoAnexado;

    public void clicarBotaoAnexarArquivos() throws AWTException {

        StringBuffer verificationErrors = new StringBuffer();
        Robot robot;

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        getDriver().findElement(By.xpath("//a[@onclick=\"foto(13);\"]")).click();

        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
        getDriver().switchTo().frame("frm_foto");

        getDriver().findElement(By.xpath("//embed")).click();

        robot = new Robot();
        robot.keyPress(KeyEvent.VK_TAB);
        robot.keyRelease(KeyEvent.VK_TAB);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);


        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btnAnexar.waitUntilClickable().click();
        divAguarde.waitUntilNotVisible();
    }

    public void aceitarPopUpInstalarPluginFlash(){
        if(waitHelper.waitAlertGetText().equals("Para anexar arquivos será necessário instalar o plugin flash no navegador.")){
            waitHelper.alertOk();
        }
    }

    public void clicarLinkHabilitarFlash(){
        lnkHabilitarFlash.waitUntilClickable().click();
    }

    public void clicarPermitirExecucaoComFlash(){
        if(waitHelper.waitAlertGetText().equals("Executar Flash")){
            waitHelper.waitAlertOk();
        }
    }

    public void habilitarFlash(){
//        aceitarPopUpInstalarPluginFlash();
//        clicarLinkHabilitarFlash();
//        clicarPermitirExecucaoComFlash();

    }

    public void clicarAdicionarArquivos(){
        getDriver().switchTo().frame(0);
        try {
            getDriver().switchTo().frame(0);
            btnAdicionarArquivos.waitUntilEnabled();
            btnAdicionarArquivos.waitUntilPresent().click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void validarArquivoAnexado(){
        getDriver().switchTo().frame(0);
        Assert.assertTrue(arquivoAnexado.waitUntilVisible().isPresent());
    }

}