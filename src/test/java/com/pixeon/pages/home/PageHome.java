package com.pixeon.pages.home;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;

public class PageHome extends BasePage {

    @FindBy(xpath = "//a[@href='scripts/../main.asp']")
    private WebElementFacade lblHome;

    @FindBy(xpath = "//*[@title='Logout']")
    private WebElementFacade btnDeslogar;

    @FindBy(xpath = "//*[@class='bd']//* [text()='Auditoria']")
    private WebElementFacade menuAuditoria;

    @FindBy(xpath = "//* [text()='Home']/../a")
    private WebElementFacade linkHome;

    //*[text()='Home']
    @FindBy(xpath = "//*[@class='information']/i[1]")
    private WebElementFacade labelsNomeEVesaoSoftware;

    @FindBy(xpath = "//*[@class='information']/i[2]")
    private WebElementFacade labelbuild;

    @FindBy(xpath = "//*/small/a")
    private WebElementFacade labelFornecedor;

    public void validaPageHome(){
        lblHome.isVisible();
    }

    public void validaVersaoSoftwareTelaHome(String nomeSoftware_versaoSoftware, String buildSoftware, String nomeFornecedor) {
        carregarTelaInicial();
        labelsNomeEVesaoSoftware.waitUntilPresent();
        Assert.assertEquals(nomeSoftware_versaoSoftware, labelsNomeEVesaoSoftware.getText());
        Assert.assertEquals(labelbuild.getText(), buildSoftware);
        Assert.assertEquals(labelFornecedor.getText(), nomeFornecedor);
    }

    public void clicarDeslogar() {
        btnDeslogar.waitUntilPresent().click();
    }

    public void validarQueMenuNaoEstaPresente(String menu) {
        carregarTelaInicial();
        Boolean existemMenu=false;
        if (menu.equals("Auditoria")) {
            existemMenu = menuAuditoria.isPresent();
            Assert.assertFalse(existemMenu);
        } else {
            //É ajusta o metodo
            Assert.assertTrue(existemMenu);
        }
    }

    public void validarTelaHomePresente() {
        try {
            carregarTelaInicial();
            linkHome.waitUntilPresent();
            Boolean  existemHome = linkHome.isPresent();
            Assert.assertTrue(existemHome);
        } catch (Exception e) {
            // TODO: handle exception
            Assert.assertTrue(false);
        }
    }

}
