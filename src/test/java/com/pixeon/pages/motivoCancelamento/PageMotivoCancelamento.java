package com.pixeon.pages.motivoCancelamento;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageMotivoCancelamento extends BasePage {

    @FindBy(name = "mot_cod_0")
    private WebElementFacade cboMotivoCancelamento;

    @FindBy(name = "obs_0")
    private WebElementFacade lblObservacao;

    @FindBy(id = "bt_mot_ok")
    private WebElementFacade btnMotivoOk;

    public void selecionarMotivoCancelamento(String motivo){
        if(cboMotivoCancelamento.isPresent()) {
            cboMotivoCancelamento.waitUntilClickable().click();
            cboMotivoCancelamento.selectByVisibleText(motivo);
        }
    }

    public void escreverObservacao(String obs){
        if(lblObservacao.isPresent()) {
            lblObservacao.type(obs);
        }
    }

    public void clicarBotaoOkMotivoCancelamento(){
        if(btnMotivoOk.isPresent()) {
            btnMotivoOk.click();
            divAguarde();
        }
    }

}
