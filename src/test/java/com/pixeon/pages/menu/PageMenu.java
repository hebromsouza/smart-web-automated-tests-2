package com.pixeon.pages.menu;

import com.pixeon.pages.BasePage;
import com.pixeon.util.SeleniumUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

import java.util.List;

public class PageMenu extends BasePage {

    @FindBy(xpath = "//*[@id='bd-main-menu']//ul/li")
    private List<WebElement> listMenuElement;


    @FindBy(xpath = "//*[@id='dw_filtro_detail_0']/select[3]")
    private WebElementFacade comboSetor;

    @FindBy(xpath = "//*[@id='dw_filtro_detail_0']/select[3]/option[@value='999']")
    private WebElementFacade setor99NaoEspecificado;

    @FindBy(xpath = "//*[@id='dw_filtro_detail_0']/select[3]/option[@value='FAT']")
    private WebElementFacade setorFaturamento;

    @FindBy(name = "dthr_ini_0")
    private WebElementFacade periodoInicial;

    public void clicarMenu(List<String> listMenu){
        SeleniumUtil.menuSmart(listMenuElement, listMenu);
    }

    public void clicarMenuOnly(List<String> listMenu){
        SeleniumUtil.menuSmart(listMenuElement, listMenu);
    }


    public void setorNaoEspecificado(){
        if(comboSetor.isPresent()){
         //   comboSetor.click();
            setor99NaoEspecificado.click();
        }
    }

    public void EscolherSetorDesejado(String setor){

        if(comboSetor.isPresent() && setor.equals("FATURAMENTO")){
            setorFaturamento.click();
        }

    }


    public void FiltrarPeloPeriodo(String dtInicial){
        String dataHoje = SeleniumUtil.getDataAtual();


        if(periodoInicial.isPresent()){
            periodoInicial.click();
       //     periodoInicial.type(dtInicial);
            periodoInicial.sendKeys(SeleniumUtil.getDataAtualFormato(dataHoje) + "00:00");
        }

    }



}