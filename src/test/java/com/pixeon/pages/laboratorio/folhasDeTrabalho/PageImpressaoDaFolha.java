package com.pixeon.pages.laboratorio.folhasDeTrabalho;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageImpressaoDaFolha extends BasePage {

    @FindBy(id = "btp_concluir")
    private WebElementFacade btnConcluir;

    public void clicarConcluir(){
        btnConcluir.waitUntilClickable().click();
    }

}
