package com.pixeon.pages.laboratorio.folhasDeTrabalho;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageGerarFolhasDeTrabalho extends BasePage {

    @FindBy(name = "str_cod_0")
    private WebElementFacade cboSetor;

    @FindBy(name = "bnc_cod_0")
    private WebElementFacade cboBancada;

    @FindBy(id = "msgBoxDiv")
    private WebElementFacade divAguarde;

    @FindBy(name = "bt_buscar_0")
    private WebElementFacade btnBuscar;

    @FindBy(name = "selected_0")
    private WebElementFacade chkFolhaTrabalho;

    @FindBy(id = "btp_sel")
    private WebElementFacade btnSelecionarFolhas;

    @FindBy(id = "btp_gerar")
    private WebElementFacade btnGerarFolhas;

    @FindBy(name = "bnc_bnc_nome_0")
    private WebElementFacade formNomeBancada;

    @FindBy(xpath = "//*[@id=\"dw_filtro_detail_0\"]/select[3]/option[@value=\"999\"]")
    private WebElementFacade setor99;


    public void setorNaoEspecificado(){
        setor99.click();
    }

    public void selecionarSetor(String setor){
        cboSetor.waitUntilClickable().click();
        cboSetor.selectByVisibleText(setor);
        divAguarde.waitUntilNotVisible();
    }



    public void selecionarbancada(String bancada){
        cboBancada.waitUntilClickable().click();
        cboBancada.selectByVisibleText(bancada);
    }

    public void clicarBuscar(){
        btnBuscar.waitUntilClickable().click();
        divAguarde.waitUntilNotVisible();
    }

    public void clicarFolhasDeTrabalho(){
        chkFolhaTrabalho.waitUntilClickable().click();
    }

    public void clicarSelecionarFolhas(){
        btnSelecionarFolhas.waitUntilClickable().click();
        // divAguarde.waitUntilNotVisible(); // Original
        divAguarde(); //ajuste gilson.lima
    }

    public void clicarGerarFolhas(){
        btnGerarFolhas.waitUntilClickable().click();
        // divAguarde.waitUntilNotVisible(); // Original
        divAguarde.isVisible();//ajuste gilson.lima
        divAguarde(); //ajuste gilson.lima
    }

    public void validarGeracaoDaFolhaDeTrabalho(String bancada){
        Assert.assertEquals(formNomeBancada.waitUntilVisible().getValue(), bancada);
    }

}