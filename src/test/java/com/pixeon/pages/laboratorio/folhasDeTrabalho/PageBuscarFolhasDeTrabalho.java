package com.pixeon.pages.laboratorio.folhasDeTrabalho;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageBuscarFolhasDeTrabalho extends BasePage {

    @FindBy(id = "bt_add")
    private WebElementFacade btnGerarFolhas;

    @FindBy(id = "bt_search")
    private WebElementFacade btnBuscar;

    @FindBy(id = "msgBoxDiv")
    private WebElementFacade divAguarde;

    @FindBy(name = "str_cod_0")
    private WebElementFacade cboSetor;

    @FindBy(name = "bnc_cod_0")
    private WebElementFacade cboBancada;

    @FindBy(name = "checado_0")
    private WebElementFacade chkFolhaEmitida;

    @FindBy(id = "bt_cancelar")
    private WebElementFacade btnCancelarSelecionados;

    @FindBy(name = "t_2_0")
    private WebElementFacade linkNumeroFolhaEmitida;

    public void clicarGerarFolhas(){
        btnGerarFolhas.waitUntilClickable().click();
    }

    public void selecionarSetor(String setor){
        cboSetor.waitUntilClickable().click();
        cboSetor.selectByVisibleText(setor);
        divAguarde.waitUntilNotVisible();
    }

    public void selecionarBancada(String bancada){
        cboBancada.click();
   //     cboBancada.selectByVisibleText(bancada);
    }

    public void clicarBuscarFolhasEmitidas(){
        btnBuscar.waitUntilClickable().click();
       // divAguarde.waitUntilNotVisible(); //Original
        divAguarde(); //ajuste gilson.lima
    }

    public String copiarNumeroDaFolhaDeTrabalho(){
        return linkNumeroFolhaEmitida.waitUntilVisible().getText();
    }

    public void checarPrimeiraFolhaEmitidaDaBusca(){
        chkFolhaEmitida.waitUntilClickable().click();
    }

    public void cancelarFolhasSelecionadas(){
        btnCancelarSelecionados.waitUntilClickable().click();
        if(waitHelper.waitAlertGetText().equals("Confirma o cancelamento da(s) folha(s) selecionada(s)?")){
            waitHelper.waitAlertOk();
            divAguarde.waitUntilNotVisible();
        }
    }

    public Boolean validarCancelamentoFolhaSelecionada(){
        if(linkNumeroFolhaEmitida.isVisible()){
            return true;
        }else{
            return false;
        }
    }

}
