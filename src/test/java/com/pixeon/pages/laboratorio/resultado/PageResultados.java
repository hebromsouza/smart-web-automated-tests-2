package com.pixeon.pages.laboratorio.resultado;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageResultados extends BasePage {

    @FindBy(xpath = "//*[@id='dw_rcl_detail_0']/input[1]")
    private WebElementFacade lblResultado;

    @FindBy(name = "atr_atr_rtxt_1")
    private WebElementFacade lblResultado2;

    @FindBy(xpath = "//span[@id='dw_osm_detail_0']/span[14]/div")
    private WebElementFacade lblObservacao;

    @FindBy(css = "input.row")
    private WebElementFacade checkExame;

    @FindBy(id = "bt_save")
    private WebElementFacade btnGravar;

    @FindBy(xpath = "//*[@id='yui-gen97']/a")
    private WebElementFacade Visualizar;

    @FindBy(xpath = "//*[@id='dw_rcl_detail_0']/span[2]/div/a")
    private WebElementFacade HIVGenotipagem;

    @FindBy(xpath = "//*[contains(text(),'Liberado')]")
    private WebElementFacade StatusLiberado;

    @FindBy(xpath = "//*[contains(text(),'Executado')]")
    private WebElementFacade StatusExecutado;

    @FindBy(xpath = "//*[contains(text(),'Exec (Av) ')]")
    private WebElementFacade StatusExec;

    @FindBy(css = "#dw_rcl_detail_0 > #hd")
    private WebElementFacade lblStatus;

    @FindBy(xpath = "//div[@class='curr' and @style='left: 33.3333%;']")
    private WebElementFacade divValorDeReferenciaMenor;

    @FindBy(xpath = "//div[@class='curr' and @style='left: 50%;']")
    private WebElementFacade divValorDeReferenciaMeio;

    @FindBy(xpath = "//div[@class='curr' and @style='left: 66.6667%;']")
    private WebElementFacade divValorDeReferenciaMaior;

    @FindBy(xpath = "//a[@class='smk_rot']")
    private WebElementFacade lblExame;

    @FindBy(id = "bt_avaliar")
    private WebElementFacade botaoAvaliar;

    @FindBy(linkText = "Executados")
    private WebElementFacade linkExecutados;

    @FindBy(id = "txtp_string")
    private WebElementFacade lblObsAvaliacao;

    @FindBy(css = "input.bt")
    private WebElementFacade botaoOk;

    @FindBy(id = "bt_liberar")
    private WebElementFacade btnLiberar;

    @FindBy(linkText = "Liberar")
    private WebElementFacade lnkLiberar;

    @FindBy(id = "bt_dupla_digit")
    private WebElementFacade btnConfirmarResultados;

    @FindBy(xpath = "//*[@id='dw_rcl_detail_0']/span[7]/a/div")
    private WebElementFacade btnLupaResultadoExame;

    @FindBy(xpath = "//*[@id=\"html_vref\"]/div[1]/span")
    private WebElementFacade ValorReferencia;

    @FindBy(xpath = "//*[@id=\"_panel89\"]/a")
    private WebElementFacade FecharJanela;

    @FindBy(name = "result_nao_editavel_0")
    private WebElementFacade txtResultadoNaoEditavel;

    @FindBy(id = "bt_print")
    private WebElementFacade btnVisualizarImprimir;

    @FindBy(name = "print_0")
    private WebElementFacade chkExame;

    @FindBy(linkText = "Resultados")
    private WebElementFacade lnkResultados;

    public void escreverResultado(String resultado) {
        divAguarde();
        if(lblResultado.isPresent()) {
            lblResultado.click();
            lblResultado.type(resultado);
        }
    }

    public void digitarResultado(String resultado2){
        lblResultado2.waitUntilClickable().type(resultado2);
    }

    public void clicarBotaoGravar(){
        if (btnGravar.isVisible()) {
            btnGravar.click();
            divAguarde();
        }
    }

    public void gravarResultado(String resultado) {
        escreverResultado(resultado);
        lblObservacao.click();
        checkExame.waitUntilClickable().click();
        if (validarValorDeReferencia(resultado)==true) {
            clicarBotaoGravar();
        }
    }

    public void VisualizarLaudo() {
        if(Visualizar.isPresent()){
            HIVGenotipagem.click();
            Visualizar.click();
            divAguarde();
        }
    }


    public void obterTextoStatus() {
        divAguarde();
        if(StatusLiberado.isPresent()){
            clicarBotaoGravar();

  //            Não faz nada, pois já tem o resultado desejado presente na tela...Se nao tiver é BUG.
 //            Assert.assertEquals("Liberado", StatusLiberado.getText());
//             System.out.println("GOOOOOOOOOL");
        } else {
            System.out.println("BUG ENCONTRADO, STATUS OBTIDO É DIFERENTE DO RESULTADO ESPERADO!");
        }
       // return "Liberado";//lblStatus.getText();
    }

    public boolean validarValorDeReferencia(String resultado) {
        boolean valor = false;
            if (resultado.equals("70,0")) {
                valor = divValorDeReferenciaMenor.isVisible();
            } else if(resultado.equals("90,0")){
                valor = divValorDeReferenciaMeio.isVisible();
            }else if (resultado.equals("110,0")) {
                valor = divValorDeReferenciaMaior.isVisible();
            }
         return valor;
    }

    public String obterTextoExame(){
        return lblExame.waitUntilVisible().getText();
    }

    public void avaliarResultadoExecutado(String resultado, String textAvaliacao){
 //       if (obterTextoStatus().equals ("Executado ")){
          if (StatusExecutado.equals ("Executado ")){

            if (validarValorDeReferencia(resultado)==true) {
                checkExame.waitUntilClickable().click();
                botaoAvaliar.waitUntilClickable().click();
                linkExecutados.waitUntilClickable().click();
                lblObsAvaliacao.waitUntilPresent().type(textAvaliacao );
                botaoOk.waitUntilClickable().click();
            }
        }
    }

    public void liberarResultado(){
        if (StatusExec.equals("Exec (Av) ")) {
         //   if (obterTextoStatus().equals("Exec (Av) ")) {
            checkExame.waitUntilClickable().click();
            btnLiberar.waitUntilClickable().click ();
            linkExecutados.waitUntilClickable().click ();
        }
    }

    public void clicarExame(){
        lblExame.click();
    }

    public void clicarLinkLiberar(){
        if(lnkLiberar.isPresent()) {
            lnkLiberar.click();
            divAguarde();
        }
    }

    public void clicarBotaoConfirmarResultaods(){
        if(btnConfirmarResultados.isPresent()) {
            btnConfirmarResultados.click();
            divAguarde();
        }
    }

    public void validarMensagemPopUp(String mensagemPopUp){
        if(waitHelper.waitAlertGetText().contains(mensagemPopUp)){
            waitHelper.alertOk();
        }
    }

    public void clicarLupaResultadosExame() {
        if(btnLupaResultadoExame.isPresent()) {
            btnLupaResultadoExame.click();
            lblResultado.waitUntilVisible().waitUntilPresent();
        }
    }

    public void ValidarValorReferencia() {
        if(ValorReferencia.isPresent()) {
            ValorReferencia.click();
            if(FecharJanela.isPresent()) {
                FecharJanela.click();
            }
        }
    }

    public void validarResultadoNaoEditavel(String resultado){
        Assert.assertEquals(resultado, txtResultadoNaoEditavel.getValue());
    }

    public void clicarBotaoVisualizarImprimirLaudo() {
        if(btnVisualizarImprimir.isPresent()) {
            btnVisualizarImprimir.click();
            divAguarde();
        }
    }

    public void clicarCheckBoxExame(){
        if (chkExame.getValue().equals("N")) chkExame.click();
    }

    public void clicarLinkResultados(){
        if(lnkResultados.isPresent()) {
            divAguarde();
            lnkResultados.waitUntilPresent().click();
            divAguarde();
        }
    }

}
