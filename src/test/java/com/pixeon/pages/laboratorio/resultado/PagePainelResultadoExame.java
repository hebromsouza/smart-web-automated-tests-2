package com.pixeon.pages.laboratorio.resultado;

import com.pixeon.datamodel.laboratorio.DataResultadoEritrograma;
import com.pixeon.datamodel.laboratorio.DataResultadoLeucograma;
import com.pixeon.pages.BasePage;
import com.pixeon.util.DataHelper;
import com.pixeon.util.SeleniumUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class PagePainelResultadoExame extends BasePage {

    @FindBy(xpath = "//div[contains(text(), 'Fatal Error!')]")
    private WebElementFacade divFatalError;

    @FindBy(name = "atr_atr_rtxt_0")
    private WebElementFacade lblResultado0;

    @FindBy(name = "atr_atr_rtxt_1")
    private WebElementFacade lblResultado1;

    @FindBy(xpath = "//*[@id='dw_leucog_detail_1']/input[@name='atr_atr_rtxt_1']")
    private WebElementFacade lblResultado1Xpath;

    @FindBy(name = "atr_atr_rtxt_2")
    private WebElementFacade lblResultado2;

    @FindBy(xpath = "//*[@id='dw_leucog_detail_2']/input[@name='atr_atr_rtxt_2']")
    private WebElementFacade lblResultado2Xpath;

    @FindBy(xpath = "//*[@id='dw_leucog_detail_3']/input[@name='atr_atr_rtxt_3']")
    private WebElementFacade lblResultado3Xpath;

    @FindBy(xpath = "//*[@id='dw_leucog_detail_4']/input[@name='atr_atr_rtxt_4']")
    private WebElementFacade lblResultado4Xpath;

    @FindBy(xpath = "//*[@id='dw_leucog_detail_5']/input[@name='atr_atr_rtxt_5']")
    private WebElementFacade lblResultado5Xpath;

    @FindBy(name = "atr_atr_rtxt_6")
    private WebElementFacade lblResultado6;

    @FindBy(xpath = "//*[@id='dw_leucog_detail_6']/input[@name='atr_atr_rtxt_6']")
    private WebElementFacade lblResultado6Xpath;

    @FindBy(xpath = "//*[@id='dw_leucog_detail_7']/input[@name='atr_atr_rtxt_7']")
    private WebElementFacade lblResultado7Xpath;

    @FindBy(xpath = "//*[@id='dw_leucog_detail_8']/input[@name='atr_atr_rtxt_8']")
    private WebElementFacade lblResultado8Xpath;

    @FindBy(xpath = "//*[@id='dw_leucog_detail_9']/input[@name='atr_atr_rtxt_9']")
    private WebElementFacade lblResultado9Xpath;

    @FindBy(xpath = "//*[@id='dw_leucog_detail_10']/input[@name='atr_atr_rtxt_10']")
    private WebElementFacade lblResultado10Xpath;

    @FindBy(xpath = "//*[@id='dw_leucog_detail_11']/input[@name='atr_atr_rtxt_11']")
    private WebElementFacade lblResultado11Xpath;

    @FindBy(id = "bt_save_hemo")
    private WebElementFacade btnGravar;

    public boolean fatalErrorPainel(){
        return divFatalError.isVisible();
    }

    public void clicarBotaoGravar(){
        btnGravar.click();
        if (!SeleniumUtil.isAlertPresent()) divAguarde();
    }

    public void preencherCamposResultadoEritrograma(List<DataResultadoEritrograma> dataResultadoEritrograma) {

        DataResultadoEritrograma data = DataHelper.getData(dataResultadoEritrograma);

        if (data.getHemoglobina() != null) lblResultado0.type(data.getHemoglobina());
        if (data.getHematocrito() != null) lblResultado1.type(data.getHematocrito());
        if (data.getHemacias() != null) lblResultado2.type(data.getHemacias());
        if (data.getRdwcv() != null) lblResultado6.type(data.getRdwcv());

    }

    public void preencherCamposResultadoLeucograma(List<DataResultadoLeucograma> dataResultadoLeucograma) {

        DataResultadoLeucograma data = DataHelper.getData(dataResultadoLeucograma);

        if (data.getMetamielocitos() != null) lblResultado1Xpath.type(data.getMetamielocitos());
        if (data.getBastoes() != null) lblResultado2Xpath.type(data.getBastoes());
        if (data.getSegmentados() != null) lblResultado3Xpath.type(data.getSegmentados());
        if (data.getEosinofilos() != null) lblResultado4Xpath.type(data.getEosinofilos());
        if (data.getLinfocitos() != null) lblResultado5Xpath.type(data.getLinfocitos());
        if (data.getLinfAtipicos() != null) lblResultado6Xpath.type(data.getLinfAtipicos());
        if (data.getMonocitos() != null) lblResultado7Xpath.type(data.getMonocitos());
        if (data.getBasofilos() != null) lblResultado8Xpath.type(data.getBasofilos());
        if (data.getBlastos() != null) lblResultado9Xpath.type(data.getBlastos());
        if (data.getPromielocitos() != null) lblResultado10Xpath.type(data.getPromielocitos());
        if (data.getMielocitos() != null) lblResultado11Xpath.typeAndTab(data.getMielocitos());

    }

}
