package com.pixeon.pages.laboratorio.resultado;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;
import org.openqa.selenium.By;

@DefaultUrl("https://outlook.office.com/mail/inbox")
public class PageEnvioEmail extends BasePage {

    @FindBy(name = "loginfmt")
    private WebElementFacade lblLoginEmail;

    @FindBy(id = "idSIButton9")
    private WebElementFacade btnAvancarEntrar;

    @FindBy(name = "passwd")
    private WebElementFacade lblSenha;

    public void realizarLoginEmail(String email, String senha) {
        lblLoginEmail.type(email);
        btnAvancarEntrar.click();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lblSenha.type(senha);
        btnAvancarEntrar.click();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clicarEmailRecebidoComOSGerada(String ordemServico) {
        getDriver().findElement(By.xpath("//span[contains(text(), '"+ordemServico+"')]")).click();
    }

    public void validarEnvioEmailPaciente(String nomePaciente) {
        Assert.assertTrue(waitHelper.existsElement(By.xpath("//h1[contains(text(), '"+nomePaciente+"')]")));
    }

}
