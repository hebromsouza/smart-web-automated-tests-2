package com.pixeon.pages.laboratorio;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageImpressaoDeLaudos extends BasePage {

    @FindBy(xpath = "//input[@name='d_print_opt_destino_0' and @value='E']")
    private WebElementFacade radDestino;

    @FindBy(id = "bt_enviar_email")
    private WebElementFacade btnEnviarPorEmail;

    @FindBy(id = "bt_enviar_outro_email")
    private WebElementFacade btnEnviarOutroEmail;

    @FindBy(id = "emailtxt")
    private WebElementFacade lblEmail;

    @FindBy(id = "btp_ok")
    private WebElementFacade btnEnviar;

    public void clicarRadioDestino(){
        radDestino.click();
    }

    public void clicarBotaoEnviarPorEmail(){
        btnEnviarPorEmail.click();

        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clicarBotaoEnviarParaOutroEmail() {
        btnEnviarOutroEmail.click();
        divAguarde();
    }

    public void escreverOutroEmail(String email){
        lblEmail.type(email);
    }

    public void clicarBotaoEnviar(){
        btnEnviar.click();
        divAguarde();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("Enviando e-mail para o paciente TESTE ADRIANO (qa.testes@pixeon.com)... Aguarde...", divAguarde.getText());
    }

}