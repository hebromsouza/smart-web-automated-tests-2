package com.pixeon.pages.laboratorio;

import com.pixeon.pages.BasePage;
import com.pixeon.pages.atendimento.orcamentoEPreAtendimento.PageOrcamentoEPreAtendimento;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class PageOrdemDeServico extends BasePage {

    @FindBy(name = "compute_1_0")
    private WebElementFacade linkOrdemDeServico;

    @FindBy(name = "compute_os_0")
    private WebElementFacade lnkOrdemDeServicoEntregaDeLaudo;

    @FindBy(xpath = "//*[@id='dw_osm_detail_0']/span[1]/a")
    private WebElementFacade NumeroOsMaisRecente;

    @FindBy(xpath = "//html/body/div[6]/div[2]/div[1]/div/div[3]/div/div/div/form[2]/span[2]/span[3]/text()")
    private WebElementFacade statusAberto;

    @FindBy(partialLinkText = "201")
    private WebElementFacade osComResultado;


    String numeroOrdemServico = PageOrcamentoEPreAtendimento.ordemServicoGeradoOrcamento;

    public void clicarNaOrdemDeServico(){
        if(linkOrdemDeServico.isPresent()) {
            linkOrdemDeServico.waitUntilPresent().click();
            divAguarde();
        }else{
            lnkOrdemDeServicoEntregaDeLaudo.waitUntilClickable().click();
            divAguarde();
        }
    }

    public void clicarLinkOrdemDeServico(String lista){
        if(lista.equals("entrega de laudo")){
            lnkOrdemDeServicoEntregaDeLaudo.waitUntilClickable().click();
        }
    }

    public void clicarLinkOrdemDeServicoGeradaNoOrcamento() {
        getDriver().findElement(By.linkText(numeroOrdemServico)).click();
        divAguarde();
    }


    public void VerificarResultadoAberto() {

        if(statusAberto.isPresent()){
            statusAberto.click();
        } else {
            System.out.println("Ordem de Serviço com status em ABERTO, não encontrado");
        }

    }

    public void clicarLinkOrdemDeServicoGerada() {

        if(osComResultado.isPresent()){
            osComResultado.click();
        } else if(NumeroOsMaisRecente.isPresent()){
                  NumeroOsMaisRecente.click();
        }

    }

}
