package com.pixeon.pages.laboratorio.localizacaoDeAmostra;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;

public class PageLocalizacaoAmostra extends BasePage {

    @FindBy(name = "amostra_0")
    private WebElementFacade lblAmostra;

    @FindBy(name = "cb_submit")
    private WebElementFacade btnLocalizar;

    @FindBy(name = "inf_volumes_0")
    private WebElementFacade chkInfVolumeUrinario;

    @FindBy(name = "rcl_rcl_tempo_diurese_0")
    private WebElementFacade lblTempo;

    @FindBy(name = "rcl_rcl_vol_diurese_0")
    private WebElementFacade lblVolume;

    @FindBy(id = "btp_gravar")
    private WebElementFacade btnGravar;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[8]")
    private WebElementFacade txtTempoDiurese;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[9]")
    private WebElementFacade txtVolumeDiurese;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[10]")
    private WebElementFacade txtLinfocitos;

    @FindBy(xpath = "//em[contains(text(), 'Exames da Amostra')]")
    private WebElementFacade txtExamesDaAmostra;

    @FindBy(name = "rcl_rcl_linfocitos_0")
    private WebElementFacade lblLinfocitos;

    public void escreverNumeroAmostra(String amostra) {
        if(lblAmostra.isPresent()) {
            lblAmostra.type(amostra);
        }
    }

    public void clicarBotaoLocalizar(){
        btnLocalizar.click();
    }

    public void validarLocalizacaoAmostra(String amostra) {
        Assert.assertTrue(waitHelper.existsElement(By.linkText(amostra)));
    }

    public void aceitarPopUpLocalizarAmostraNovamente() {
        if(waitHelper.waitAlertGetText().equals("Amostra já localizada. Deseja registrar nova localização?")){
            waitHelper.alertOk();
        }

    }

    public void clicarCheckInformarVolumeUrinarioLinfocito() {
        if(chkInfVolumeUrinario.isPresent()) {
            divAguarde();
            chkInfVolumeUrinario.click();
        }
    }

    public void escreverQuantidadeHoras(String tempo) {
        if(lblTempo.isPresent()) {
            divAguarde();
            lblTempo.type(tempo);
        }
    }

    public void escreverQuantidadeVolume(String volume) {
        if(lblVolume.isPresent()) {
            divAguarde();
            lblVolume.type(volume);
        }
    }

    public void clicarBotaoGravar() {
        btnGravar.click();
        divAguarde();
        btnGravar.waitUntilNotVisible();
    }

    public void validarTempoVolumeUrinario(String tempo) {
        Assert.assertEquals(tempo, txtTempoDiurese.getText());
    }

    public void validarVolumeDoVolumeUrinario(String volume){
        Assert.assertEquals(volume, txtVolumeDiurese.getText());
    }

    public void abrirPainelDeAmostra(String numeroAmostraItem) {
        getDriver().findElement(By.linkText(numeroAmostraItem)).click();
        txtExamesDaAmostra.waitUntilVisible();
    }

    public void escreverQuantidadeLinfocitos(String linfocitos) {
        lblLinfocitos.type(linfocitos);
    }

    public void validarLinfocitos(String linfocitos) {
        Assert.assertEquals(linfocitos, txtLinfocitos.getText());
    }

}
