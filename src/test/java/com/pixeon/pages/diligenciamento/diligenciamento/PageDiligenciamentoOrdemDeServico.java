package com.pixeon.pages.diligenciamento.diligenciamento;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageDiligenciamentoOrdemDeServico extends BasePage {

    @FindBy(id = "bt_smm_dilig")
    private WebElementFacade btnDiligenciamento;

    @FindBy(xpath = "//*[@id='bt_dll_novo']")
    private WebElementFacade btnNovoDeligenciamento;

    @FindBy(name = "dll_edl_cod_0")
    private WebElementFacade cboEvento;

    @FindBy(name = "dll_obs_0")
    private WebElementFacade lblObservcao;

    @FindBy(id = "bt_gravar")
    private WebElementFacade btnGravar;

    @FindBy(xpath = "//*[@id='dwp_dll_detail_0']/span[2]/div")
    private WebElementFacade xpathEvento;

    @FindBy(name = "compute_1_0")
    private WebElementFacade icoExcluir;

    public void clicarBotaoDiligenciamento(){
        if(btnDiligenciamento.isPresent()) {
            btnDiligenciamento.waitUntilClickable().click();
            divAguarde();
        }
    }

    public void clicarBotaoNovo(){
        if(btnNovoDeligenciamento.isPresent()) {
            btnNovoDeligenciamento.click();
        }
    }

    public void selecionarEvento(String evento){
        if(cboEvento.isPresent()) {
            cboEvento.click();
            cboEvento.selectByVisibleText(evento);
            divAguarde();
        }
    }

    public void incluirObservacao(){
        if(lblObservcao.isPresent()) {
            lblObservcao.type("Teste Diligencimento Automação");
        }
    }

    public void clicarBotaoGravar(){
        if(btnGravar.isPresent()) {
            btnGravar.waitUntilClickable().click();
            divAguarde();
        }
    }

    public void clicarExcluir(){
        if(icoExcluir.isPresent()) {
            icoExcluir.click();
            divAguarde();
        }
    }

    public void validarMensagemErroExclusao(String mensagemErro){
        try {
            Thread.sleep(1000);
            boolean alert = waitHelper.alertGetText().contains("Somente o usuário que registro o diligenciamento pode excluí-lo");
            mensagemErro.equals(waitHelper.alertGetText());

            if(alert==true){
                waitHelper.waitAlertOk();
            }
        } catch (Exception e) {
            Assert.fail("BUG ENCONTRADO. O RESULTADO ESPERADO É DIFERENTE DO RESULTADO ENCONTRADO");
            System.out.println("Mensagem de Alerta não encontrada...seguindo o fluxo de login.");
        }

    }

    public void clicarOkAlert(){
        waitHelper.waitAlertOk();
    }

    public void validarExclusaoDiligenciamento(){
        if(xpathEvento.isPresent()) {
            xpathEvento.waitUntilNotVisible();
            Assert.assertFalse(xpathEvento.isVisible());
        }
    }

    public void validarInclusaoDiligenciamento(String evento){
        if(xpathEvento.isPresent()){
            xpathEvento.getTextValue().equals(evento);
        } else {
            Assert.fail("BUG ENCONTRADO. O RESULTADO ESPERADO É DIFERENTE DO ReSULTADO OBTIDO");
        }

    }

}
