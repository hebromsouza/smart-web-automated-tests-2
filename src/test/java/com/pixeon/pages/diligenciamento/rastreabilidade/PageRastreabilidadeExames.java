package com.pixeon.pages.diligenciamento.rastreabilidade;

import com.pixeon.pages.BasePage;
import com.pixeon.pages.atendimento.ordemDeServico.PageInserirItensOrdemDeServico;
import org.junit.Assert;
import org.openqa.selenium.By;

public class PageRastreabilidadeExames extends BasePage {

    public void validarNumeroAmostraRastreabilidade() {
        Assert.assertTrue(waitHelper.existsElement(By.xpath("//span[contains(text(), '" + PageInserirItensOrdemDeServico.numeroAmostraItem + "')]")));
    }

}
