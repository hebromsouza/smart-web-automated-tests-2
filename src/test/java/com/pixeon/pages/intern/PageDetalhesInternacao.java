package com.pixeon.pages.intern;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageDetalhesInternacao extends BasePage {

    @FindBy(xpath = "//*[@id='dwp_pep_det_detail_0']/span[contains(text(), 'Aberto')]")
    private WebElementFacade spanStatus;

    @FindBy(xpath = "//*[@id='dw_pac_detail_0']/span/a/div[contains(text(), 'Detalhes da internação/tratamento')]")
    private WebElementFacade lnkInternacaoTratamento;

    public void clicarIconeInternacaoTratamento(){
        lnkInternacaoTratamento.waitUntilClickable().click();
    }

    public void verificarExistenciaStatusAberto(){
        Assert.assertTrue(spanStatus.isVisible());
    }

}
