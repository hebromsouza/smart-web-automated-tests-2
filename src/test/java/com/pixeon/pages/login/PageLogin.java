package com.pixeon.pages.login;

import com.pixeon.datamodel.login.DataLogin;
import com.pixeon.pages.BasePage;
import com.pixeon.util.DataHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//@DefaultUrl("http://localhost/smart/SmartQA/")
@DefaultUrl("http://localhost/smartweb/")
public class PageLogin extends BasePage {
    @FindBy(xpath = "//*[@title='Logout']")
    private WebElementFacade btnDeslogar;

    @FindBy(id="sle_sLogin")
    private WebElementFacade LoginSmartweb;

    @FindBy(id="sle_sSenha")
    private WebElementFacade SenhaSmartweb;

    @FindBy(xpath="//input[@type='submit']")
    private WebElementFacade botaoFazerLogin;

    @FindBy(xpath="//div[@title='Logout']")
    private WebElementFacade botaoFazerLogout;

    @FindBy(id="str-list")
    private WebElementFacade comboSelecioneRecepcao;

    @FindBy(xpath="//*[@id='str-list']/option[@value='070']")
    private WebElementFacade recepcaoAdmCentroMedico;

    @FindBy(xpath="//*[@id='str-list']/option[@value='CON']")
    private WebElementFacade recepcaoConsultorios;

    @FindBy(xpath="//*[@id='str-list']/option[@value='065']")
    private WebElementFacade recepcaoHospitalMedicware;
    @FindBy(xpath="//*[@id='str-list']/option[@value='REC']")
    private WebElementFacade recepcaoPixeon;

    @FindBy(id="bt_sel_recep")
    private WebElementFacade botaoOKRecepcao;

    @FindBy(id = "_panel0")
    private WebElementFacade divRecepcao;

    @FindBy(id = "str-list")
    private WebElementFacade cbxRecepcao;

    @FindBy(id = "msg_erro")
    private WebElementFacade lblLoginInvalido;

    @FindBy(xpath = "//*[@class='msg error']")
    private WebElementFacade msgErro;

    @FindBy(xpath = "//*[@class='information']/i")
    private List<WebElementFacade> labelsVesaoSoftware;

    @FindBy(xpath = "//*[@class='information']//a")
    private WebElementFacade labelFornecedor;

    @FindBy(id = "divMsgBox")
    private WebElementFacade divAguarde;

    @FindBy(xpath = "//*[contains(text(),'Close')]")
    private WebElementFacade closeJanelas;


    public void informarUsuarioSenhaSmartweb(String usuario, String senha) throws InterruptedException {
        if (LoginSmartweb.isVisible() && SenhaSmartweb.isVisible() && SenhaSmartweb.isEnabled() ) {
            LoginSmartweb.click();
            LoginSmartweb.clear();
            LoginSmartweb.type(usuario);

            SenhaSmartweb.clear();
            SenhaSmartweb.type(senha);
            botaoFazerLogin.click();
            Thread.sleep(1000);
        } else {
            //Assert.assertFalse("Não foi possível efetuar o Login. Por favor, tente novamente",true);
            Open.url("http://localhost/smart/SmartQA/");
            if (LoginSmartweb.isVisible() && SenhaSmartweb.isVisible() && SenhaSmartweb.isEnabled() ) {
                LoginSmartweb.click();
                LoginSmartweb.clear();
                LoginSmartweb.type(usuario);

                SenhaSmartweb.clear();
                SenhaSmartweb.type(senha);
                botaoFazerLogin.click();
                Thread.sleep(1000);
            } else {
                Open.browserOn();
                Open.url("http://localhost/smart/SmartQA/");
                Open.url("http://localhost/smart/SmartQA/");
                if (LoginSmartweb.isVisible() && SenhaSmartweb.isVisible() && SenhaSmartweb.isEnabled() ) {
                    LoginSmartweb.click();
                    LoginSmartweb.clear();
                    LoginSmartweb.type(usuario);

                    SenhaSmartweb.clear();
                    SenhaSmartweb.type(senha);
                    botaoFazerLogin.click();
                    Thread.sleep(1000);
                } else {
                    // Assert.assertFalse("Não foi possível efetuar o Login. Por favor, tente novamente",true);


                }

            }

        }
    }



    public void clicarDeslogarSeExistit() {
        if (btnDeslogar.isPresent() && btnDeslogar.isVisible() ){
            btnDeslogar.waitUntilPresent().click();
        }else if (btnDeslogar.isPresent() && btnDeslogar.isVisible() ){
            btnDeslogar.waitUntilPresent().click();
        }

    }

    public void inserirUsuario(String usuario) {
        if(LoginSmartweb.isPresent() && LoginSmartweb.isVisible()) {
            LoginSmartweb.waitUntilPresent().type(usuario);
        }
    }





    public void inserirSenha(String senha) {
        if(SenhaSmartweb.isPresent()) {
            SenhaSmartweb.waitUntilPresent().type(senha);
        }
    }

    public void preencheCamposLogin(List<DataLogin> dataLogin) {
        DataLogin data = DataHelper.getData(dataLogin);

        if(data.getLogin() != null)
            LoginSmartweb.type(data.getLogin());

        if(data.getSenha() != null)
            SenhaSmartweb.type(data.getSenha());

    }

    public void preencheGrupoDeUsuario(String grupoDeUsuario) {
        cbxRecepcao.waitUntilPresent();
        cbxRecepcao.waitUntilVisible();
        cbxRecepcao.click();
        cbxRecepcao.selectByVisibleText(grupoDeUsuario);
    }

    public void fecharJanelasAbertas(){
        divAguarde();

        //Fechar Janelas Nivel 1
        if (closeJanelas.isVisible() && closeJanelas.isPresent()) {
            closeJanelas.click();

            //Fechar Janelas Nivel 2 (se esta presente -> clica -> senao nada faz.
            if (closeJanelas.isVisible() && closeJanelas.isPresent()) {
                closeJanelas.click();

                //Fechar Janelas Nivel 3
                if (closeJanelas.isVisible() && closeJanelas.isPresent()) {
                    closeJanelas.click();

                    //Fechar Janelas Nivel 4
                    if (closeJanelas.isVisible() && closeJanelas.isPresent()) {
                        closeJanelas.click();
                    }
                }
            }

        }
    }

    public void efetuarLogoutSmartweb(){
        fecharJanelasAbertas();
        divAguarde();
        if (botaoFazerLogout.isVisible() && botaoFazerLogout.isPresent()) {
            botaoFazerLogout.click();

            getDriver().close();
            getDriver().quit();

        } else {
            Assert.assertFalse("Não foi possível efetuar o Logout. Por favor, tente novamente!",true);
        }
    }

    public void clicaBtnLogin(){
        botaoFazerLogin.waitUntilVisible().click();
        waitHelper.alertOk();
        divAguarde.waitUntilNotVisible();
    }


    public void validaSolicitouPreenchimentoRecepcao() {
        if(divRecepcao.isPresent()) {
            divRecepcao.shouldBeVisible();
        }
    }


    public void preencheRecepcao(String recepcao) {
        divAguarde();
        if(recepcao.equals("CONSULTORIOS")){
            if(comboSelecioneRecepcao.isPresent() && comboSelecioneRecepcao.isVisible()&& comboSelecioneRecepcao.isEnabled()) {

                comboSelecioneRecepcao.waitUntilClickable().click();
                comboSelecioneRecepcao.selectByVisibleText(recepcao);
                recepcaoConsultorios.click();
                botaoOKRecepcao.click();
                divAguarde.waitUntilNotVisible();
            }

        } else if(recepcao.equals("ADM CENTRO MEDICO")) {
            if (comboSelecioneRecepcao.isPresent()) {
                comboSelecioneRecepcao.click();
                recepcaoAdmCentroMedico.click();
                botaoOKRecepcao.click();
                divAguarde.waitUntilNotVisible();
            }
        }

    }

    public void clicaBtnOkRecepcao() {
        botaoOKRecepcao.click();
    }

    public void validaLoginInvalido() {
        lblLoginInvalido.shouldBePresent();
    }

    public void validarTempoDaSecao(String tempo, String mensagem) {
        try {

            int tempoEmSegundos =(Integer.parseInt(tempo))*60;
            for (int i = 1; i < tempoEmSegundos; i++){
                Thread.sleep(1000);
            }
            Thread.sleep(2000);
            Assert.assertEquals(msgErro.getTextValue(),mensagem );



        } catch (InterruptedException e) {
            e.printStackTrace();
            Assert.assertTrue(false);
        }
    }

    public void versaoSoftwareTelaLogin(String nomeSoftware, String nomeFornecedor, String versaoSoftware, String buildSoftware) {

        LoginSmartweb.waitUntilPresent();
        List<String> listMassa = Arrays.asList(nomeSoftware, versaoSoftware, nomeFornecedor, buildSoftware);
        List<WebElementFacade> listElemets = this.labelsVesaoSoftware;
        ArrayList<WebElementFacade> listAllElemets = new ArrayList(listElemets);
        listAllElemets.add(labelFornecedor);
        int numeroDados = 0;

        for (String txt : listMassa) {
            for (WebElementFacade el : listAllElemets) {
                if (el.getText().contains(txt)) {
                    ++numeroDados;
                }
            }
        }
        Assert.assertTrue(numeroDados == listMassa.size());
    }

    public void validarMensagemDeErroNaTelaDeLogin(String mensagem) {
        if ( !(mensagem != null) || !mensagem.equals("")) {
            Assert.assertEquals(msgErro.getTextValue(), mensagem);
        }
    }

}
