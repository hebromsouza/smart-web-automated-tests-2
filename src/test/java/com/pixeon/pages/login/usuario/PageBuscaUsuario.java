package com.pixeon.pages.login.usuario;

import com.pixeon.datamodel.login.DataBuscaUsusario;
import com.pixeon.pages.BasePage;
import com.pixeon.util.DataHelper;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;

import java.util.List;

public class PageBuscaUsuario extends BasePage {

    //cboStatus
    @FindBy(xpath = "//*[@name='usr_stat_0']")
    private WebElementFacade cboStatus;

    //login
    @FindBy(xpath = "//*[@name='usr_login_0']")
    private WebElementFacade txtLogin;

    //nome
    @FindBy(xpath = "//*[@name='usr_nome_0']")
    private WebElementFacade txtNome;

    //BtnFiltro
    @FindBy(xpath = "//*[@name='cb_submit_0' and  @value='Filtrar']")
    private WebElementFacade btnFiltro;


    public void carregarTelaInicial() {
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }


   //preencheCamposDeBuscaUsuario
    public void preencheCamposDeBuscaUsuario(List<DataBuscaUsusario> dataBuscaUsusario) {
        DataBuscaUsusario data = DataHelper.getData(dataBuscaUsusario);

        if (data.getStatus() != null) {
            cboStatus.waitUntilEnabled().click();
            cboStatus.selectByVisibleText(data.getStatus());
        }
        if (data.getLogin() != null) {
            txtLogin.waitUntilPresent().type(data.getLogin());
        }
        if (data.getNome() != null) {
            txtNome.waitUntilPresent().type(data.getNome());
        }
    }


    public void clickFiltrar() {
        btnFiltro.waitUntilPresent().click();
    }


    public void selecionarOUsuario(String usuario ) {
        // carregarTelaInicial();
        try {

            btnFiltro.waitUntilPresent();
            //Thread.sleep(1200);
            String xp="//*[@class='frame']//*[text()='"+usuario+"']";
            getDriver().findElement(By.xpath(xp)).click();
        } catch (StaleElementReferenceException e) {
            Logger.getLogger("StaleElementReferenceException - item da busca: " + e.getMessage());
            Assert.assertTrue(false);
        } catch (Exception e) {
            Logger.getLogger("StaleElementReferenceException -  item da busca " + e.getMessage());
            Assert.assertTrue(false);
        }
    }
}
