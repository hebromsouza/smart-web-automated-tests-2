package com.pixeon.pages.login.usuario;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageDadosDoUsuario extends BasePage {

    //chk Ativo
    @FindBy(xpath = "//*[text()='Ativo?']/.//*[@name='usr_status_0']")
    private WebElementFacade chkAtivo;

    //btn gravar
    @FindBy(xpath = "//*[text()='Gravar'][@class='save']")
    private WebElementFacade btnGravar;

    @FindBy(xpath = "//*[text()='Close'][@class='container-close']")
    private WebElementFacade btnFechar;

    //chkSemPrazo
    @FindBy(xpath = "//*[text()='Sem prazo']/.//*[@name='compute_usr_valid_0']")
    private WebElementFacade chkSemPrazo;

    //txtprazo
    @FindBy(xpath = "//*[@name='usr_usr_dthr_valid_0']")
    private WebElementFacade txtprazo;

    public void carregarTelaInicial() {
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
    }



    public void desativarMarcarcaoChkAtivo() {
        chkAtivo.waitUntilPresent();
        if(chkAtivo.isSelected()) {
            chkAtivo.click();
        }
    }

    public void ativoMarcarcaoChkAtivo() {
        chkAtivo.waitUntilPresent();
        if(!chkAtivo.isSelected()) {
            chkAtivo.click();
        }
    }



    public void desativarMarcarcaoChkSemPrazo() {
        chkSemPrazo.waitUntilPresent();
        if(chkSemPrazo.isSelected()) {
            chkSemPrazo.click();
        }
    }

    public void ativoMarcarcaoChkSemPrazo() {
        chkSemPrazo.waitUntilPresent();
        if(!chkSemPrazo.isSelected()) {
            chkSemPrazo.click();
        }

    }


    public void clickGravar() {
        btnGravar.waitUntilPresent().click();
    }

    public void btnFechar() {



      //  try {
        
           // getDriver().switchTo().defaultContent();
           // getDriver().switchTo().frame("mainFrame");
        	//int size = getDriver().findElements(By.tagName("mainFrame")).size();
        	//getDriver().switchTo().frame(0);
            
            btnFechar.waitUntilPresent().click();

      //  } catch (Exception e) {
      //      System.out.println("tesrtwe"+e);
      //  }



    }

    public void informarTxtPrazo(String data) {
        txtprazo.waitUntilPresent().click();
        txtprazo.waitUntilPresent().sendKeys(data);
    }

}
