package com.pixeon.pages.relatorios;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageInternacaoTempoPermanencia extends BasePage {

    @FindBy(name = "e_dt_ini_2")
    private WebElementFacade lblDatainicio;

    @FindBy(name = "e_dt_fim_2")
    private WebElementFacade lblDataFim;

    @FindBy(name = "e_nnumero_0")
    private WebElementFacade lblTempoPermanecia;

    public void escreverDataInicio(String dtInicio){
        lblDatainicio.click();
        lblDatainicio.sendKeys(dtInicio);
    }

    public void escreverDataFim(String dtFim){
        lblDataFim.click();
        lblDataFim.sendKeys(dtFim);
    }

    public void limparCampoDataInicio(){
        lblDatainicio.clear();
    }

    public void limparCampoDataFim(){
        lblDataFim.clear();
    }

    public void escreverTempoPermanencia(String tempoPermanencia){
        lblTempoPermanecia.click();
        lblTempoPermanecia.type(tempoPermanencia);
    }

    public void limparCampoTempoPermanencia() {
        lblTempoPermanecia.clear();
    }

}
