package com.pixeon.pages.relatorios;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageRelatorioMovimentacaoCaixa extends BasePage {

    @FindBy(name = "data_inicio_0")
    private WebElementFacade lblDataInicio;

    @FindBy(name = "recepcao_0")
    private WebElementFacade cboRecepcao;

    @FindBy(name = "usr_login_0")
    private WebElementFacade cboUsuario;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnOk;

    public void escreverDataInicialHoje(String dtIni) {
        lblDataInicio.waitUntilClickable().click();
        lblDataInicio.sendKeys(dtIni);
    }

    public void selecionarRecepcao(String recepcao) {
        cboRecepcao.click();
        cboRecepcao.selectByVisibleText(recepcao);
    }

    public void selecionarUsuario(String usuario) {
        cboUsuario.click();
        cboUsuario.selectByVisibleText(usuario);
    }

    public void clicarBotaoOk() {
        btnOk.click();
        divAguarde();
        divAguarde();
    }

}
