package com.pixeon.pages.relatorios;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageExamesRealizadosColeta extends BasePage {

    @FindBy(name = "e_dthr_2")
    private WebElementFacade lblDataInicial;

    @FindBy(name = "e_dthr_3")
    private WebElementFacade lblDataFinal;

    @FindBy(name = "e_svalor_0")
    private WebElementFacade lblSetorSolicitante;

    @FindBy(name = "e_svalor_1")
    private WebElementFacade lblStatusExecucao;

    @FindBy(name = "e_svalor_4")
    private WebElementFacade lblCodigoItem;

    public void escreverDataInicial(String dtInicial){
        lblDataInicial.click();
        lblDataInicial.sendKeys(dtInicial);
    }

    public void escreverDataFinal(String dtFinal){
        lblDataFinal.click();
        lblDataFinal.sendKeys(dtFinal);
    }

    public void escreverSetorSolicitante(String setorSolicitante){
        lblSetorSolicitante.click();
        lblSetorSolicitante.type(setorSolicitante);
    }

    public void limparCampoStatusExecucao(){
        lblStatusExecucao.clear();
    }

    public void limparCampoDataInicial(){
        lblDataInicial.clear();
    }

    public void limparCampoDataFinal(){
        lblDataFinal.clear();
    }

    public void limparCampoCodigoItem() {
        lblCodigoItem.clear();
    }

}
