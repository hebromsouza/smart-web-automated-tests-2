package com.pixeon.pages.relatorios;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageAtendimentosPorRecepcionista extends BasePage {

    @FindBy(name = "e_dt_ini_2")
    private WebElementFacade lblDataInicio;

    @FindBy(name = "e_dt_fim_2")
    private WebElementFacade lblDataFim;

    @FindBy(name = "e_svalor_1")
    private WebElementFacade lblSetorSolicitante;

    @FindBy(id = "bt_emitir")
    private WebElementFacade btnEmitirRelatorio;

    public void escreverDataInicio(String dtIni){
        lblDataInicio.click();
        lblDataInicio.sendKeys(dtIni);
    }

    public void escreverDataFim(String dtFim){
        lblDataFim.click();
        lblDataFim.sendKeys(dtFim);
    }

    public void escreverSetorSolicitante(String setorSolicitante) {
        lblSetorSolicitante.type(setorSolicitante);
    }

    public void clicarBotaoEmitirRelatorio() {
        btnEmitirRelatorio.click();
        divAguarde();
    }

}
