package com.pixeon.pages.relatorios;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class PageRelatorios extends BasePage {

    @FindBy(xpath = "//*[@title='Ajuda Online']")
    private WebElementFacade btnAjudaOnline;

    @FindBy(xpath = "//*[@title='Emissor de Relatório']")
    private WebElementFacade btnEmissorDeRelatorios;

    public void clicarIconeAjudaOnline(){
        btnAjudaOnline.click();
    }

    public void clicarBotaoEmissorDeRelatorios(){
        btnEmissorDeRelatorios.waitUntilVisible().click();
        divAguarde();
    }

    public void abrirRelatorio(String relatorio){
        getDriver().findElement(By.linkText(relatorio)).click();
    }

}
