package com.pixeon.pages.relatorios;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class PageExamesPorDataDeEntrega extends BasePage {

    @FindBy(name = "e_dt_ini_1")
    private WebElementFacade lblDataInicio;

    @FindBy(name = "e_dt_fim_1")
    private WebElementFacade lblDataFim;

    public void escreverDataInicio(String dtInicio){
        lblDataInicio.click();
        lblDataInicio.sendKeys(dtInicio);
    }

    public void escreverDataFim(String dtFim){
        lblDataFim.click();
        lblDataFim.sendKeys(dtFim);
    }

}
