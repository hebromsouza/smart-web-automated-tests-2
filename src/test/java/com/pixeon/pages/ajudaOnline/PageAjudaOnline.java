package com.pixeon.pages.ajudaOnline;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageAjudaOnline extends BasePage {

    @FindBy(linkText = "Rotinas e Cenários")
    private WebElementFacade linkRotinasCenarios;

    @FindBy(xpath = "//*[@id=\"content\"]/div[3]/ul/li/span[2]/a")
    private WebElementFacade criadoSteffani;

    @FindBy(id = "SmartWeb-Definição")
    private WebElementFacade SmartWebDefinicao;

    @FindBy(id = "username-fake")
    private WebElementFacade UserNameEmail;

    @FindBy(id = "os_password")
    private WebElementFacade PassWord;

    @FindBy(id = "loginButton")
    private WebElementFacade BotaoLogin;


    @FindBy(xpath = "//*[@id=\"main-content\"]/div[1]/ul/li[2]/span/a")
    private WebElementFacade rotcen;

    @FindBy(xpath = "//*[@id=\"bd-menu\"]/div/h1/a/span")
    private WebElementFacade PixeonMedicalSystemTitle;

    public void validarPaginaAjudaOnline(){

        // Switch to new window opened
        String winHandleBefore = getDriver().getWindowHandle();

        //Troca para a nova aba
        for(String winHandle : getDriver().getWindowHandles()){
            getDriver().switchTo().window(winHandle);
        }

        //Aguarda alguns segundos //Realiza operações na nova aba aberta
        getWaitForTimeout().minusSeconds(1000);

        //Faz Login, se opcao visivel
        if(UserNameEmail.isVisible() && UserNameEmail.isEnabled()){
            UserNameEmail.type("teste@teste.com");
            PassWord.type("testedoc");
           //  UserNameEmail.type("confluencedoc@pixeon.com");
           //   PassWord.type("djaJuX3a");
            BotaoLogin.click();
        }

        //Aguarda alguns segundos //Realiza operações na nova aba aberta
        getWaitForTimeout().minusSeconds(2000);

        Assert.assertTrue(criadoSteffani.isVisible());
        Assert.assertTrue(SmartWebDefinicao.getText().equals("Definição"));

        // Feche a nova janela, se essa janela não for mais necessária
        getDriver().close();

        // Voltar ao navegador original (primeira janela)
        getDriver().switchTo().window(winHandleBefore);

        //Ativa a aba de retorno (original) para dar Foco e permitir o clique no Logouf (Hooks (@After)
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");

    }

}
