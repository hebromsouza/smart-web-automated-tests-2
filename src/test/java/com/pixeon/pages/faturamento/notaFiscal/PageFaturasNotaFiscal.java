package com.pixeon.pages.faturamento.notaFiscal;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

import java.awt.*;

public class PageFaturasNotaFiscal extends BasePage {

    @FindBy(name = "e_dtini_0")
    private WebElementFacade lblDataInicio;

    @FindBy(name = "e_dtfim_0")
    private WebElementFacade lblDataFim;

    @FindBy(xpath = "//*[@id='dw_busca_detail_0']/input[@value='Filtrar']")
    private WebElementFacade btnFiltrar;

    @FindBy(xpath = "//*[@id='dw_fature_detail_0']/span[contains(text(),'Aberta')]")
    private WebElementFacade statusAberta;


    @FindBy(name = "e_nnum_0")
    private WebElementFacade lblNumeroNotaFiscal;

    public void escreverDataInicioHoje() {
        lblDataInicio.waitUntilClickable().click();
     //   lblDataInicio.sendKeys(SeleniumUtil.getDataAtualFormato("dd/MM/yyyy 00:00"));
    }

    public void escreverDataFimHoje() {
        lblDataFim.waitUntilClickable().click();
       // lblDataFim.sendKeys(SeleniumUtil.getDataAtualFormato("dd/MM/yyyy ") + "23:59");
    }

    public void periodoInicial(String data) {
        if(lblDataInicio.isPresent()&& lblDataInicio.isEnabled()) {
            lblDataInicio.click();
            lblDataInicio.sendKeys(data);
        }
    }


    public void clicarBotaoFiltrar() {
        if(btnFiltrar.isPresent()) {
            btnFiltrar.click();
            if(btnFiltrar.isPresent()){
                btnFiltrar.click();
            }
        }
    }

    public void verificarExistenciaNumeroNotaFiscal(String status) {

        if(statusAberta.isVisible()){
            statusAberta.containsText("Aberta");
            statusAberta.shouldBePresent();
            //Valida como verdadeiro se o statusAberto está presente
            Assert.assertTrue(statusAberta.isPresent());
        } else {
            SystemColor.RED.brighter();
            Assert.fail("BUG ENCONTRADO, status Aberta ou Nota Fiscal não encontrado!");
        }

    //    Assert.assertTrue(waitHelper.existsElement(By.xpath("//a[contains(text(), '" + PagePagamento.numeroNotaFiscal + "')]")));
    //    Assert.assertEquals(getDriver().findElement(By.xpath("//a[contains(text(), '" + PagePagamento.numeroNotaFiscal + "')]/../../span[4]")).getText(), status);

    }

    public void escreverNumeroNotaGerada() {
        if(lblNumeroNotaFiscal.isPresent()) {
            lblNumeroNotaFiscal.click();
            lblNumeroNotaFiscal.type("2873");
        }
    }

}
