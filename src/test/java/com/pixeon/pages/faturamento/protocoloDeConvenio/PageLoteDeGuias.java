package com.pixeon.pages.faturamento.protocoloDeConvenio;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

public class PageLoteDeGuias extends BasePage {

    @FindBy(name = "fpc_fpc_ind_ciente_recebido_0")
    private WebElementFacade chkRecebido;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnGravar;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnBuscar;

    @FindBy(xpath = "//*[@id='dw_fpc_tab_detail_0']/span[5]/div")
    private WebElementFacade ObservacaoLote;

    @FindBy(name = "compute_1_0")
    private WebElementFacade lblNumeroProtocolo;

    @FindBy(name = "fpc_obs_0")
    private WebElementFacade lblObservacao;

    @FindBy(linkText = "Buscar Lote")
    private WebElementFacade lnkBuscarLote;

    @FindBy(partialLinkText = "201")
    private WebElementFacade osEncontrada201;

    @FindBy(name = "str_cod_0")
    private WebElementFacade cboSetor;

    @FindBy(partialLinkText = "201")
    private WebElementFacade osNaoFaturada;

    @FindBy(xpath = "//*[@id='dw_smm_detail_1']/span[1]/a")
    private WebElementFacade osPendente;

    @FindBy(xpath = "//*[@id='dw_smm_detail_0']/span[1]/a")
    private WebElementFacade osAberta;

    @FindBy(linkText = "Clique aqui")
    private WebElementFacade cliqueAqui;

    @FindBy(linkText = "Editar Itens")
    private WebElementFacade editarItens;

    @FindBy(linkText = "Pagamento")
    private WebElementFacade botaoPagamento;

    @FindBy(name = "cb_pagar")
    private WebElementFacade btnPagar;


    @FindBy(xpath = "//*[@id=\"_panel0_h\"]")
    private WebElementFacade telaQuestionario;

    @FindBy(id = "bt_excluir")
    private WebElementFacade btnExcluir;

    @FindBy(xpath = "//a[contains(text(), 'Aberta')]")
    private WebElementFacade TextOSAberta;

    @FindBy(name = "recebido_0")
    private WebElementFacade cboRecebidos;

    @FindBy(name = "cb_submit")
    private WebElementFacade btnGravarPagamento;

    @FindBy(xpath = "//*[@id='dw_rdi_summary']/input")
    private WebElementFacade btnGravarFormaDePagamento;

    @FindBy(xpath = "//a[1][@class='container-close']")
    private WebElementFacade btnFechar;

    @FindBy(xpath = "//*[contains(text(),'Close')]")
    private WebElementFacade closeJanelas;

    public static String numeroProtocolo;
    
    public void clicarCheckRecebidos() {
        if(chkRecebido.isPresent()) {
            chkRecebido.waitUntilClickable().click();
        }
    }

    public void escreverObservacao() {
        if(lblObservacao.isPresent()) {
            lblObservacao.type("Teste Observação");
        }
    }

    public String clicarBotaoGravar() {
        if(btnGravar.isPresent()) {
            btnGravar.click();
        }

        return numeroProtocolo;
    }

    public void pagarOSComStatusAberta(String mensagem) {

        if(mensagem.equals("Existem atendimentos não pagos, logo o fechamento não poderá ser realizado.")){
            try {
                Thread.sleep(1000);
                boolean alert = waitHelper.alertGetText().contains("Existem atendimentos não pagos, logo o fechamento não poderá ser realizado.");

                if(alert==true){
                    waitHelper.waitAlertOk();
                }
            } catch (Exception e) {
                Assert.fail("POSSIVEL BUG ENCONTRADO. O RESULTADO OBTIDO, È DIFERENTE DO RESULTADO ESPERADO");
            }
        }


    }


    public void clicarLinkBuscarLote() {
        if(lnkBuscarLote.isPresent()) {
            lnkBuscarLote.click();
            divAguarde();
        }
    }

    public void selecionarLoteRecebidos(){
        if(cboRecebidos.isPresent()) {
            cboRecebidos.click();
            cboRecebidos.selectByVisibleText("Sim");
        }
    }

    public void clicarBotaoBuscar() {
        if(btnBuscar.isPresent()) {
            btnBuscar.waitUntilClickable().click();
            divAguarde();
        }
    }

    public void validarExistenciaLote() {
        if(ObservacaoLote.isPresent()) {
            ObservacaoLote.getTextValue().contains("Observação");
            clicarFecharPainel2();
        } else{
            Assert.fail("POSSIVEL BUG ENCONTRADO, LOTE NAO FOI ENCONTRADO, FAVOR< VERIFIQUE, FAZENDO UMA ANALISE MANUAL");
        }
    }

    public void scroolUp(){
        //executeScript("window.scrollBy(x-pixels,y-pixels)");
        //x-pixels é o número no eixo x, ele se move para a esquerda se o número for positivo e para a direita se o número for negativo .
        //y-pixels é o número no eixo y, se move para baixo se o número for positivo e passa para cima se o número estiver negativo.

        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("mainFrame");
        divAguarde();
        ((JavascriptExecutor)getDriver()).executeScript("window.scrollBy (0,-1000)");
        //ROLAGEM PARA CIMA. Foi necessário chamar o switchTo para a rolagem ser realizada na barra de navegaçao mais externa.
        //Comentando as duas linhas do switch acima, a rolagem ficará dentro da tela de Pagamento(tela em que está o FOCO)

    }

    public void pagarOSDoFechamentoDeCaixaComStatusAberta() {

        //OS e o setor devem ser exibidos
        while (osEncontrada201.isPresent() && cboSetor.isPresent() ){
            //Clicar na OS Encontrada
            osEncontrada201.click();

            if(cliqueAqui.isPresent()){
                cliqueAqui.click();
            }

            if(editarItens.isPresent()){
                editarItens.click();
            }

            ExcluirQuestionario();

            iniciarPagamento();

        }

    }


    public void carregarTelaPagamento(){
        divAguarde();
        getDriver().switchTo().frame(0);
        divAguarde();
    }


    public void iniciarPagamento(){
        divAguarde();
        if (botaoPagamento.waitUntilPresent().isPresent()) {
            botaoPagamento.click();
            divAguarde();
        }

        carregarTelaPagamento();

        divAguarde();
        if(btnPagar.waitUntilPresent().isPresent()){
            btnPagar.click();
        }

        divAguarde();
        if(btnGravarPagamento.waitUntilPresent().isPresent()){
            btnGravarPagamento.click();
        }
        divAguarde();
        if(btnGravarFormaDePagamento.waitUntilPresent().isPresent()){
            btnGravarFormaDePagamento.click();
        }

        //Rolar para cima para Fechar janela, ficar visivel
        scroolUp();

        if(closeJanelas.isPresent() && closeJanelas.isVisible()){
            closeJanelas.click();
        }

        osAberta.getTextValue().equals("Aberto");
    }

    public void confirmarAlerta(){
        waitHelper.alertOk();
    }

    public void ExcluirQuestionario(){
        if(telaQuestionario.isPresent()){
            telaQuestionario.click();

            if(btnExcluir.isPresent()) {
                btnExcluir.click();
                confirmarAlerta();
            }
        }
    }

    public void verificarExistenciaNumeroProtocoloGerado() {
        Assert.assertTrue(waitHelper.existsElement(By.xpath("//a[contains(text(), '" + numeroProtocolo + "')]")));

//        try {
//            Thread.sleep(1000);
//            boolean alert = waitHelper.alertGetText().contains("O usuário MEDICWARE já está logado no sistema em outra sessão.");
//
//            if(alert==true){
//                //    System.out.println("Estou dentro do IF contains");
//                waitHelper.waitAlertOk();
//            }
//        } catch (Exception e) {
//            System.out.println("########## LOGIN REALIZADO COM SUCESSO! INICIANDO O CENÁRIO DE TESTE ##############");
//        }
    }
}