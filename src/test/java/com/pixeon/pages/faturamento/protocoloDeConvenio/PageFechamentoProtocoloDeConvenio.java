package com.pixeon.pages.faturamento.protocoloDeConvenio;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageFechamentoProtocoloDeConvenio extends BasePage {

    @FindBy(name = "status_0")
    private WebElementFacade cboStatus;

    @FindBy(xpath = "//*[@id='dw_osm_list_detail_0']/span[7]/a")
    private WebElementFacade statusOS;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnBuscar;

    @FindBy(id = "btGerar")
    private WebElementFacade btnGerarLote;

    @FindBy(xpath = "//a[contains(text(),'Aberta')]")
    private WebElementFacade osStatusAberta;


    public void selecionarStatus(String status) {
        if(cboStatus.isPresent()) {
            cboStatus.waitUntilClickable().click();
            cboStatus.selectByVisibleText(status);
        }
    }

    public void ValidarStatus(String status) {
        if(statusOS.isPresent()) {
            String text = statusOS.getText();
            if (text.equals("Aberta")) {
                statusOS.isPresent();
                statusOS.getTextValue().equals("Aberta");

            } else {
                Assert.fail("BUG ENCONTRADO, STATUS: ABERTA, não encontrado. RESULTADO ESPERADO é DIFERENTE DO RESULTADO OBTIDO");
            }
        }

    }


    public void clicarBotaoBuscar() {
        if(btnBuscar.isPresent()) {
            btnBuscar.click();
            divAguarde();
        }
    }
    public void verificarFaturasNaoPagas() {
        //Verifica se  existe outro status diferente do que vem por parametro
        if(osStatusAberta.isPresent()){
            osStatusAberta.click();
            osStatusAberta.getValue();
        }

    }


    public void clicarBotaoGerarLote() {
        if(btnGerarLote.isPresent()) {
            btnGerarLote.click();
            divAguarde();
        }
    }

    public void aceitarPopUpConfirmarGerarLote(){
      //  Assert.assertEquals(waitHelper.waitAlertGetText(), "Confirmar gerar o protocolo de convênio das OSs listadas em aberto?");
        waitHelper.alertOk();
    }

}
