package com.pixeon.pages.faturamento.faturas;

import com.pixeon.pages.BasePage;
import com.pixeon.pages.atendimento.ordemDeServico.PageLancamentoDeOrdemDeServico;
import com.pixeon.util.SeleniumUtil;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class PageFaturamento extends BasePage {

    @FindBy(name = "nfs_dt_emis_0")
    private WebElementFacade lblDataEmissao;

    @FindBy(name = "nfs_nfs_dt_vcto_0")
    private WebElementFacade lblDataVencimento;

    @FindBy(name = "nfs_emp_cod_0")
    private WebElementFacade cboEmpresaConvenio;

    @FindBy(name = "nfs_emp_codigo_0")
    private WebElementFacade cboConvenio;

    @FindBy(name = "contrat_cod_0")
    private WebElementFacade cboContratado;

    @FindBy(name = "dt_inicial_0")
    private WebElementFacade lblDataAtendimentoInicial;

    @FindBy(name = "dt_final_0")
    private WebElementFacade lblDataAtendimentoFinal;

    @FindBy(name = "cb_processar_0")
    private WebElementFacade btnProcessar;

    public void digitarDataEmissaoHoje() {
       if(lblDataEmissao.isPresent()) {
           lblDataEmissao.click();
           lblDataEmissao.sendKeys(SeleniumUtil.getDataAtualFormato("dd/MM/yyyy"));
       }
    }

    public void digitarDataVencimentohoje() {
        if(lblDataVencimento.isPresent()) {
            lblDataVencimento.click();
            lblDataVencimento.sendKeys(SeleniumUtil.getDataAtualFormato("dd/MM/yyyy"));
        }
    }

    public void selecionarEmpresaConvenio(String empresaConvenio) {
        if(cboEmpresaConvenio.isPresent()) {
            cboEmpresaConvenio.click();
            cboEmpresaConvenio.selectByVisibleText(empresaConvenio);
            divAguarde();
        }
    }

    public void selecionarConvenio(String convenio) {
        if(cboConvenio.isPresent()) {
            cboConvenio.click();
            cboConvenio.selectByVisibleText(convenio);
            divAguarde();
        }
    }

    public void selecionarContratado(String contratado) {
        if(cboContratado.isPresent()) {
            cboContratado.click();
            cboContratado.selectByVisibleText(contratado);
            divAguarde();
        }
    }

    public void digitarPeriodoAtendimentoHoje() {
        if(lblDataAtendimentoInicial.isPresent()) {
            lblDataAtendimentoInicial.click();
            lblDataAtendimentoInicial.sendKeys(SeleniumUtil.getDataAtualFormato("dd/MM/yyyy"));
            lblDataAtendimentoFinal.click();
            lblDataAtendimentoFinal.sendKeys(SeleniumUtil.getDataAtualFormato("dd/MM/yyyy"));
        }
    }

    public void clicarBotaoProcessar() {
        if(btnProcessar.isPresent()) {
            btnProcessar.click();
            divAguarde();
            alertMsgNaoEncontradosGuiaNoPeriodoInformado();
        }
    }

    public void alertMsgNaoEncontradosGuiaNoPeriodoInformado() {
        try {
            Thread.sleep(1000);
            boolean alert = waitHelper.alertGetText().contains("Atenção!");

            if (alert == true) {
                waitHelper.waitAlertOk();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }


    public void validarExistenciaOrdemDeServicoFaturada() {
        waitHelper.existsElement(By.xpath("//[a(contains(text(), '" + PageLancamentoDeOrdemDeServico.numeroOS + "'))]"));
    }

}
