package com.pixeon.pages.faturamento.recepcaoCaixa;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

import java.awt.*;

public class PageFechamentoCaixa extends BasePage {

    @FindBy(id = "btGerar")
    private WebElementFacade btnGerar;

    @FindBy(xpath = "//*[@id='dw_osm_list_detail_0']/span[7]/a")
    private WebElementFacade statusEnviada;


    public void clicarBotaoFechamentoCaixa() {
        if(btnGerar.isPresent()) {
            btnGerar.waitUntilClickable().click();
        }
    }

    public void validarExisteciaOrdemDeServico() {
        if(statusEnviada.isPresent()){

            //String text =  statusEnviada.getText();

            statusEnviada.shouldBePresent();
            statusEnviada.containsText("Enviada");

        } else {
            System.out.println("BUG ENCONTRADO, status Enviado ou Ordem de Serviço não encontrado!");
            SystemColor.RED.brighter();
        }

    }

}
