package com.pixeon.pages.estoque;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;

public class PageAtendimentoDeSolicitacoes extends BasePage {

    @FindBy(name = "sba_cod_0")
    private WebElementFacade cboCentroEstocador;

    @FindBy(name = "sma_tipo_0")
    private WebElementFacade cboSolicitacao;

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnFiltrar;

    @FindBy(name = "ism_ism_qtde_compra_0")
    private WebElementFacade lblQuantidadeBaixa;

    @FindBy(id = "btp_imprimir")
    private WebElementFacade btnImprimir;

    @FindBy(id = "btp_gravar")
    private WebElementFacade btnGravar;

    @FindBy(name = "pendente_0")
    private WebElementFacade chkPendentes;

    public void selecionarCentroEstocador(String centroEstocador){
        cboCentroEstocador.waitUntilClickable().click();
        cboCentroEstocador.selectByVisibleText(centroEstocador);
    }

    public void selecionarSolicitacao(String tipoSolicitacao){
        cboSolicitacao.click();
        cboSolicitacao.selectByVisibleText(tipoSolicitacao);
    }

    public void clicarBotaoFiltrar(){
        btnFiltrar.waitUntilVisible().click();
        divAguarde();
    }

    public void clicarLinkNumeroSolicitacao(String numeroSolicitacao){
        divAguarde();
        getDriver().findElement(By.linkText(numeroSolicitacao)).click();

    }

    public void digitarQuantidadeBaixa(String quantidade){
        if (!lblQuantidadeBaixa.getValue().equals(quantidade)) {
            lblQuantidadeBaixa.waitUntilEnabled().type(quantidade);
        }
    }

    public void clicarBotaoImprimir(){
        btnImprimir.click();
        divAguarde();
    }

    public void carregarTela(){
        getDriver().switchTo().frame(0);
    }

    public void validarImpressaoAtendimentoSolicitacoes(){
//        carregarTela();
//        System.out.println(getDriver().findElement(By.xpath("//*[@src='http://localhost/smart/smartqa/scripts-gen/getTempFile.asp?type=application/pdf&id=MEDICWARE15305_0411201916404624115003.pdf#navpanes=0&toolbar=1']")).getText());
//        getDriver().findElement(By.xpath("//*[@src='http://localhost/smart/smartqa/scripts-gen/getTempFile.asp?type=application/pdf&id=MEDICWARE15319_0611201914162726710937.pdf#navpanes=0&toolbar=1']")).getText();
    }

    public void aceitarPopUpAtendimento() {
        if (waitHelper.waitAlertGetText().equals("Processo Concluído.")){
            waitHelper.waitAlertOk();
        }

    }

    public void validarAusenciaSolicitacaoAtendimentoPendente() {
        Assert.assertFalse(waitHelper.existsElement(By.linkText(PageSolicitacaoDeTransferenciaDeMateriais.serieNumeroSolicitacao)));
    }

    public void clicarBotaoGravar() {
        btnGravar.click();

       String text =  getDriver().getTitle();
       System.out.println(text);

    }

    public void clicarCheckPendentes(){
        chkPendentes.click();
    }

    public void validarPresencaSolicitacaoAtendimentoSemPendencia() {
        Assert.assertTrue(waitHelper.existsElement(By.linkText(PageSolicitacaoDeTransferenciaDeMateriais.serieNumeroSolicitacao)));
    }

    public void aceitarPopUpAtendimentoParcial() {
        if (waitHelper.waitAlertGetText().equals("Manter solicitação do item SGE MATERIAL 1 em aberto?")) waitHelper.alertOk();
    }
}
