package com.pixeon.pages.estoque;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;

public class PageSolicitacaoDeTransferenciaDeMateriais extends BasePage {

    @FindBy(name = "sma_sma_str_cod_0")
    private WebElementFacade cboSetorSolicitante;

    @FindBy(name = "sma_sma_sba_cod_0")
    private WebElementFacade cboSubAlmoxarifado;

    @FindBy(name = "sma_sma_sba_rep_0")
    private WebElementFacade cboTransferirPara;

    @FindBy(name = "sma_sma_obs_0")
    private WebElementFacade lblObservacao;

    @FindBy(name = "sma_serie_0")
    private WebElementFacade lblSerieSolicitacao;

    @FindBy(name = "sma_num_0")
    private WebElementFacade lblNumeroSolicitacao;

    @FindBy(name = "b_submit_0")
    private WebElementFacade btnGravar;

    @FindBy(name = "sle_busca")
    private WebElementFacade lblBuscarItem;

    @FindBy(name = "qtde_0")
    private WebElementFacade lblQuantidadeItem;

    @FindBy(id = "bt_mat_gravar")
    private WebElementFacade btnAdicionar;

    @FindBy(xpath = "//div[contains(text(), 'Buscar Solicitação')]")
    private WebElementFacade iconBuscarSolicitacoes;

    @FindBy(name = "compute_1_0")
    private WebElementFacade lnkNumeroSolicitacao;

    @FindBy(name = "marcar_0")
    private WebElementFacade chkItem;

    @FindBy(id = "acaoItem")
    private WebElementFacade cboAcaoItem;

    @FindBy(id = "listCancel")
    private WebElementFacade chkListarCanceladas;

    @FindBy(xpath = "//div[contains(text(), 'Cancelar Solicitação')]")
    private WebElementFacade iconCancelarSolicitacao;

    String serieSolicitacao;
    String numeroSolicitacao;
    public static String serieNumeroSolicitacao;

    public void selecionarSetorSolicitante(String setorSolicitante){
        cboSetorSolicitante.waitUntilClickable().click();
        cboSetorSolicitante.selectByVisibleText(setorSolicitante);
    }

    public void selecionarSubAlmoxarifado(String subAlmoxarifado){
        cboSubAlmoxarifado.click();
        cboSubAlmoxarifado.selectByVisibleText(subAlmoxarifado);
    }

    public void selecionarSubAlmoxarifadoParaTransferencia(String subalmoxarifadoTransf){
        cboTransferirPara.click();
        cboTransferirPara.selectByVisibleText(subalmoxarifadoTransf);
    }

    public void digitarObservacao(String observacao){
        lblObservacao.type(observacao);
    }

    public String capturarSerieNumeroSolicitacao(){
        serieNumeroSolicitacao = null;
        serieSolicitacao = lblSerieSolicitacao.waitUntilVisible().getValue();
        numeroSolicitacao = lblNumeroSolicitacao.getValue();
        serieNumeroSolicitacao = serieSolicitacao + "." + numeroSolicitacao;
        return serieNumeroSolicitacao;
    }

    public void clicarBotaoGravar(){
        btnGravar.click();
        divAguarde();
    }

    public void digitarItem(String item){
        lblBuscarItem.waitUntilEnabled().type(item);
    }

    public void clicarBotaoBuscar(){
        getDriver().findElement(By.cssSelector("button[type='submit']")).click();
        divAguarde();
    }

    public void digitarQuantidadeMaterial(String quantidade){
        lblQuantidadeItem.waitUntilVisible().type(quantidade);
    }

    public void clicarBotaoAdicionar(){
        btnAdicionar.click();
        divAguarde();
    }

    public void clicarIconeBuscarSolicitacoes(){
        iconBuscarSolicitacoes.click();
    }

    public void validarNumeroDaSolicitacaoDeTransferenciaDeMateriais(){
        Assert.assertEquals(PageSolicitacaoDeTransferenciaDeMateriais.serieNumeroSolicitacao, lnkNumeroSolicitacao.getText());
    }

    public void clicarCheckItem() {
        chkItem.waitUntilClickable().click();
    }

    public void selecionarAcaoCancelar() {
        cboAcaoItem.click();
        cboAcaoItem.selectByVisibleText("Cancelar");
    }

    public void aceitarPopUpCancelamento() {
        if (waitHelper.waitAlertGetText().equals("Confirma cancelar os itens selecionados?")) waitHelper.alertOk();
    }

    public void validarItemNotVisible() {
        Assert.assertFalse(waitHelper.existsElement(By.linkText(serieNumeroSolicitacao)));
    }

    public void clicarChekListarCanceladas(){
        chkListarCanceladas.click();
        divAguarde();
    }

    public void validarItemVisivel() {
        Assert.assertTrue(waitHelper.existsElement(By.linkText(serieNumeroSolicitacao)));
    }

    public void validarQuantidadeItemZerado() {
        Assert.assertEquals(getDriver().findElement(By.xpath("//*[@id='dw_sma_busca_detail_0']/span[5]")).getText(), "0");
    }

    public void clicarIconeCancelarSolicitacao() {
        if(iconCancelarSolicitacao.isPresent()){
            iconCancelarSolicitacao.click();
        }

    }

    public void aceitarPopUpCancelamentoSolicitacao() {
        if (waitHelper.waitAlertGetText().equals("Confirma o cancelamento da Solicitação ?")) waitHelper.alertOk();
    }

    public void validarStatusSolicitacao(String status) {
        Assert.assertEquals(getDriver().findElement(By.xpath("//*[@id='dw_sma_busca_detail_0']/span[6]")).getText(), status);
    }
}