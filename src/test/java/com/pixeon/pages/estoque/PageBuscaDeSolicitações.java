package com.pixeon.pages.estoque;

import com.pixeon.pages.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class PageBuscaDeSolicitações extends BasePage {

    @FindBy(name = "cb_submit_0")
    private WebElementFacade btnBuscar;

    @FindBy(name = "compute_3_0")
    private WebElementFacade lnkNumeroSolicitacao;

    public void clicarBotaoBuscar(){
        btnBuscar.click();
        divAguarde();
    }

    public void validarBuscaDeNumeroDeSolicitacaoDeTransferenciaDeMateriais(){
        Assert.assertEquals(PageSolicitacaoDeTransferenciaDeMateriais.serieNumeroSolicitacao, lnkNumeroSolicitacao.getText());
    }

}
