package com.pixeon;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

// @smartweb - Sistema SmartWeb
// @smartpep - Sistema SmartWeb com funcionalidades aprovadas pelo SBIS
// @top10 - Casos do projeto TOP10
// @bugExterno - Casos fruto de bugs abertos pelos clientes
// @bugInterno - Casos fruto de bugs abertos pela qualidade
// @quarentena - Casos que não passam por erro do sistema que ainda não foram corrigidos ou precisa de manutenção
// @EmDesenvolvimento - Casos que ainda estão em desenvolvimento
// @erroRegressao - Erros causados no teste de regressão
// @brokenTeste - Casos que falham quando executados via maven
// @doc - Funcionalidades que precisam ainda passar pelo processo de documentação
// @pass
// @pass11
// @passOK
// @testeHoje

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features",
        dryRun = false,
        plugin = {"pretty", "junit:target/run_smartweb.xml", "html:target/run_smartweb.html","json:target/run_smartweb.json"},
        //  plugin = {"json:target/cucumber-report-composite.json", "pretty",
        //        "html:target/cucumber/","rerun:target/rerun.txt" },
        monochrome = true,
        tags = "@pass"
       // tags = "@testeHoje"
)

public class DefinitionTestSuite {

}
