package com.pixeon.datamodel.financeiro;

public class DataBaixaCompromisso {

    private String status;
    private String empresa;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }
}
