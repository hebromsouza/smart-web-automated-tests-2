package com.pixeon.datamodel.laboratorio;

public class DataResultadoEritrograma {

    private String hemoglobina;
    private String hematocrito;
    private String hemacias;
    private String rdwcv;

    public String getHemoglobina(){
        return hemoglobina;
    }

    public void setHemoglobina(String hemoglobina) {
        this.hemoglobina = hemoglobina;
    }

    public String getHematocrito(){
        return hematocrito;
    }

    public void setHematocrito(String hematocrito) {
        this.hematocrito = hematocrito;
    }

    public String getHemacias() {
        return hemacias;
    }

    public void setHemacias(String hemacias) {
        this.hemacias = hemacias;
    }

    public String getRdwcv() {
        return rdwcv;
    }

    public void setRdwcv(String rdwcv) {
        this.rdwcv = rdwcv;
    }

}
