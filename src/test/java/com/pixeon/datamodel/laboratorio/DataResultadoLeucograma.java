package com.pixeon.datamodel.laboratorio;

public class DataResultadoLeucograma {

    private String metamielocitos;
    private String bastoes;
    private String segmentados;
    private String eosinofilos;
    private String linfocitos;
    private String linfAtipicos;
    private String monocitos;
    private String basofilos;
    private String blastos;
    private String promielocitos;
    private String mielocitos;

    public String getMetamielocitos() {
        return metamielocitos;
    }

    public void setMetamielocitos(String metamielocitos) {
        this.metamielocitos = metamielocitos;
    }

    public String getBastoes() {
        return bastoes;
    }

    public void setBastoes(String bastoes) {
        this.bastoes = bastoes;
    }

    public String getSegmentados() {
        return segmentados;
    }

    public void setSegmentados(String segmentados) {
        this.segmentados = segmentados;
    }

    public String getEosinofilos() {
        return eosinofilos;
    }

    public void setEosinofilos(String eosinofilos) {
        this.eosinofilos = eosinofilos;
    }

    public String getLinfocitos() {
        return linfocitos;
    }

    public void setLinfocitos(String linfocitos) {
        this.linfocitos = linfocitos;
    }

    public String getLinfAtipicos() {
        return linfAtipicos;
    }

    public void setLinfAtipicos(String linfAtipicos) {
        this.linfAtipicos = linfAtipicos;
    }

    public String getMonocitos() {
        return monocitos;
    }

    public void setMonocitos(String monocitos) {
        this.monocitos = monocitos;
    }

    public String getBasofilos() {
        return basofilos;
    }

    public void setBasofilos(String basofilos) {
        this.basofilos = basofilos;
    }

    public String getBlastos() {
        return blastos;
    }

    public void setBlastos(String blastos) {
        this.blastos = blastos;
    }

    public String getPromielocitos() {
        return promielocitos;
    }

    public void setPromielocitos(String promielocitos) {
        this.promielocitos = promielocitos;
    }

    public String getMielocitos() {
        return mielocitos;
    }

    public void setMielocitos(String mielocitos) {
        this.mielocitos = mielocitos;
    }
}
