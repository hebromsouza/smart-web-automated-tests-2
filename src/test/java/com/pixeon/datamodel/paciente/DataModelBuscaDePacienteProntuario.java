package com.pixeon.datamodel.paciente;

public class DataModelBuscaDePacienteProntuario {
    private String nome;
    private String registro;
    private String prontuario;
    private String CPF;
    private String RG;
    private String nascimento;

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getRegistro() {
        return registro;
    }
    public void setRegistro(String registro) {
        this.registro = registro;
    }
    public String getProntuario() {
        return prontuario;
    }
    public void setProntuario(String prontuario) {
        this.prontuario = prontuario;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getRG() {
        return RG;
    }

    public void setRG(String RG) {
        this.RG = RG;
    }

    public String getNacimento() {
        return nascimento;
    }

    public void setNacimento(String nacimento) {
        this.nascimento = nacimento;
    }
}
