package com.pixeon.datamodel.reservaDeOS;

public class DataReservaOS {

    private String serieOs;
    private String quantidade;
    private String setorSolic;
    private String observacao;


    public String getSerieOs() {
        return serieOs;
    }

    public void setSerieOs(String serieOs) {
        this.serieOs = serieOs;
    }

    public String getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(String quantidade) {
        this.quantidade = quantidade;
    }

    public String getSetorSolic() {
        return setorSolic;
    }

    public void setSetorSolic(String setorSolic) {
        this.setorSolic = setorSolic;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

}
