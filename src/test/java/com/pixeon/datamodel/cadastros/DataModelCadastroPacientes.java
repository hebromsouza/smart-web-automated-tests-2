package com.pixeon.datamodel.cadastros;
//GETTERS e SETTERS de TODOS OS DADOS DO CADASTRO DO PACIENTE
public class DataModelCadastroPacientes {

    //Dados Básicos do Cadastro do Paciente
    private String nomeSocial;
    private String nomeMae;
    private String rg;
    private String cpf;
    private String dtNascimento;
    private String sexo;
    private String estadoCivil;

    //Dados da Operadora de Saúde
    private String operadora;
    private String validade;
    private String matricula;
    private String plano;
    private String acomodacao;

    //Dados Contato do Paciente
    private  String cep;
    private  String tipoLogradouro;
    private  String logradouro;
    private  String numero;
    private  String cidade;
    private  String bairro;
    private  String complemento;
    private  String email;
    private  String telefone;
    private  String celular;
    private  String smsOuWhatsApp;
    private  String contato;
    private  String outrotelefone;



    public String getNomeSocial() {
        return nomeSocial;
    }

    public void setNomeSocial(String nomeSocial) {
        this.nomeSocial = nomeSocial;
    }

    public String getNomeMae() {
        return nomeMae;
    }

    public void setNomeMae(String nomeMae) {
        this.nomeMae = nomeMae;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getDtNascimento() {
        return dtNascimento;
    }

    public void setDtNascimento(String dtNascimento) {
        this.dtNascimento = dtNascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    //GETs e SETs  da Operadora de Saúde

    public String getOperadora() {
        return operadora;
    }

    public void setOperadora(String operadora) {
        this.operadora = operadora;
    }

    public String getValidade() {
        return validade;
    }

    public void setValidade(String validade) {
        this.validade = validade;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getAcomodacao() {
        return acomodacao;
    }

    public void setAcomodacao(String acomodacao) {
        this.acomodacao = acomodacao;
    }

    public String getPlano() {
        return plano;
    }

    public void setPlano(String plano) {
        this.plano = plano;
    }


    //GETs e SETs  da Dados Contato Paciente


    public String getCep() {
        return cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public String getCidade() {
        return cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public String getCelular() {
        return celular;
    }

    public String getSmsOuWhatsApp() {
        return smsOuWhatsApp;
    }

    public String getNumero() {
        return numero;
    }

    public String getContato() {
        return contato;
    }

    public String getEmail() {
        return email;
    }

    public String getOutrotelefone() {
        return outrotelefone;
    }

    public String getTelefone() {
        return telefone;
    }


    public void setCelular(String celular) {
        this.celular = celular;
    }

    public void setSmsOuWhatsApp(String smsOuWhatsApp) {
        this.smsOuWhatsApp = smsOuWhatsApp;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setOutrotelefone(String outrotelefone) {
        this.outrotelefone = outrotelefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }



}


