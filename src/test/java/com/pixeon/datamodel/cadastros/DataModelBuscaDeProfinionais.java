package com.pixeon.datamodel.cadastros;

public class DataModelBuscaDeProfinionais {
    private String conselho;
    private String nome;

    public String conselho() {
        return conselho;
    }

    public void conselho(String conselho) {
        this.conselho = conselho;
    }

    public String nome() {
        return nome;
    }

    public void nome(String nome) {
        this.nome = nome;
    }
}
