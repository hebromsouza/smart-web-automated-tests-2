package com.pixeon.datamodel.busca_auditoria;

public class DataBuscaAuditoria {

    private String tipoDeEventos;
    private String descricao;
    private String periodoInicio;
    private String periodoFim;
    private String usuario;
    private String registroAfetado;
    private String identificador;
    private String paceinte;

    public String getTipoDeEventos() {
        return tipoDeEventos;
    }

    public void setTipoDeEventos(String tipoDeEventos) {
        this.tipoDeEventos = tipoDeEventos;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getPeriodoInicio() {
        return periodoInicio;
    }

    public void setPeriodoInicio(String periodoInicio) {
        this.periodoInicio = periodoInicio;
    }

    public String getPeriodoFim() {
        return periodoFim;
    }

    public void setPeriodoFim(String periodoFim) {
        this.periodoFim = periodoFim;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getRegistroAfetado() {
        return registroAfetado;
    }

    public void setRegistroAfetado(String registroAfetado) {
        this.registroAfetado = registroAfetado;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getPaceinte() {
        return paceinte;
    }

    public void setPaceinte(String paceinte) {
        this.paceinte = paceinte;
    }


}
