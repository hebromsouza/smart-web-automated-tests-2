package com.pixeon.datamodel.ordemDeServico;

public class DataOrdemDeServico {

    private int crmPrestador;
    private String nomePrestador;
    private String nomeConvenio;
    private int cnpjSolicitente;
    private String nomeEmpresaSolicitante;
    private String setorSolicitante;

    public int getCrmPrestador(){
        return crmPrestador;
    }

    public void setCrmPrestador(int crmPrestador){
        this.crmPrestador = crmPrestador;
    }

    public String getNomePrestador(){
        return nomePrestador;
    }

    public void setNomePrestador(String nomePrestador){
        this.nomePrestador = nomePrestador;
    }

    public String getNomeEmpresa(){
        return nomeConvenio;
    }

    public void setNomeConvenio(String nomeConvenio){
        this.nomeConvenio = nomeConvenio;
    }

    public int getCnpjSolicitente(){
        return cnpjSolicitente;
    }

    public void setCnpjSolicitente(int cnpjSolicitente){
        this.cnpjSolicitente = cnpjSolicitente;
    }

    public String getNomeEmpresaSolicitante(){
        return nomeEmpresaSolicitante;
    }

    public void setNomeEmpresaSolicitante(String nomeEmpresaSolicitante){
        this.nomeEmpresaSolicitante = nomeEmpresaSolicitante;
    }

    public String getSetorSolicitante() {
        return setorSolicitante;
    }

    public void setSetorSolicitante(String setorSolicitante) {
        this.setorSolicitante = setorSolicitante;
    }

}
