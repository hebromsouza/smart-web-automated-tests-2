# smart-web

Testes automatizados Smart Web.

1. Serenity Properties -> Tutorial 
https://github.com/serenity-bdd/serenity-documentation/blob/master/src/asciidoc/system-props.adoc
2. Verificar versão do serenity na máquina e ajustar no POM.
   1. Exemplo: <artifactId>pixeon-automation-framework</artifactId>
      <version>1.8</version>
3. Verificar local de instalação do smartweb e atualizar no arquivo PageLogin.
4. Executar: mvn clean test install